# Documentación tecnica Sitio Web Pasionistas - 27 de Septiembre de 2018
#### Autor: Sergio Mesa - Trusty Digital S.A.S

Esta documentación describe los elementos tecnicos del sitio web Pasionistas

## Arquitectura & Tecnologia

1. El servicio esta expuesto en firebase hosting en la url [https://pasionistas-conferencia.firebaseapp.com/](URL "https://pasionistas-conferencia.firebaseapp.com/").

2. Se utilizo firebase firestore para el almacenamiento de datos
3. Se utilizo firebase firestorage para el almacenamiento de documentos
4. El desarrollo esta distribuido entre los proyectos pasionistas-conferencia & pasionistas-website en firebase

## Arquitectura & Tecnologia
El proyecto de conferencia pasionista se desarrollo en Angular 6.0.2, en el se utilizaron las siguientes librerias de terceros:

1. "@covalent/core": "^1.0.1",
2. "@ng-bootstrap/ng-bootstrap": "^2.0.0-alpha.0",
3. "@ng-bootstrap/schematics": "^2.0.0-alpha.1",
4. "@ngx-pwa/local-storage": "^6.0.0",
5. "angular2-fontawesome": "^5.2.1",
6. "bootstrap": "^4.0.0",
7. "core-js": "^2.5.4",
8. "document-register-element": "^1.7.2",
9. "firebase": "^5.5.0",
10. "font-awesome": "^4.7.0",
11. "jquery": "^3.3.1",
12. "ng2-search-filter": "^0.4.7",
13. "ngx-spinner": "^6.0.0",
14. "ngx-store": "^2.0.0",
15. "rxjs": "^6.3.2",
16. "rxjs-compat": "^6.3.2",
17. "sweetalert2": "^7.26.28",
18. "zone.js": "^0.8.26"

## Ejecutar local
1. Ir a la carpeta a ejecutar
2. Ejecutar en consola el comand  npm i & ng s -o

## Modelo de datos

La plataforma conforma un modelo de datos NoSQL basado en Firestore en un modelo de datos indexados que permite almacenar la información 
del sitio web de forma parametrizable

### asistentes
Esta colección almacena la información del asistente de la conferencia, se relaciona al modelo de autenticación de firebase auth por medio del campo uid(unique id) de la colección, los atributos que posee son:

* correo: Correo electrónico
* nombre: Nombre de asistente
* pais: País
* configuracion: Configuración a la que pertenece
* idioma: Idioma del asistente
* foto: URL foto del asistente
* uid: Id de usuario de autenticación


### actividades
Esta colección almacena la información de las actividades de calendario, los atributos que posee son:

* fecha: Fecha de actividad
* horainicio: Hora inicio
* horafin: Hora fin
* titulo: Titulo en español
* tituloingles: Titulo en ingles
* tituloitaliano: Titulo en italiano
* descripcion: Descripcion en español
* descripcioningles: Descripcion en ingles
* descripcionitaliano: Descripcion en italiano

### cronicas
Esta colección almacena la información de las cronicas mostradas en el sitio, los atributos que posee son:

* idioma: Idioma de la cronica
* resumen: Resumen corto de la cronica
* titulo: Titulo de la cronica
* contenido: Contenido en html de la cronica

### documentos
Esta colección almacena la información de los documentos que se utilizan en el sitio, los atributos que posee son:

* idioma: Idioma del documento
* nombre: Nombre del documento
* ruta: Ruta de carpeta del documento
* tipo: Tipo de documento
* url: Url del documento

### encuestas
Esta colección almacena la información de las encuestas que se hacen el sitio, los atributos que posee son:

* titulo: Titulo en español
* tituloingles: Titulo en ingles
* tituloitaliano: Titulo en italiano
* pregunta: Pregunta en español
* preguntaingles: Pregunta en ingles
* preguntaitaliano: Pregunta en italiano
* si: Votos Si
* no: Votos No
* blanco: Votos en blanco
* estado: Estado de la encuesta(Abierta, Cerrada)

### respuestas-votaciones
Esta colección almacena la información de las respuestas de las votaciones, los atributos que posee son:

* encuestaid: Id de la encuesta a la que pertenece
* respuesta: Respuesta del usuario
* uid: Id de usuario que hizo la publicación


### foros
Esta colección almacena la información de los foros que se hacen el sitio, los atributos que posee son:

* idioma: Idioma del foro
* objetivo: Descripción del objetivo del foro
* titulo: Titulo del foro
* hilos: Número de hilos del foro
* foto: Url de la foto
* contenido: Contenido en html del foro


### hilos-foros
Esta colección almacena la información los hilos de los foros, los atributos que posee son:

* foroid: Id del foro al que pertenece
* mensaje: Mensaje enviado en el hilo
* uid: Id de usuario que hizo la publicación


### grupos
Esta colección almacena los grupos de asistentes que se muestran en la pagina, los atributos que posee son:

* asistentes: Array con los id de los asistentes
* nombre: Nombre el grupo


### proyectos
Esta colección almacena la informaciónde los proyectos mostrados en el sitio, los atributos que posee son:

* idioma: Idioma de la cronica
* resumen: Resumen corto de la cronica
* titulo: Titulo de la cronica
* foto: Foto del proyecto

### usuarios-sistemas
Esta colección almacena los usuarios administrativos del sistema, los atributos que posee son:

* nombre: Nombre del usuario
* correo: Correo electrónico
* uid: Id del usuario

## Componententes del desarrollo

### Agenda
Componenten que muestra la agenda de eventos y tareas en la pagina

### banner-carousel
Componenten que muestrá en heroe banner en la pagina ppal

### barra-navegacion
Componenten que el menu de navegación e interactua para enrutar

### biblioteca
Componenten que carga e interactura para la biblioteca

### Cronicas
Componenten que carga e interactura para las cronicas

### liturgias
Componenten que carga e interactura para las litugias

### mensaje-papa
Componenten que muestra el mensaje del papa

# Pagina web conferecia

### asistentes
Pagina con lista de asistentes

### asistentes-grupos
Pagina con los asistentes de un grupo

### cronicas-detalle
Pagina que muestra el detalle de las cronicas

### encuesta
Pagina que lista las encuestas del sitio

### foro-detalle
Pagina que muestra el detalle del foro

### grupos
Pagina que lista los grupos disponibles de asistentes

### home
Pagina inicio post autenticación

### inicio
Pagina inicio de autenticación

### proyectos
Pagina que lista los proyectos

### proyectos-detalle
Pagina con el detalle de un proyecto seleccionado


## Providers del desarrollo

### authService
Proveedor de servicios de autenticación

### localStorageService
Proveedor de servicios de almacenamiento local

### resourcesServices
Proveedor de consulta de información al modelo de datos

### uploadServicices
Proveedor de carga de documentos al storage


## Pipes del desarrollo

### orderBy
Pipe para ordenamiento de información asc y desc


# Pagina web administrador

## Componententes del desarrollo

### barra-navegacion
Barra de navegación del administración

## Paginas 

### asistentes
Pagina de administración de asistentes

### autenticacion
Pagina de autenticación administrador

### calendario
Pagina de administración de calendario

### cronicas
Pagina de administración de cronicas

### documentos
Pagina de administración de documentos

### encuestas
Pagina de administración de encuestas

### foros
Pagina de administración de foros

### grupos
Pagina de administración de grupos

### ingreso
Pagina de ingreso al admin

### parametros-categorias
Pagina de administración de parametros









