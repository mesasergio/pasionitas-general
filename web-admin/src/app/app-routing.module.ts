import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { AsistentesComponent } from './paginas/asistentes/asistentes.component';
import { AutenticacionComponent } from './paginas/autenticacion/autenticacion.component';
import { DocumentosComponent } from './paginas/documentos/documentos.component';
import { CarpetasComponent } from './paginas/documentos/carpetas/carpetas.component';
import { TareasComponent } from './paginas/tareas/tareas.component';
import { ParametrosCategoriasComponent } from './paginas/parametros-categorias/parametros-categorias.component';
import { CalendarioComponent } from './paginas/calendario/calendario.component';
import { ParametrosSeccionesDocumentosComponent } from './paginas/parametros-secciones-documentos/parametros-secciones-documentos.component';
import { ProyectosComponent } from './paginas/proyectos/proyectos.component';
import { CronicasComponent } from './paginas/cronicas/cronicas.component';
import { EncuestasComponent } from './paginas/encuestas/encuestas.component';
import { ForosComponent } from './paginas/foros/foros.component';
import { MensajePapaComponent } from '../paginas/mensaje-papa/mensaje-papa.component';
import { IngresoComponent } from './paginas/ingreso/ingreso.component';
import { UsuariosComponent } from './paginas/usuarios/usuarios.component';
import { InicioComponent } from './paginas/inicio/inicio.component';
import { GruposComponent } from './paginas/grupos/grupos.component';

const options: ExtraOptions = {
  useHash: true
};

const routes: Routes = [
  { path: "login", component: IngresoComponent },
  { path: "inicio", component: InicioComponent },
  { path: "asistentes", component: AsistentesComponent },
  { path: "documentos/:padre", component: DocumentosComponent },
  { path: "carpetas/:padre", component: CarpetasComponent },
  { path: "tareas", component: TareasComponent },
  { path: "calendario", component: CalendarioComponent },
  { path: "proyectos", component: ProyectosComponent },
  { path: "cronicas", component: CronicasComponent },
  { path: "administradores", component: UsuariosComponent },
  { path: "grupos", component: GruposComponent },

  { path: "parametros/categorias", component: ParametrosCategoriasComponent },
  {
    path: "parametros/secciones-documentos",
    component: ParametrosSeccionesDocumentosComponent
  },
  { path: "parametros/mensaje-papa", component: MensajePapaComponent },

  { path: "encuestas", component: EncuestasComponent },
  { path: "foros", component: ForosComponent },
  { path: "**", pathMatch: "full", redirectTo: "login" }

];

@NgModule({
  imports: [RouterModule.forRoot(routes,options)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
