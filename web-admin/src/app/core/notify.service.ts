import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { MatSnackBar } from '@angular/material';


export interface Msg {
  content: string;
  style: string;
}

@Injectable()
export class NotifyService {

  constructor(
    public snackBar: MatSnackBar
  ){

  }

  private _msgSource = new Subject<Msg | null>();

  msg = this._msgSource.asObservable();

  update(content: string, style: 'error' | 'info' | 'success') {
    const msg: Msg = { content, style };
    this._msgSource.next(msg);
  }

  clear() {
    this._msgSource.next(null);
  }

  okMessage(content:string, time = 10000){
    this.snackBar.open(content, 'Cerrar', {
      duration: time,
      panelClass: 'greenMessage'
    });
  }

  errorMessage(content: string, time = 1500) {
    this.snackBar.open(content, 'Cerrar', {
      duration: time,
      panelClass: 'redMessage',
    });
  }

  alertMessage(content: string, time = 1500) {
    this.snackBar.open(content, 'Cerrar', {
      duration: time
    });
  }
}
