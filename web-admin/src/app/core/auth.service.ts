import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { NotifyService } from './notify.service';

import { Observable } from 'rxjs/Observable';
import { switchMap } from 'rxjs/operators';
import { ResourcesService } from './resources.service';

interface User {
  uid: string;
  correo?: string | null;
  estado?: string;
}

interface Admin {
  uid?: string;
  correo?: string;
  nombre?: string;
  apellido?:string;
  rol?:string;
  estado?: boolean;
}

@Injectable()
export class AuthService {

  user: Observable<User | null>;

  constructor(private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private notify: NotifyService,
    private _db: ResourcesService
  ) {

    this.user = this.afAuth.authState
      .switchMap((user) => {
        if (user) {
          return this._db.col$(`usuarios-sistema`, ref => ref.where('uid', '==', user.uid));
        } else {
          return Observable.of(null);
        }
      });
  }

  get authenticated(): boolean {
    return this.user !== null;
  }

  get currentUserObservable(): any {
    return this.afAuth.authState
  }

  getCurrentUser(){
    return this.afAuth.authState
      .switchMap((user) => {
        if (user) {
          return this._db.col$(`usuarios-sistema`, ref => ref.where('uid', '==', user.uid))
        } else {
          return Observable.of(null);
        }
      });
  }

  // Registrar usuario administrador
  createAdminWithEmail(email: string, password: string, info: Admin ) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((user) => {
        return this.updateUserData(user, info);
      })
      .catch((error) => this.handleError(error));
  }


  // Sets user data to firestore after succesful login
  private updateUserData(user: User, info: Admin) {

    const userRef: AngularFirestoreDocument<Admin> = this.afs.doc(`usuarios-sistema/${user.uid}`);
    const role:string = 'Administrador';

    const data: Admin = {
      uid: user.uid,
      correo: info.correo,
      nombre: info.nombre,
      apellido: info.apellido,
      rol:role,
      estado: true
    };
    return userRef.set(data);
  }

  emailLogin(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then((user) => {

          this._db.col$(`usuarios-sistema`, ref => ref.where('uid', '==', user.uid)).subscribe((res)=>{
            if(res.length > 0){
              resolve(res[0])
            }else{
              this.notify.errorMessage(`El usuario con correo electrónico ${email} no es administrador del sistema`, 2000)
              reject()
            }
          })
        })
        .catch((error) => {
          let message;

          switch (error.code) {
            case 'auth/user-not-found':
              message = 'Usuario no existe'
              break;
            case 'auth/wrong-password':
              message = 'Contraseña incorrecta'
              break;
            case 'auth/invalid-email':
              message = 'Formato de correo invalido'
              break;
            default:
              message = 'Usuario o contraseña invalido'
              break;
          }
          this.notify.errorMessage(message, 2000)
          reject(error)
        });
    })

  }

  // Sends email allowing user to reset password
  resetPassword(email: string) {
    const fbAuth = firebase.auth();

    return fbAuth.sendPasswordResetEmail(email)
      .then(() => this.notify.update('Password update email sent', 'info'))
      .catch((error) => this.handleError(error));
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }

  // If error, console log and notify user
  private handleError(error: Error) {
    console.error(error);
    this.notify.update(error.message, 'error');
  }
}
