import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/switchMap';
import { timestamp } from 'rxjs/operators/timestamp';
import { HttpClient } from '../../../node_modules/@angular/common/http';
type CollectionPredicate<T> = string | AngularFirestoreCollection<T>;
type DocPredicate<T> = string | AngularFirestoreDocument<T>;


export interface UsuarioSistema {
  id?: string;
  nombre?: string;
  apellido?: string;
  correo?: string;
  rol?:string;
  createdAt?: any;
  updatedAt?: any;
  estado?: boolean;
  foto?: string;
}

export interface Usuario {
  uid?: string;
  correo?: string | null;
  foto?: string;
  nombre?: string;
  apellido?: string;
  contrasena?: string;
  createdAt?: any;
  updatedAt?: any;
  tipodocu?: string;
  numerodocu?: string;
  movil?: string;
  sexo?: string;
  fechanacimiento?: string;
  ciudad?: string;

}


export interface Taxi {
  id?: string;
  placa?: string;
  vin?: string;
  motor?: string;
  kilometraje?: string;
  propietario?: string;
  empresa?: string;
  marca?:string;
  modelo?:string;
  fechadesoat?: any;
  fechadeextintor?: any;
  fechadertm?: any;
  estado?: boolean;
  foto?: string;
}

export interface DocumentoConductor {
  tipodocumento?:string;
  nombre?:string;
  url?:string;
  id?:string;
  createdAt?: any;
  updatedAt?: any;
}

export interface DocumentoTaxi {
  tipodocumento?: string;
  nombre?: string;
  url?: string;
  id?: string;
  createdAt?: any;
  updatedAt?: any;
}

export interface Section {
  id?:string;
  name?:string;
}

export interface Subsection {
  id?: string;
  name?: string;
}

export interface DataDocumentDialog {
  typedocument: string,
  file: File
}

@Injectable()
export class ResourcesService {

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage,
    private db: AngularFirestore,
    private http: HttpClient
  ) { }


  getPaises(){
    return this.http.get('assets/paises.json');
  }

  // Get Reference
  col<T>(ref: CollectionPredicate<T>, queryFn?): AngularFirestoreCollection<T> {
    return typeof ref === 'string' ? this.afs.collection<T>(ref,queryFn) : ref
  }

  doc<T>(ref: DocPredicate<T>): AngularFirestoreDocument<T> {
    return typeof ref === 'string' ? this.afs.doc<T>(ref) : ref
  }

  // Get Data
  doc$<T>(ref: DocPredicate<T>): Observable<T>{
    return this.doc(ref).snapshotChanges().map(doc => {
      return doc.payload.data() as T
    })
  }

  col$<T>(ref:CollectionPredicate<T>, queryFn?): Observable<T[]>{
    return this.col(ref,queryFn).snapshotChanges().map(docs => {
      return docs.map(a => a.payload.doc.data()) as T[]
    })
  }

  // With Id
  colWithIds$<T>(ref: CollectionPredicate<T>, queryFn?): Observable<any[]>{
    return this.col(ref,queryFn).snapshotChanges().map(action => {
      return action.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return {id, ...data};
      })
    });
  }

  inspectDoc(ref: DocPredicate<any>): void {
    const tick = new Date().getTime()
    this.doc(ref).snapshotChanges()
      .take(1)
      .do(d => {
        const tock = new Date().getTime() - tick
        console.log(`Loaded Document in ${tock}ms`, d)
      })
      .subscribe()
  }

  inspectCol(ref: CollectionPredicate<any>): void {
    const tick = new Date().getTime()
    this.col(ref).snapshotChanges()
      .take(1)
      .do(c => {
        const tock = new Date().getTime() - tick
        console.log(`Loaded Collection in ${tock}ms`, c)
      })
      .subscribe()
  }

  // Write Data

  update<T>(ref: DocPredicate<T>, data: any) {
    return this.doc(ref).update({
      ...data,
      updatedAt: this.timestamp
    })
  }

  set<T>(ref: DocPredicate<T>, data: any) {
    const timestamp = this.timestamp
    return this.doc(ref).set({
      ...data,
      updatedAt: timestamp,
      createdAt: timestamp
    })
  }

  add<T>(ref: CollectionPredicate<T>, data) {
    const timestamp = this.timestamp
    return this.col(ref).add({
      ...data,
      updatedAt: timestamp,
      createdAt: timestamp
    })
  }

  delete<T>(ref: DocPredicate<T>) {
    return this.doc(ref).delete()
  }

  upsert<T>(ref: DocPredicate<T>, data: any) {
    const doc = this.doc(ref).snapshotChanges().take(1).toPromise()
    return doc.then(snap => {
      return snap.payload.exists ? this.update(ref, data) : this.set(ref, data)
    })
  }

  // Get Firebase Serve Timestamp
  get timestamp(){
    return firebase.firestore.FieldValue.serverTimestamp()
  }

  // GeoPoint
  geopoint(lat:number, lng: number){
    return new firebase.firestore.GeoPoint(lat,lng);
  }


  // Documents
  connect(host: DocPredicate<any>, key: string, doc: DocPredicate<any>) {
    return this.doc(host).update({ [key]: this.doc(doc).ref })
  }

  /// returns a documents references mapped to AngularFirestoreDocument
  docWithRefs$<T>(ref: DocPredicate<T>) {
    return this.doc$(ref).map(doc => {
      for (const k of Object.keys(doc)) {
        if (doc[k] instanceof firebase.firestore.DocumentReference) {
          doc[k] = this.doc(doc[k].path)
        }
      }
      return doc
    })
  }

}
