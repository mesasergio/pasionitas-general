import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { IAlertConfig } from '../../../../node_modules/@covalent/core/dialogs';
import { Router } from '@angular/router';
import { AuthService } from '../../core/auth.service';
import { ResourcesService } from '../../core/resources.service';
import { TdLoadingService } from '@covalent/core/loading';
import { TdDialogService } from '@covalent/core/dialogs';
import { NotifyService } from '../../core/notify.service';

const config: IAlertConfig = {
  message: "Se ha enviado un enlace de restauración al correo electrónico",
  disableClose: false, // defaults to false
  title: "Atención", //OPTIONAL, hides if not provided
  closeButton: "Aceptar", //OPTIONAL, defaults to 'CANCEL'
  width: "500px" //OPTIONAL, defaults to 400px
};

const errorConfig: IAlertConfig = {
  message: "Es necesario que ingrese un correo electrónico",
  disableClose: false, // defaults to false
  title: "Error", //OPTIONAL, hides if not provided
  closeButton: "Cerrar", //OPTIONAL, defaults to 'CANCEL'
  width: "500px" //OPTIONAL, defaults to 400px
};

@Component({
  selector: "app-ingreso",
  templateUrl: "./ingreso.component.html",
  styleUrls: ["./ingreso.component.scss"]
})
export class IngresoComponent implements OnInit {
  item = {
    user: "",
    password: ""
  };
  showNavBar: boolean = false;
  showLogin: boolean = true;

  constructor(
    public _route: Router,
    private _loadingService: TdLoadingService,
    private _dialogService: TdDialogService,
    private _viewContainerRef: ViewContainerRef,
    public _auth: AuthService,
    public _notify: NotifyService
  ) { }

  ngOnInit() { }

  login() {
    this._auth.emailLogin(this.item.user, this.item.password).then(() => {
      this._route.navigate(["inicio"]);
    })
      .catch((error) => {
        this._notify.errorMessage(`Usuario o contraseña invalidos`, 2000)

      })
  }

  restart() {
    if (this.item.user) {
      this._loadingService.register();
      this._dialogService.openAlert(config);
      this._loadingService.resolve();
    } else {
      this._dialogService.openAlert(errorConfig);
    }
  }
}
