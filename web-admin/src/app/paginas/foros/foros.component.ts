import { Component, OnInit } from "@angular/core";
import { ResourcesService } from "../../core/resources.service";
import { TdDialogService } from "@covalent/core/dialogs";
import { MatDialog, MatSnackBar } from "@angular/material";
import { CrearForoComponent } from "./dialogos/crear-foro/crear-foro.component";


@Component({
  selector: "app-foros",
  templateUrl: "./foros.component.html",
  styleUrls: ["./foros.component.scss"]
})
export class ForosComponent implements OnInit {
  events: any[];

  header: any;

  constructor(
    public db: ResourcesService,
    private _dialogService: TdDialogService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {}

  crear() {
    let dialogRef = this.dialog.open(CrearForoComponent, {
      width: "60%"
    });
  }
}
