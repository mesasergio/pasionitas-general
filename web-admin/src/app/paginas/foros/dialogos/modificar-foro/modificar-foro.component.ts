import { Component, OnInit, Inject } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { UploadService, Upload } from "../../../../core/upload.service";
import { TdLoadingService } from "@covalent/core/loading";

@Component({
  selector: "app-modificar-foro",
  templateUrl: "./modificar-foro.component.html",
  styleUrls: ["./modificar-foro.component.scss"]
})
export class ModificarForoComponent implements OnInit {
  info: any = {};
  files: any = {};

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public upload: UploadService,
    public dialogRef: MatDialogRef<ModificarForoComponent>,
    private _loadingService: TdLoadingService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _upload: UploadService
  ) {
    this.info = data.item;
  }

  ngOnInit() {}

  guardar() {
    this._loadingService.register();
    console.log('guardo')
    this.db
      .update(`foros/${this.info.id}`, this.info)
      .then(() => {
        this.notify.update("Foro guardado", "success");
        this._loadingService.resolve();
        this.cerrar();
      })
      .catch(err => {
        this._loadingService.resolve();
        this.notify.update("Error creando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  fileSelectMsg: string = "No file selected yet.";
  fileUploadMsg: string = "No file uploaded yet.";
  file: any;
  disabled: boolean = false;

  selectEvent(file: File): void {
    console.log(file);
    this.fileSelectMsg = file.name;
    this.info.file = file;
  }

  uploadEvent(file: File): void {
    this.fileUploadMsg = file.name;
    this.info.file = file;
  }

  cancelEvent(): void {
    this.fileSelectMsg = "No file selected yet.";
    this.fileUploadMsg = "No file uploaded yet.";
    this.info.file = null;
  }

  cargarImagen(files: FileList | File, tipo): void {
    if (files instanceof FileList) {
    } else {
      var currentUpload = new Upload(files);
      this._upload.uploadPic(currentUpload).then(url => {
        this.info.foto = url;
      });
    }
  }

  eliminarFoto() {
    this.info.foto = "";
  }
}
