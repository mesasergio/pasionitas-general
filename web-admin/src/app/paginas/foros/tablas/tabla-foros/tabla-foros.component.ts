import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog
} from "@angular/material";
import { Router } from "@angular/router";
import { TdLoadingService } from "@covalent/core/loading";
import { TdDialogService, IConfirmConfig } from "@covalent/core/dialogs";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { ModificarForoComponent } from "../../dialogos/modificar-foro/modificar-foro.component";

// Configuración de confirm eliminar
const configDel: IConfirmConfig = {
  message: "¿Está seguro de eliminar el registro?",
  disableClose: false, // defaults to false
  title: "Confirmación", //OPTIONAL, hides if not provided
  cancelButton: "Cancelar", //OPTIONAL, defaults to 'CANCEL'
  acceptButton: "Aceptar", //OPTIONAL, defaults to 'ACCEPT'
  width: "500px" //OPTIONAL, defaults to 400px
};

@Component({
  selector: 'tabla-foros',
  templateUrl: './tabla-foros.component.html',
  styleUrls: ['./tabla-foros.component.scss']
})
export class TablaForosComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  displayedColumns = ["titulo", "objetivo", "hilos", "idioma", "createdAt", "updatedAt", "opciones"];

  lista: any[] = [];

  constructor(
    public db: ResourcesService,
    private _loadingService: TdLoadingService,
    private _dialogService: TdDialogService,
    private _notify: NotifyService,
    private _router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.cargarLista();
  }

  cargarLista() {
    this.db.colWithIds$("foros").subscribe((resp: any[]) => {
      this.lista = resp;

      this.lista.forEach(element => {

        let consulta = this.db.colWithIds$('hilos-foros',ref=>ref.where('foroid','==',element.id)).subscribe((hilos:any[])=>{
          element.hilos = hilos.length;
        })

      });
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ir(item) {
    let dialogRef = this.dialog.open(ModificarForoComponent, {
      width: "60%",
      data: { item: item }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  eliminar(item) {
    this._dialogService
      .openConfirm(configDel)
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.db
            .delete(`foros/${item.id}`)
            .then(res => {
              this._notify.okMessage("Foro eliminado");
            })
            .catch(err => {
              this._notify.errorMessage(err);
            });
        } else {
        }
      });
  }

}
