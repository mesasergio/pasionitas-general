import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TdDialogService, IConfirmConfig } from '@covalent/core/dialogs';
import { ResourcesService } from '../../../../core/resources.service';
import { NotifyService } from '../../../../core/notify.service';
import { ModificarCalendarioComponent } from '../../dialogo/modificar/modificar.component';

// Configuración de confirm eliminar
const configDel: IConfirmConfig = {
  message: '¿Está seguro de eliminar el registro?',
  disableClose: false, // defaults to false
  title: 'Confirmación', //OPTIONAL, hides if not provided
  cancelButton: 'Cancelar', //OPTIONAL, defaults to 'CANCEL'
  acceptButton: 'Aceptar', //OPTIONAL, defaults to 'ACCEPT'
  width: '500px', //OPTIONAL, defaults to 400px
}
@Component({
  selector: 'tabla-calendario',
  templateUrl: './tabla-calendario.component.html',
  styleUrls: ['./tabla-calendario.component.scss']
})
export class TablaCalendarioComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
    displayedColumns = ["dia", "fecha", "horainicio", "horafin", "titulo", "actividad","opciones"];

  lista: any[] = [];

  constructor(
    public db: ResourcesService,
    private _loadingService: TdLoadingService,
    private _dialogService: TdDialogService,
    private _notify: NotifyService,
    private _router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.cargarLista();
  }

  cargarLista() {
      this.db
        .colWithIds$("actividades", ref =>
          ref.orderBy("dia", "asc").orderBy("horainicio", "asc")
        )
        .subscribe((resp: any[]) => {
          this.lista = resp;
          this.dataSource = new MatTableDataSource(this.lista);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
  }

  ir(item) {
    let dialogRef = this.dialog.open(ModificarCalendarioComponent, {
      width: "60%",
      data: { item: item }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  eliminar(item) {
    this._dialogService
      .openConfirm(configDel)
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.db
            .delete(`actividades/${item.id}`)
            .then(res => {
              this._notify.okMessage("Asistente eliminado");
            })
            .catch(err => {
              this._notify.errorMessage(err);
            });
        } else {
        }
      });
  }
}
