import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../core/resources.service';
import { TdDialogService } from '@covalent/core/dialogs';
import { MatDialog, MatSnackBar } from '@angular/material';
import { CrearCalendarioComponent } from './dialogo/crear/crear.component';



@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.scss']
})
export class CalendarioComponent implements OnInit {

  events: any[];

  header: any;

  constructor(
    public db: ResourcesService,
    private _dialogService: TdDialogService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {

  }

  crear() {
    let dialogRef = this.dialog.open(CrearCalendarioComponent, {
      width: "60%"
    });
  }

}
