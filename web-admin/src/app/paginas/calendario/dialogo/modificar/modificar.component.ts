import { Component, OnInit, Inject } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";
import { UploadService, Upload } from '../../../../core/upload.service';

@Component({
  selector: "app-modificar",
  templateUrl: "./modificar.component.html",
  styleUrls: ["./modificar.component.scss"]
})
export class ModificarCalendarioComponent implements OnInit {
  info: any = {};
  imagenes: any[] = [];
  files: any = {};
  paises: any[] = [];

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ModificarCalendarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _upload: UploadService
  ) {
    this.info = data.item;
    this.db.getPaises().subscribe((resp: any[]) => {
      this.paises = resp;
      console.log(this.paises)
    })
  }

  ngOnInit() { }

  guardar() {
    this.db
      .update(`actividades/${this.info.id}`, this.info)
      .then(() => {
        this.snackBar.open("Actividad modificado", "Cerrar", {
          duration: 2000,
          horizontalPosition: "left"
        });
        this.cerrar();
      })
      .catch(err => {
        this.notify.update("Error modificando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

}
