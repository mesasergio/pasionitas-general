import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CrearUsuarioComponent } from './dialogo/crear/crear.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {


  constructor(
    public dialog: MatDialog
  ) {}

  ngOnInit() {
  }

  crear(){

    let dialogRef = this.dialog.open(CrearUsuarioComponent, {
      width: "60%",
    });

  }

}
