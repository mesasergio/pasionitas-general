import { Component, OnInit, Inject } from '@angular/core';
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: "app-modificar",
  templateUrl: "./modificar.component.html",
  styleUrls: ["./modificar.component.scss"]
})
export class ModificarUsuarioComponent implements OnInit {
  info: any = {};

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ModificarUsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.info = data.item;
  }

  ngOnInit() {}

  guardar() {
    this.db
      .update(`usuarios-sistema/${this.info.id}`, this.info)
      .then(() => {
        this.snackBar.open('Usuario modificado', 'Cerrar', {
          duration: 2000,
          horizontalPosition: "left"
        });
        this.cerrar();
      })
      .catch(err => {
        this.notify.update("Error modificando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }
}
