import { Component, OnInit } from '@angular/core';
import { CrearDocumentoComponent } from './dialogo/crear/crear.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: "app-documentos",
  templateUrl: "./documentos.component.html",
  styleUrls: ["./documentos.component.scss"]
})
export class DocumentosComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  subir() {
    let dialogRef = this.dialog.open(CrearDocumentoComponent, {
      width: "60%"
    });
  }

}
