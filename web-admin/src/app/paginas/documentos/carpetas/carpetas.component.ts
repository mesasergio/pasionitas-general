import { Component, OnInit, ViewChild } from '@angular/core';
import { CrearCarpetaDialogoComponent } from './dialogo/dialogo.component';
import { MatDialog, MatSnackBarHorizontalPosition } from '@angular/material';
import { ResourcesService } from '../../../core/resources.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import Swal from "sweetalert2";
import { NotifyService } from '../../../core/notify.service';
import { MatSnackBar } from "@angular/material";

import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import { TdLoadingService } from "@covalent/core/loading";
import { TdDialogService, IConfirmConfig } from "@covalent/core/dialogs";
import { CrearDocumentoComponent } from '../dialogo/crear/crear.component';
import { ModificarCarpetaComponent } from './dialogo/modificar-carpeta/modificar-carpeta.component';
import { ModificarDocumentoComponent } from '../dialogo/modificar-documento/modificar-documento.component';


// Configuración de confirm eliminar
const configDel: IConfirmConfig = {
  message: "¿Está seguro de eliminar el documento?",
  disableClose: false, // defaults to false
  title: "Confirmación", //OPTIONAL, hides if not provided
  cancelButton: "Cancelar", //OPTIONAL, defaults to 'CANCEL'
  acceptButton: "Aceptar", //OPTIONAL, defaults to 'ACCEPT'
  width: "500px" //OPTIONAL, defaults to 400px
};

@Component({
  selector: "app-carpetas",
  templateUrl: "./carpetas.component.html",
  styleUrls: ["./carpetas.component.scss"]
})
export class CarpetasComponent {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  displayedColumns = ["nombre", "idioma","type", "createdAt", "opciones"];

  lista: any[] = [];
  padre: any = 0;
  rutas: any[] = [];
  listaRutas: any[] = [];
  documentos:any[] = [];
  padreant:any;

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public dialog: MatDialog,
    public _router: Router,
    private _route: ActivatedRoute,
    public snackBar: MatSnackBar,
    public location: Location,
    private _dialogService: TdDialogService,
  ) {
    this._router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this._route.params.subscribe(params => {
          this.padre = params.padre;
          this.cargarInfo();
          this.cargarLista();
        });
      }
    });
  }

  cargarRutas() {
    let rutasqry = this.db.colWithIds$("rutas").subscribe((rutas: any[]) => {
      this.listaRutas = rutas;
      rutasqry.unsubscribe();
    });
  }

  cargarInfo() {
    let carpetasqry = this.db
      .colWithIds$("rutas", ref => ref.where("padre", "==", this.padre))
      .subscribe((resp: any[]) => {
        this.lista = resp;
        if (this.padre != 0) {
          this.rutas = [];
          this.armarRuta(this.padre);
        }
        carpetasqry.unsubscribe();
      });
  }

  armarRuta(padre) {

    if(this.padreant == padre) return;

    this.padreant = padre;

    if (this.padre != 0 && this.listaRutas.length == 0) {

      let rutasqry = this.db.colWithIds$("rutas").subscribe((rutas: any[]) => {
        this.listaRutas = rutas;
        rutasqry.unsubscribe();
        let ruta = this.listaRutas.find(x => x.id == padre);

        if (ruta) {
          if (ruta.padre) this.armarRuta(ruta.padre);
          this.rutas.push(ruta);
        }
      });

    } else if (this.padre != 0 && this.listaRutas.length > 0) {
      console.log('En el segundo', padre)

      let ruta = this.listaRutas.find(x => x.id == padre);
      if (ruta) {
        if (ruta.padre) this.armarRuta(ruta.padre);
        this.rutas.push(ruta);
      }
    }
  }

  cargarLista() {
    this.db.colWithIds$(`documentos`,ref=>ref.where('ruta','==',this.padre)).subscribe((resp: any[]) => {
      this.documentos = resp;
      this.dataSource = new MatTableDataSource(this.documentos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ir(item) {
    let dialogRef = this.dialog.open(ModificarCarpetaComponent, {
      width: "60%",
      data: { item: this.padre }
    });

    dialogRef.afterClosed().subscribe(result => { });
  }


  irDocumento(item) {
    let dialogRef = this._dialogService.open(ModificarDocumentoComponent, {
      width: "60%",
      data: { item: item }
    });

    dialogRef.afterClosed().subscribe(result => { });
  }

  eliminarDocumento(item) {
    this._dialogService
      .openConfirm(configDel)
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.db
            .delete(`documentos/${item.id}`)
            .then(res => {
              this.notify.okMessage("Documento eliminado");
            })
            .catch(err => {
              this.notify.errorMessage(err);
            });
        } else {
        }
      });
  }

  crear() {
    let dialogRef = this.dialog.open(CrearCarpetaDialogoComponent, {
      width: "60%",
      data: { padre: this.padre }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.lista = [];
      this.rutas = [];
      this.listaRutas = [];
      this.cargarInfo();
    });
  }

  irSubCarpeta(item) {
    this._router.navigate([`carpetas/${item.id}`]);
  }

  atras(id) {
    if (this.padre != 0) {
      this._router.navigate([`carpetas/${id}`]);
    }
  }

  eliminar() {
    Swal({
      title: "Confirmación de borrado",
      text: "¿Está seguro de borrar esta carpeta y todo su contenido?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar"
    }).then(result => {
      if (result.value) {
        this.db.delete(`rutas/${this.padre}`).then(() => {
          this.snackBar.open("Carpeta eliminada", "Cerrar", {
            duration: 2000,
            horizontalPosition: "left"
          });
          if (this.rutas.length > 1){
            this._router.navigate([
              `carpetas/${this.rutas[this.rutas.length - 2].id}`
            ]);
          }else{
            this.rutas = [];
            this._router.navigate([
              `carpetas/0`
            ]);
          }
        });
      }
    });
  }

  subir() {
    let dialogRef = this.dialog.open(CrearDocumentoComponent, {
      width: "60%",
      data: { padre: this.padre }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }
}
