import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";
import { ResourcesService } from '../../../../../core/resources.service';
import { NotifyService } from '../../../../../core/notify.service';
import { Upload, UploadService } from '../../../../../core/upload.service';

@Component({
  selector: "app-modificar-carpeta",
  templateUrl: "./modificar-carpeta.component.html",
  styleUrls: ["./modificar-carpeta.component.scss"]
})
export class ModificarCarpetaComponent implements OnInit {
  info: any = {};
  id:any;
  files: any = {};


  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public snackBar: MatSnackBar,
    public upload: UploadService,
    public dialogRef: MatDialogRef<ModificarCarpetaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.id = data.item;
    let cons = this.db.docWithRefs$(`rutas/${data.item}`).subscribe(resp=>{
      this.info = resp;
      cons.unsubscribe();
      console.log(resp)
    })
    //this.info = data.item;
  }

  ngOnInit() {}

  guardar() {
    console.log('Aqui')
    this.db
      .update(`rutas/${this.id}`, this.info)
      .then(() => {
        this.snackBar.open("Carpeta modificado", "Cerrar", {
          duration: 2000,
          horizontalPosition: "left"
        });
        this.cerrar();
      })
      .catch(err => {
        this.notify.update("Error modificando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  cargarImagen(files: FileList | File, tipo): void {
    if (files instanceof FileList) {
    } else {
      var currentUpload = new Upload(files);
      this.upload.uploadPic(currentUpload).then(url => {
        this.info.foto = url;
      });
    }
  }

  eliminarFoto() {
    this.info.foto = "";
  }
}
