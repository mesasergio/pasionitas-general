import { Component, OnInit, Inject } from '@angular/core';
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UploadService, Upload } from '../../../../core/upload.service';
import { TdLoadingService } from "@covalent/core/loading";

@Component({
  selector: "app-crear",
  templateUrl: "./crear.component.html",
  styleUrls: ["./crear.component.scss"]
})
export class CrearDocumentoComponent implements OnInit {
  info: any = {};
  files: any = {};

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public upload: UploadService,
    public dialogRef: MatDialogRef<CrearDocumentoComponent>,
    private _loadingService: TdLoadingService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _upload: UploadService
  ) {}

  ngOnInit() {}

  guardar() {
    let file: Upload = new Upload(this.info.file);

    this._loadingService.register();

    this.upload
      .uploadDocumento(file)
      .then(url => {
        this.info.type = this.info.file.type;
        delete this.info.file;
        this.info.url = url;
        this.info.ruta = this.data.padre;

        this.db
          .add("documentos", this.info)
          .then(() => {
            this.notify.update("Documento guardado", "success");
            this._loadingService.resolve();
            this.cerrar();
          })
          .catch(err => {
            this._loadingService.resolve();
            this.notify.update("Error creando registro", "error");
          });
      })
      .catch(err => {
        this._loadingService.resolve();
        this.notify.update("Ocurrio un error subiendo el documento", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  fileSelectMsg: string = "No file selected yet.";
  fileUploadMsg: string = "No file uploaded yet.";
  file: any;
  disabled: boolean = false;

  selectEvent(file: File): void {
    console.log(file);
    this.fileSelectMsg = file.name;
    this.info.file = file;
  }

  uploadEvent(file: File): void {
    this.fileUploadMsg = file.name;
    this.info.file = file;
  }

  cancelEvent(): void {
    this.fileSelectMsg = "No file selected yet.";
    this.fileUploadMsg = "No file uploaded yet.";
    this.info.file = null;
  }

  cargarImagen(files: FileList | File, tipo): void {
    if (files instanceof FileList) {
    } else {
      var currentUpload = new Upload(files);
      this._upload.uploadPic(currentUpload).then(url => {
        this.info.foto = url;
      });
    }
  }

  eliminarFoto() {
    this.info.foto = "";
  }
}
