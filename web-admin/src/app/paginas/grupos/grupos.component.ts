import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../core/resources.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { TdDialogService } from '@covalent/core/dialogs';
import { CrearGrupoComponent } from './dialogos/crear-grupo/crear-grupo.component';

@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.component.html',
  styleUrls: ['./grupos.component.scss']
})
export class GruposComponent implements OnInit {

  events: any[];

  header: any;

  constructor(
    public db: ResourcesService,
    private _dialogService: TdDialogService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() { }

  crear() {
    let dialogRef = this.dialog.open(CrearGrupoComponent, {
      width: "80%"
    });
  }
}
