import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from '../../../../core/notify.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { UploadService, Upload } from "../../../../core/upload.service";
import { TdLoadingService } from "@covalent/core/loading";
import { AgregarAsistenteComponent } from '../agregar-asistente/agregar-asistente.component';
import { TdDialogService, IConfirmConfig } from '@covalent/core/dialogs';

const configDel: IConfirmConfig = {
  message: "¿Está seguro de eliminar el registro?",
  disableClose: false, // defaults to false
  title: "Confirmación", //OPTIONAL, hides if not provided
  cancelButton: "Cancelar", //OPTIONAL, defaults to 'CANCEL'
  acceptButton: "Aceptar", //OPTIONAL, defaults to 'ACCEPT'
  width: "500px" //OPTIONAL, defaults to 400px
};

@Component({
  selector: "app-crear-grupo",
  templateUrl: "./crear-grupo.component.html",
  styleUrls: ["./crear-grupo.component.scss"]
})
export class CrearGrupoComponent implements OnInit {
  info: any = {
    asistentes:[]
  };

  files: any = {};
  sort: MatSort;

  lista: any[] = [];
  allAsistentes: any[] = [];
  asistentesAgregados: any[] = [];

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public upload: UploadService,
    public dialogRef: MatDialogRef<CrearGrupoComponent>,
    private _loadingService: TdLoadingService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _upload: UploadService,
    public dialog: MatDialog,
    private _dialogService: TdDialogService,
    private _notify: NotifyService
  ) {}

  ngOnInit() {
    this.cargarLista();
  }


  cargarLista() {
    this.db.colWithIds$("asistentes").subscribe((resp: any[]) => {

      this.allAsistentes = resp;

      this.allAsistentes.forEach((element, index) => {
        let asistente = this.info.asistentes.find(x => x.asistenteid == element.id);
        if (asistente) {
          this.lista.push(element);
        }
      });
    });
  }

  eliminar(index) {
    console.log(index);
    this.info.asistentes.splice(index,1);
    this.lista.splice(index,1);
  }

  guardar() {
    this._loadingService.register();

    this.db
      .add("grupos", this.info)
      .then(() => {
        this.notify.update("Grupo guardado", "success");
        this._loadingService.resolve();
        this.cerrar();
      })
      .catch(err => {
        this._loadingService.resolve();
        this.notify.update("Error creando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }


  cargarImagen(files: FileList | File, tipo): void {
    if (files instanceof FileList) {
    } else {
      var currentUpload = new Upload(files);
      this._upload.uploadPic(currentUpload).then(url => {
        this.info.foto = url;
      });
    }
  }

  eliminarFoto() {
    this.info.foto = "";
  }

  agregar() {
    let dialogRef = this.dialog.open(AgregarAsistenteComponent, {
      width: "60%",
      data: { item: this.info.asistentes }
    });

    dialogRef.afterClosed().subscribe((resp:any[]) => {


      if(resp.length > 0){

        this.lista = [];

        resp.forEach(element => {
          this.info.asistentes.push({
            asistenteid: element.id
          })
        });

        this.allAsistentes.forEach((element, index) => {
          let asistente = this.info.asistentes.find(x => x.asistenteid == element.id);
          if (asistente) {
            this.lista.push(element);
          }
        });

      }
    })
  }
}
