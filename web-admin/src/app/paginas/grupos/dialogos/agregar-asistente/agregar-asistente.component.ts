import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ResourcesService } from '../../../../core/resources.service';

@Component({
  selector: "app-agregar-asistente",
  templateUrl: "./agregar-asistente.component.html",
  styleUrls: ["./agregar-asistente.component.scss"]
})
export class AgregarAsistenteComponent implements OnInit {
  displayedColumns = ["selection", "correo", "nombre"];
  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<Element>(true, []);

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  lista: any[] = [];
  asistentes:any[] = [];

  constructor(
    public db: ResourcesService,
    public dialogRef: MatDialogRef<AgregarAsistenteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    this.asistentes = this.data.item;
    this.cargarLista();
  }

  cargarLista() {
    this.db.colWithIds$("asistentes").subscribe((resp: any[]) => {


      resp.forEach((element, index) => {
        let asistente = this.asistentes.find(x => x.asistenteid == element.id);
        if (!asistente) {
          this.lista.push(element);
        }
      });
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }


  cerrar(): void {
    this.dialogRef.close([]);
  }

  guardar() {
    this.dialogRef.close(this.selection.selected);
  }
}
