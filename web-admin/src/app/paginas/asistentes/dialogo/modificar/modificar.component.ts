import { Component, OnInit, Inject } from '@angular/core';
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Upload, UploadService } from '../../../../core/upload.service';

@Component({
  selector: "app-modificar",
  templateUrl: "./modificar.component.html",
  styleUrls: ["./modificar.component.scss"]
})
export class ModificarAsistenteComponent implements OnInit {
  info: any = {};
  imagenes: any[] = [];
  files: any = {};
  paises: any[] = [];

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ModificarAsistenteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _upload: UploadService
  ) {
    this.info = data.item;
    this.db.getPaises().subscribe((resp: any[]) => {
      this.paises = resp;
    })
  }

  ngOnInit() {}

  guardar() {
    this.db
      .update(`asistentes/${this.info.id}`, this.info)
      .then(() => {
        this.snackBar.open("Asistente modificado", "Cerrar", {
          duration: 2000,
          horizontalPosition: "left"
        });
        this.cerrar();
      })
      .catch(err => {
        this.notify.update("Error modificando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  cargarImagen(files: FileList | File, tipo): void {
    if (files instanceof FileList) {
    } else {
      var currentUpload = new Upload(files);
      this._upload.uploadPic(currentUpload).then(url => {
        this.info.foto = url;
      });
    }
  }

  eliminarFoto() {
    this.info.foto = "";
  }
}
