import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../../../core/resources.service';
import { NotifyService } from '../../../../core/notify.service';
import { MatDialogRef } from '@angular/material';
import { UploadService, Upload } from '../../../../core/upload.service';

@Component({
  selector: "app-crear",
  templateUrl: "./crear.component.html",
  styleUrls: ["./crear.component.scss"]
})
export class CrearAsistenteComponent implements OnInit {
  info: any = {};
  imagenes: any[] = [];
  files: any = {};
  paises: any[] = [];

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public dialogRef: MatDialogRef<CrearAsistenteComponent>,
    public _upload: UploadService
  ) {
    this.db.getPaises().subscribe((resp:any[])=>{
      this.paises = resp;
      console.log(this.paises)
    })
  }

  ngOnInit() {}

  guardar() {
    this.db
      .add("asistentes", this.info)
      .then(() => {
        this.notify.update("Asistente creado exitosamente", "success");
        this.cerrar();
      })
      .catch(err => {
        this.notify.update("Error creando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  cargarImagen(files: FileList | File, tipo): void {

    if (files instanceof FileList) {} else {
      var currentUpload = new Upload(files);
      this._upload.uploadPic(currentUpload).then(url => {
        this.info.foto = url;
      });

    }
  }

  eliminarFoto(){
    this.info.foto = "";
  }
}
