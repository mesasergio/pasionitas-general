import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CrearAsistenteComponent } from './dialogo/crear/crear.component';
import { ModificarAsistenteComponent } from './dialogo/modificar/modificar.component';

@Component({
  selector: 'app-asistentes',
  templateUrl: './asistentes.component.html',
  styleUrls: ['./asistentes.component.scss']
})
export class AsistentesComponent implements OnInit {


  constructor(
    public dialog: MatDialog
  ) {}

  ngOnInit() {
  }

  crear(){

    let dialogRef = this.dialog.open(CrearAsistenteComponent, {
      width: "60%",
    });

  }

}
