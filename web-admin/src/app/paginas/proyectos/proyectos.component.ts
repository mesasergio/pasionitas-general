import { Component, OnInit } from "@angular/core";
import { ResourcesService } from "../../core/resources.service";
import { TdDialogService } from "@covalent/core/dialogs";
import { MatDialog, MatSnackBar } from "@angular/material";
import { CrearProyectoComponent } from './dialogos/crear-proyecto/crear-proyecto.component';


@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
  styleUrls: ['./proyectos.component.scss']
})
export class ProyectosComponent implements OnInit {

  events: any[];

  header: any;

  constructor(
    public db: ResourcesService,
    private _dialogService: TdDialogService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {

  }

  crear() {
    let dialogRef = this.dialog.open(CrearProyectoComponent, {
      width: "60%"
    });
  }

}
