import { Component, OnInit, Inject } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { UploadService, Upload } from "../../../../core/upload.service";
import { TdLoadingService } from "@covalent/core/loading";

@Component({
  selector: "app-crear-proyecto",
  templateUrl: "./crear-proyecto.component.html",
  styleUrls: ["./crear-proyecto.component.scss"]
})
export class CrearProyectoComponent implements OnInit {
  info: any = {};
  files: any = {};

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public upload: UploadService,
    public dialogRef: MatDialogRef<CrearProyectoComponent>,
    private _loadingService: TdLoadingService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _upload: UploadService
  ) {}

  ngOnInit() {}

  guardar() {
    this._loadingService.register();

    this.db
      .add("proyectos", this.info)
      .then(() => {
        this.notify.update("Proyecto guardado", "success");
        this._loadingService.resolve();
        this.cerrar();
      })
      .catch(err => {
        this._loadingService.resolve();
        this.notify.update("Error creando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  fileSelectMsg: string = "No file selected yet.";
  fileUploadMsg: string = "No file uploaded yet.";
  file: any;
  disabled: boolean = false;

  cargarImagen(files: FileList | File, tipo): void {
    if (files instanceof FileList) {
    } else {
      var currentUpload = new Upload(files);
      this._upload.uploadPic(currentUpload).then(url => {
        this.info.foto = url;
      });
    }
  }

  cargarPdf(files: FileList | File): void {
    if (files instanceof FileList) {
    } else {
      var currentUpload = new Upload(files);
      this._upload.uploadDocumento(currentUpload).then(url => {
        this.info.documentourl = url;
      });
    }
  }

  eliminarFoto() {
    this.info.foto = "";
  }

  eliminarPdf() {
    this.info.documentourl = "";
  }
}
