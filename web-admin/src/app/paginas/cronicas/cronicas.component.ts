import { Component, OnInit } from "@angular/core";
import { ResourcesService } from "../../core/resources.service";
import { TdDialogService } from "@covalent/core/dialogs";
import { MatDialog, MatSnackBar } from "@angular/material";
import { CrearCronicaComponent } from './dialogos/crear-cronica/crear-cronica.component';


@Component({
  selector: "app-cronicas",
  templateUrl: "./cronicas.component.html",
  styleUrls: ["./cronicas.component.scss"]
})
export class CronicasComponent implements OnInit {
  events: any[];

  header: any;

  constructor(
    public db: ResourcesService,
    private _dialogService: TdDialogService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {}

  crear() {
    let dialogRef = this.dialog.open(CrearCronicaComponent, {
      width: "60%"
    });
  }
}
