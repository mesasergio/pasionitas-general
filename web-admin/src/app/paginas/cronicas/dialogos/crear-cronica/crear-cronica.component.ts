import { Component, OnInit, Inject } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { UploadService, Upload } from "../../../../core/upload.service";
import { TdLoadingService } from "@covalent/core/loading";

@Component({
  selector: "app-crear-cronica",
  templateUrl: "./crear-cronica.component.html",
  styleUrls: ["./crear-cronica.component.scss"]
})
export class CrearCronicaComponent implements OnInit {
  info: any = {};
  files: any = {};

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public upload: UploadService,
    public dialogRef: MatDialogRef<CrearCronicaComponent>,
    private _loadingService: TdLoadingService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _upload: UploadService
  ) {}

  ngOnInit() {}

  guardar() {
    this._loadingService.register();

    this.db
      .add("cronicas", this.info)
      .then(() => {
        this.notify.update("Crónica guardada", "success");
        this._loadingService.resolve();
        this.cerrar();
      })
      .catch(err => {
        this._loadingService.resolve();
        this.notify.update("Error creando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  fileSelectMsg: string = "No file selected yet.";
  fileUploadMsg: string = "No file uploaded yet.";
  file: any;
  disabled: boolean = false;

  selectEvent(file: File): void {
    console.log(file);
    this.fileSelectMsg = file.name;
    this.info.file = file;
  }

  uploadEvent(file: File): void {
    this.fileUploadMsg = file.name;
    this.info.file = file;
  }

  cancelEvent(): void {
    this.fileSelectMsg = "No file selected yet.";
    this.fileUploadMsg = "No file uploaded yet.";
    this.info.file = null;
  }

  cargarImagen(files: FileList | File, tipo): void {
    if (files instanceof FileList) {
    } else {
      var currentUpload = new Upload(files);
      this._upload.uploadPic(currentUpload).then(url => {
        this.info.foto = url;
      });
    }
  }

  eliminarFoto() {
    this.info.foto = "";
  }
}
