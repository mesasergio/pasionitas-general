import { Component, OnInit } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef } from "@angular/material";

@Component({
  selector: "app-crear",
  templateUrl: "./crear.component.html",
  styleUrls: ["./crear.component.scss"]
})
export class CrearCategoriaDialogoComponent implements OnInit {
  info: any = {};

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public dialogRef: MatDialogRef<CrearCategoriaDialogoComponent>
  ) {}

  ngOnInit() {}

  guardar() {
    this.db
      .add("categorias", this.info)
      .then(() => {
        this.notify.update("Categoria creada exitosamente", "success");
        this.cerrar();
      })
      .catch(err => {
        this.notify.update("Error creando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }
}
