import { Component, OnInit } from "@angular/core";
import { ResourcesService } from "../../core/resources.service";
import { TdDialogService } from "@covalent/core/dialogs";
import { MatDialog, MatSnackBar } from "@angular/material";
import { CrearEncuestaComponent } from "./dialogos/crear-encuesta/crear-encuesta.component";

@Component({
  selector: "app-encuestas",
  templateUrl: "./encuestas.component.html",
  styleUrls: ["./encuestas.component.scss"]
})
export class EncuestasComponent implements OnInit {
  events: any[];

  header: any;

  constructor(
    public db: ResourcesService,
    private _dialogService: TdDialogService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {}

  crear() {
    let dialogRef = this.dialog.open(CrearEncuestaComponent, {
      width: "60%"
    });
  }
}
