import { Component, OnInit, Inject } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";

@Component({
  selector: "app-modificar-encuesta",
  templateUrl: "./modificar-encuesta.component.html",
  styleUrls: ["./modificar-encuesta.component.scss"]
})
export class ModificarEncuestaComponent implements OnInit {
  info: any = {};

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ModificarEncuestaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.info = data.item;
  }

  ngOnInit() {}

  guardar() {
    this.db
      .update(`encuestas/${this.info.id}`, this.info)
      .then(() => {
        this.snackBar.open("Encuesta modificada", "Cerrar", {
          duration: 2000,
          horizontalPosition: "left"
        });
        this.cerrar();
      })
      .catch(err => {
        this.notify.update("Error modificando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }
}
