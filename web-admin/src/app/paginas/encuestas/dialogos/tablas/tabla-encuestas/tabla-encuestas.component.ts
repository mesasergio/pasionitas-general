import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog
} from "@angular/material";
import { Router } from "@angular/router";
import { TdLoadingService } from "@covalent/core/loading";
import { TdDialogService, IConfirmConfig } from "@covalent/core/dialogs";
import { ResourcesService } from '../../../../../core/resources.service';
import { NotifyService } from '../../../../../core/notify.service';
import { ModificarEncuestaComponent } from '../../modificar-encuesta/modificar-encuesta.component';


// Configuración de confirm eliminar
const configDel: IConfirmConfig = {
  message: "¿Está seguro de eliminar el registro?",
  disableClose: false, // defaults to false
  title: "Confirmación", //OPTIONAL, hides if not provided
  cancelButton: "Cancelar", //OPTIONAL, defaults to 'CANCEL'
  acceptButton: "Aceptar", //OPTIONAL, defaults to 'ACCEPT'
  width: "500px" //OPTIONAL, defaults to 400px
};
@Component({
  selector: 'tabla-encuestas',
  templateUrl: './tabla-encuestas.component.html',
  styleUrls: ['./tabla-encuestas.component.scss']
})
export class TablaEncuestasComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  displayedColumns = ["titulo", "pregunta", "si", "no","blanco", "estado","createdAt","updatedAt", "opciones"];

  lista: any[] = [];

  constructor(
    public db: ResourcesService,
    private _loadingService: TdLoadingService,
    private _dialogService: TdDialogService,
    private _notify: NotifyService,
    private _router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.cargarLista();
  }

  cargarLista() {
    this.db.colWithIds$("encuestas").subscribe((resp: any[]) => {
      this.lista = resp;
      this.lista.forEach(element => {
        let respuestas = this.db.colWithIds$("respuestas-votaciones", ref => ref.where('encuestaid','==',element.id)).subscribe((respu:any[])=>{
          let respsi:number = 0;
          let respno: number = 0;
          let respblanco: number = 0;

          respu.forEach(res => {
            if (res.respuesta == 'si'){
              respsi += 1;
            }
            else if(res.respuesta == 'no'){
              respno += 1;
            }
            else if(res.respuesta == 'blanco'){
              respblanco += 1;
            }
          });

          element.si = respsi;
          element.no = respno;
          element.blanco = respblanco;

        });
      });
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ir(item) {
    let dialogRef = this.dialog.open(ModificarEncuestaComponent, {
      width: "60%",
      data: { item: item }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  cambiarEstado(item){

    if(item.estado == 'Abierta'){
      item.estado = 'Finalizada'
    }else{
      item.estado = 'Abierta'
    }

    this._dialogService
      .openConfirm({
        message: `¿Está seguro de cambiar esta encuesta a ${item.estado}`,
        disableClose: false, // defaults to false
        title: "Confirmación", //OPTIONAL, hides if not provided
        cancelButton: "Cancelar", //OPTIONAL, defaults to 'CANCEL'
        acceptButton: "Aceptar", //OPTIONAL, defaults to 'ACCEPT'
        width: "500px" //OPTIONAL, defaults to 400px
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
            this.db.update(`encuestas/${item.id}`,{
              estado: item.estado
            }).then(()=>{
              this._notify.okMessage(`Encuesta ${item.estado}`);
            })
        } else {
          if (item.estado == 'Abierta') {
            item.estado = 'Finalizada'
          } else {
            item.estado = 'Abierta'
          }
        }
      });
  }

  eliminar(item) {
    this._dialogService
      .openConfirm(configDel)
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.db
            .delete(`encuestas/${item.id}`)
            .then(res => {
              this._notify.okMessage("Encuesta eliminada");
            })
            .catch(err => {
              this._notify.errorMessage(err);
            });
        } else {
        }
      });
  }

}
