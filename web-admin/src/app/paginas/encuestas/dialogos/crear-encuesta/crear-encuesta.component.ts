import { Component, OnInit, Inject } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { UploadService, Upload } from "../../../../core/upload.service";
import { TdLoadingService } from "@covalent/core/loading";

@Component({
  selector: "app-crear-encuesta",
  templateUrl: "./crear-encuesta.component.html",
  styleUrls: ["./crear-encuesta.component.scss"]
})
export class CrearEncuestaComponent implements OnInit {
  info: any = {};
  files: any = {};

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public upload: UploadService,
    public dialogRef: MatDialogRef<CrearEncuestaComponent>,
    private _loadingService: TdLoadingService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _upload: UploadService
  ) {}

  ngOnInit() {}

  guardar() {

    this._loadingService.register();
    this.info.si = 0;
    this.info.no = 0;

    this.db
      .add("encuestas", this.info)
      .then(() => {
        this.notify.update("Encuesta guardada", "success");
        this._loadingService.resolve();
        this.cerrar();
      })
      .catch(err => {
        this._loadingService.resolve();
        this.notify.update("Error creando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }
}
