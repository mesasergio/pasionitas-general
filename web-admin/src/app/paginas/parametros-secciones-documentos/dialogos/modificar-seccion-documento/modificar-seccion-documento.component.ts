import { Component, OnInit, Inject } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";

@Component({
  selector: "app-modificar-seccion-documento",
  templateUrl: "./modificar-seccion-documento.component.html",
  styleUrls: ["./modificar-seccion-documento.component.scss"]
})
export class ModificarSeccionDocumentoComponent implements OnInit {
  info: any = {};

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ModificarSeccionDocumentoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.info = data.item;
  }

  ngOnInit() {}

  guardar() {
    this.db
      .update(`secciones-documentos/${this.info.id}`, this.info)
      .then(() => {
        this.snackBar.open("Sección modificada", "Cerrar", {
          duration: 2000,
          horizontalPosition: "left"
        });
        this.cerrar();
      })
      .catch(err => {
        this.notify.update("Error modificando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }
}
