import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';

import { Router } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TdDialogService, IConfirmConfig } from '@covalent/core/dialogs';
import { ResourcesService } from '../../core/resources.service';
import { NotifyService } from '../../core/notify.service';
import { ModificarSeccionDocumentoComponent } from './dialogos/modificar-seccion-documento/modificar-seccion-documento.component';


// Configuración de confirm eliminar
const configDel: IConfirmConfig = {
  message: '¿Está seguro de eliminar el registro?',
  disableClose: false, // defaults to false
  title: 'Confirmación', //OPTIONAL, hides if not provided
  cancelButton: 'Cancelar', //OPTIONAL, defaults to 'CANCEL'
  acceptButton: 'Aceptar', //OPTIONAL, defaults to 'ACCEPT'
  width: '500px', //OPTIONAL, defaults to 400px
}
@Component({
  selector: 'app-parametros-secciones-documentos',
  templateUrl: './parametros-secciones-documentos.component.html',
  styleUrls: ['./parametros-secciones-documentos.component.scss']
})
export class ParametrosSeccionesDocumentosComponent implements OnInit {

@ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  displayedColumns = [
    "seccion",
    "idcarpeta"
  ];

  lista: any[] = [];

  constructor(
    public db: ResourcesService,
    private _loadingService: TdLoadingService,
    private _dialogService: TdDialogService,
    private _notify: NotifyService,
    private _router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.cargarLista();
  }

  cargarLista() {
    this.db.colWithIds$("secciones-documentos").subscribe((resp: any[]) => {
      this.lista = resp;
      this.lista.forEach((element,index) => {
        if(!element.seccion){
          this.lista.splice(index,1);
        }
      });
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ir(item) {
    let dialogRef = this.dialog.open(ModificarSeccionDocumentoComponent, {
      width: "60%",
      data: { item: item }
    });

    dialogRef.afterClosed().subscribe(result => {});
  }

}
