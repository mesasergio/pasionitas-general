import { Component, OnInit, Inject } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";
import { TdLoadingService } from '@covalent/core/loading';
import { Upload, UploadService } from '../../../../core/upload.service';

@Component({
  selector: "app-modificar",
  templateUrl: "./modificar.component.html",
  styleUrls: ["./modificar.component.scss"]
})
export class ModificarTareaDialogoComponent implements OnInit {
  info: any = {};
  file: any;
  fileingles: any;
  fileitaliano: any;

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ModificarTareaDialogoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _loadingService: TdLoadingService,
    public _uploadService: UploadService
  ) {
    this.info = data.item;
  }

  ngOnInit() {}

  guardar() {
    this.db
      .update(`tareas/${this.info.id}`, this.info)
      .then(() => {
        this.snackBar.open("Tarea modificado", "Cerrar", {
          duration: 2000,
          horizontalPosition: "left"
        });
        this.cerrar();
      })
      .catch(err => {
        this.notify.update("Error modificando registro", "error");
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  fileSelectMsg: string = "No file selected yet.";
  fileUploadMsg: string = "No file uploaded yet.";
  disabled: boolean = false;

  uploadEvent(file: File): void {
    this.fileUploadMsg = file.name;
    this.file = file;

    this._loadingService.register();

    let archivo: Upload = new Upload(this.file);

    this._uploadService.uploadDocumento(archivo).then(url => {
      this.info.url = url;
      this._loadingService.resolve();
    });
  }

  uploadEventIngles(file: File): void {
    this.fileUploadMsg = file.name;
    this.fileingles = file;

    this._loadingService.register();

    let archivo: Upload = new Upload(this.fileingles);

    this._uploadService.uploadDocumento(archivo).then(url => {
      this.info.urlingles = url;
      this._loadingService.resolve();
    });
  }

  uploadEventItaliano(file: File): void {
    this.fileUploadMsg = file.name;
    this.fileitaliano = file;

    this._loadingService.register();

    let archivo: Upload = new Upload(this.fileitaliano);

    this._uploadService.uploadDocumento(archivo).then(url => {
      this.info.urlitaliano = url;
      this._loadingService.resolve();
    });
  }

  eliminarFile(){
    this.info.url = "";
  }

  eliminarFileIngles() {
    this.info.urlingles = "";
  }

  eliminarFileItaliano() {
    this.info.urlitaliano = "";
  }

  cancelEvent(): void {
    this.fileSelectMsg = "No file selected yet.";
    this.fileUploadMsg = "No file uploaded yet.";
    this.info.file = null;
  }
}
