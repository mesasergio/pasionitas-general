import { Component, OnInit } from "@angular/core";
import { ResourcesService } from "../../../../core/resources.service";
import { NotifyService } from "../../../../core/notify.service";
import { MatDialogRef } from "@angular/material";
import { UploadService, Upload } from '../../../../core/upload.service';
import { TdLoadingService } from "@covalent/core/loading";

@Component({
  selector: "app-crear",
  templateUrl: "./crear.component.html",
  styleUrls: ["./crear.component.scss"]
})
export class CrearTareaDialogoComponent implements OnInit {
  info: any = {};

  file:any;
  fileingles:any;
  fileitaliano:any;

  constructor(
    public db: ResourcesService,
    public notify: NotifyService,
    public dialogRef: MatDialogRef<CrearTareaDialogoComponent>,
    public _uploadService: UploadService,
    private _loadingService: TdLoadingService

  ) {}

  ngOnInit() {}

  guardar() {

    // Consulto en que index esta la ultima tarea
    let query = this.db
      .colWithIds$("tareas", ref =>
        ref.where("estado", "==", this.info.estado).orderBy("index", "asc")
      )
      .subscribe((resp: any[]) => {
        query.unsubscribe();

        let index: number = 0;

        if (resp.length > 0) {
          let ultimo = resp[resp.length - 1];

          if (ultimo.index) {
            index = ultimo.index + 1;
          } else {
            index = 1;
          }
        } else {
          index = 1;
        }

        this.info.index = index;

        this.db
          .add("tareas", this.info)
          .then(() => {
            this.notify.update("Tarea creada exitosamente", "success");
            this.cerrar();
          })
          .catch(err => {
            this.notify.update("Error creando registro", "error");
          });
      });
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  fileSelectMsg: string = "No file selected yet.";
  fileUploadMsg: string = "No file uploaded yet.";
  disabled: boolean = false;


  uploadEvent(file: File): void {

    this.fileUploadMsg = file.name;
    this.file= file;

    this._loadingService.register();

    let archivo: Upload = new Upload(this.file);

    this._uploadService
      .uploadDocumento(archivo)
      .then(url => {
        this.info.url = url
        this._loadingService.resolve();
      })

  }

  uploadEventIngles(file: File): void {

    this.fileUploadMsg = file.name;
    this.fileingles = file;

    this._loadingService.register();

    let archivo: Upload = new Upload(this.fileingles);

    this._uploadService
      .uploadDocumento(archivo)
      .then(url => {
        this.info.urlingles = url
        this._loadingService.resolve();
      })

  }

  uploadEventItaliano(file: File): void {
    this.fileUploadMsg = file.name;
    this.fileitaliano = file;

    this._loadingService.register();

    let archivo: Upload = new Upload(this.fileitaliano);

    this._uploadService
      .uploadDocumento(archivo)
      .then(url => {
        this.info.urlitaliano = url
        this._loadingService.resolve();
      })

  }

  cancelEvent(): void {
    this.fileSelectMsg = "No file selected yet.";
    this.fileUploadMsg = "No file uploaded yet.";
    this.info.file = null;
  }
}
