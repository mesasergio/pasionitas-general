import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../core/resources.service';
import { TdDialogService, IConfirmConfig } from "@covalent/core/dialogs";
import { CrearTareaDialogoComponent } from './dialogo/crear/crear.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ModificarTareaDialogoComponent } from './dialogo/modificar/modificar.component';
import Swal from "sweetalert2";

@Component({
  selector: "app-tareas",
  templateUrl: "./tareas.component.html",
  styleUrls: ["./tareas.component.scss"]
})
export class TareasComponent implements OnInit {
  tareasPendientes: any[] = [];
  tareasEnProceso: any[] = [];
  tareasEnRevision: any[] = [];
  tareasTerminadas: any[] = [];


  constructor(
    public db: ResourcesService,
    private _dialogService: TdDialogService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  orderedProduct($event: any, i) {
    console.log('Evento', $event);
    console.log('i', i)
  }

  ngOnInit() {
    this.db
      .colWithIds$(`tareas`, ref =>
        ref.where("estado", "==", "pendiente").orderBy("index", "asc")
      )
      .subscribe((resp: any[]) => {
        this.tareasPendientes = resp;
      });

    this.db
      .colWithIds$(`tareas`, ref =>
        ref.where("estado", "==", "proceso").orderBy("index", "asc")
      )
      .subscribe((resp: any[]) => {
        this.tareasEnProceso = resp;
      });

    this.db
      .colWithIds$(`tareas`, ref =>
        ref.where("estado", "==", "revision").orderBy("index", "asc")
      )
      .subscribe((resp: any[]) => {
        this.tareasEnRevision = resp;
      });

    this.db
      .colWithIds$(`tareas`, ref =>
        ref.where("estado", "==", "terminada").orderBy("index", "asc")
      )
      .subscribe((resp: any[]) => {
        this.tareasTerminadas = resp;
      });
  }

  onItemPendiente(e: any) {
    e.dragData.index = this.tareasPendientes.length + 1;
    this.db.update(`tareas/${e.dragData.id}`, {
      estado: "pendiente",
      index: e.dragData.index
    });
  }

  onItemProceso(e: any) {
    e.dragData.index = this.tareasEnProceso.length + 1;
    this.db.update(`tareas/${e.dragData.id}`, {
      estado: "proceso",
      index: e.dragData.index
    });
  }

  onItemRevision(e: any) {
    e.dragData.index = this.tareasEnRevision.length + 1;
    this.db.update(`tareas/${e.dragData.id}`, {
      estado: "revision",
      index: e.dragData.index
    });
  }

  onItemTerminado(e: any) {
    e.dragData.index = this.tareasTerminadas.length + 1;
    this.db.update(`tareas/${e.dragData.id}`, {
      estado: "terminada",
      index: e.dragData.index
    });
  }

  crear() {
    let dialogRef = this.dialog.open(CrearTareaDialogoComponent, {
      width: "60%"
    });
  }

  ir(item) {
    let dialogRef = this.dialog.open(ModificarTareaDialogoComponent, {
      width: "60%",
      data: { item: item }
    });

    dialogRef.afterClosed().subscribe(result => {});
  }

  eliminar(item) {
    Swal({
      title: "Confirmación de borrado",
      text: "¿Está seguro de borrar esta tarea?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar"
    }).then(result => {
      if (result.value) {
        this.db.delete(`tareas/${item.id}`).then(() => {
          this.snackBar.open("Tarea eliminada", "Cerrar", {
            duration: 2000,
            horizontalPosition: "left"
          });
        });
      }
    });
  }

  upIndex(lista:any[],index){

    if(index != 0){

      let anterior = lista[index-1];
      let actual = lista[index];

      anterior.index = actual.index;
      actual.index -=1;

      // Actualizo
      this.db.update(`tareas/${anterior.id}`,{
        index: anterior.index
      })

      this.db.update(`tareas/${actual.id}`,{
        index: actual.index
      })

    }


  }

  downIndex(lista:any[],index){

    if(index != lista.length - 1){
      let siguiente = lista[index + 1];
      let actual = lista[index];

      siguiente.index = actual.index;
      actual.index += 1;

      // Actualizo
      this.db.update(`tareas/${siguiente.id}`, {
        index: siguiente.index
      })

      this.db.update(`tareas/${actual.id}`, {
        index: actual.index
      })
    }

  }
}
