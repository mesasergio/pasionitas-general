import { Component } from "@angular/core";
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState
} from "@angular/cdk/layout";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { AuthService } from "../core/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "barra-navegacion",
  templateUrl: "./barra-navegacion.component.html",
  styleUrls: ["./barra-navegacion.component.css"]
})
export class BarraNavegacionComponent {
  info: any = {};
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  constructor(
    private breakpointObserver: BreakpointObserver,
    public _auth: AuthService,
    public _route: Router
  ) {
    this._auth.getCurrentUser().subscribe((user: any[]) => {
      if (user.length > 0) {
        this.info = user[0];
      } else {
        this.info = {};
        this._route.navigate(["login"]);
      }
    });
  }

  close() {
    this._auth.signOut();
    this._route.navigate(["login"]);
  }
}
