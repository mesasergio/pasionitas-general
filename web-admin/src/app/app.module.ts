import { HttpClientModule } from '@angular/common/http';import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TimepickerModule } from "ngx-bootstrap";

// Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { MatTooltipModule } from '@angular/material/tooltip';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AutenticacionComponent } from './paginas/autenticacion/autenticacion.component';
import { AsistentesComponent } from './paginas/asistentes/asistentes.component';
import { BarraNavegacionComponent } from './barra-navegacion/barra-navegacion.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatCardModule, MatDialogModule, MatFormFieldModule, MatSelectModule, MatInputModule, MatSnackBarModule, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule } from '@angular/material';
import { CrearAsistenteComponent } from "./paginas/asistentes/dialogo/crear/crear.component";
import { FormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';
import { CovalentLoadingModule } from "@covalent/core/loading";
import { CovalentDialogsModule } from "@covalent/core/dialogs";
import { ResourcesService } from './core/resources.service';
import { NotifyService } from './core/notify.service';
import { AuthService } from './core/auth.service';
import { UploadService } from './core/upload.service';
import { DocumentosComponent } from './paginas/documentos/documentos.component';
import { CrearDocumentoComponent } from "./paginas/documentos/dialogo/crear/crear.component";
import { CovalentFileModule } from "@covalent/core/file";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CdkTreeModule } from '@angular/cdk/tree';
import { CarpetasComponent } from './paginas/documentos/carpetas/carpetas.component';
import { CrearCarpetaDialogoComponent } from './paginas/documentos/carpetas/dialogo/dialogo.component';
import { ModificarAsistenteComponent } from './paginas/asistentes/dialogo/modificar/modificar.component';
import { TareasComponent } from './paginas/tareas/tareas.component';
import { NgDragDropModule } from 'ng-drag-drop';
import { CrearTareaDialogoComponent } from './paginas/tareas/dialogo/crear/crear.component';
import { ParametrosCategoriasComponent } from './paginas/parametros-categorias/parametros-categorias.component';
import { CrearCategoriaDialogoComponent } from "./paginas/parametros-categorias/dialogo/crear/crear.component";
import { ModificarCategoriaDialogoComponent } from "./paginas/parametros-categorias/dialogo/modificar/modificar.component";
import { ModificarTareaDialogoComponent } from "./paginas/tareas/dialogo/modificar/modificar.component";
import { MatTabsModule } from '@angular/material/tabs';


import { DndModule } from 'ng2-dnd';
import { CalendarioComponent } from './paginas/calendario/calendario.component';
import { CrearCalendarioComponent } from './paginas/calendario/dialogo/crear/crear.component';
import { ModificarCalendarioComponent } from './paginas/calendario/dialogo/modificar/modificar.component';
import { TablaCalendarioComponent } from './paginas/calendario/tablas/tabla-calendario/tabla-calendario.component';
import { TablaAsistentesComponent } from './paginas/asistentes/tabla-asistentes/tabla-asistentes.component';
import { TablaDocumentosComponent } from './paginas/documentos/tabla-documentos/tabla-documentos.component';
import { ParametrosSeccionesDocumentosComponent } from './paginas/parametros-secciones-documentos/parametros-secciones-documentos.component';
import { ModificarSeccionDocumentoComponent } from './paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component';
import { ModificarCarpetaComponent } from './paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component';
import { ModificarDocumentoComponent } from './paginas/documentos/dialogo/modificar-documento/modificar-documento.component';
import { ProyectosComponent } from './paginas/proyectos/proyectos.component';
import { CrearProyectoComponent } from './paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component';
import { ModificarProyectoComponent } from './paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component';
import { TablaProyectosComponent } from './paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component';
import { FroalaEditorModule, FroalaViewModule } from "angular-froala-wysiwyg";
import { CronicasComponent } from './paginas/cronicas/cronicas.component';
import { CrearCronicaComponent } from './paginas/cronicas/dialogos/crear-cronica/crear-cronica.component';
import { ModificarCronicaComponent } from './paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component';
import { TablaCronicasComponent } from './paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component';
import { EncuestasComponent } from './paginas/encuestas/encuestas.component';
import { CrearEncuestaComponent } from './paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component';
import { ModificarEncuestaComponent } from './paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component';
import { TablaEncuestasComponent } from './paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component';
import { ForosComponent } from './paginas/foros/foros.component';
import { TablaForosComponent } from './paginas/foros/tablas/tabla-foros/tabla-foros.component';
import { CrearForoComponent } from './paginas/foros/dialogos/crear-foro/crear-foro.component';
import { ModificarForoComponent } from './paginas/foros/dialogos/modificar-foro/modificar-foro.component';
import { MensajePapaComponent } from '../paginas/mensaje-papa/mensaje-papa.component';
import { IngresoComponent } from './paginas/ingreso/ingreso.component';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { UsuariosComponent } from './paginas/usuarios/usuarios.component';
import { TablaUsuariosComponent } from './paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component';
import { CrearUsuarioComponent } from './paginas/usuarios/dialogo/crear/crear.component';
import { ModificarUsuarioComponent } from './paginas/usuarios/dialogo/modificar/modificar.component';
import { InicioComponent } from './paginas/inicio/inicio.component';
import { GruposComponent } from './paginas/grupos/grupos.component';
import { TablaGruposComponent } from './paginas/grupos/tablas/tabla-grupos/tabla-grupos.component';
import { CrearGrupoComponent } from './paginas/grupos/dialogos/crear-grupo/crear-grupo.component';
import { ModificarGrupoComponent } from './paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component';
import { AgregarAsistenteComponent } from './paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component';



export const firebaseConfig = {
  apiKey: "AIzaSyAaLH5P2QXTeSNN2S8wLkYEftf_3qj9UTw",
  authDomain: "pasionistas-conferencia.firebaseapp.com",
  databaseURL: "https://pasionistas-conferencia.firebaseio.com",
  projectId: "pasionistas-conferencia",
  storageBucket: "pasionistas-conferencia.appspot.com",
  messagingSenderId: "799598907957"
};

@NgModule({
  declarations: [
    AppComponent,
    AutenticacionComponent,
    AsistentesComponent,
    BarraNavegacionComponent,
    TablaAsistentesComponent,
    CrearAsistenteComponent,
    TablaDocumentosComponent,
    DocumentosComponent,
    CrearDocumentoComponent,
    CarpetasComponent,
    CrearCarpetaDialogoComponent,
    ModificarAsistenteComponent,
    TareasComponent,
    CrearTareaDialogoComponent,
    ParametrosCategoriasComponent,
    CrearCategoriaDialogoComponent,
    ModificarCategoriaDialogoComponent,
    ModificarTareaDialogoComponent,
    CalendarioComponent,
    CrearCalendarioComponent,
    ModificarCalendarioComponent,
    TablaCalendarioComponent,
    ParametrosSeccionesDocumentosComponent,
    ModificarSeccionDocumentoComponent,
    ModificarCarpetaComponent,
    ModificarDocumentoComponent,
    ProyectosComponent,
    CrearProyectoComponent,
    ModificarProyectoComponent,
    TablaProyectosComponent,
    CronicasComponent,
    CrearCronicaComponent,
    ModificarCronicaComponent,
    TablaCronicasComponent,
    EncuestasComponent,
    CrearEncuestaComponent,
    ModificarEncuestaComponent,
    TablaEncuestasComponent,
    ForosComponent,
    TablaForosComponent,
    CrearForoComponent,
    ModificarForoComponent,
    MensajePapaComponent,
    IngresoComponent,
    UsuariosComponent,
    TablaUsuariosComponent,
    CrearUsuarioComponent,
    ModificarUsuarioComponent,
    InicioComponent,
    GruposComponent,
    TablaGruposComponent,
    CrearGrupoComponent,
    ModificarGrupoComponent,
    AgregarAsistenteComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ServiceWorkerModule.register("/ngsw-worker.js", {
      enabled: environment.production
    }),
    TimepickerModule.forRoot(),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    NgxMaterialTimepickerModule.forRoot(),
    DndModule.forRoot(),
    NgDragDropModule.forRoot(),
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    AngularFireAuthModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatSelectModule,
    MatSnackBarModule,
    FormsModule,
    CovalentLoadingModule,
    CovalentDialogsModule,
    CovalentFileModule,
    FlexLayoutModule,
    CdkTreeModule,
    MatTooltipModule,
    MatTabsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule
  ],
  entryComponents: [
    CrearAsistenteComponent,
    CrearDocumentoComponent,
    CrearCarpetaDialogoComponent,
    ModificarAsistenteComponent,
    CrearTareaDialogoComponent,
    CrearCategoriaDialogoComponent,
    ModificarCategoriaDialogoComponent,
    ModificarTareaDialogoComponent,
    CrearCalendarioComponent,
    ModificarCalendarioComponent,
    ModificarSeccionDocumentoComponent,
    ModificarCarpetaComponent,
    ModificarDocumentoComponent,
    CrearProyectoComponent,
    ModificarProyectoComponent,
    CrearCronicaComponent,
    ModificarCronicaComponent,
    CrearEncuestaComponent,
    ModificarEncuestaComponent,
    CrearForoComponent,
    ModificarForoComponent,
    CrearUsuarioComponent,
    ModificarUsuarioComponent,
    CrearGrupoComponent,
    ModificarGrupoComponent,
    AgregarAsistenteComponent
  ],
  providers: [ResourcesService, NotifyService, AuthService, UploadService],
  bootstrap: [AppComponent]
})
export class AppModule {}
