import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../app/core/resources.service';
import { NotifyService } from '../../app/core/notify.service';
import { TdLoadingService } from '@covalent/core/loading';
import { MatSnackBar } from '@angular/material';
import { UploadService, Upload } from '../../app/core/upload.service';

@Component({
  selector: "app-mensaje-papa",
  templateUrl: "./mensaje-papa.component.html",
  styleUrls: ["./mensaje-papa.component.scss"]
})
export class MensajePapaComponent implements OnInit {
  info: any = {};
  file: any;
  fileingles: any;
  fileitaliano: any;

  fileSelectMsg: string = "No file selected yet.";
  fileUploadMsg: string = "No file uploaded yet.";
  disabled: boolean = false;

  constructor(
    public db: ResourcesService,
    public snackBar: MatSnackBar,
    private _loadingService: TdLoadingService,
    public _uploadService: UploadService
  ) {
    this.db
      .docWithRefs$("secciones-documentos/mensaje-papa")
      .subscribe((resp: any) => {
        this.info = resp;
      });
  }

  ngOnInit() {}

  guardar() {
    this.db
      .set("secciones-documentos/mensaje-papa", this.info)
      .then(() => {
        this.snackBar.open("Datos guardados", "Cerrar", {
          duration: 2000
        });
        this._loadingService.resolve();
      })
      .catch(err => {
        this._loadingService.resolve();
        this.snackBar.open("Error guardando cambios", "Cerrar", {
          duration: 2000
        });
      });
  }

  uploadEvent(file: File): void {
    this.fileUploadMsg = file.name;
    this.file = file;

    this._loadingService.register();

    let archivo: Upload = new Upload(this.file);

    this._uploadService.uploadDocumento(archivo).then(url => {
      this.info.url = url;
      this._loadingService.resolve();
    });
  }

  uploadEventIngles(file: File): void {
    this.fileUploadMsg = file.name;
    this.fileingles = file;

    this._loadingService.register();

    let archivo: Upload = new Upload(this.fileingles);

    this._uploadService.uploadDocumento(archivo).then(url => {
      this.info.urlingles = url;
      this._loadingService.resolve();
    });
  }

  uploadEventItaliano(file: File): void {
    this.fileUploadMsg = file.name;
    this.fileitaliano = file;

    this._loadingService.register();

    let archivo: Upload = new Upload(this.fileitaliano);

    this._uploadService.uploadDocumento(archivo).then(url => {
      this.info.urlitaliano = url;
      this._loadingService.resolve();
    });
  }

  eliminarFile() {
    this.info.url = "";
  }

  eliminarFileIngles() {
    this.info.urlingles = "";
  }

  eliminarFileItaliano() {
    this.info.urlitaliano = "";
  }

  cancelEvent(): void {
    this.fileSelectMsg = "No file selected yet.";
    this.fileUploadMsg = "No file uploaded yet.";
    this.info.file = null;
  }
}
