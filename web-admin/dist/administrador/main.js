(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _paginas_asistentes_asistentes_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./paginas/asistentes/asistentes.component */ "./src/app/paginas/asistentes/asistentes.component.ts");
/* harmony import */ var _paginas_documentos_documentos_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./paginas/documentos/documentos.component */ "./src/app/paginas/documentos/documentos.component.ts");
/* harmony import */ var _paginas_documentos_carpetas_carpetas_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./paginas/documentos/carpetas/carpetas.component */ "./src/app/paginas/documentos/carpetas/carpetas.component.ts");
/* harmony import */ var _paginas_tareas_tareas_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./paginas/tareas/tareas.component */ "./src/app/paginas/tareas/tareas.component.ts");
/* harmony import */ var _paginas_parametros_categorias_parametros_categorias_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./paginas/parametros-categorias/parametros-categorias.component */ "./src/app/paginas/parametros-categorias/parametros-categorias.component.ts");
/* harmony import */ var _paginas_calendario_calendario_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./paginas/calendario/calendario.component */ "./src/app/paginas/calendario/calendario.component.ts");
/* harmony import */ var _paginas_parametros_secciones_documentos_parametros_secciones_documentos_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./paginas/parametros-secciones-documentos/parametros-secciones-documentos.component */ "./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.ts");
/* harmony import */ var _paginas_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./paginas/proyectos/proyectos.component */ "./src/app/paginas/proyectos/proyectos.component.ts");
/* harmony import */ var _paginas_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./paginas/cronicas/cronicas.component */ "./src/app/paginas/cronicas/cronicas.component.ts");
/* harmony import */ var _paginas_encuestas_encuestas_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./paginas/encuestas/encuestas.component */ "./src/app/paginas/encuestas/encuestas.component.ts");
/* harmony import */ var _paginas_foros_foros_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./paginas/foros/foros.component */ "./src/app/paginas/foros/foros.component.ts");
/* harmony import */ var _paginas_mensaje_papa_mensaje_papa_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../paginas/mensaje-papa/mensaje-papa.component */ "./src/paginas/mensaje-papa/mensaje-papa.component.ts");
/* harmony import */ var _paginas_ingreso_ingreso_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./paginas/ingreso/ingreso.component */ "./src/app/paginas/ingreso/ingreso.component.ts");
/* harmony import */ var _paginas_usuarios_usuarios_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./paginas/usuarios/usuarios.component */ "./src/app/paginas/usuarios/usuarios.component.ts");
/* harmony import */ var _paginas_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./paginas/inicio/inicio.component */ "./src/app/paginas/inicio/inicio.component.ts");
/* harmony import */ var _paginas_grupos_grupos_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./paginas/grupos/grupos.component */ "./src/app/paginas/grupos/grupos.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var options = {
    useHash: true
};
var routes = [
    { path: "login", component: _paginas_ingreso_ingreso_component__WEBPACK_IMPORTED_MODULE_14__["IngresoComponent"] },
    { path: "inicio", component: _paginas_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_16__["InicioComponent"] },
    { path: "asistentes", component: _paginas_asistentes_asistentes_component__WEBPACK_IMPORTED_MODULE_2__["AsistentesComponent"] },
    { path: "documentos/:padre", component: _paginas_documentos_documentos_component__WEBPACK_IMPORTED_MODULE_3__["DocumentosComponent"] },
    { path: "carpetas/:padre", component: _paginas_documentos_carpetas_carpetas_component__WEBPACK_IMPORTED_MODULE_4__["CarpetasComponent"] },
    { path: "tareas", component: _paginas_tareas_tareas_component__WEBPACK_IMPORTED_MODULE_5__["TareasComponent"] },
    { path: "calendario", component: _paginas_calendario_calendario_component__WEBPACK_IMPORTED_MODULE_7__["CalendarioComponent"] },
    { path: "proyectos", component: _paginas_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_9__["ProyectosComponent"] },
    { path: "cronicas", component: _paginas_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_10__["CronicasComponent"] },
    { path: "administradores", component: _paginas_usuarios_usuarios_component__WEBPACK_IMPORTED_MODULE_15__["UsuariosComponent"] },
    { path: "grupos", component: _paginas_grupos_grupos_component__WEBPACK_IMPORTED_MODULE_17__["GruposComponent"] },
    { path: "parametros/categorias", component: _paginas_parametros_categorias_parametros_categorias_component__WEBPACK_IMPORTED_MODULE_6__["ParametrosCategoriasComponent"] },
    {
        path: "parametros/secciones-documentos",
        component: _paginas_parametros_secciones_documentos_parametros_secciones_documentos_component__WEBPACK_IMPORTED_MODULE_8__["ParametrosSeccionesDocumentosComponent"]
    },
    { path: "parametros/mensaje-papa", component: _paginas_mensaje_papa_mensaje_papa_component__WEBPACK_IMPORTED_MODULE_13__["MensajePapaComponent"] },
    { path: "encuestas", component: _paginas_encuestas_encuestas_component__WEBPACK_IMPORTED_MODULE_11__["EncuestasComponent"] },
    { path: "foros", component: _paginas_foros_foros_component__WEBPACK_IMPORTED_MODULE_12__["ForosComponent"] },
    { path: "**", pathMatch: "full", redirectTo: "login" }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, options)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: firebaseConfig, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "firebaseConfig", function() { return firebaseConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_material_timepicker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-material-timepicker */ "./node_modules/ngx-material-timepicker/esm5/ngx-material-timepicker.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angularfire2 */ "./node_modules/angularfire2/index.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_service_worker__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/service-worker */ "./node_modules/@angular/service-worker/fesm5/service-worker.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _paginas_autenticacion_autenticacion_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./paginas/autenticacion/autenticacion.component */ "./src/app/paginas/autenticacion/autenticacion.component.ts");
/* harmony import */ var _paginas_asistentes_asistentes_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./paginas/asistentes/asistentes.component */ "./src/app/paginas/asistentes/asistentes.component.ts");
/* harmony import */ var _barra_navegacion_barra_navegacion_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./barra-navegacion/barra-navegacion.component */ "./src/app/barra-navegacion/barra-navegacion.component.ts");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _paginas_asistentes_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./paginas/asistentes/dialogo/crear/crear.component */ "./src/app/paginas/asistentes/dialogo/crear/crear.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _core_auth_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./core/auth.service */ "./src/app/core/auth.service.ts");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _paginas_documentos_documentos_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./paginas/documentos/documentos.component */ "./src/app/paginas/documentos/documentos.component.ts");
/* harmony import */ var _paginas_documentos_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./paginas/documentos/dialogo/crear/crear.component */ "./src/app/paginas/documentos/dialogo/crear/crear.component.ts");
/* harmony import */ var _covalent_core_file__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @covalent/core/file */ "./node_modules/@covalent/core/esm5/covalent-core-file.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/esm5/tree.es5.js");
/* harmony import */ var _paginas_documentos_carpetas_carpetas_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./paginas/documentos/carpetas/carpetas.component */ "./src/app/paginas/documentos/carpetas/carpetas.component.ts");
/* harmony import */ var _paginas_documentos_carpetas_dialogo_dialogo_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./paginas/documentos/carpetas/dialogo/dialogo.component */ "./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.ts");
/* harmony import */ var _paginas_asistentes_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./paginas/asistentes/dialogo/modificar/modificar.component */ "./src/app/paginas/asistentes/dialogo/modificar/modificar.component.ts");
/* harmony import */ var _paginas_tareas_tareas_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./paginas/tareas/tareas.component */ "./src/app/paginas/tareas/tareas.component.ts");
/* harmony import */ var ng_drag_drop__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ng-drag-drop */ "./node_modules/ng-drag-drop/index.js");
/* harmony import */ var ng_drag_drop__WEBPACK_IMPORTED_MODULE_36___default = /*#__PURE__*/__webpack_require__.n(ng_drag_drop__WEBPACK_IMPORTED_MODULE_36__);
/* harmony import */ var _paginas_tareas_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./paginas/tareas/dialogo/crear/crear.component */ "./src/app/paginas/tareas/dialogo/crear/crear.component.ts");
/* harmony import */ var _paginas_parametros_categorias_parametros_categorias_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./paginas/parametros-categorias/parametros-categorias.component */ "./src/app/paginas/parametros-categorias/parametros-categorias.component.ts");
/* harmony import */ var _paginas_parametros_categorias_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./paginas/parametros-categorias/dialogo/crear/crear.component */ "./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.ts");
/* harmony import */ var _paginas_parametros_categorias_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./paginas/parametros-categorias/dialogo/modificar/modificar.component */ "./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.ts");
/* harmony import */ var _paginas_tareas_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./paginas/tareas/dialogo/modificar/modificar.component */ "./src/app/paginas/tareas/dialogo/modificar/modificar.component.ts");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var ng2_dnd__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ng2-dnd */ "./node_modules/ng2-dnd/ng2-dnd.es5.js");
/* harmony import */ var _paginas_calendario_calendario_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./paginas/calendario/calendario.component */ "./src/app/paginas/calendario/calendario.component.ts");
/* harmony import */ var _paginas_calendario_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./paginas/calendario/dialogo/crear/crear.component */ "./src/app/paginas/calendario/dialogo/crear/crear.component.ts");
/* harmony import */ var _paginas_calendario_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./paginas/calendario/dialogo/modificar/modificar.component */ "./src/app/paginas/calendario/dialogo/modificar/modificar.component.ts");
/* harmony import */ var _paginas_calendario_tablas_tabla_calendario_tabla_calendario_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./paginas/calendario/tablas/tabla-calendario/tabla-calendario.component */ "./src/app/paginas/calendario/tablas/tabla-calendario/tabla-calendario.component.ts");
/* harmony import */ var _paginas_asistentes_tabla_asistentes_tabla_asistentes_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./paginas/asistentes/tabla-asistentes/tabla-asistentes.component */ "./src/app/paginas/asistentes/tabla-asistentes/tabla-asistentes.component.ts");
/* harmony import */ var _paginas_documentos_tabla_documentos_tabla_documentos_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./paginas/documentos/tabla-documentos/tabla-documentos.component */ "./src/app/paginas/documentos/tabla-documentos/tabla-documentos.component.ts");
/* harmony import */ var _paginas_parametros_secciones_documentos_parametros_secciones_documentos_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./paginas/parametros-secciones-documentos/parametros-secciones-documentos.component */ "./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.ts");
/* harmony import */ var _paginas_parametros_secciones_documentos_dialogos_modificar_seccion_documento_modificar_seccion_documento_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component */ "./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.ts");
/* harmony import */ var _paginas_documentos_carpetas_dialogo_modificar_carpeta_modificar_carpeta_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component */ "./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.ts");
/* harmony import */ var _paginas_documentos_dialogo_modificar_documento_modificar_documento_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./paginas/documentos/dialogo/modificar-documento/modificar-documento.component */ "./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.ts");
/* harmony import */ var _paginas_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./paginas/proyectos/proyectos.component */ "./src/app/paginas/proyectos/proyectos.component.ts");
/* harmony import */ var _paginas_proyectos_dialogos_crear_proyecto_crear_proyecto_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component */ "./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.ts");
/* harmony import */ var _paginas_proyectos_dialogos_modificar_proyecto_modificar_proyecto_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component */ "./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.ts");
/* harmony import */ var _paginas_proyectos_tablas_tabla_proyectos_tabla_proyectos_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component */ "./src/app/paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component.ts");
/* harmony import */ var angular_froala_wysiwyg__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! angular-froala-wysiwyg */ "./node_modules/angular-froala-wysiwyg/index.js");
/* harmony import */ var _paginas_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./paginas/cronicas/cronicas.component */ "./src/app/paginas/cronicas/cronicas.component.ts");
/* harmony import */ var _paginas_cronicas_dialogos_crear_cronica_crear_cronica_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./paginas/cronicas/dialogos/crear-cronica/crear-cronica.component */ "./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.ts");
/* harmony import */ var _paginas_cronicas_dialogos_modificar_cronica_modificar_cronica_component__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component */ "./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.ts");
/* harmony import */ var _paginas_cronicas_tablas_tabla_cronicas_tabla_cronicas_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component */ "./src/app/paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component.ts");
/* harmony import */ var _paginas_encuestas_encuestas_component__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./paginas/encuestas/encuestas.component */ "./src/app/paginas/encuestas/encuestas.component.ts");
/* harmony import */ var _paginas_encuestas_dialogos_crear_encuesta_crear_encuesta_component__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ./paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component */ "./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.ts");
/* harmony import */ var _paginas_encuestas_dialogos_modificar_encuesta_modificar_encuesta_component__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ./paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component */ "./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.ts");
/* harmony import */ var _paginas_encuestas_dialogos_tablas_tabla_encuestas_tabla_encuestas_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component */ "./src/app/paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component.ts");
/* harmony import */ var _paginas_foros_foros_component__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./paginas/foros/foros.component */ "./src/app/paginas/foros/foros.component.ts");
/* harmony import */ var _paginas_foros_tablas_tabla_foros_tabla_foros_component__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./paginas/foros/tablas/tabla-foros/tabla-foros.component */ "./src/app/paginas/foros/tablas/tabla-foros/tabla-foros.component.ts");
/* harmony import */ var _paginas_foros_dialogos_crear_foro_crear_foro_component__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./paginas/foros/dialogos/crear-foro/crear-foro.component */ "./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.ts");
/* harmony import */ var _paginas_foros_dialogos_modificar_foro_modificar_foro_component__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ./paginas/foros/dialogos/modificar-foro/modificar-foro.component */ "./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.ts");
/* harmony import */ var _paginas_mensaje_papa_mensaje_papa_component__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ../paginas/mensaje-papa/mensaje-papa.component */ "./src/paginas/mensaje-papa/mensaje-papa.component.ts");
/* harmony import */ var _paginas_ingreso_ingreso_component__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! ./paginas/ingreso/ingreso.component */ "./src/app/paginas/ingreso/ingreso.component.ts");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var _paginas_usuarios_usuarios_component__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! ./paginas/usuarios/usuarios.component */ "./src/app/paginas/usuarios/usuarios.component.ts");
/* harmony import */ var _paginas_usuarios_tablas_tabla_usuarios_tabla_usuarios_component__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ./paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component */ "./src/app/paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component.ts");
/* harmony import */ var _paginas_usuarios_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! ./paginas/usuarios/dialogo/crear/crear.component */ "./src/app/paginas/usuarios/dialogo/crear/crear.component.ts");
/* harmony import */ var _paginas_usuarios_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! ./paginas/usuarios/dialogo/modificar/modificar.component */ "./src/app/paginas/usuarios/dialogo/modificar/modificar.component.ts");
/* harmony import */ var _paginas_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! ./paginas/inicio/inicio.component */ "./src/app/paginas/inicio/inicio.component.ts");
/* harmony import */ var _paginas_grupos_grupos_component__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! ./paginas/grupos/grupos.component */ "./src/app/paginas/grupos/grupos.component.ts");
/* harmony import */ var _paginas_grupos_tablas_tabla_grupos_tabla_grupos_component__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! ./paginas/grupos/tablas/tabla-grupos/tabla-grupos.component */ "./src/app/paginas/grupos/tablas/tabla-grupos/tabla-grupos.component.ts");
/* harmony import */ var _paginas_grupos_dialogos_crear_grupo_crear_grupo_component__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! ./paginas/grupos/dialogos/crear-grupo/crear-grupo.component */ "./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.ts");
/* harmony import */ var _paginas_grupos_dialogos_modificar_grupo_modificar_grupo_component__WEBPACK_IMPORTED_MODULE_82__ = __webpack_require__(/*! ./paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component */ "./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.ts");
/* harmony import */ var _paginas_grupos_dialogos_agregar_asistente_agregar_asistente_component__WEBPACK_IMPORTED_MODULE_83__ = __webpack_require__(/*! ./paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component */ "./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// Firebase















































































var firebaseConfig = {
    apiKey: "AIzaSyAaLH5P2QXTeSNN2S8wLkYEftf_3qj9UTw",
    authDomain: "pasionistas-conferencia.firebaseapp.com",
    databaseURL: "https://pasionistas-conferencia.firebaseio.com",
    projectId: "pasionistas-conferencia",
    storageBucket: "pasionistas-conferencia.appspot.com",
    messagingSenderId: "799598907957"
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"],
                _paginas_autenticacion_autenticacion_component__WEBPACK_IMPORTED_MODULE_14__["AutenticacionComponent"],
                _paginas_asistentes_asistentes_component__WEBPACK_IMPORTED_MODULE_15__["AsistentesComponent"],
                _barra_navegacion_barra_navegacion_component__WEBPACK_IMPORTED_MODULE_16__["BarraNavegacionComponent"],
                _paginas_asistentes_tabla_asistentes_tabla_asistentes_component__WEBPACK_IMPORTED_MODULE_48__["TablaAsistentesComponent"],
                _paginas_asistentes_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_19__["CrearAsistenteComponent"],
                _paginas_documentos_tabla_documentos_tabla_documentos_component__WEBPACK_IMPORTED_MODULE_49__["TablaDocumentosComponent"],
                _paginas_documentos_documentos_component__WEBPACK_IMPORTED_MODULE_27__["DocumentosComponent"],
                _paginas_documentos_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_28__["CrearDocumentoComponent"],
                _paginas_documentos_carpetas_carpetas_component__WEBPACK_IMPORTED_MODULE_32__["CarpetasComponent"],
                _paginas_documentos_carpetas_dialogo_dialogo_component__WEBPACK_IMPORTED_MODULE_33__["CrearCarpetaDialogoComponent"],
                _paginas_asistentes_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_34__["ModificarAsistenteComponent"],
                _paginas_tareas_tareas_component__WEBPACK_IMPORTED_MODULE_35__["TareasComponent"],
                _paginas_tareas_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_37__["CrearTareaDialogoComponent"],
                _paginas_parametros_categorias_parametros_categorias_component__WEBPACK_IMPORTED_MODULE_38__["ParametrosCategoriasComponent"],
                _paginas_parametros_categorias_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_39__["CrearCategoriaDialogoComponent"],
                _paginas_parametros_categorias_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_40__["ModificarCategoriaDialogoComponent"],
                _paginas_tareas_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_41__["ModificarTareaDialogoComponent"],
                _paginas_calendario_calendario_component__WEBPACK_IMPORTED_MODULE_44__["CalendarioComponent"],
                _paginas_calendario_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_45__["CrearCalendarioComponent"],
                _paginas_calendario_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_46__["ModificarCalendarioComponent"],
                _paginas_calendario_tablas_tabla_calendario_tabla_calendario_component__WEBPACK_IMPORTED_MODULE_47__["TablaCalendarioComponent"],
                _paginas_parametros_secciones_documentos_parametros_secciones_documentos_component__WEBPACK_IMPORTED_MODULE_50__["ParametrosSeccionesDocumentosComponent"],
                _paginas_parametros_secciones_documentos_dialogos_modificar_seccion_documento_modificar_seccion_documento_component__WEBPACK_IMPORTED_MODULE_51__["ModificarSeccionDocumentoComponent"],
                _paginas_documentos_carpetas_dialogo_modificar_carpeta_modificar_carpeta_component__WEBPACK_IMPORTED_MODULE_52__["ModificarCarpetaComponent"],
                _paginas_documentos_dialogo_modificar_documento_modificar_documento_component__WEBPACK_IMPORTED_MODULE_53__["ModificarDocumentoComponent"],
                _paginas_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_54__["ProyectosComponent"],
                _paginas_proyectos_dialogos_crear_proyecto_crear_proyecto_component__WEBPACK_IMPORTED_MODULE_55__["CrearProyectoComponent"],
                _paginas_proyectos_dialogos_modificar_proyecto_modificar_proyecto_component__WEBPACK_IMPORTED_MODULE_56__["ModificarProyectoComponent"],
                _paginas_proyectos_tablas_tabla_proyectos_tabla_proyectos_component__WEBPACK_IMPORTED_MODULE_57__["TablaProyectosComponent"],
                _paginas_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_59__["CronicasComponent"],
                _paginas_cronicas_dialogos_crear_cronica_crear_cronica_component__WEBPACK_IMPORTED_MODULE_60__["CrearCronicaComponent"],
                _paginas_cronicas_dialogos_modificar_cronica_modificar_cronica_component__WEBPACK_IMPORTED_MODULE_61__["ModificarCronicaComponent"],
                _paginas_cronicas_tablas_tabla_cronicas_tabla_cronicas_component__WEBPACK_IMPORTED_MODULE_62__["TablaCronicasComponent"],
                _paginas_encuestas_encuestas_component__WEBPACK_IMPORTED_MODULE_63__["EncuestasComponent"],
                _paginas_encuestas_dialogos_crear_encuesta_crear_encuesta_component__WEBPACK_IMPORTED_MODULE_64__["CrearEncuestaComponent"],
                _paginas_encuestas_dialogos_modificar_encuesta_modificar_encuesta_component__WEBPACK_IMPORTED_MODULE_65__["ModificarEncuestaComponent"],
                _paginas_encuestas_dialogos_tablas_tabla_encuestas_tabla_encuestas_component__WEBPACK_IMPORTED_MODULE_66__["TablaEncuestasComponent"],
                _paginas_foros_foros_component__WEBPACK_IMPORTED_MODULE_67__["ForosComponent"],
                _paginas_foros_tablas_tabla_foros_tabla_foros_component__WEBPACK_IMPORTED_MODULE_68__["TablaForosComponent"],
                _paginas_foros_dialogos_crear_foro_crear_foro_component__WEBPACK_IMPORTED_MODULE_69__["CrearForoComponent"],
                _paginas_foros_dialogos_modificar_foro_modificar_foro_component__WEBPACK_IMPORTED_MODULE_70__["ModificarForoComponent"],
                _paginas_mensaje_papa_mensaje_papa_component__WEBPACK_IMPORTED_MODULE_71__["MensajePapaComponent"],
                _paginas_ingreso_ingreso_component__WEBPACK_IMPORTED_MODULE_72__["IngresoComponent"],
                _paginas_usuarios_usuarios_component__WEBPACK_IMPORTED_MODULE_74__["UsuariosComponent"],
                _paginas_usuarios_tablas_tabla_usuarios_tabla_usuarios_component__WEBPACK_IMPORTED_MODULE_75__["TablaUsuariosComponent"],
                _paginas_usuarios_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_76__["CrearUsuarioComponent"],
                _paginas_usuarios_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_77__["ModificarUsuarioComponent"],
                _paginas_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_78__["InicioComponent"],
                _paginas_grupos_grupos_component__WEBPACK_IMPORTED_MODULE_79__["GruposComponent"],
                _paginas_grupos_tablas_tabla_grupos_tabla_grupos_component__WEBPACK_IMPORTED_MODULE_80__["TablaGruposComponent"],
                _paginas_grupos_dialogos_crear_grupo_crear_grupo_component__WEBPACK_IMPORTED_MODULE_81__["CrearGrupoComponent"],
                _paginas_grupos_dialogos_modificar_grupo_modificar_grupo_component__WEBPACK_IMPORTED_MODULE_82__["ModificarGrupoComponent"],
                _paginas_grupos_dialogos_agregar_asistente_agregar_asistente_component__WEBPACK_IMPORTED_MODULE_83__["AgregarAsistenteComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"],
                _angular_service_worker__WEBPACK_IMPORTED_MODULE_11__["ServiceWorkerModule"].register("/ngsw-worker.js", {
                    enabled: _environments_environment__WEBPACK_IMPORTED_MODULE_12__["environment"].production
                }),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_4__["TimepickerModule"].forRoot(),
                angular_froala_wysiwyg__WEBPACK_IMPORTED_MODULE_58__["FroalaEditorModule"].forRoot(),
                angular_froala_wysiwyg__WEBPACK_IMPORTED_MODULE_58__["FroalaViewModule"].forRoot(),
                ngx_material_timepicker__WEBPACK_IMPORTED_MODULE_3__["NgxMaterialTimepickerModule"].forRoot(),
                ng2_dnd__WEBPACK_IMPORTED_MODULE_43__["DndModule"].forRoot(),
                ng_drag_drop__WEBPACK_IMPORTED_MODULE_36__["NgDragDropModule"].forRoot(),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["BrowserAnimationsModule"],
                angularfire2__WEBPACK_IMPORTED_MODULE_5__["AngularFireModule"].initializeApp(firebaseConfig),
                angularfire2_firestore__WEBPACK_IMPORTED_MODULE_7__["AngularFirestoreModule"].enablePersistence(),
                angularfire2_storage__WEBPACK_IMPORTED_MODULE_6__["AngularFireStorageModule"],
                angularfire2_auth__WEBPACK_IMPORTED_MODULE_73__["AngularFireAuthModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_17__["LayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatSnackBarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_20__["FormsModule"],
                _covalent_core_loading__WEBPACK_IMPORTED_MODULE_21__["CovalentLoadingModule"],
                _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_22__["CovalentDialogsModule"],
                _covalent_core_file__WEBPACK_IMPORTED_MODULE_29__["CovalentFileModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_30__["FlexLayoutModule"],
                _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_31__["CdkTreeModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_8__["MatTooltipModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_42__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatCheckboxModule"]
            ],
            entryComponents: [
                _paginas_asistentes_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_19__["CrearAsistenteComponent"],
                _paginas_documentos_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_28__["CrearDocumentoComponent"],
                _paginas_documentos_carpetas_dialogo_dialogo_component__WEBPACK_IMPORTED_MODULE_33__["CrearCarpetaDialogoComponent"],
                _paginas_asistentes_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_34__["ModificarAsistenteComponent"],
                _paginas_tareas_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_37__["CrearTareaDialogoComponent"],
                _paginas_parametros_categorias_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_39__["CrearCategoriaDialogoComponent"],
                _paginas_parametros_categorias_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_40__["ModificarCategoriaDialogoComponent"],
                _paginas_tareas_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_41__["ModificarTareaDialogoComponent"],
                _paginas_calendario_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_45__["CrearCalendarioComponent"],
                _paginas_calendario_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_46__["ModificarCalendarioComponent"],
                _paginas_parametros_secciones_documentos_dialogos_modificar_seccion_documento_modificar_seccion_documento_component__WEBPACK_IMPORTED_MODULE_51__["ModificarSeccionDocumentoComponent"],
                _paginas_documentos_carpetas_dialogo_modificar_carpeta_modificar_carpeta_component__WEBPACK_IMPORTED_MODULE_52__["ModificarCarpetaComponent"],
                _paginas_documentos_dialogo_modificar_documento_modificar_documento_component__WEBPACK_IMPORTED_MODULE_53__["ModificarDocumentoComponent"],
                _paginas_proyectos_dialogos_crear_proyecto_crear_proyecto_component__WEBPACK_IMPORTED_MODULE_55__["CrearProyectoComponent"],
                _paginas_proyectos_dialogos_modificar_proyecto_modificar_proyecto_component__WEBPACK_IMPORTED_MODULE_56__["ModificarProyectoComponent"],
                _paginas_cronicas_dialogos_crear_cronica_crear_cronica_component__WEBPACK_IMPORTED_MODULE_60__["CrearCronicaComponent"],
                _paginas_cronicas_dialogos_modificar_cronica_modificar_cronica_component__WEBPACK_IMPORTED_MODULE_61__["ModificarCronicaComponent"],
                _paginas_encuestas_dialogos_crear_encuesta_crear_encuesta_component__WEBPACK_IMPORTED_MODULE_64__["CrearEncuestaComponent"],
                _paginas_encuestas_dialogos_modificar_encuesta_modificar_encuesta_component__WEBPACK_IMPORTED_MODULE_65__["ModificarEncuestaComponent"],
                _paginas_foros_dialogos_crear_foro_crear_foro_component__WEBPACK_IMPORTED_MODULE_69__["CrearForoComponent"],
                _paginas_foros_dialogos_modificar_foro_modificar_foro_component__WEBPACK_IMPORTED_MODULE_70__["ModificarForoComponent"],
                _paginas_usuarios_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_76__["CrearUsuarioComponent"],
                _paginas_usuarios_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_77__["ModificarUsuarioComponent"],
                _paginas_grupos_dialogos_crear_grupo_crear_grupo_component__WEBPACK_IMPORTED_MODULE_81__["CrearGrupoComponent"],
                _paginas_grupos_dialogos_modificar_grupo_modificar_grupo_component__WEBPACK_IMPORTED_MODULE_82__["ModificarGrupoComponent"],
                _paginas_grupos_dialogos_agregar_asistente_agregar_asistente_component__WEBPACK_IMPORTED_MODULE_83__["AgregarAsistenteComponent"]
            ],
            providers: [_core_resources_service__WEBPACK_IMPORTED_MODULE_23__["ResourcesService"], _core_notify_service__WEBPACK_IMPORTED_MODULE_24__["NotifyService"], _core_auth_service__WEBPACK_IMPORTED_MODULE_25__["AuthService"], _core_upload_service__WEBPACK_IMPORTED_MODULE_26__["UploadService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/barra-navegacion/barra-navegacion.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/barra-navegacion/barra-navegacion.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n    height: 100%;\n}\n\nspan {\n    font-weight: 100;\n}\n\nmat-toolbar {\n    border-top: 3px solid #ff7a59;\n    height: 60px;\n    background-color: #2e3f50 !important;\n    color: #fff;\n}\n\n.mat-button{\n    height: 100% !important;\n}\n\n.example-spacer {\n    flex: 1 1 auto;\n}\n\n.titulo {\n    margin-right: 1em;\n}\n\n.foto-perfil {\n    width: 32px;\n    height: 32px;\n    border-radius: 32px;\n    margin-right: 10px;\n}\n"

/***/ }),

/***/ "./src/app/barra-navegacion/barra-navegacion.component.html":
/*!******************************************************************!*\
  !*** ./src/app/barra-navegacion/barra-navegacion.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\n  <mat-sidenav-content>\n    <mat-toolbar>\n      <span class=\"titulo\">Pasionistas</span>\n\n      <button mat-button  routerLinkActive=\"active\" [routerLink]=\"['/tareas']\">\n        Tareas\n      </button>\n\n      <button mat-button routerLinkActive=\"active\" [routerLink]=\"['/calendario']\">\n        Calendario\n      </button>\n\n      <button mat-button [routerLink]=\"['/foros']\">\n        Foros\n      </button>\n\n      <button mat-button [routerLink]=\"['/cronicas']\">\n        Crónicas\n      </button>\n\n      <button mat-button [routerLink]=\"['/proyectos']\">\n        Proyecto\n      </button>\n\n      <button mat-button [routerLink]=\"['/encuestas']\">\n        Encuestas\n      </button>\n\n      <button mat-button [routerLink]=\"['/carpetas/0']\">\n        Documentos\n      </button>\n\n\n      <button mat-button [routerLink]=\"['/grupos']\">\n        Grupos\n      </button>\n\n\n\n      <button mat-button [matMenuTriggerFor]=\"menuMaestros\">\n        Maestros\n        <mat-icon class=\"example-icon\">arrow_drop_down</mat-icon>\n      </button>\n\n      <mat-menu #menuMaestros=\"matMenu\" yPosition=\"below\">\n\n        <button mat-menu-item [routerLink]=\"['/administradores']\">\n          Administradores\n        </button>\n\n        <button mat-menu-item [routerLink]=\"['/asistentes']\">\n          Asistentes\n        </button>\n\n        <!--<button mat-menu-item [routerLink]=\"['/parametros/categorias']\">\n          Categorias\n        </button>-->\n\n        <button mat-menu-item [routerLink]=\"['/parametros/secciones-documentos']\">\n          Secciones de documentos\n        </button>\n\n        <button mat-menu-item [routerLink]=\"['/parametros/mensaje-papa']\">\n          Mensaje del papa\n        </button>\n\n      </mat-menu>\n\n      <span class=\"example-spacer\"></span>\n      <button mat-button [matMenuTriggerFor]=\"menuPerfil\">\n        <img class=\"foto-perfil\" src=\"assets/icons/user.png\">\n        {{info.nombre}}\n        <mat-icon class=\"example-icon\">arrow_drop_down</mat-icon>\n      </button>\n      <mat-menu #menuPerfil=\"matMenu\" yPosition=\"below\">\n        <button mat-menu-item (click)=\"close()\">\n          Cerrar sesión\n        </button>\n      </mat-menu>\n    </mat-toolbar>\n  </mat-sidenav-content>\n</mat-sidenav-container>\n"

/***/ }),

/***/ "./src/app/barra-navegacion/barra-navegacion.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/barra-navegacion/barra-navegacion.component.ts ***!
  \****************************************************************/
/*! exports provided: BarraNavegacionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarraNavegacionComponent", function() { return BarraNavegacionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/auth.service */ "./src/app/core/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BarraNavegacionComponent = /** @class */ (function () {
    function BarraNavegacionComponent(breakpointObserver, _auth, _route) {
        var _this = this;
        this.breakpointObserver = breakpointObserver;
        this._auth = _auth;
        this._route = _route;
        this.info = {};
        this.isHandset$ = this.breakpointObserver
            .observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) { return result.matches; }));
        this._auth.getCurrentUser().subscribe(function (user) {
            if (user.length > 0) {
                _this.info = user[0];
            }
            else {
                _this.info = {};
                _this._route.navigate(["login"]);
            }
        });
    }
    BarraNavegacionComponent.prototype.close = function () {
        this._auth.signOut();
        this._route.navigate(["login"]);
    };
    BarraNavegacionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "barra-navegacion",
            template: __webpack_require__(/*! ./barra-navegacion.component.html */ "./src/app/barra-navegacion/barra-navegacion.component.html"),
            styles: [__webpack_require__(/*! ./barra-navegacion.component.css */ "./src/app/barra-navegacion/barra-navegacion.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["BreakpointObserver"],
            _core_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], BarraNavegacionComponent);
    return BarraNavegacionComponent;
}());



/***/ }),

/***/ "./src/app/core/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/core/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var _notify_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var _resources_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./resources.service */ "./src/app/core/resources.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AuthService = /** @class */ (function () {
    function AuthService(afAuth, afs, router, notify, _db) {
        var _this = this;
        this.afAuth = afAuth;
        this.afs = afs;
        this.router = router;
        this.notify = notify;
        this._db = _db;
        this.user = this.afAuth.authState
            .switchMap(function (user) {
            if (user) {
                return _this._db.col$("usuarios-sistema", function (ref) { return ref.where('uid', '==', user.uid); });
            }
            else {
                return rxjs_Observable__WEBPACK_IMPORTED_MODULE_6__["Observable"].of(null);
            }
        });
    }
    Object.defineProperty(AuthService.prototype, "authenticated", {
        get: function () {
            return this.user !== null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "currentUserObservable", {
        get: function () {
            return this.afAuth.authState;
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.getCurrentUser = function () {
        var _this = this;
        return this.afAuth.authState
            .switchMap(function (user) {
            if (user) {
                return _this._db.col$("usuarios-sistema", function (ref) { return ref.where('uid', '==', user.uid); });
            }
            else {
                return rxjs_Observable__WEBPACK_IMPORTED_MODULE_6__["Observable"].of(null);
            }
        });
    };
    // Registrar usuario administrador
    AuthService.prototype.createAdminWithEmail = function (email, password, info) {
        var _this = this;
        return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
            .then(function (user) {
            return _this.updateUserData(user, info);
        })
            .catch(function (error) { return _this.handleError(error); });
    };
    // Sets user data to firestore after succesful login
    AuthService.prototype.updateUserData = function (user, info) {
        var userRef = this.afs.doc("usuarios-sistema/" + user.uid);
        var role = 'Administrador';
        var data = {
            uid: user.uid,
            correo: info.correo,
            nombre: info.nombre,
            apellido: info.apellido,
            rol: role,
            estado: true
        };
        return userRef.set(data);
    };
    AuthService.prototype.emailLogin = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signInWithEmailAndPassword(email, password)
                .then(function (user) {
                _this._db.col$("usuarios-sistema", function (ref) { return ref.where('uid', '==', user.uid); }).subscribe(function (res) {
                    if (res.length > 0) {
                        resolve(res[0]);
                    }
                    else {
                        _this.notify.errorMessage("El usuario con correo electr\u00F3nico " + email + " no es administrador del sistema", 2000);
                        reject();
                    }
                });
            })
                .catch(function (error) {
                var message;
                switch (error.code) {
                    case 'auth/user-not-found':
                        message = 'Usuario no existe';
                        break;
                    case 'auth/wrong-password':
                        message = 'Contraseña incorrecta';
                        break;
                    case 'auth/invalid-email':
                        message = 'Formato de correo invalido';
                        break;
                    default:
                        message = 'Usuario o contraseña invalido';
                        break;
                }
                _this.notify.errorMessage(message, 2000);
                reject(error);
            });
        });
    };
    // Sends email allowing user to reset password
    AuthService.prototype.resetPassword = function (email) {
        var _this = this;
        var fbAuth = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]();
        return fbAuth.sendPasswordResetEmail(email)
            .then(function () { return _this.notify.update('Password update email sent', 'info'); })
            .catch(function (error) { return _this.handleError(error); });
    };
    AuthService.prototype.signOut = function () {
        var _this = this;
        this.afAuth.auth.signOut().then(function () {
            _this.router.navigate(['/']);
        });
    };
    // If error, console log and notify user
    AuthService.prototype.handleError = function (error) {
        console.error(error);
        this.notify.update(error.message, 'error');
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [angularfire2_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            angularfire2_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"],
            _resources_service__WEBPACK_IMPORTED_MODULE_7__["ResourcesService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/core/notify.service.ts":
/*!****************************************!*\
  !*** ./src/app/core/notify.service.ts ***!
  \****************************************/
/*! exports provided: NotifyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotifyService", function() { return NotifyService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotifyService = /** @class */ (function () {
    function NotifyService(snackBar) {
        this.snackBar = snackBar;
        this._msgSource = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.msg = this._msgSource.asObservable();
    }
    NotifyService.prototype.update = function (content, style) {
        var msg = { content: content, style: style };
        this._msgSource.next(msg);
    };
    NotifyService.prototype.clear = function () {
        this._msgSource.next(null);
    };
    NotifyService.prototype.okMessage = function (content, time) {
        if (time === void 0) { time = 10000; }
        this.snackBar.open(content, 'Cerrar', {
            duration: time,
            panelClass: 'greenMessage'
        });
    };
    NotifyService.prototype.errorMessage = function (content, time) {
        if (time === void 0) { time = 1500; }
        this.snackBar.open(content, 'Cerrar', {
            duration: time,
            panelClass: 'redMessage',
        });
    };
    NotifyService.prototype.alertMessage = function (content, time) {
        if (time === void 0) { time = 1500; }
        this.snackBar.open(content, 'Cerrar', {
            duration: time
        });
    };
    NotifyService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
    ], NotifyService);
    return NotifyService;
}());



/***/ }),

/***/ "./src/app/core/resources.service.ts":
/*!*******************************************!*\
  !*** ./src/app/core/resources.service.ts ***!
  \*******************************************/
/*! exports provided: ResourcesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResourcesService", function() { return ResourcesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_do__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/do */ "./node_modules/rxjs-compat/_esm5/add/operator/do.js");
/* harmony import */ var rxjs_add_operator_take__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/take */ "./node_modules/rxjs-compat/_esm5/add/operator/take.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var rxjs_add_operator_switchMap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/switchMap */ "./node_modules/rxjs-compat/_esm5/add/operator/switchMap.js");
/* harmony import */ var _node_modules_angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../node_modules/@angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ResourcesService = /** @class */ (function () {
    function ResourcesService(afs, storage, db, http) {
        this.afs = afs;
        this.storage = storage;
        this.db = db;
        this.http = http;
    }
    ResourcesService.prototype.getPaises = function () {
        return this.http.get('assets/paises.json');
    };
    // Get Reference
    ResourcesService.prototype.col = function (ref, queryFn) {
        return typeof ref === 'string' ? this.afs.collection(ref, queryFn) : ref;
    };
    ResourcesService.prototype.doc = function (ref) {
        return typeof ref === 'string' ? this.afs.doc(ref) : ref;
    };
    // Get Data
    ResourcesService.prototype.doc$ = function (ref) {
        return this.doc(ref).snapshotChanges().map(function (doc) {
            return doc.payload.data();
        });
    };
    ResourcesService.prototype.col$ = function (ref, queryFn) {
        return this.col(ref, queryFn).snapshotChanges().map(function (docs) {
            return docs.map(function (a) { return a.payload.doc.data(); });
        });
    };
    // With Id
    ResourcesService.prototype.colWithIds$ = function (ref, queryFn) {
        return this.col(ref, queryFn).snapshotChanges().map(function (action) {
            return action.map(function (a) {
                var data = a.payload.doc.data();
                var id = a.payload.doc.id;
                return __assign({ id: id }, data);
            });
        });
    };
    ResourcesService.prototype.inspectDoc = function (ref) {
        var tick = new Date().getTime();
        this.doc(ref).snapshotChanges()
            .take(1)
            .do(function (d) {
            var tock = new Date().getTime() - tick;
            console.log("Loaded Document in " + tock + "ms", d);
        })
            .subscribe();
    };
    ResourcesService.prototype.inspectCol = function (ref) {
        var tick = new Date().getTime();
        this.col(ref).snapshotChanges()
            .take(1)
            .do(function (c) {
            var tock = new Date().getTime() - tick;
            console.log("Loaded Collection in " + tock + "ms", c);
        })
            .subscribe();
    };
    // Write Data
    ResourcesService.prototype.update = function (ref, data) {
        return this.doc(ref).update(__assign({}, data, { updatedAt: this.timestamp }));
    };
    ResourcesService.prototype.set = function (ref, data) {
        var timestamp = this.timestamp;
        return this.doc(ref).set(__assign({}, data, { updatedAt: timestamp, createdAt: timestamp }));
    };
    ResourcesService.prototype.add = function (ref, data) {
        var timestamp = this.timestamp;
        return this.col(ref).add(__assign({}, data, { updatedAt: timestamp, createdAt: timestamp }));
    };
    ResourcesService.prototype.delete = function (ref) {
        return this.doc(ref).delete();
    };
    ResourcesService.prototype.upsert = function (ref, data) {
        var _this = this;
        var doc = this.doc(ref).snapshotChanges().take(1).toPromise();
        return doc.then(function (snap) {
            return snap.payload.exists ? _this.update(ref, data) : _this.set(ref, data);
        });
    };
    Object.defineProperty(ResourcesService.prototype, "timestamp", {
        // Get Firebase Serve Timestamp
        get: function () {
            return firebase_app__WEBPACK_IMPORTED_MODULE_3__["firestore"].FieldValue.serverTimestamp();
        },
        enumerable: true,
        configurable: true
    });
    // GeoPoint
    ResourcesService.prototype.geopoint = function (lat, lng) {
        return new firebase_app__WEBPACK_IMPORTED_MODULE_3__["firestore"].GeoPoint(lat, lng);
    };
    // Documents
    ResourcesService.prototype.connect = function (host, key, doc) {
        return this.doc(host).update((_a = {}, _a[key] = this.doc(doc).ref, _a));
        var _a;
    };
    /// returns a documents references mapped to AngularFirestoreDocument
    ResourcesService.prototype.docWithRefs$ = function (ref) {
        var _this = this;
        return this.doc$(ref).map(function (doc) {
            for (var _i = 0, _a = Object.keys(doc); _i < _a.length; _i++) {
                var k = _a[_i];
                if (doc[k] instanceof firebase_app__WEBPACK_IMPORTED_MODULE_3__["firestore"].DocumentReference) {
                    doc[k] = _this.doc(doc[k].path);
                }
            }
            return doc;
        });
    };
    ResourcesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"],
            angularfire2_storage__WEBPACK_IMPORTED_MODULE_2__["AngularFireStorage"],
            angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"],
            _node_modules_angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"]])
    ], ResourcesService);
    return ResourcesService;
}());



/***/ }),

/***/ "./src/app/core/upload.service.ts":
/*!****************************************!*\
  !*** ./src/app/core/upload.service.ts ***!
  \****************************************/
/*! exports provided: Upload, UploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Upload", function() { return Upload; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadService", function() { return UploadService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _resources_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Upload = /** @class */ (function () {
    function Upload(file) {
        this.createdAt = new Date();
        this.file = file;
    }
    return Upload;
}());

var UploadService = /** @class */ (function () {
    function UploadService(storage, db, _loadingService) {
        this.storage = storage;
        this.db = db;
        this._loadingService = _loadingService;
    }
    UploadService.prototype.uploadDocumento = function (upload) {
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_2__["storage"]().ref();
            var now = new Date().getTime();
            var uploadTask = storageRef.child("/documentos/" + now).put(upload.file);
            uploadTask.on(firebase_app__WEBPACK_IMPORTED_MODULE_2__["storage"].TaskEvent.STATE_CHANGED, function (snapshot) {
                upload.progress =
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            }, function (error) {
                return reject(error);
            }, function () {
                upload.url = uploadTask.snapshot.downloadURL;
                upload.name = upload.file.name;
                resolve(upload.url);
            });
        });
    };
    UploadService.prototype.uploadPic = function (upload) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._loadingService.register();
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_2__["storage"]().ref();
            var now = new Date().getTime();
            var uploadTask = storageRef.child("/fotos/" + now).put(upload.file);
            uploadTask.on(firebase_app__WEBPACK_IMPORTED_MODULE_2__["storage"].TaskEvent.STATE_CHANGED, function (snapshot) {
                upload.progress =
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            }, function (error) {
                return reject(error);
            }, function () {
                upload.url = uploadTask.snapshot.downloadURL;
                upload.name = upload.file.name;
                _this._loadingService.resolve();
                resolve(upload.url);
            });
        });
    };
    UploadService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [angularfire2_storage__WEBPACK_IMPORTED_MODULE_1__["AngularFireStorage"],
            _resources_service__WEBPACK_IMPORTED_MODULE_3__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_4__["TdLoadingService"]])
    ], UploadService);
    return UploadService;
}());



/***/ }),

/***/ "./src/app/paginas/asistentes/asistentes.component.html":
/*!**************************************************************!*\
  !*** ./src/app/paginas/asistentes/asistentes.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <span class=\"titulo-seccion\">Asistentes</span>\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" matTooltip=\"Crear nuevo registro\" mat-raised-button (click)=\"crear()\">\n          <mat-icon matListIcon class=\"small\">add</mat-icon>\n          Crear asistente\n        </button>\n      </div>\n    </mat-toolbar>\n  </div>\n  <tabla-asistentes></tabla-asistentes>\n</div>\n\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/asistentes.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/paginas/asistentes/asistentes.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column;\n  min-width: 300px; }\n\n.example-header {\n  min-height: 64px;\n  padding: 25px 24px 0;\n  background-color: #fff;\n  color: white; }\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%; }\n\n.mat-table {\n  overflow: auto;\n  height: 50vh; }\n\ninput.mat-input-element {\n  color: black; }\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n  max-height: 500px;\n  min-width: 300px; }\n\n.mat-table {\n  overflow: auto;\n  max-height: 500px; }\n\n.mat-column-select {\n  overflow: visible; }\n\n.mat-column-placa {\n  flex: 0 0 10%;\n  text-align: left; }\n\n.mat-column-propietario {\n  flex: 0 0 20%; }\n\n.mat-column-flota {\n  flex: 0 0 20%; }\n\n.mat-column-estado {\n  flex: 0 0 20%; }\n\n.label-active {\n  background: green;\n  color: white;\n  padding: .5em;\n  border-radius: 3px; }\n\n.label-inactive {\n  background: red;\n  color: white;\n  padding: .5em;\n  border-radius: 3px; }\n\n.estatus-cell {\n  overflow: initial; }\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/asistentes.component.ts":
/*!************************************************************!*\
  !*** ./src/app/paginas/asistentes/asistentes.component.ts ***!
  \************************************************************/
/*! exports provided: AsistentesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AsistentesComponent", function() { return AsistentesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dialogo/crear/crear.component */ "./src/app/paginas/asistentes/dialogo/crear/crear.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AsistentesComponent = /** @class */ (function () {
    function AsistentesComponent(dialog) {
        this.dialog = dialog;
    }
    AsistentesComponent.prototype.ngOnInit = function () {
    };
    AsistentesComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_2__["CrearAsistenteComponent"], {
            width: "60%",
        });
    };
    AsistentesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-asistentes',
            template: __webpack_require__(/*! ./asistentes.component.html */ "./src/app/paginas/asistentes/asistentes.component.html"),
            styles: [__webpack_require__(/*! ./asistentes.component.scss */ "./src/app/paginas/asistentes/asistentes.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], AsistentesComponent);
    return AsistentesComponent;
}());



/***/ }),

/***/ "./src/app/paginas/asistentes/dialogo/crear/crear.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/paginas/asistentes/dialogo/crear/crear.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Registro de asistente</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de perfil</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\"[ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\" >\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre completo\" required>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-12\">\n      <mat-form-field required>\n        <input type=\"email\" matInput [(ngModel)]=\"info.correo\" placeholder=\"Correo electrónico\">\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-4\">\n      <mat-form-field>\n        <input type=\"text\" matInput [(ngModel)]=\"info.configuracion\" placeholder=\"Configuración\">\n      </mat-form-field>\n    </div>\n\n\n\n\n    <div class=\"col-md-4\">\n      <mat-form-field>\n        <mat-select placeholder=\"País\" required [(ngModel)]=\"info.pais\">\n          <mat-option [value]=\"item.name\" *ngFor=\"let item of paises\">{{item.name}}</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-4\">\n      <mat-form-field>\n        <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n          <mat-option value=\"español\">Español</mat-option>\n          <mat-option value=\"ingles\">Ingles</mat-option>\n          <mat-option value=\"italiano\">Italiano</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n\n    <div class=\"col-md-4\">\n      <mat-form-field>\n        <mat-select placeholder=\"Tipo de asistente\" required [(ngModel)]=\"info.tipo\">\n          <mat-option value=\"participante\">Participante</mat-option>\n          <mat-option value=\"staff\">Staff</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-4\">\n      <mat-form-field required>\n        <input type=\"number\" matInput [(ngModel)]=\"info.posicion\" placeholder=\"Posición\">\n      </mat-form-field>\n    </div>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre || !info.correo || !info.configuracion || !info.idioma\">Crear asistente</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/dialogo/crear/crear.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/paginas/asistentes/dialogo/crear/crear.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/dialogo/crear/crear.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/paginas/asistentes/dialogo/crear/crear.component.ts ***!
  \*********************************************************************/
/*! exports provided: CrearAsistenteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearAsistenteComponent", function() { return CrearAsistenteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CrearAsistenteComponent = /** @class */ (function () {
    function CrearAsistenteComponent(db, notify, dialogRef, _upload) {
        var _this = this;
        this.db = db;
        this.notify = notify;
        this.dialogRef = dialogRef;
        this._upload = _upload;
        this.info = {};
        this.imagenes = [];
        this.files = {};
        this.paises = [];
        this.db.getPaises().subscribe(function (resp) {
            _this.paises = resp;
            console.log(_this.paises);
        });
    }
    CrearAsistenteComponent.prototype.ngOnInit = function () { };
    CrearAsistenteComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .add("asistentes", this.info)
            .then(function () {
            _this.notify.update("Asistente creado exitosamente", "success");
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error creando registro", "error");
        });
    };
    CrearAsistenteComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearAsistenteComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) { }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    CrearAsistenteComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    CrearAsistenteComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear",
            template: __webpack_require__(/*! ./crear.component.html */ "./src/app/paginas/asistentes/dialogo/crear/crear.component.html"),
            styles: [__webpack_require__(/*! ./crear.component.scss */ "./src/app/paginas/asistentes/dialogo/crear/crear.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], CrearAsistenteComponent);
    return CrearAsistenteComponent;
}());



/***/ }),

/***/ "./src/app/paginas/asistentes/dialogo/modificar/modificar.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/paginas/asistentes/dialogo/modificar/modificar.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar asistente</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <div class=\"row\">\n\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de perfil</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre completo\" required>\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-12\">\n      <mat-form-field required>\n        <input type=\"email\" matInput [(ngModel)]=\"info.correo\" placeholder=\"Correo electrónico\">\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-4\">\n      <mat-form-field>\n        <input type=\"text\" matInput [(ngModel)]=\"info.configuracion\" placeholder=\"Configuración\">\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-4\">\n      <mat-form-field>\n        <mat-select placeholder=\"País\" required [(ngModel)]=\"info.pais\">\n          <mat-option [value]=\"item.name\" *ngFor=\"let item of paises\">{{item.name}}</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-4\">\n      <mat-form-field>\n        <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n          <mat-option value=\"español\">Español</mat-option>\n          <mat-option value=\"ingles\">Ingles</mat-option>\n          <mat-option value=\"italiano\">Italiano</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-4\">\n      <mat-form-field>\n        <mat-select placeholder=\"Tipo de asistente\" [(ngModel)]=\"info.tipo\">\n          <mat-option value=\"participante\">Participante</mat-option>\n          <mat-option value=\"staff\">Staff</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-4\">\n      <mat-form-field required>\n        <input type=\"number\" matInput [(ngModel)]=\"info.posicion\" placeholder=\"Posición\">\n      </mat-form-field>\n    </div>\n\n  </div>\n\n\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre || !info.correo \">Modificar</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/dialogo/modificar/modificar.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/paginas/asistentes/dialogo/modificar/modificar.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/dialogo/modificar/modificar.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/paginas/asistentes/dialogo/modificar/modificar.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ModificarAsistenteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarAsistenteComponent", function() { return ModificarAsistenteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ModificarAsistenteComponent = /** @class */ (function () {
    function ModificarAsistenteComponent(db, notify, snackBar, dialogRef, data, _upload) {
        var _this = this;
        this.db = db;
        this.notify = notify;
        this.snackBar = snackBar;
        this.dialogRef = dialogRef;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.imagenes = [];
        this.files = {};
        this.paises = [];
        this.info = data.item;
        this.db.getPaises().subscribe(function (resp) {
            _this.paises = resp;
        });
    }
    ModificarAsistenteComponent.prototype.ngOnInit = function () { };
    ModificarAsistenteComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .update("asistentes/" + this.info.id, this.info)
            .then(function () {
            _this.snackBar.open("Asistente modificado", "Cerrar", {
                duration: 2000,
                horizontalPosition: "left"
            });
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error modificando registro", "error");
        });
    };
    ModificarAsistenteComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarAsistenteComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    ModificarAsistenteComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    ModificarAsistenteComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar",
            template: __webpack_require__(/*! ./modificar.component.html */ "./src/app/paginas/asistentes/dialogo/modificar/modificar.component.html"),
            styles: [__webpack_require__(/*! ./modificar.component.scss */ "./src/app/paginas/asistentes/dialogo/modificar/modificar.component.scss")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], ModificarAsistenteComponent);
    return ModificarAsistenteComponent;
}());



/***/ }),

/***/ "./src/app/paginas/asistentes/tabla-asistentes/tabla-asistentes.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/paginas/asistentes/tabla-asistentes/tabla-asistentes.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-cell {\n    color: #0091ae;\n    cursor: pointer;\n}\n\n.mat-cell:hover {\n    text-decoration: underline;\n}\n\n.mat-column-nombre {\n    flex: 0 0 20%;\n    text-align: left;\n}\n\n.mat-column-correo {\n    flex: 0 0 20%;\n    text-align: left;\n}\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/tabla-asistentes/tabla-asistentes.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/paginas/asistentes/tabla-asistentes/tabla-asistentes.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n    <ng-container matColumnDef=\"uid\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Uid</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.uid}}</mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"posicion\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Posición</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.posicion}}</mat-cell>\n    </ng-container>\n\n    <!-- Id Column -->\n    <ng-container matColumnDef=\"correo\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>email</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.correo}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"nombre\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre asistente</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.nombre}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"configuracion\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Configuración</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.configuracion}}</mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"idioma\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>idioma</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.idioma}}</mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"tipo\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Tipo</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.tipo}}</mat-cell>\n    </ng-container>\n\n    <!-- Checkbox Column -->\n    <ng-container matColumnDef=\"opciones\">\n      <mat-header-cell *matHeaderCellDef></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n\n          <button mat-menu-item (click)=\"eliminar(row)\">Eliminar</button>\n        </mat-menu>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n\n  <mat-paginator #paginator\n    [length]=\"lista.length\"\n    [pageIndex]=\"0\"\n    [pageSize]=\"50\"\n    [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/tabla-asistentes/tabla-asistentes.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/paginas/asistentes/tabla-asistentes/tabla-asistentes.component.ts ***!
  \***********************************************************************************/
/*! exports provided: TablaAsistentesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablaAsistentesComponent", function() { return TablaAsistentesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../dialogo/modificar/modificar.component */ "./src/app/paginas/asistentes/dialogo/modificar/modificar.component.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../core/notify.service */ "./src/app/core/notify.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: '¿Está seguro de eliminar el registro?',
    disableClose: false,
    title: 'Confirmación',
    cancelButton: 'Cancelar',
    acceptButton: 'Aceptar',
    width: '500px',
};
var TablaAsistentesComponent = /** @class */ (function () {
    function TablaAsistentesComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = ["posicion", "correo", "nombre", "configuracion", "tipo", "idioma", "opciones"];
        this.lista = [];
    }
    TablaAsistentesComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    TablaAsistentesComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("asistentes").subscribe(function (resp) {
            _this.lista = resp;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    TablaAsistentesComponent.prototype.ir = function (item) {
        console.log(item);
        var dialogRef = this.dialog.open(_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_6__["ModificarAsistenteComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    TablaAsistentesComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("asistentes/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Asistente eliminado");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], TablaAsistentesComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], TablaAsistentesComponent.prototype, "sort", void 0);
    TablaAsistentesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "tabla-asistentes",
            template: __webpack_require__(/*! ./tabla-asistentes.component.html */ "./src/app/paginas/asistentes/tabla-asistentes/tabla-asistentes.component.html"),
            styles: [__webpack_require__(/*! ./tabla-asistentes.component.css */ "./src/app/paginas/asistentes/tabla-asistentes/tabla-asistentes.component.css")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_5__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_7__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], TablaAsistentesComponent);
    return TablaAsistentesComponent;
}());



/***/ }),

/***/ "./src/app/paginas/autenticacion/autenticacion.component.html":
/*!********************************************************************!*\
  !*** ./src/app/paginas/autenticacion/autenticacion.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  autenticacion works!\n</p>\n"

/***/ }),

/***/ "./src/app/paginas/autenticacion/autenticacion.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/paginas/autenticacion/autenticacion.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/paginas/autenticacion/autenticacion.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/paginas/autenticacion/autenticacion.component.ts ***!
  \******************************************************************/
/*! exports provided: AutenticacionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutenticacionComponent", function() { return AutenticacionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AutenticacionComponent = /** @class */ (function () {
    function AutenticacionComponent() {
    }
    AutenticacionComponent.prototype.ngOnInit = function () {
    };
    AutenticacionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-autenticacion',
            template: __webpack_require__(/*! ./autenticacion.component.html */ "./src/app/paginas/autenticacion/autenticacion.component.html"),
            styles: [__webpack_require__(/*! ./autenticacion.component.scss */ "./src/app/paginas/autenticacion/autenticacion.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AutenticacionComponent);
    return AutenticacionComponent;
}());



/***/ }),

/***/ "./src/app/paginas/calendario/calendario.component.html":
/*!**************************************************************!*\
  !*** ./src/app/paginas/calendario/calendario.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <button mat-button class=\"btn-ruta\">Calendario de actividades</button>\n\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" (click)=\"crear()\">\n          <mat-icon matListIcon class=\"small\">add</mat-icon>\n          Crear actividad\n        </button>\n      </div>\n\n    </mat-toolbar>\n  </div>\n\n  <tabla-calendario></tabla-calendario>\n\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/calendario/calendario.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/paginas/calendario/calendario.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  min-height: calc(90vh - 60px);\n  padding: 0 1em; }\n  .row .col-md-3 {\n    background: #f5f8fa;\n    border: 1px solid #dfe3eb;\n    padding: 0; }\n  .row .col-md-3 .header {\n      border-bottom: 1px solid #dfe3eb;\n      padding: 16px 16px;\n      font-size: 12px;\n      -webkit-hyphens: none;\n          -ms-hyphens: none;\n              hyphens: none;\n      overflow: hidden !important;\n      text-overflow: ellipsis !important;\n      white-space: nowrap !important;\n      font-weight: 600;\n      text-transform: uppercase;\n      font-family: \"Roboto\"; }\n  .row .col-md-3 .tareas {\n      margin: 1em;\n      min-height: calc(90vh - 120px); }\n  .row .col-md-3 .tareas mat-card {\n        margin-bottom: 1em; }\n  .titulo {\n  margin: 0 !important;\n  color: #0091ae;\n  cursor: pointer; }\n  .titulo:hover {\n  font-weight: 500; }\n"

/***/ }),

/***/ "./src/app/paginas/calendario/calendario.component.ts":
/*!************************************************************!*\
  !*** ./src/app/paginas/calendario/calendario.component.ts ***!
  \************************************************************/
/*! exports provided: CalendarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarioComponent", function() { return CalendarioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dialogo/crear/crear.component */ "./src/app/paginas/calendario/dialogo/crear/crear.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CalendarioComponent = /** @class */ (function () {
    function CalendarioComponent(db, _dialogService, dialog, snackBar) {
        this.db = db;
        this._dialogService = _dialogService;
        this.dialog = dialog;
        this.snackBar = snackBar;
    }
    CalendarioComponent.prototype.ngOnInit = function () {
    };
    CalendarioComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_4__["CrearCalendarioComponent"], {
            width: "60%"
        });
    };
    CalendarioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-calendario',
            template: __webpack_require__(/*! ./calendario.component.html */ "./src/app/paginas/calendario/calendario.component.html"),
            styles: [__webpack_require__(/*! ./calendario.component.scss */ "./src/app/paginas/calendario/calendario.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__["TdDialogService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], CalendarioComponent);
    return CalendarioComponent;
}());



/***/ }),

/***/ "./src/app/paginas/calendario/dialogo/crear/crear.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/paginas/calendario/dialogo/crear/crear.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Crear actividad</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <div class=\"row\" style=\"margin-top:1em;\">\n\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <mat-select placeholder=\"Tipo de actividad\" required [(ngModel)]=\"info.actividad\">\n          <mat-option value=\"inicio\">Bienvenida, presentación, inicio</mat-option>\n          <mat-option value=\"estado\">Estado de la congregación, compartiendo en configuraciones</mat-option>\n          <mat-option value=\"identidad\">Identidad pasionista, formación, Vida de comunidad y visión</mat-option>\n          <mat-option value=\"solidaridad\">Solidaridad y finanzas</mat-option>\n          <mat-option value=\"comision\">Comisión juridica</mat-option>\n          <mat-option value=\"nuevos\">Nuevos puntos, nuevas fronteras, trabajos laicos</mat-option>\n          <mat-option value=\"liderazgo\">Liderazgo,Elecciones</mat-option>\n          <mat-option value=\"propuesta\">Propuestas formuladas y votaciones</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n  </div>\n\n  <mat-tab-group>\n    <mat-tab label=\"Español\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título de la actividad\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Descripción\" [(ngModel)]=\"info.descripcion\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n    <mat-tab label=\"Ingles\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloingles\" placeholder=\"Título ingles\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Descripción ingles\" [(ngModel)]=\"info.descripcioningles\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n    <mat-tab label=\"Italiano\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloitaliano\" placeholder=\"Título italiano\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Descripción italiano\" [(ngModel)]=\"info.descripcionitaliano\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n  </mat-tab-group>\n\n\n  <div class=\"row\" style=\"margin-top:1em;\">\n\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <input matInput type=\"number\" [(ngModel)]=\"info.dia\" placeholder=\"Día de la actividad\" required>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <input matInput [(ngModel)]=\"info.fecha\" [matDatepicker]=\"picker\" placeholder=\"Fecha actividad\" readonly>\n        <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n        <mat-datepicker #picker></mat-datepicker>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-6\">\n      <label>Hora Inicio</label>\n      <timepicker placeholder=\"Hora inicio\" [(ngModel)]=\"info.horainicio\" meridians=\"24H\" [showMeridian]=\"false\"></timepicker>\n    </div>\n\n    <div class=\"col-md-6\">\n      <label>Hora Fin</label>\n      <timepicker placeholder=\"Hora inicio\" [(ngModel)]=\"info.horafin\" meridians=\"24H\" [showMeridian]=\"false\"></timepicker>\n    </div>\n\n  </div>\n\n\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.descripcion || !info.dia || !info.fecha || !info.horainicio || !info.horafin\">Crear actividad</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/calendario/dialogo/crear/crear.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/paginas/calendario/dialogo/crear/crear.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/calendario/dialogo/crear/crear.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/paginas/calendario/dialogo/crear/crear.component.ts ***!
  \*********************************************************************/
/*! exports provided: CrearCalendarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearCalendarioComponent", function() { return CrearCalendarioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CrearCalendarioComponent = /** @class */ (function () {
    function CrearCalendarioComponent(db, notify, dialogRef) {
        this.db = db;
        this.notify = notify;
        this.dialogRef = dialogRef;
        this.info = {
            horafin: new Date(),
            horainicio: new Date(),
        };
    }
    CrearCalendarioComponent.prototype.ngOnInit = function () { };
    CrearCalendarioComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .add("actividades", this.info)
            .then(function () {
            _this.notify.update("Actividad creada exitosamente", "success");
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error creando registro", "error");
        });
    };
    CrearCalendarioComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearCalendarioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear",
            template: __webpack_require__(/*! ./crear.component.html */ "./src/app/paginas/calendario/dialogo/crear/crear.component.html"),
            styles: [__webpack_require__(/*! ./crear.component.scss */ "./src/app/paginas/calendario/dialogo/crear/crear.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"]])
    ], CrearCalendarioComponent);
    return CrearCalendarioComponent;
}());



/***/ }),

/***/ "./src/app/paginas/calendario/dialogo/modificar/modificar.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/paginas/calendario/dialogo/modificar/modificar.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar actividad</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <div class=\"row\" style=\"margin-top:1em;\">\n\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <mat-select placeholder=\"Tipo de actividad\" required [(ngModel)]=\"info.actividad\">\n          <mat-option value=\"inicio\">Bienvenida, presentación, inicio</mat-option>\n          <mat-option value=\"estado\">Estado de la congregación, compartiendo en configuraciones</mat-option>\n          <mat-option value=\"identidad\">Identidad pasionista, formación, Vida de comunidad y visión</mat-option>\n          <mat-option value=\"solidaridad\">Solidaridad y finanzas</mat-option>\n          <mat-option value=\"comision\">Comisión juridica</mat-option>\n          <mat-option value=\"nuevos\">Nuevos puntos, nuevas fronteras, trabajos laicos</mat-option>\n          <mat-option value=\"liderazgo\">Liderazgo,Elecciones</mat-option>\n          <mat-option value=\"propuesta\">Propuestas formuladas y votaciones</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n  </div>\n\n  <mat-tab-group>\n    <mat-tab label=\"Español\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título de la actividad\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Descripción\" [(ngModel)]=\"info.descripcion\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n    <mat-tab label=\"Ingles\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloingles\" placeholder=\"Título ingles\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Descripción ingles\" [(ngModel)]=\"info.descripcioningles\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n    <mat-tab label=\"Italiano\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloitaliano\" placeholder=\"Título italiano\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Descripción italiano\" [(ngModel)]=\"info.descripcionitaliano\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n  </mat-tab-group>\n\n\n  <div class=\"row\" style=\"margin-top:1em;\">\n\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <input matInput type=\"number\" [(ngModel)]=\"info.dia\" placeholder=\"Día de la actividad\" required>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <input matInput [(ngModel)]=\"info.fecha\" [matDatepicker]=\"picker\" placeholder=\"Fecha actividad\">\n        <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n        <mat-datepicker #picker></mat-datepicker>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-6\">\n      <label>Hora Inicio</label>\n      <timepicker placeholder=\"Hora inicio\" [(ngModel)]=\"info.horainicio\" meridians=\"24H\" [showMeridian]=\"false\"></timepicker>\n    </div>\n\n    <div class=\"col-md-6\">\n      <label>Hora Fin</label>\n      <timepicker placeholder=\"Hora inicio\" [(ngModel)]=\"info.horafin\" meridians=\"24H\" [showMeridian]=\"false\"></timepicker>\n    </div>\n\n  </div>\n\n\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.descripcion || !info.dia || !info.fecha || !info.horainicio || !info.horafin\">Guardar actividad</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/calendario/dialogo/modificar/modificar.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/paginas/calendario/dialogo/modificar/modificar.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/paginas/calendario/dialogo/modificar/modificar.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/paginas/calendario/dialogo/modificar/modificar.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ModificarCalendarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarCalendarioComponent", function() { return ModificarCalendarioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ModificarCalendarioComponent = /** @class */ (function () {
    function ModificarCalendarioComponent(db, notify, snackBar, dialogRef, data, _upload) {
        var _this = this;
        this.db = db;
        this.notify = notify;
        this.snackBar = snackBar;
        this.dialogRef = dialogRef;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.imagenes = [];
        this.files = {};
        this.paises = [];
        this.info = data.item;
        this.db.getPaises().subscribe(function (resp) {
            _this.paises = resp;
            console.log(_this.paises);
        });
    }
    ModificarCalendarioComponent.prototype.ngOnInit = function () { };
    ModificarCalendarioComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .update("actividades/" + this.info.id, this.info)
            .then(function () {
            _this.snackBar.open("Actividad modificado", "Cerrar", {
                duration: 2000,
                horizontalPosition: "left"
            });
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error modificando registro", "error");
        });
    };
    ModificarCalendarioComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarCalendarioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar",
            template: __webpack_require__(/*! ./modificar.component.html */ "./src/app/paginas/calendario/dialogo/modificar/modificar.component.html"),
            styles: [__webpack_require__(/*! ./modificar.component.scss */ "./src/app/paginas/calendario/dialogo/modificar/modificar.component.scss")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], ModificarCalendarioComponent);
    return ModificarCalendarioComponent;
}());



/***/ }),

/***/ "./src/app/paginas/calendario/tablas/tabla-calendario/tabla-calendario.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/paginas/calendario/tablas/tabla-calendario/tabla-calendario.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n    <!-- Id Column -->\n    <ng-container matColumnDef=\"id\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>ID</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.id}}</mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"dia\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Día</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.dia}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"fecha\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Fecha</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.fecha | date:'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"horainicio\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Hora inicio</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">\n        <input matInput [ngxTimepicker]=\"horainicio\" [format]=\"24\" [(ngModel)]=\"row.horainicio\" readonly>\n        <ngx-material-timepicker #horainicio></ngx-material-timepicker>\n      </mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"horafin\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Hora fin</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">\n        <input matInput [ngxTimepicker]=\"horafin\" [format]=\"24\" [(ngModel)]=\"row.horafin\" readonly>\n        <ngx-material-timepicker #horafin></ngx-material-timepicker>\n      </mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"titulo\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Titulo</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.titulo}}</mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"actividad\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Actividad</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">\n        <span *ngIf=\"row.actividad == 'inicio'\">Bienvenida, presentación, inicio</span>\n        <span *ngIf=\"row.actividad == 'estado'\">Estado de la congregación, compartiendo en configuraciones</span>\n        <span *ngIf=\"row.actividad == 'identidad'\">Identidad pasionista, formación, Vida de comunidad y visión</span>\n        <span *ngIf=\"row.actividad == 'solidaridad'\">Solidaridad y finanzas</span>\n        <span *ngIf=\"row.actividad == 'comision'\">Comisión juridica</span>\n        <span *ngIf=\"row.actividad == 'nuevos'\">Nuevos puntos, nuevas fronteras, trabajos laicos</span>\n        <span *ngIf=\"row.actividad == 'liderazgo'\">Liderazgo, elecciones</span>\n        <span *ngIf=\"row.actividad == 'propuesta'\">Propuestas formuladas y votaciones</span>\n      </mat-cell>\n\n    </ng-container>\n\n    <!-- Checkbox Column -->\n    <ng-container matColumnDef=\"opciones\">\n      <mat-header-cell *matHeaderCellDef></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n\n          <button mat-menu-item (click)=\"eliminar(row)\">Eliminar</button>\n        </mat-menu>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n\n  <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/calendario/tablas/tabla-calendario/tabla-calendario.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/paginas/calendario/tablas/tabla-calendario/tabla-calendario.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-cell {\n  color: #0091ae;\n  cursor: pointer; }\n\n.mat-cell:hover {\n  text-decoration: underline; }\n\n.mat-column-nombre {\n  flex: 0 0 30%;\n  text-align: left; }\n\n.mat-column-correo {\n  flex: 0 0 30%;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/paginas/calendario/tablas/tabla-calendario/tabla-calendario.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/paginas/calendario/tablas/tabla-calendario/tabla-calendario.component.ts ***!
  \******************************************************************************************/
/*! exports provided: TablaCalendarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablaCalendarioComponent", function() { return TablaCalendarioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../dialogo/modificar/modificar.component */ "./src/app/paginas/calendario/dialogo/modificar/modificar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: '¿Está seguro de eliminar el registro?',
    disableClose: false,
    title: 'Confirmación',
    cancelButton: 'Cancelar',
    acceptButton: 'Aceptar',
    width: '500px',
};
var TablaCalendarioComponent = /** @class */ (function () {
    function TablaCalendarioComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = ["dia", "fecha", "horainicio", "horafin", "titulo", "actividad", "opciones"];
        this.lista = [];
    }
    TablaCalendarioComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    TablaCalendarioComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db
            .colWithIds$("actividades", function (ref) {
            return ref.orderBy("dia", "asc").orderBy("horainicio", "asc");
        })
            .subscribe(function (resp) {
            _this.lista = resp;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    TablaCalendarioComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_7__["ModificarCalendarioComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    TablaCalendarioComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("actividades/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Asistente eliminado");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], TablaCalendarioComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], TablaCalendarioComponent.prototype, "sort", void 0);
    TablaCalendarioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'tabla-calendario',
            template: __webpack_require__(/*! ./tabla-calendario.component.html */ "./src/app/paginas/calendario/tablas/tabla-calendario/tabla-calendario.component.html"),
            styles: [__webpack_require__(/*! ./tabla-calendario.component.scss */ "./src/app/paginas/calendario/tablas/tabla-calendario/tabla-calendario.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_5__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], TablaCalendarioComponent);
    return TablaCalendarioComponent;
}());



/***/ }),

/***/ "./src/app/paginas/cronicas/cronicas.component.html":
/*!**********************************************************!*\
  !*** ./src/app/paginas/cronicas/cronicas.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <button mat-button class=\"btn-ruta\">Crónicas</button>\n\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" (click)=\"crear()\">\n          <mat-icon matListIcon class=\"small\">add</mat-icon>\n          Crear crónica\n        </button>\n      </div>\n\n    </mat-toolbar>\n  </div>\n\n  <tabla-cronicas></tabla-cronicas>\n\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/cronicas/cronicas.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/paginas/cronicas/cronicas.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  min-height: calc(90vh - 60px);\n  padding: 0 1em; }\n  .row .col-md-3 {\n    background: #f5f8fa;\n    border: 1px solid #dfe3eb;\n    padding: 0; }\n  .row .col-md-3 .header {\n      border-bottom: 1px solid #dfe3eb;\n      padding: 16px 16px;\n      font-size: 12px;\n      -webkit-hyphens: none;\n          -ms-hyphens: none;\n              hyphens: none;\n      overflow: hidden !important;\n      text-overflow: ellipsis !important;\n      white-space: nowrap !important;\n      font-weight: 600;\n      text-transform: uppercase;\n      font-family: \"Roboto\"; }\n  .row .col-md-3 .tareas {\n      margin: 1em;\n      min-height: calc(90vh - 120px); }\n  .row .col-md-3 .tareas mat-card {\n        margin-bottom: 1em; }\n  .titulo {\n  margin: 0 !important;\n  color: #0091ae;\n  cursor: pointer; }\n  .titulo:hover {\n  font-weight: 500; }\n"

/***/ }),

/***/ "./src/app/paginas/cronicas/cronicas.component.ts":
/*!********************************************************!*\
  !*** ./src/app/paginas/cronicas/cronicas.component.ts ***!
  \********************************************************/
/*! exports provided: CronicasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CronicasComponent", function() { return CronicasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialogos_crear_cronica_crear_cronica_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dialogos/crear-cronica/crear-cronica.component */ "./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CronicasComponent = /** @class */ (function () {
    function CronicasComponent(db, _dialogService, dialog, snackBar) {
        this.db = db;
        this._dialogService = _dialogService;
        this.dialog = dialog;
        this.snackBar = snackBar;
    }
    CronicasComponent.prototype.ngOnInit = function () { };
    CronicasComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogos_crear_cronica_crear_cronica_component__WEBPACK_IMPORTED_MODULE_4__["CrearCronicaComponent"], {
            width: "60%"
        });
    };
    CronicasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-cronicas",
            template: __webpack_require__(/*! ./cronicas.component.html */ "./src/app/paginas/cronicas/cronicas.component.html"),
            styles: [__webpack_require__(/*! ./cronicas.component.scss */ "./src/app/paginas/cronicas/cronicas.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__["TdDialogService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], CronicasComponent);
    return CronicasComponent;
}());



/***/ }),

/***/ "./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Nuevo cronica</h1>\n\n<div mat-dialog-content class=\"example-container\" style=\"    display: flex;\">\n\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de crónica</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título\">\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n          <mat-option value=\"español\">Español</mat-option>\n          <mat-option value=\"ingles\">Ingles</mat-option>\n          <mat-option value=\"italiano\">Italiano</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <textarea matInput placeholder=\"Resumen\" [(ngModel)]=\"info.resumen\"></textarea>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-12\">\n      <div [froalaEditor] [(froalaModel)]=\"info.contenido\">Ingrese contenido de crónica</div>\n    </div>\n\n  </div>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.resumen || !info.idioma\">Crear cronica</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.ts ***!
  \************************************************************************************/
/*! exports provided: CrearCronicaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearCronicaComponent", function() { return CrearCronicaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var CrearCronicaComponent = /** @class */ (function () {
    function CrearCronicaComponent(db, notify, upload, dialogRef, _loadingService, data, _upload) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.files = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
    }
    CrearCronicaComponent.prototype.ngOnInit = function () { };
    CrearCronicaComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.db
            .add("cronicas", this.info)
            .then(function () {
            _this.notify.update("Crónica guardada", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    CrearCronicaComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearCronicaComponent.prototype.selectEvent = function (file) {
        console.log(file);
        this.fileSelectMsg = file.name;
        this.info.file = file;
    };
    CrearCronicaComponent.prototype.uploadEvent = function (file) {
        this.fileUploadMsg = file.name;
        this.info.file = file;
    };
    CrearCronicaComponent.prototype.cancelEvent = function () {
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.info.file = null;
    };
    CrearCronicaComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    CrearCronicaComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    CrearCronicaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear-cronica",
            template: __webpack_require__(/*! ./crear-cronica.component.html */ "./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.html"),
            styles: [__webpack_require__(/*! ./crear-cronica.component.scss */ "./src/app/paginas/cronicas/dialogos/crear-cronica/crear-cronica.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], CrearCronicaComponent);
    return CrearCronicaComponent;
}());



/***/ }),

/***/ "./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar crónica</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de proyecto</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título\">\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n          <mat-option value=\"español\">Español</mat-option>\n          <mat-option value=\"ingles\">Ingles</mat-option>\n          <mat-option value=\"italiano\">Italiano</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <textarea matInput placeholder=\"Resumen\" [(ngModel)]=\"info.resumen\"></textarea>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-12\">\n      <div [froalaEditor] [(froalaModel)]=\"info.contenido\"></div>\n    </div>\n\n  </div>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo ||  !info.resumen || !info.idioma\">Guardar crónica</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.ts ***!
  \********************************************************************************************/
/*! exports provided: ModificarCronicaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarCronicaComponent", function() { return ModificarCronicaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var ModificarCronicaComponent = /** @class */ (function () {
    function ModificarCronicaComponent(db, notify, upload, dialogRef, _loadingService, data, _upload) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.files = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
        this.info = data.item;
    }
    ModificarCronicaComponent.prototype.ngOnInit = function () { };
    ModificarCronicaComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.db
            .update("cronicas/" + this.info.id, this.info)
            .then(function () {
            _this.notify.update("Crónica guardada", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    ModificarCronicaComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarCronicaComponent.prototype.selectEvent = function (file) {
        console.log(file);
        this.fileSelectMsg = file.name;
        this.info.file = file;
    };
    ModificarCronicaComponent.prototype.uploadEvent = function (file) {
        this.fileUploadMsg = file.name;
        this.info.file = file;
    };
    ModificarCronicaComponent.prototype.cancelEvent = function () {
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.info.file = null;
    };
    ModificarCronicaComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    ModificarCronicaComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    ModificarCronicaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar-cronica",
            template: __webpack_require__(/*! ./modificar-cronica.component.html */ "./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.html"),
            styles: [__webpack_require__(/*! ./modificar-cronica.component.scss */ "./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], ModificarCronicaComponent);
    return ModificarCronicaComponent;
}());



/***/ }),

/***/ "./src/app/paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n    <!-- Id Column -->\n    <ng-container matColumnDef=\"titulo\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Titulo</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.titulo}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"resumen\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Resumen</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.resumen | slice:0:100}} ...</mat-cell>\n    </ng-container>\n\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"idioma\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Idioma</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.idioma}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"createdAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Creado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.createdAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"updatedAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Modificado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.updatedAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n    <!-- Checkbox Column -->\n    <ng-container matColumnDef=\"opciones\">\n      <mat-header-cell *matHeaderCellDef></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n\n          <button mat-menu-item (click)=\"eliminar(row)\">Eliminar</button>\n        </mat-menu>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n\n  <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-cell {\n  color: #0091ae;\n  cursor: pointer; }\n\n.mat-cell:hover {\n  text-decoration: underline; }\n\n.mat-column-nombre {\n  flex: 0 0 30%;\n  text-align: left; }\n\n.mat-column-correo {\n  flex: 0 0 30%;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component.ts ***!
  \************************************************************************************/
/*! exports provided: TablaCronicasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablaCronicasComponent", function() { return TablaCronicasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _dialogos_modificar_cronica_modificar_cronica_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../dialogos/modificar-cronica/modificar-cronica.component */ "./src/app/paginas/cronicas/dialogos/modificar-cronica/modificar-cronica.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: "¿Está seguro de eliminar el registro?",
    disableClose: false,
    title: "Confirmación",
    cancelButton: "Cancelar",
    acceptButton: "Aceptar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var TablaCronicasComponent = /** @class */ (function () {
    function TablaCronicasComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = ["titulo", "resumen", "idioma", "createdAt", "updatedAt", "opciones"];
        this.lista = [];
    }
    TablaCronicasComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    TablaCronicasComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("cronicas").subscribe(function (resp) {
            _this.lista = resp;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    TablaCronicasComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogos_modificar_cronica_modificar_cronica_component__WEBPACK_IMPORTED_MODULE_7__["ModificarCronicaComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    TablaCronicasComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("cronicas/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Crónica eliminada");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], TablaCronicasComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], TablaCronicasComponent.prototype, "sort", void 0);
    TablaCronicasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'tabla-cronicas',
            template: __webpack_require__(/*! ./tabla-cronicas.component.html */ "./src/app/paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component.html"),
            styles: [__webpack_require__(/*! ./tabla-cronicas.component.scss */ "./src/app/paginas/cronicas/tablas/tabla-cronicas/tabla-cronicas.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_5__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], TablaCronicasComponent);
    return TablaCronicasComponent;
}());



/***/ }),

/***/ "./src/app/paginas/documentos/carpetas/carpetas.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/paginas/documentos/carpetas/carpetas.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor\">\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <button mat-button (click)=\"atras(0)\" class=\"btn-ruta\">Documentos</button>\n\n      <span *ngFor=\"let item of rutas\">\n        <mat-icon class=\"arrow_right\" *ngIf=\"padre != 0\">></mat-icon>\n        <button mat-button (click)=\"atras(item.id)\" *ngIf=\"padre != 0\" class=\"btn-ruta\">{{item.nombre}}</button>\n      </span>\n\n    </mat-toolbar>\n\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <span style=\"font-weight: 100;margin-left: 15px;background: #2e3f4f;padding: 2px 10px !important;border-radius: 5px;color:#fff;\" *ngIf=\"padre != 0\">\n        <strong style=\"font-weight: 100; font-size: .8em;\">ID Carpeta:</strong>\n        <span style=\"font-weight: 100; font-size: .8em; margin-left: 10px;\">{{padre}}</span>\n      </span>\n\n      <div class=\"button-row md-button-right\">\n\n\n        <button mat-button class=\"btn-modificar\" (click)=\"ir()\" *ngIf=\"padre != 0\">\n          Modificar\n        </button>\n\n        <button mat-button class=\"btn-crear\" (click)=\"crear()\">\n          Crear carpeta\n        </button>\n\n        <button mat-button class=\"btn-crear documento\" [matMenuTriggerFor]=\"menuCrear\" *ngIf=\"padre != 0\">\n          Cargar documento\n        </button>\n\n        <mat-menu #menuCrear=\"matMenu\">\n          <button mat-menu-item (click)=\"subir()\">\n            Subir archivo\n          </button>\n        </mat-menu>\n\n        <button mat-button class=\"btn-eliminar\" (click)=\"eliminar()\" *ngIf=\"padre != 0\">\n          Eliminar\n        </button>\n      </div>\n    </mat-toolbar>\n\n  </div>\n\n  <p class=\"titulo\" *ngIf=\"lista.length > 0\">Carpetas</p>\n  <div class=\"contenedor-carpetas\" *ngIf=\"lista.length > 0\">\n    <mat-card *ngFor=\"let item of lista\" (click)=\"irSubCarpeta(item)\">\n      {{item.nombre}}\n    </mat-card>\n  </div>\n\n  <p class=\"titulo\" *ngIf=\" documentos.length > 0\">Documentos</p>\n\n  <div class=\"mat-elevation-z8 contenedor-documentos\" *ngIf=\"documentos.length > 0\">\n    <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n      <!-- Name Column -->\n      <ng-container matColumnDef=\"nombre\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre documento</mat-header-cell>\n        <mat-cell *matCellDef=\"let row\">{{row.nombre}}</mat-cell>\n      </ng-container>\n\n      <ng-container matColumnDef=\"idioma\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>idioma</mat-header-cell>\n        <mat-cell *matCellDef=\"let row\">{{row.idioma}}</mat-cell>\n      </ng-container>\n\n      <ng-container matColumnDef=\"type\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Tipo</mat-header-cell>\n        <mat-cell *matCellDef=\"let row\">{{row.type}}</mat-cell>\n      </ng-container>\n\n      <!-- Name Column -->\n      <ng-container matColumnDef=\"createdAt\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Fecha de subida</mat-header-cell>\n        <mat-cell *matCellDef=\"let row\">{{row.createdAt | date: 'dd/MM/yyyy'}}</mat-cell>\n      </ng-container>\n\n      <!-- Checkbox Column -->\n      <ng-container matColumnDef=\"opciones\">\n        <mat-header-cell *matHeaderCellDef></mat-header-cell>\n        <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n          <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n            <mat-icon>&#5010;</mat-icon>\n          </button>\n          <mat-menu #menu=\"matMenu\">\n            <a mat-menu-item [href]=\"row.url\" target=\"blank\">Ver documento</a>\n            <button mat-menu-item (click)=\"irDocumento(row)\">Modificar</button>\n            <button mat-menu-item (click)=\"eliminarDocumento(row)\">Eliminar</button>\n          </mat-menu>\n        </mat-cell>\n      </ng-container>\n\n      <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n      <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n    </mat-table>\n\n    <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n    </mat-paginator>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/documentos/carpetas/carpetas.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/paginas/documentos/carpetas/carpetas.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  font-family: \"Roboto\"; }\n\n.mat-card {\n  margin: .2em .5em .2em 0;\n  background: #fff;\n  color: #2e3f4f;\n  border: 1px solid rgba(46, 63, 79, 0.1);\n  text-shadow: 1px 0px #636161;\n  font-weight: 300;\n  cursor: pointer;\n  padding: 1em; }\n\n.mat-card:hover {\n  background: rgba(46, 63, 79, 0.09); }\n\n.carpetas {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 0 auto;\n  position: relative;\n  margin-bottom: 1em;\n  background: #fff; }\n\n.contenedor-carpetas {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 0 auto;\n  position: relative;\n  margin-bottom: 1em;\n  background: #fff;\n  padding: 1em; }\n\np.titulo {\n  width: 100%;\n  color: rgba(46, 63, 79, 0.68);\n  padding-left: 1em; }\n\n.atras-btn {\n  cursor: pointer; }\n\n.atras-btn:hover {\n  text-decoration: underline; }\n\n.contenedor-documentos {\n  margin: 2em 0em 0em 1em; }\n"

/***/ }),

/***/ "./src/app/paginas/documentos/carpetas/carpetas.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/paginas/documentos/carpetas/carpetas.component.ts ***!
  \*******************************************************************/
/*! exports provided: CarpetasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarpetasComponent", function() { return CarpetasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dialogo_dialogo_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dialogo/dialogo.component */ "./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../dialogo/crear/crear.component */ "./src/app/paginas/documentos/dialogo/crear/crear.component.ts");
/* harmony import */ var _dialogo_modificar_carpeta_modificar_carpeta_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./dialogo/modificar-carpeta/modificar-carpeta.component */ "./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.ts");
/* harmony import */ var _dialogo_modificar_documento_modificar_documento_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../dialogo/modificar-documento/modificar-documento.component */ "./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














// Configuración de confirm eliminar
var configDel = {
    message: "¿Está seguro de eliminar el documento?",
    disableClose: false,
    title: "Confirmación",
    cancelButton: "Cancelar",
    acceptButton: "Aceptar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var CarpetasComponent = /** @class */ (function () {
    function CarpetasComponent(db, notify, dialog, _router, _route, snackBar, location, _dialogService) {
        var _this = this;
        this.db = db;
        this.notify = notify;
        this.dialog = dialog;
        this._router = _router;
        this._route = _route;
        this.snackBar = snackBar;
        this.location = location;
        this._dialogService = _dialogService;
        this.displayedColumns = ["nombre", "idioma", "type", "createdAt", "opciones"];
        this.lista = [];
        this.padre = 0;
        this.rutas = [];
        this.listaRutas = [];
        this.documentos = [];
        this._router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
                _this._route.params.subscribe(function (params) {
                    _this.padre = params.padre;
                    _this.cargarInfo();
                    _this.cargarLista();
                });
            }
        });
    }
    CarpetasComponent.prototype.cargarRutas = function () {
        var _this = this;
        var rutasqry = this.db.colWithIds$("rutas").subscribe(function (rutas) {
            _this.listaRutas = rutas;
            rutasqry.unsubscribe();
        });
    };
    CarpetasComponent.prototype.cargarInfo = function () {
        var _this = this;
        var carpetasqry = this.db
            .colWithIds$("rutas", function (ref) { return ref.where("padre", "==", _this.padre); })
            .subscribe(function (resp) {
            _this.lista = resp;
            if (_this.padre != 0) {
                _this.rutas = [];
                _this.armarRuta(_this.padre);
            }
            carpetasqry.unsubscribe();
        });
    };
    CarpetasComponent.prototype.armarRuta = function (padre) {
        var _this = this;
        if (this.padreant == padre)
            return;
        this.padreant = padre;
        if (this.padre != 0 && this.listaRutas.length == 0) {
            var rutasqry_1 = this.db.colWithIds$("rutas").subscribe(function (rutas) {
                _this.listaRutas = rutas;
                rutasqry_1.unsubscribe();
                var ruta = _this.listaRutas.find(function (x) { return x.id == padre; });
                if (ruta) {
                    if (ruta.padre)
                        _this.armarRuta(ruta.padre);
                    _this.rutas.push(ruta);
                }
            });
        }
        else if (this.padre != 0 && this.listaRutas.length > 0) {
            console.log('En el segundo', padre);
            var ruta = this.listaRutas.find(function (x) { return x.id == padre; });
            if (ruta) {
                if (ruta.padre)
                    this.armarRuta(ruta.padre);
                this.rutas.push(ruta);
            }
        }
    };
    CarpetasComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("documentos", function (ref) { return ref.where('ruta', '==', _this.padre); }).subscribe(function (resp) {
            _this.documentos = resp;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](_this.documentos);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    CarpetasComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogo_modificar_carpeta_modificar_carpeta_component__WEBPACK_IMPORTED_MODULE_10__["ModificarCarpetaComponent"], {
            width: "60%",
            data: { item: this.padre }
        });
        dialogRef.afterClosed().subscribe(function (result) { });
    };
    CarpetasComponent.prototype.irDocumento = function (item) {
        var dialogRef = this._dialogService.open(_dialogo_modificar_documento_modificar_documento_component__WEBPACK_IMPORTED_MODULE_11__["ModificarDocumentoComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) { });
    };
    CarpetasComponent.prototype.eliminarDocumento = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("documentos/" + item.id)
                    .then(function (res) {
                    _this.notify.okMessage("Documento eliminado");
                })
                    .catch(function (err) {
                    _this.notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    CarpetasComponent.prototype.crear = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_dialogo_dialogo_component__WEBPACK_IMPORTED_MODULE_1__["CrearCarpetaDialogoComponent"], {
            width: "60%",
            data: { padre: this.padre }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            _this.lista = [];
            _this.rutas = [];
            _this.listaRutas = [];
            _this.cargarInfo();
        });
    };
    CarpetasComponent.prototype.irSubCarpeta = function (item) {
        this._router.navigate(["carpetas/" + item.id]);
    };
    CarpetasComponent.prototype.atras = function (id) {
        if (this.padre != 0) {
            this._router.navigate(["carpetas/" + id]);
        }
    };
    CarpetasComponent.prototype.eliminar = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default()({
            title: "Confirmación de borrado",
            text: "¿Está seguro de borrar esta carpeta y todo su contenido?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar"
        }).then(function (result) {
            if (result.value) {
                _this.db.delete("rutas/" + _this.padre).then(function () {
                    _this.snackBar.open("Carpeta eliminada", "Cerrar", {
                        duration: 2000,
                        horizontalPosition: "left"
                    });
                    if (_this.rutas.length > 1) {
                        _this._router.navigate([
                            "carpetas/" + _this.rutas[_this.rutas.length - 2].id
                        ]);
                    }
                    else {
                        _this.rutas = [];
                        _this._router.navigate([
                            "carpetas/0"
                        ]);
                    }
                });
            }
        });
    };
    CarpetasComponent.prototype.subir = function () {
        var dialogRef = this.dialog.open(_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_9__["CrearDocumentoComponent"], {
            width: "60%",
            data: { padre: this.padre }
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], CarpetasComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], CarpetasComponent.prototype, "sort", void 0);
    CarpetasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-carpetas",
            template: __webpack_require__(/*! ./carpetas.component.html */ "./src/app/paginas/documentos/carpetas/carpetas.component.html"),
            styles: [__webpack_require__(/*! ./carpetas.component.scss */ "./src/app/paginas/documentos/carpetas/carpetas.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_3__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_7__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_8__["TdDialogService"]])
    ], CarpetasComponent);
    return CarpetasComponent;
}());



/***/ }),

/***/ "./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Nueva carpeta</h1>\n\n<div mat-dialog-content class=\"example-container\">\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de carpeta</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n  </div>\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre en Español\" required>\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombreingles\" placeholder=\"Nombre en Inglés\">\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombreitaliano\" placeholder=\"Nombre en Italiano\">\n  </mat-form-field>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre\">Crear carpeta</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.ts ***!
  \**************************************************************************/
/*! exports provided: CrearCarpetaDialogoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearCarpetaDialogoComponent", function() { return CrearCarpetaDialogoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var CrearCarpetaDialogoComponent = /** @class */ (function () {
    function CrearCarpetaDialogoComponent(db, notify, upload, dialogRef, _loadingService, data) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this.info = {};
        this.files = {};
    }
    CrearCarpetaDialogoComponent.prototype.ngOnInit = function () { };
    CrearCarpetaDialogoComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.info.padre = this.data.padre;
        this.db
            .add("rutas", this.info)
            .then(function () {
            _this.notify.update("Carpeta creada", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando carpeta", "error");
        });
    };
    CrearCarpetaDialogoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearCarpetaDialogoComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this.upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    CrearCarpetaDialogoComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    CrearCarpetaDialogoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-dialogo",
            template: __webpack_require__(/*! ./dialogo.component.html */ "./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.html"),
            styles: [__webpack_require__(/*! ./dialogo.component.scss */ "./src/app/paginas/documentos/carpetas/dialogo/dialogo.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object])
    ], CrearCarpetaDialogoComponent);
    return CrearCarpetaDialogoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.html":
/*!********************************************************************************************************!*\
  !*** ./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar carpeta</h1>\n\n<div mat-dialog-content class=\"example-container\">\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\"\n          class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de carpeta</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n  </div>\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre en Español\" required>\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombreingles\" placeholder=\"Nombre en Inglés\">\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombreitaliano\" placeholder=\"Nombre en Italiano\">\n  </mat-form-field>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre\">Crear carpeta</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.scss ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: ModificarCarpetaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarCarpetaComponent", function() { return ModificarCarpetaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/upload.service */ "./src/app/core/upload.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ModificarCarpetaComponent = /** @class */ (function () {
    function ModificarCarpetaComponent(db, notify, snackBar, upload, dialogRef, data) {
        var _this = this;
        this.db = db;
        this.notify = notify;
        this.snackBar = snackBar;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this.data = data;
        this.info = {};
        this.files = {};
        this.id = data.item;
        var cons = this.db.docWithRefs$("rutas/" + data.item).subscribe(function (resp) {
            _this.info = resp;
            cons.unsubscribe();
            console.log(resp);
        });
        //this.info = data.item;
    }
    ModificarCarpetaComponent.prototype.ngOnInit = function () { };
    ModificarCarpetaComponent.prototype.guardar = function () {
        var _this = this;
        console.log('Aqui');
        this.db
            .update("rutas/" + this.id, this.info)
            .then(function () {
            _this.snackBar.open("Carpeta modificado", "Cerrar", {
                duration: 2000,
                horizontalPosition: "left"
            });
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error modificando registro", "error");
        });
    };
    ModificarCarpetaComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarCarpetaComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this.upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    ModificarCarpetaComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    ModificarCarpetaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar-carpeta",
            template: __webpack_require__(/*! ./modificar-carpeta.component.html */ "./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.html"),
            styles: [__webpack_require__(/*! ./modificar-carpeta.component.scss */ "./src/app/paginas/documentos/carpetas/dialogo/modificar-carpeta/modificar-carpeta.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_3__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], ModificarCarpetaComponent);
    return ModificarCarpetaComponent;
}());



/***/ }),

/***/ "./src/app/paginas/documentos/dialogo/crear/crear.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/paginas/documentos/dialogo/crear/crear.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Nuevo documento</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n          <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n            <mat-icon>add</mat-icon>\n            <p>Foto de documento</p>\n          </td-file-input>\n        </div>\n\n\n        <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n          <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n            <mat-icon>delete</mat-icon>\n          </button>\n        </div>\n      </div>\n  </div>\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre del documento\" required>\n  </mat-form-field>\n\n  <mat-form-field>\n    <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n      <mat-option value=\"español\">Español</mat-option>\n      <mat-option value=\"ingles\">Ingles</mat-option>\n      <mat-option value=\"italiano\">Italiano</mat-option>\n    </mat-select>\n  </mat-form-field>\n\n  <td-file-upload #singleFileUpload [(ngModel)]=\"info.file\" (select)=\"selectEvent($event)\" (upload)=\"uploadEvent($event)\"\n    (cancel)=\"cancelEvent()\" required>\n    <mat-icon>file_upload</mat-icon>\n    <span>{{ singleFileUpload.value?.name }}</span>\n    <ng-template td-file-input-label>\n      <mat-icon>attach_file</mat-icon>\n      <span>\n        Seleccione archivo\n        <span [hidden]=\"!singleFileUpload?.required\">*</span>\n      </span>\n    </ng-template>\n  </td-file-upload>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre || !info.file || !info.idioma\">Crear documento</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/documentos/dialogo/crear/crear.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/paginas/documentos/dialogo/crear/crear.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/documentos/dialogo/crear/crear.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/paginas/documentos/dialogo/crear/crear.component.ts ***!
  \*********************************************************************/
/*! exports provided: CrearDocumentoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearDocumentoComponent", function() { return CrearDocumentoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var CrearDocumentoComponent = /** @class */ (function () {
    function CrearDocumentoComponent(db, notify, upload, dialogRef, _loadingService, data, _upload) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.files = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
    }
    CrearDocumentoComponent.prototype.ngOnInit = function () { };
    CrearDocumentoComponent.prototype.guardar = function () {
        var _this = this;
        var file = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](this.info.file);
        this._loadingService.register();
        this.upload
            .uploadDocumento(file)
            .then(function (url) {
            _this.info.type = _this.info.file.type;
            delete _this.info.file;
            _this.info.url = url;
            _this.info.ruta = _this.data.padre;
            _this.db
                .add("documentos", _this.info)
                .then(function () {
                _this.notify.update("Documento guardado", "success");
                _this._loadingService.resolve();
                _this.cerrar();
            })
                .catch(function (err) {
                _this._loadingService.resolve();
                _this.notify.update("Error creando registro", "error");
            });
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Ocurrio un error subiendo el documento", "error");
        });
    };
    CrearDocumentoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearDocumentoComponent.prototype.selectEvent = function (file) {
        console.log(file);
        this.fileSelectMsg = file.name;
        this.info.file = file;
    };
    CrearDocumentoComponent.prototype.uploadEvent = function (file) {
        this.fileUploadMsg = file.name;
        this.info.file = file;
    };
    CrearDocumentoComponent.prototype.cancelEvent = function () {
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.info.file = null;
    };
    CrearDocumentoComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    CrearDocumentoComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    CrearDocumentoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear",
            template: __webpack_require__(/*! ./crear.component.html */ "./src/app/paginas/documentos/dialogo/crear/crear.component.html"),
            styles: [__webpack_require__(/*! ./crear.component.scss */ "./src/app/paginas/documentos/dialogo/crear/crear.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], CrearDocumentoComponent);
    return CrearDocumentoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar documento</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de documento</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n  </div>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre del documento\" required>\n  </mat-form-field>\n\n  <mat-form-field>\n    <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n      <mat-option value=\"español\">Español</mat-option>\n      <mat-option value=\"ingles\">Ingles</mat-option>\n      <mat-option value=\"italiano\">Italiano</mat-option>\n    </mat-select>\n  </mat-form-field>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre || !info.idioma\">Crear documento</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: ModificarDocumentoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarDocumentoComponent", function() { return ModificarDocumentoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var ModificarDocumentoComponent = /** @class */ (function () {
    function ModificarDocumentoComponent(db, notify, upload, dialogRef, _loadingService, data, _upload) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.files = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
        this.info = data.item;
    }
    ModificarDocumentoComponent.prototype.ngOnInit = function () { };
    ModificarDocumentoComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.db
            .update("documentos/" + this.info.id, this.info)
            .then(function () {
            _this.notify.update("Documento guardado", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    ModificarDocumentoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarDocumentoComponent.prototype.selectEvent = function (file) {
        console.log(file);
        this.fileSelectMsg = file.name;
        this.info.file = file;
    };
    ModificarDocumentoComponent.prototype.uploadEvent = function (file) {
        this.fileUploadMsg = file.name;
        this.info.file = file;
    };
    ModificarDocumentoComponent.prototype.cancelEvent = function () {
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.info.file = null;
    };
    ModificarDocumentoComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    ModificarDocumentoComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    ModificarDocumentoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar-documento",
            template: __webpack_require__(/*! ./modificar-documento.component.html */ "./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.html"),
            styles: [__webpack_require__(/*! ./modificar-documento.component.scss */ "./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], ModificarDocumentoComponent);
    return ModificarDocumentoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/documentos/documentos.component.html":
/*!**************************************************************!*\
  !*** ./src/app/paginas/documentos/documentos.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <span class=\"titulo-seccion\">Documentos</span>\n      <div class=\"button-row md-button-right\">\n        <button mat-button [matMenuTriggerFor]=\"menuCrear\"  class=\"btn-crear\">\n          Crear documento\n          <mat-icon class=\"example-icon\">arrow_drop_down</mat-icon>\n        </button>\n        <mat-menu #menuCrear=\"matMenu\">\n          <button mat-menu-item (click)=\"subir()\">\n             Subir archivo\n          </button>\n        </mat-menu>\n      </div>\n    </mat-toolbar>\n  </div>\n  <tabla-documentos></tabla-documentos>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/documentos/documentos.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/paginas/documentos/documentos.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/paginas/documentos/documentos.component.ts":
/*!************************************************************!*\
  !*** ./src/app/paginas/documentos/documentos.component.ts ***!
  \************************************************************/
/*! exports provided: DocumentosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentosComponent", function() { return DocumentosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dialogo/crear/crear.component */ "./src/app/paginas/documentos/dialogo/crear/crear.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DocumentosComponent = /** @class */ (function () {
    function DocumentosComponent(dialog) {
        this.dialog = dialog;
    }
    DocumentosComponent.prototype.ngOnInit = function () { };
    DocumentosComponent.prototype.subir = function () {
        var dialogRef = this.dialog.open(_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_1__["CrearDocumentoComponent"], {
            width: "60%"
        });
    };
    DocumentosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-documentos",
            template: __webpack_require__(/*! ./documentos.component.html */ "./src/app/paginas/documentos/documentos.component.html"),
            styles: [__webpack_require__(/*! ./documentos.component.scss */ "./src/app/paginas/documentos/documentos.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], DocumentosComponent);
    return DocumentosComponent;
}());



/***/ }),

/***/ "./src/app/paginas/documentos/tabla-documentos/tabla-documentos.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/paginas/documentos/tabla-documentos/tabla-documentos.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/paginas/documentos/tabla-documentos/tabla-documentos.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/paginas/documentos/tabla-documentos/tabla-documentos.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"nombre\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre documento</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.nombre}}</mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"idioma\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>idioma</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.idioma}}</mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"type\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Tipo</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.type}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"createdAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Fecha de subida</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.createdAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n    <!-- Checkbox Column -->\n    <ng-container matColumnDef=\"opciones\">\n      <mat-header-cell *matHeaderCellDef></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n          <a mat-menu-item [href]=\"row.url\" target=\"blank\">Ver documento</a>\n          <button mat-menu-item (click)=\"ir(row)\">Modificar</button>\n          <button mat-menu-item  (click)=\"eliminar(row)\">Eliminar</button>\n        </mat-menu>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n\n  <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/documentos/tabla-documentos/tabla-documentos.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/paginas/documentos/tabla-documentos/tabla-documentos.component.ts ***!
  \***********************************************************************************/
/*! exports provided: TablaDocumentosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablaDocumentosComponent", function() { return TablaDocumentosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _dialogo_modificar_documento_modificar_documento_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../dialogo/modificar-documento/modificar-documento.component */ "./src/app/paginas/documentos/dialogo/modificar-documento/modificar-documento.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: "¿Está seguro de eliminar el documento?",
    disableClose: false,
    title: "Confirmación",
    cancelButton: "Cancelar",
    acceptButton: "Aceptar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var TablaDocumentosComponent = /** @class */ (function () {
    function TablaDocumentosComponent(db, _loadingService, _dialogService, _notify, _router) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.displayedColumns = ["nombre", "idioma", "type", "createdAt", "opciones"];
        this.lista = [];
    }
    TablaDocumentosComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    TablaDocumentosComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("documentos").subscribe(function (resp) {
            _this.lista = resp;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    TablaDocumentosComponent.prototype.ir = function (item) {
        var dialogRef = this._dialogService.open(_dialogo_modificar_documento_modificar_documento_component__WEBPACK_IMPORTED_MODULE_7__["ModificarDocumentoComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) { });
    };
    TablaDocumentosComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("documentos/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Documento eliminado");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], TablaDocumentosComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], TablaDocumentosComponent.prototype, "sort", void 0);
    TablaDocumentosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "tabla-documentos",
            template: __webpack_require__(/*! ./tabla-documentos.component.html */ "./src/app/paginas/documentos/tabla-documentos/tabla-documentos.component.html"),
            styles: [__webpack_require__(/*! ./tabla-documentos.component.css */ "./src/app/paginas/documentos/tabla-documentos/tabla-documentos.component.css")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_5__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], TablaDocumentosComponent);
    return TablaDocumentosComponent;
}());



/***/ }),

/***/ "./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Nueva encuesta</h1>\n\n<div mat-dialog-content class=\"example-container\" style=\"    display: flex;\">\n\n\n\n  <mat-tab-group>\n    <mat-tab label=\"Español\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título pregunta\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Pregunta\" [(ngModel)]=\"info.pregunta\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n    <mat-tab label=\"Ingles\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloingles\" placeholder=\"Título ingles\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Pregunta ingles\" [(ngModel)]=\"info.preguntaingles\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n    <mat-tab label=\"Italiano\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloitaliano\" placeholder=\"Título italiano\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Pregunta italiano\" [(ngModel)]=\"info.preguntaitaliano\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n  </mat-tab-group>\n\n\n\n  <mat-form-field>\n    <mat-select placeholder=\"Estado\" required [(ngModel)]=\"info.estado\">\n      <mat-option value=\"Abierta\">Abierta</mat-option>\n      <mat-option value=\"Finalizada\">Finalizada</mat-option>\n    </mat-select>\n  </mat-form-field>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.pregunta || !info.estado\">Crear encuesta</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.ts ***!
  \***************************************************************************************/
/*! exports provided: CrearEncuestaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearEncuestaComponent", function() { return CrearEncuestaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var CrearEncuestaComponent = /** @class */ (function () {
    function CrearEncuestaComponent(db, notify, upload, dialogRef, _loadingService, data, _upload) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.files = {};
    }
    CrearEncuestaComponent.prototype.ngOnInit = function () { };
    CrearEncuestaComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.info.si = 0;
        this.info.no = 0;
        this.db
            .add("encuestas", this.info)
            .then(function () {
            _this.notify.update("Encuesta guardada", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    CrearEncuestaComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearEncuestaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear-encuesta",
            template: __webpack_require__(/*! ./crear-encuesta.component.html */ "./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.html"),
            styles: [__webpack_require__(/*! ./crear-encuesta.component.scss */ "./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], CrearEncuestaComponent);
    return CrearEncuestaComponent;
}());



/***/ }),

/***/ "./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar encuesta</h1>\n\n<div mat-dialog-content class=\"example-container\" style=\"    display: flex;\">\n\n\n\n  <mat-tab-group>\n    <mat-tab label=\"Español\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título pregunta\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Pregunta\" [(ngModel)]=\"info.pregunta\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n    <mat-tab label=\"Ingles\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloingles\" placeholder=\"Título ingles\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Pregunta ingles\" [(ngModel)]=\"info.preguntaingles\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n    <mat-tab label=\"Italiano\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloitaliano\" placeholder=\"Título italiano\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Pregunta italiano\" [(ngModel)]=\"info.preguntaitaliano\"></textarea>\n      </mat-form-field>\n    </mat-tab>\n  </mat-tab-group>\n\n\n\n  <mat-form-field>\n    <mat-select placeholder=\"Estado\" required [(ngModel)]=\"info.estado\">\n      <mat-option value=\"Abierta\">Abierta</mat-option>\n      <mat-option value=\"Finalizada\">Finalizada</mat-option>\n    </mat-select>\n  </mat-form-field>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.pregunta || !info.estado\">Guardar encuesta</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: ModificarEncuestaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarEncuestaComponent", function() { return ModificarEncuestaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var ModificarEncuestaComponent = /** @class */ (function () {
    function ModificarEncuestaComponent(db, notify, snackBar, dialogRef, data) {
        this.db = db;
        this.notify = notify;
        this.snackBar = snackBar;
        this.dialogRef = dialogRef;
        this.data = data;
        this.info = {};
        this.info = data.item;
    }
    ModificarEncuestaComponent.prototype.ngOnInit = function () { };
    ModificarEncuestaComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .update("encuestas/" + this.info.id, this.info)
            .then(function () {
            _this.snackBar.open("Encuesta modificada", "Cerrar", {
                duration: 2000,
                horizontalPosition: "left"
            });
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error modificando registro", "error");
        });
    };
    ModificarEncuestaComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarEncuestaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar-encuesta",
            template: __webpack_require__(/*! ./modificar-encuesta.component.html */ "./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.html"),
            styles: [__webpack_require__(/*! ./modificar-encuesta.component.scss */ "./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.scss")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], Object])
    ], ModificarEncuestaComponent);
    return ModificarEncuestaComponent;
}());



/***/ }),

/***/ "./src/app/paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component.html":
/*!**************************************************************************************************!*\
  !*** ./src/app/paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n    <!-- Id Column -->\n    <ng-container matColumnDef=\"titulo\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Titulo</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.titulo}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"pregunta\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Pregunta</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.pregunta}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"si\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Total Si</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.si}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"no\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Total No</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.no}}</mat-cell>\n    </ng-container>\n\n    <ng-container matColumnDef=\"blanco\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Total blanco</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.blanco}}</mat-cell>\n    </ng-container>\n\n\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"estado\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Estado</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.estado}}</mat-cell>\n    </ng-container>\n\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"createdAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Creado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.createdAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"updatedAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Modificado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.updatedAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n    <!-- Checkbox Column -->\n    <ng-container matColumnDef=\"opciones\">\n      <mat-header-cell *matHeaderCellDef></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n\n          <button mat-menu-item (click)=\"eliminar(row)\">Eliminar</button>\n          <button mat-menu-item (click)=\"cambiarEstado(row)\">{{(row.estado=='Abierta')?'Finalizar':'Reactivar'}}</button>\n        </mat-menu>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n\n  <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component.scss":
/*!**************************************************************************************************!*\
  !*** ./src/app/paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component.scss ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-cell {\n  color: #0091ae;\n  cursor: pointer; }\n\n.mat-cell:hover {\n  text-decoration: underline; }\n\n.mat-column-pregunta {\n  flex: 0 0 30%;\n  text-align: left; }\n\n.mat-column-correo {\n  flex: 0 0 30%;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component.ts ***!
  \************************************************************************************************/
/*! exports provided: TablaEncuestasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablaEncuestasComponent", function() { return TablaEncuestasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _modificar_encuesta_modificar_encuesta_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../modificar-encuesta/modificar-encuesta.component */ "./src/app/paginas/encuestas/dialogos/modificar-encuesta/modificar-encuesta.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: "¿Está seguro de eliminar el registro?",
    disableClose: false,
    title: "Confirmación",
    cancelButton: "Cancelar",
    acceptButton: "Aceptar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var TablaEncuestasComponent = /** @class */ (function () {
    function TablaEncuestasComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = ["titulo", "pregunta", "si", "no", "blanco", "estado", "createdAt", "updatedAt", "opciones"];
        this.lista = [];
    }
    TablaEncuestasComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    TablaEncuestasComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("encuestas").subscribe(function (resp) {
            _this.lista = resp;
            _this.lista.forEach(function (element) {
                var respuestas = _this.db.colWithIds$("respuestas-votaciones", function (ref) { return ref.where('encuestaid', '==', element.id); }).subscribe(function (respu) {
                    var respsi = 0;
                    var respno = 0;
                    var respblanco = 0;
                    respu.forEach(function (res) {
                        if (res.respuesta == 'si') {
                            respsi += 1;
                        }
                        else if (res.respuesta == 'no') {
                            respno += 1;
                        }
                        else if (res.respuesta == 'blanco') {
                            respblanco += 1;
                        }
                    });
                    element.si = respsi;
                    element.no = respno;
                    element.blanco = respblanco;
                });
            });
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    TablaEncuestasComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_modificar_encuesta_modificar_encuesta_component__WEBPACK_IMPORTED_MODULE_7__["ModificarEncuestaComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    TablaEncuestasComponent.prototype.cambiarEstado = function (item) {
        var _this = this;
        if (item.estado == 'Abierta') {
            item.estado = 'Finalizada';
        }
        else {
            item.estado = 'Abierta';
        }
        this._dialogService
            .openConfirm({
            message: "\u00BFEst\u00E1 seguro de cambiar esta encuesta a " + item.estado,
            disableClose: false,
            title: "Confirmación",
            cancelButton: "Cancelar",
            acceptButton: "Aceptar",
            width: "500px" //OPTIONAL, defaults to 400px
        })
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db.update("encuestas/" + item.id, {
                    estado: item.estado
                }).then(function () {
                    _this._notify.okMessage("Encuesta " + item.estado);
                });
            }
            else {
                if (item.estado == 'Abierta') {
                    item.estado = 'Finalizada';
                }
                else {
                    item.estado = 'Abierta';
                }
            }
        });
    };
    TablaEncuestasComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("encuestas/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Encuesta eliminada");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], TablaEncuestasComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], TablaEncuestasComponent.prototype, "sort", void 0);
    TablaEncuestasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'tabla-encuestas',
            template: __webpack_require__(/*! ./tabla-encuestas.component.html */ "./src/app/paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component.html"),
            styles: [__webpack_require__(/*! ./tabla-encuestas.component.scss */ "./src/app/paginas/encuestas/dialogos/tablas/tabla-encuestas/tabla-encuestas.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_5__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], TablaEncuestasComponent);
    return TablaEncuestasComponent;
}());



/***/ }),

/***/ "./src/app/paginas/encuestas/encuestas.component.html":
/*!************************************************************!*\
  !*** ./src/app/paginas/encuestas/encuestas.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <button mat-button class=\"btn-ruta\">Encuestas</button>\n\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" (click)=\"crear()\">\n          <mat-icon matListIcon class=\"small\">add</mat-icon>\n          Crear encuesta\n        </button>\n      </div>\n\n    </mat-toolbar>\n  </div>\n\n  <tabla-encuestas></tabla-encuestas>\n\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/encuestas.component.scss":
/*!************************************************************!*\
  !*** ./src/app/paginas/encuestas/encuestas.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  min-height: calc(90vh - 60px);\n  padding: 0 1em; }\n  .row .col-md-3 {\n    background: #f5f8fa;\n    border: 1px solid #dfe3eb;\n    padding: 0; }\n  .row .col-md-3 .header {\n      border-bottom: 1px solid #dfe3eb;\n      padding: 16px 16px;\n      font-size: 12px;\n      -webkit-hyphens: none;\n          -ms-hyphens: none;\n              hyphens: none;\n      overflow: hidden !important;\n      text-overflow: ellipsis !important;\n      white-space: nowrap !important;\n      font-weight: 600;\n      text-transform: uppercase;\n      font-family: \"Roboto\"; }\n  .row .col-md-3 .tareas {\n      margin: 1em;\n      min-height: calc(90vh - 120px); }\n  .row .col-md-3 .tareas mat-card {\n        margin-bottom: 1em; }\n  .titulo {\n  margin: 0 !important;\n  color: #0091ae;\n  cursor: pointer; }\n  .titulo:hover {\n  font-weight: 500; }\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/encuestas.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/paginas/encuestas/encuestas.component.ts ***!
  \**********************************************************/
/*! exports provided: EncuestasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EncuestasComponent", function() { return EncuestasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialogos_crear_encuesta_crear_encuesta_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dialogos/crear-encuesta/crear-encuesta.component */ "./src/app/paginas/encuestas/dialogos/crear-encuesta/crear-encuesta.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EncuestasComponent = /** @class */ (function () {
    function EncuestasComponent(db, _dialogService, dialog, snackBar) {
        this.db = db;
        this._dialogService = _dialogService;
        this.dialog = dialog;
        this.snackBar = snackBar;
    }
    EncuestasComponent.prototype.ngOnInit = function () { };
    EncuestasComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogos_crear_encuesta_crear_encuesta_component__WEBPACK_IMPORTED_MODULE_4__["CrearEncuestaComponent"], {
            width: "60%"
        });
    };
    EncuestasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-encuestas",
            template: __webpack_require__(/*! ./encuestas.component.html */ "./src/app/paginas/encuestas/encuestas.component.html"),
            styles: [__webpack_require__(/*! ./encuestas.component.scss */ "./src/app/paginas/encuestas/encuestas.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__["TdDialogService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], EncuestasComponent);
    return EncuestasComponent;
}());



/***/ }),

/***/ "./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Nuevo foro</h1>\n\n<div mat-dialog-content class=\"example-container\" style=\"    display: flex;\">\n\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de foro</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título\">\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n          <mat-option value=\"español\">Español</mat-option>\n          <mat-option value=\"ingles\">Ingles</mat-option>\n          <mat-option value=\"italiano\">Italiano</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <textarea matInput placeholder=\"Objetivo\" [(ngModel)]=\"info.objetivo\"></textarea>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-12\">\n      <div [froalaEditor] [(froalaModel)]=\"info.contenido\">Ingrese la introducción del foro</div>\n    </div>\n\n  </div>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.objetivo || !info.idioma\">Crear foro</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.ts ***!
  \***************************************************************************/
/*! exports provided: CrearForoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearForoComponent", function() { return CrearForoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var CrearForoComponent = /** @class */ (function () {
    function CrearForoComponent(db, notify, upload, dialogRef, _loadingService, data, _upload) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.files = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
    }
    CrearForoComponent.prototype.ngOnInit = function () { };
    CrearForoComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.db
            .add("foros", this.info)
            .then(function () {
            _this.notify.update("Hilo guardado", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    CrearForoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearForoComponent.prototype.selectEvent = function (file) {
        console.log(file);
        this.fileSelectMsg = file.name;
        this.info.file = file;
    };
    CrearForoComponent.prototype.uploadEvent = function (file) {
        this.fileUploadMsg = file.name;
        this.info.file = file;
    };
    CrearForoComponent.prototype.cancelEvent = function () {
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.info.file = null;
    };
    CrearForoComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    CrearForoComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    CrearForoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear-foro",
            template: __webpack_require__(/*! ./crear-foro.component.html */ "./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.html"),
            styles: [__webpack_require__(/*! ./crear-foro.component.scss */ "./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], CrearForoComponent);
    return CrearForoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar foro</h1>\n\n<div mat-dialog-content class=\"example-container\" style=\"    display: flex;\">\n\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de foro</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título\">\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n          <mat-option value=\"español\">Español</mat-option>\n          <mat-option value=\"ingles\">Ingles</mat-option>\n          <mat-option value=\"italiano\">Italiano</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <textarea matInput placeholder=\"Objetivo\" [(ngModel)]=\"info.objetivo\"></textarea>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-12\">\n      <div [froalaEditor] [(froalaModel)]=\"info.contenido\">Ingrese la introducción del foro</div>\n    </div>\n\n  </div>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.objetivo || !info.idioma\">Guardar foro</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.ts ***!
  \***********************************************************************************/
/*! exports provided: ModificarForoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarForoComponent", function() { return ModificarForoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var ModificarForoComponent = /** @class */ (function () {
    function ModificarForoComponent(db, notify, upload, dialogRef, _loadingService, data, _upload) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.files = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
        this.info = data.item;
    }
    ModificarForoComponent.prototype.ngOnInit = function () { };
    ModificarForoComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        console.log('guardo');
        this.db
            .update("foros/" + this.info.id, this.info)
            .then(function () {
            _this.notify.update("Foro guardado", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    ModificarForoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarForoComponent.prototype.selectEvent = function (file) {
        console.log(file);
        this.fileSelectMsg = file.name;
        this.info.file = file;
    };
    ModificarForoComponent.prototype.uploadEvent = function (file) {
        this.fileUploadMsg = file.name;
        this.info.file = file;
    };
    ModificarForoComponent.prototype.cancelEvent = function () {
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.info.file = null;
    };
    ModificarForoComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    ModificarForoComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    ModificarForoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar-foro",
            template: __webpack_require__(/*! ./modificar-foro.component.html */ "./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.html"),
            styles: [__webpack_require__(/*! ./modificar-foro.component.scss */ "./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], ModificarForoComponent);
    return ModificarForoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/foros/foros.component.html":
/*!****************************************************!*\
  !*** ./src/app/paginas/foros/foros.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <button mat-button class=\"btn-ruta\">Foros</button>\n\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" (click)=\"crear()\">\n          <mat-icon matListIcon class=\"small\">add</mat-icon>\n          Crear foro\n        </button>\n      </div>\n\n    </mat-toolbar>\n  </div>\n\n  <tabla-foros></tabla-foros>\n\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/foros/foros.component.scss":
/*!****************************************************!*\
  !*** ./src/app/paginas/foros/foros.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  min-height: calc(90vh - 60px);\n  padding: 0 1em; }\n  .row .col-md-3 {\n    background: #f5f8fa;\n    border: 1px solid #dfe3eb;\n    padding: 0; }\n  .row .col-md-3 .header {\n      border-bottom: 1px solid #dfe3eb;\n      padding: 16px 16px;\n      font-size: 12px;\n      -webkit-hyphens: none;\n          -ms-hyphens: none;\n              hyphens: none;\n      overflow: hidden !important;\n      text-overflow: ellipsis !important;\n      white-space: nowrap !important;\n      font-weight: 600;\n      text-transform: uppercase;\n      font-family: \"Roboto\"; }\n  .row .col-md-3 .tareas {\n      margin: 1em;\n      min-height: calc(90vh - 120px); }\n  .row .col-md-3 .tareas mat-card {\n        margin-bottom: 1em; }\n  .titulo {\n  margin: 0 !important;\n  color: #0091ae;\n  cursor: pointer; }\n  .titulo:hover {\n  font-weight: 500; }\n"

/***/ }),

/***/ "./src/app/paginas/foros/foros.component.ts":
/*!**************************************************!*\
  !*** ./src/app/paginas/foros/foros.component.ts ***!
  \**************************************************/
/*! exports provided: ForosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForosComponent", function() { return ForosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialogos_crear_foro_crear_foro_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dialogos/crear-foro/crear-foro.component */ "./src/app/paginas/foros/dialogos/crear-foro/crear-foro.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ForosComponent = /** @class */ (function () {
    function ForosComponent(db, _dialogService, dialog, snackBar) {
        this.db = db;
        this._dialogService = _dialogService;
        this.dialog = dialog;
        this.snackBar = snackBar;
    }
    ForosComponent.prototype.ngOnInit = function () { };
    ForosComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogos_crear_foro_crear_foro_component__WEBPACK_IMPORTED_MODULE_4__["CrearForoComponent"], {
            width: "60%"
        });
    };
    ForosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-foros",
            template: __webpack_require__(/*! ./foros.component.html */ "./src/app/paginas/foros/foros.component.html"),
            styles: [__webpack_require__(/*! ./foros.component.scss */ "./src/app/paginas/foros/foros.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__["TdDialogService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], ForosComponent);
    return ForosComponent;
}());



/***/ }),

/***/ "./src/app/paginas/foros/tablas/tabla-foros/tabla-foros.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/paginas/foros/tablas/tabla-foros/tabla-foros.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n    <!-- Id Column -->\n    <ng-container matColumnDef=\"titulo\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Titulo</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.titulo}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"objetivo\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Introducción</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.objetivo | slice:0:200}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"hilos\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Nro.Hilos</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.hilos}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"idioma\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Idioma</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.idioma}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"createdAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Creado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.createdAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"updatedAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Modificado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.updatedAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n    <!-- Checkbox Column -->\n    <ng-container matColumnDef=\"opciones\">\n      <mat-header-cell *matHeaderCellDef></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n\n          <button mat-menu-item (click)=\"eliminar(row)\">Eliminar</button>\n        </mat-menu>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n\n  <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/foros/tablas/tabla-foros/tabla-foros.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/paginas/foros/tablas/tabla-foros/tabla-foros.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-cell {\n  color: #0091ae;\n  cursor: pointer; }\n\n.mat-cell:hover {\n  text-decoration: underline; }\n\n.mat-column-objetivo {\n  flex: 0 0 30%;\n  text-align: left; }\n\n.mat-column-correo {\n  flex: 0 0 30%;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/paginas/foros/tablas/tabla-foros/tabla-foros.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/paginas/foros/tablas/tabla-foros/tabla-foros.component.ts ***!
  \***************************************************************************/
/*! exports provided: TablaForosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablaForosComponent", function() { return TablaForosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _dialogos_modificar_foro_modificar_foro_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../dialogos/modificar-foro/modificar-foro.component */ "./src/app/paginas/foros/dialogos/modificar-foro/modificar-foro.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: "¿Está seguro de eliminar el registro?",
    disableClose: false,
    title: "Confirmación",
    cancelButton: "Cancelar",
    acceptButton: "Aceptar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var TablaForosComponent = /** @class */ (function () {
    function TablaForosComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = ["titulo", "objetivo", "hilos", "idioma", "createdAt", "updatedAt", "opciones"];
        this.lista = [];
    }
    TablaForosComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    TablaForosComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("foros").subscribe(function (resp) {
            _this.lista = resp;
            _this.lista.forEach(function (element) {
                var consulta = _this.db.colWithIds$('hilos-foros', function (ref) { return ref.where('foroid', '==', element.id); }).subscribe(function (hilos) {
                    element.hilos = hilos.length;
                });
            });
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    TablaForosComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogos_modificar_foro_modificar_foro_component__WEBPACK_IMPORTED_MODULE_7__["ModificarForoComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    TablaForosComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("foros/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Foro eliminado");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], TablaForosComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], TablaForosComponent.prototype, "sort", void 0);
    TablaForosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'tabla-foros',
            template: __webpack_require__(/*! ./tabla-foros.component.html */ "./src/app/paginas/foros/tablas/tabla-foros/tabla-foros.component.html"),
            styles: [__webpack_require__(/*! ./tabla-foros.component.scss */ "./src/app/paginas/foros/tablas/tabla-foros/tabla-foros.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_5__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], TablaForosComponent);
    return TablaForosComponent;
}());



/***/ }),

/***/ "./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Agregar asistente a grupo</h1>\n\n<div mat-dialog-content class=\"example-container\" style=\"display: flex;\">\n\n\n  <div class=\"example-header\">\n    <mat-form-field>\n      <mat-icon matSuffix class=\"tc-blue-900\">search</mat-icon>\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Buscar...\">\n    </mat-form-field>\n  </div>\n\n  <div class=\"example-container mat-elevation-z8\">\n\n    <mat-table [dataSource]=\"dataSource\" matSort>\n\n      <ng-container matColumnDef=\"selection\">\n        <mat-header-cell *matHeaderCellDef>\n          <mat-checkbox (change)=\"$event ? masterToggle() : null\" [checked]=\"selection.hasValue() && isAllSelected()\"\n            [indeterminate]=\"selection.hasValue() && !isAllSelected()\">\n          </mat-checkbox>\n        </mat-header-cell>\n        <mat-cell *matCellDef=\"let row\">\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selection.toggle(row) : null\" [checked]=\"selection.isSelected(row)\">\n          </mat-checkbox>\n        </mat-cell>\n      </ng-container>\n\n      <!-- Id Column -->\n      <ng-container matColumnDef=\"correo\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>email</mat-header-cell>\n        <mat-cell *matCellDef=\"let row\">{{row.correo}}</mat-cell>\n      </ng-container>\n\n      <!-- Name Column -->\n      <ng-container matColumnDef=\"nombre\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre asistente</mat-header-cell>\n        <mat-cell *matCellDef=\"let row\">{{row.nombre}}</mat-cell>\n      </ng-container>\n\n      <mat-header-row *matHeaderRowDef=\"displayedColumns\">\n\n      </mat-header-row>\n      <mat-row *matRowDef=\"let row; columns: displayedColumns;\">\n      </mat-row>\n\n    </mat-table>\n\n    <mat-paginator [pageSizeOptions]=\"[10, 20, 30, 100]\"></mat-paginator>\n  </div>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"this.selection.selected.length == 0\">Agregar</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n\n.label-asistente {\n  margin-top: 2em !important;\n  margin-bottom: 2em !important;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.6) !important;\n  width: 99% !important; }\n\n.mat-column-selection {\n  flex: 0 0 10%;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.ts ***!
  \******************************************************************************************/
/*! exports provided: AgregarAsistenteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgregarAsistenteComponent", function() { return AgregarAsistenteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var AgregarAsistenteComponent = /** @class */ (function () {
    function AgregarAsistenteComponent(db, dialogRef, data) {
        this.db = db;
        this.dialogRef = dialogRef;
        this.data = data;
        this.displayedColumns = ["selection", "correo", "nombre"];
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_2__["SelectionModel"](true, []);
        this.lista = [];
        this.asistentes = [];
    }
    AgregarAsistenteComponent.prototype.ngOnInit = function () {
        this.asistentes = this.data.item;
        this.cargarLista();
    };
    AgregarAsistenteComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("asistentes").subscribe(function (resp) {
            resp.forEach(function (element, index) {
                var asistente = _this.asistentes.find(function (x) { return x.asistenteid == element.id; });
                if (!asistente) {
                    _this.lista.push(element);
                }
            });
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    AgregarAsistenteComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    AgregarAsistenteComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    AgregarAsistenteComponent.prototype.masterToggle = function () {
        var _this = this;
        this.isAllSelected()
            ? this.selection.clear()
            : this.dataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    AgregarAsistenteComponent.prototype.cerrar = function () {
        this.dialogRef.close([]);
    };
    AgregarAsistenteComponent.prototype.guardar = function () {
        this.dialogRef.close(this.selection.selected);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], AgregarAsistenteComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], AgregarAsistenteComponent.prototype, "sort", void 0);
    AgregarAsistenteComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-agregar-asistente",
            template: __webpack_require__(/*! ./agregar-asistente.component.html */ "./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.html"),
            styles: [__webpack_require__(/*! ./agregar-asistente.component.scss */ "./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.scss")]
        }),
        __param(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_3__["ResourcesService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], AgregarAsistenteComponent);
    return AgregarAsistenteComponent;
}());



/***/ }),

/***/ "./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Nuevo grupo</h1>\n\n<div mat-dialog-content class=\"example-container\" style=\"    display: flex;\">\n\n\n  <div class=\"row\">\n\n    <div class=\"col-md-6\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre\">\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <input type=\"text\" matInput [(ngModel)]=\"info.descripcion\" placeholder=\"Descripción\">\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-12\">\n      <label class=\"label-asistente\">Lista de asistentes</label>\n\n      <div class=\"button-row md-button-right\" style=\"top: 6em;\">\n\n      </div>\n\n      <div class=\"mat-elevation-z8\">\n        <table style=\"width:100%\" class=\"lista\">\n          <tr>\n            <th>EMAIL</th>\n            <th>NOMBRE</th>\n            <th style=\" text-align: center; width: 100px;\">\n                <button mat-button class=\"btn-crear\" (click)=\"agregar()\">\n                  Agregar\n                </button>\n            </th>\n          </tr>\n          <tr *ngIf=\"lista.length == 0\">\n            <td>No existen asistentes en este grupo</td>\n            <td></td>\n            <td></td>\n          </tr>\n          <tr *ngFor=\"let item of lista; let i = index;\">\n            <td>{{item.correo}}</td>\n            <td>{{item.nombre}}</td>\n            <td  style=\"width: 100px;\">\n              <button mat-button class=\"btn-crear\" (click)=\"eliminar(i)\">\n                Remover\n              </button>\n            </td>\n          </tr>\n        </table>\n      </div>\n\n    </div>\n\n  </div>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre\">Crear</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n\n.label-asistente {\n  margin-top: 2em !important;\n  margin-bottom: 2em !important;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.6) !important;\n  width: 99% !important; }\n\ntable.lista th {\n  text-align: left !important;\n  padding: 10px 10px;\n  border: 1px solid rgba(0, 0, 0, 0.1);\n  color: #fb7a5a;\n  font-weight: 100;\n  border-bottom: 3px solid; }\n\ntable.lista td {\n  padding: 10px;\n  background: #dedbdb;\n  border-right: 1px solid #fff;\n  border-bottom: 1px solid #fff; }\n"

/***/ }),

/***/ "./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.ts ***!
  \******************************************************************************/
/*! exports provided: CrearGrupoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearGrupoComponent", function() { return CrearGrupoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _agregar_asistente_agregar_asistente_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../agregar-asistente/agregar-asistente.component */ "./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.ts");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};








var configDel = {
    message: "¿Está seguro de eliminar el registro?",
    disableClose: false,
    title: "Confirmación",
    cancelButton: "Cancelar",
    acceptButton: "Aceptar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var CrearGrupoComponent = /** @class */ (function () {
    function CrearGrupoComponent(db, notify, upload, dialogRef, _loadingService, data, _upload, dialog, _dialogService, _notify) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.dialog = dialog;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this.info = {
            asistentes: []
        };
        this.files = {};
        this.lista = [];
        this.allAsistentes = [];
        this.asistentesAgregados = [];
    }
    CrearGrupoComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    CrearGrupoComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("asistentes").subscribe(function (resp) {
            _this.allAsistentes = resp;
            _this.allAsistentes.forEach(function (element, index) {
                var asistente = _this.info.asistentes.find(function (x) { return x.asistenteid == element.id; });
                if (asistente) {
                    _this.lista.push(element);
                }
            });
        });
    };
    CrearGrupoComponent.prototype.eliminar = function (index) {
        console.log(index);
        this.info.asistentes.splice(index, 1);
        this.lista.splice(index, 1);
    };
    CrearGrupoComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.db
            .add("grupos", this.info)
            .then(function () {
            _this.notify.update("Grupo guardado", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    CrearGrupoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearGrupoComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    CrearGrupoComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    CrearGrupoComponent.prototype.agregar = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_agregar_asistente_agregar_asistente_component__WEBPACK_IMPORTED_MODULE_6__["AgregarAsistenteComponent"], {
            width: "60%",
            data: { item: this.info.asistentes }
        });
        dialogRef.afterClosed().subscribe(function (resp) {
            if (resp.length > 0) {
                _this.lista = [];
                resp.forEach(function (element) {
                    _this.info.asistentes.push({
                        asistenteid: element.id
                    });
                });
                _this.allAsistentes.forEach(function (element, index) {
                    var asistente = _this.info.asistentes.find(function (x) { return x.asistenteid == element.id; });
                    if (asistente) {
                        _this.lista.push(element);
                    }
                });
            }
        });
    };
    CrearGrupoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear-grupo",
            template: __webpack_require__(/*! ./crear-grupo.component.html */ "./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.html"),
            styles: [__webpack_require__(/*! ./crear-grupo.component.scss */ "./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_7__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"]])
    ], CrearGrupoComponent);
    return CrearGrupoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar grupo</h1>\n\n<div mat-dialog-content class=\"example-container\" style=\"    display: flex;\">\n\n\n  <div class=\"row\">\n\n    <div class=\"col-md-6\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre\">\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <input type=\"text\" matInput [(ngModel)]=\"info.descripcion\" placeholder=\"Descripción\">\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-12\">\n      <label class=\"label-asistente\">Lista de asistentes</label>\n\n      <div class=\"button-row md-button-right\" style=\"top: 6em;\">\n\n      </div>\n\n      <div class=\"mat-elevation-z8\">\n        <table style=\"width:100%\" class=\"lista\">\n          <tr>\n            <th>EMAIL</th>\n            <th>NOMBRE</th>\n            <th style=\" text-align: center; width: 100px;\">\n              <button mat-button class=\"btn-crear\" (click)=\"agregar()\">\n                Agregar\n              </button>\n            </th>\n          </tr>\n          <tr *ngIf=\"lista.length == 0\">\n            <td>No existen asistentes en este grupo</td>\n            <td></td>\n            <td></td>\n          </tr>\n          <tr *ngFor=\"let item of lista; let i = index;\">\n            <td>{{item.correo}}</td>\n            <td>{{item.nombre}}</td>\n            <td style=\"width: 100px;\">\n              <button mat-button class=\"btn-crear\" (click)=\"eliminar(i)\">\n                Remover\n              </button>\n            </td>\n          </tr>\n        </table>\n      </div>\n\n    </div>\n\n  </div>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre\">Guardar</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.scss":
/*!****************************************************************************************!*\
  !*** ./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.scss ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n\n.label-asistente {\n  margin-top: 2em !important;\n  margin-bottom: 2em !important;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.6) !important;\n  width: 99% !important; }\n\ntable.lista th {\n  text-align: left !important;\n  padding: 10px 10px;\n  border: 1px solid rgba(0, 0, 0, 0.1);\n  color: #fb7a5a;\n  font-weight: 100;\n  border-bottom: 3px solid; }\n\ntable.lista td {\n  padding: 10px;\n  background: #dedbdb;\n  border-right: 1px solid #fff;\n  border-bottom: 1px solid #fff; }\n"

/***/ }),

/***/ "./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.ts ***!
  \**************************************************************************************/
/*! exports provided: ModificarGrupoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarGrupoComponent", function() { return ModificarGrupoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _agregar_asistente_agregar_asistente_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../agregar-asistente/agregar-asistente.component */ "./src/app/paginas/grupos/dialogos/agregar-asistente/agregar-asistente.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







var ModificarGrupoComponent = /** @class */ (function () {
    function ModificarGrupoComponent(db, notify, upload, dialogRef, _loadingService, data, _upload, dialog) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.dialog = dialog;
        this.info = {
            asistentes: []
        };
        this.files = {};
        this.lista = [];
        this.allAsistentes = [];
        this.asistentesAgregados = [];
        this.info = data.item;
    }
    ModificarGrupoComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    ModificarGrupoComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("asistentes").subscribe(function (resp) {
            _this.allAsistentes = resp;
            _this.allAsistentes.forEach(function (element, index) {
                var asistente = _this.info.asistentes.find(function (x) { return x.asistenteid == element.id; });
                if (asistente) {
                    _this.lista.push(element);
                }
            });
        });
    };
    ModificarGrupoComponent.prototype.eliminar = function (index) {
        console.log(index);
        this.info.asistentes.splice(index, 1);
        this.lista.splice(index, 1);
    };
    ModificarGrupoComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.db
            .update("grupos/" + this.info.id, this.info)
            .then(function () {
            _this.notify.update("Grupo guardado", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    ModificarGrupoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarGrupoComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    ModificarGrupoComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    ModificarGrupoComponent.prototype.agregar = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_agregar_asistente_agregar_asistente_component__WEBPACK_IMPORTED_MODULE_6__["AgregarAsistenteComponent"], {
            width: "60%",
            data: { item: this.info.asistentes }
        });
        dialogRef.afterClosed().subscribe(function (resp) {
            if (resp.length > 0) {
                _this.lista = [];
                resp.forEach(function (element) {
                    _this.info.asistentes.push({
                        asistenteid: element.id
                    });
                });
                _this.allAsistentes.forEach(function (element, index) {
                    var asistente = _this.info.asistentes.find(function (x) { return x.asistenteid == element.id; });
                    if (asistente) {
                        _this.lista.push(element);
                    }
                });
            }
        });
    };
    ModificarGrupoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar-grupo",
            template: __webpack_require__(/*! ./modificar-grupo.component.html */ "./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.html"),
            styles: [__webpack_require__(/*! ./modificar-grupo.component.scss */ "./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]])
    ], ModificarGrupoComponent);
    return ModificarGrupoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/grupos/grupos.component.html":
/*!******************************************************!*\
  !*** ./src/app/paginas/grupos/grupos.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <button mat-button class=\"btn-ruta\">Grupos</button>\n\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" (click)=\"crear()\">\n          Crear grupo\n        </button>\n      </div>\n\n    </mat-toolbar>\n  </div>\n\n  <tabla-grupos></tabla-grupos>\n\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/grupos/grupos.component.scss":
/*!******************************************************!*\
  !*** ./src/app/paginas/grupos/grupos.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  min-height: calc(90vh - 60px);\n  padding: 0 1em; }\n  .row .col-md-3 {\n    background: #f5f8fa;\n    border: 1px solid #dfe3eb;\n    padding: 0; }\n  .row .col-md-3 .header {\n      border-bottom: 1px solid #dfe3eb;\n      padding: 16px 16px;\n      font-size: 12px;\n      -webkit-hyphens: none;\n          -ms-hyphens: none;\n              hyphens: none;\n      overflow: hidden !important;\n      text-overflow: ellipsis !important;\n      white-space: nowrap !important;\n      font-weight: 600;\n      text-transform: uppercase;\n      font-family: \"Roboto\"; }\n  .row .col-md-3 .tareas {\n      margin: 1em;\n      min-height: calc(90vh - 120px); }\n  .row .col-md-3 .tareas mat-card {\n        margin-bottom: 1em; }\n  .titulo {\n  margin: 0 !important;\n  color: #0091ae;\n  cursor: pointer; }\n  .titulo:hover {\n  font-weight: 500; }\n"

/***/ }),

/***/ "./src/app/paginas/grupos/grupos.component.ts":
/*!****************************************************!*\
  !*** ./src/app/paginas/grupos/grupos.component.ts ***!
  \****************************************************/
/*! exports provided: GruposComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GruposComponent", function() { return GruposComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _dialogos_crear_grupo_crear_grupo_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dialogos/crear-grupo/crear-grupo.component */ "./src/app/paginas/grupos/dialogos/crear-grupo/crear-grupo.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GruposComponent = /** @class */ (function () {
    function GruposComponent(db, _dialogService, dialog, snackBar) {
        this.db = db;
        this._dialogService = _dialogService;
        this.dialog = dialog;
        this.snackBar = snackBar;
    }
    GruposComponent.prototype.ngOnInit = function () { };
    GruposComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogos_crear_grupo_crear_grupo_component__WEBPACK_IMPORTED_MODULE_4__["CrearGrupoComponent"], {
            width: "80%"
        });
    };
    GruposComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-grupos',
            template: __webpack_require__(/*! ./grupos.component.html */ "./src/app/paginas/grupos/grupos.component.html"),
            styles: [__webpack_require__(/*! ./grupos.component.scss */ "./src/app/paginas/grupos/grupos.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_3__["TdDialogService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
    ], GruposComponent);
    return GruposComponent;
}());



/***/ }),

/***/ "./src/app/paginas/grupos/tablas/tabla-grupos/tabla-grupos.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/paginas/grupos/tablas/tabla-grupos/tabla-grupos.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n    <!-- Id Column -->\n    <ng-container matColumnDef=\"nombre\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.nombre}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"descripcion\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Descripción</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.descripcion | slice:0:200}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"asistentes\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Nro.Asistentes</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.asistentes.length}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"createdAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Creado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.createdAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"updatedAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Modificado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.updatedAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n    <!-- Checkbox Column -->\n    <ng-container matColumnDef=\"opciones\">\n      <mat-header-cell *matHeaderCellDef></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n\n          <button mat-menu-item (click)=\"eliminar(row)\">Eliminar</button>\n        </mat-menu>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n\n  <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/grupos/tablas/tabla-grupos/tabla-grupos.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/paginas/grupos/tablas/tabla-grupos/tabla-grupos.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-cell {\n  color: #0091ae;\n  cursor: pointer; }\n\n.mat-cell:hover {\n  text-decoration: underline; }\n\n.mat-column-objetivo {\n  flex: 0 0 30%;\n  text-align: left; }\n\n.mat-column-correo {\n  flex: 0 0 30%;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/paginas/grupos/tablas/tabla-grupos/tabla-grupos.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/paginas/grupos/tablas/tabla-grupos/tabla-grupos.component.ts ***!
  \******************************************************************************/
/*! exports provided: TablaGruposComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablaGruposComponent", function() { return TablaGruposComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _dialogos_modificar_grupo_modificar_grupo_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../dialogos/modificar-grupo/modificar-grupo.component */ "./src/app/paginas/grupos/dialogos/modificar-grupo/modificar-grupo.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: "¿Está seguro de eliminar el registro?",
    disableClose: false,
    title: "Confirmación",
    cancelButton: "Cancelar",
    acceptButton: "Aceptar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var TablaGruposComponent = /** @class */ (function () {
    function TablaGruposComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = ["nombre", "descripcion", "asistentes", "createdAt", "updatedAt", "opciones"];
        this.lista = [];
    }
    TablaGruposComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    TablaGruposComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("grupos").subscribe(function (resp) {
            _this.lista = resp;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    TablaGruposComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogos_modificar_grupo_modificar_grupo_component__WEBPACK_IMPORTED_MODULE_7__["ModificarGrupoComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    TablaGruposComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("grupos/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Grupo eliminado");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], TablaGruposComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], TablaGruposComponent.prototype, "sort", void 0);
    TablaGruposComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'tabla-grupos',
            template: __webpack_require__(/*! ./tabla-grupos.component.html */ "./src/app/paginas/grupos/tablas/tabla-grupos/tabla-grupos.component.html"),
            styles: [__webpack_require__(/*! ./tabla-grupos.component.scss */ "./src/app/paginas/grupos/tablas/tabla-grupos/tabla-grupos.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_5__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], TablaGruposComponent);
    return TablaGruposComponent;
}());



/***/ }),

/***/ "./src/app/paginas/ingreso/ingreso.component.html":
/*!********************************************************!*\
  !*** ./src/app/paginas/ingreso/ingreso.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Loguin cuando no ese logueado -->\n<div class=\"main-container\">\n  <!-- Sidebar Access -->\n  <aside class=\"sidebar-large\">\n    <div class=\"user-access\">\n\n      <div class=\"user-access-header\">\n        <a>\n          <img src=\"assets/LogoEsp.svg\" class=\"logo\" width=\"50\">\n        </a>\n        <p class=\"intro-title\" style=\"margin-top: 1em;\">Ingresa a tu cuenta</p>\n      </div>\n\n      <div class=\"user-access-form\" style=\"margin-top:2em;\">\n\n        <form (ngSubmit)=\"login()\" *ngIf=\"showLogin\">\n          <div class=\"input-wrapper\">\n            <input autocomplete=\"off\" type=\"text\" name=\"email\" id=\"email\" class=\"email\" placeholder=\"Correo electrónico\"\n              [(ngModel)]=\"item.user\">\n          </div>\n          <div class=\"input-wrapper\">\n\n            <input autocomplete=\"off\" type=\"password\" name=\"password\" id=\"password\" class=\"clave\" placeholder=\"Contraseña\"\n              [(ngModel)]=\"item.password\">\n          </div>\n          <input type=\"submit\" name=\"submit\" id=\"submit\" value=\"INGRESAR\" class=\"enviar\">\n        </form>\n      </div>\n      <div class=\"user-access-form\" *ngIf=\"!showLogin\">\n\n        <form (ngSubmit)=\"restart()\">\n          <div class=\"input-wrapper\">\n            <input autocomplete=\"off\" type=\"text\" name=\"email\" id=\"email\" class=\"email\" placeholder=\"Correo electrónico para restablecer\"\n              [(ngModel)]=\"item.user\">\n          </div>\n          <input type=\"submit\" name=\"submit\" id=\"submit\" value=\"Restablecer\" class=\"enviar\">\n        </form>\n      </div>\n      <div class=\"user-access-footer\" *ngIf=\"!showLogin\">\n        <p class=\"link\">\n          <a (click)=\"showLogin = !showLogin\">Iniciar sesión</a>\n        </p>\n      </div>\n    </div>\n  </aside>\n  <!-- Content Slideshow  -->\n  <section class=\"content\">\n    <div class=\"hero-login bg-cover\" style=\"background-image: url('assets/background-banner.jpg');\">\n\n    </div>\n  </section>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/ingreso/ingreso.component.scss":
/*!********************************************************!*\
  !*** ./src/app/paginas/ingreso/ingreso.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-input-element {\n  color: #215d78 !important;\n  font-size: 1.5em !important; }\n\nmat-cell {\n  display: flow-root !important;\n  padding-top: 5px !important; }\n\n* {\n  box-sizing: border-box;\n  -moz-box-sizing: border-box; }\n\nhtml {\n  font-size: 62.5%;\n  height: 100%; }\n\nbody {\n  font-size: 14px;\n  font-family: 'Roboto', sans-serif;\n  color: #8492a6;\n  padding: 0;\n  margin: 0;\n  width: 100%;\n  height: 100%;\n  position: relative;\n  box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale; }\n\np {\n  font-family: 'Roboto', sans-serif;\n  color: #8492a6;\n  font-size: 14px; }\n\nlabel {\n  font-family: 'Roboto', sans-serif;\n  color: #8492a6;\n  font-size: 14px; }\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  font-family: 'Roboto', sans-serif;\n  color: #00b19d; }\n\na {\n  color: #00b19d;\n  transition: .2s;\n  text-decoration: none; }\n\na:hover {\n  color: #008e79;\n  transition: .2s; }\n\nhr {\n  border: 0;\n  height: 1px;\n  color: rgba(0, 0, 0, 0.1);\n  background-color: rgba(0, 0, 0, 0.1); }\n\ninput[type=\"text\"],\ninput[type=\"password\"] {\n  border-radius: 4px;\n  border: 1px solid rgba(0, 0, 0, 0.2);\n  color: #58595f;\n  box-shadow: none;\n  height: 3rem;\n  outline: 0; }\n\ninput[type=\"text\"]:hover,\ninput[type=\"password\"]:hover {\n  box-shadow: none; }\n\ninput[type=\"text\"]:focus,\ninput[type=\"password\"]:focus {\n  box-shadow: none;\n  border: solid 1px #00c3ad; }\n\nbutton:focus {\n  outline: 0; }\n\n::-webkit-input-placeholder {\n  color: rgba(56, 56, 60, 0.4); }\n\n:-moz-placeholder {\n  color: rgba(56, 56, 60, 0.4);\n  opacity: 1; }\n\n::-moz-placeholder {\n  color: #38383c;\n  opacity: .4; }\n\n:-ms-input-placeholder {\n  color: rgba(56, 56, 60, 0.4); }\n\n::-ms-input-placeholder {\n  color: rgba(56, 56, 60, 0.4); }\n\n@-webkit-keyframes autofill {\n  to {\n    color: #58595f;\n    background: #FFF; } }\n\ninput:-webkit-autofill {\n  -webkit-animation-name: autofill;\n  -webkit-animation-fill-mode: both; }\n\n.main-container {\n  display: table;\n  width: 100%;\n  height: 100%; }\n\n.content {\n  display: table-cell;\n  height: 100%;\n  background-color: #eff2f7;\n  text-align: right;\n  vertical-align: top;\n  color: #FFF; }\n\n@media screen and (max-width: 480px) {\n  .content {\n    display: block;\n    overflow: auto;\n    text-align: center; } }\n\n.sidebar {\n  display: table-cell;\n  width: 160px;\n  height: 100%;\n  padding: 100px 80px;\n  background-color: #FFF; }\n\n@media screen and (max-width: 480px) {\n  .sidebar {\n    display: block;\n    width: 100%;\n    padding: 40px 40px; } }\n\n.sidebar-large {\n  display: table-cell;\n  width: 500px;\n  height: 100vh;\n  padding: 200px 80px;\n  background-color: #a8adb6; }\n\n@media screen and (max-width: 480px) {\n  .sidebar-large {\n    display: block;\n    width: 100%;\n    padding: 40px 40px; } }\n\n.hero-login {\n  height: 100%; }\n\n@media screen and (max-width: 480px) {\n  .hero-login {\n    padding: 40px 40px; } }\n\n.intro-title {\n  font-size: 18px;\n  margin-bottom: 0;\n  color: #fff; }\n\n.intro-summary {\n  color: #00b19d;\n  margin-top: 5px;\n  font-size: 97%; }\n\n.quote {\n  width: 450px;\n  color: #FFF;\n  font-size: 16px;\n  bottom: 0;\n  right: 0;\n  margin: 50px 80px 80px 0;\n  position: absolute; }\n\n@media screen and (max-width: 480px) {\n  .quote {\n    width: 100%;\n    float: left;\n    position: relative; } }\n\n.help-call {\n  padding-top: 15px;\n  margin-right: 70px; }\n\n@media screen and (max-width: 480px) {\n  .help-call {\n    margin-right: 0; } }\n\n.help-call p {\n  color: #FFF;\n  display: inline-block;\n  margin-right: 8px; }\n\n@media screen and (max-width: 480px) {\n  .help-call {\n    margin-right: 0; } }\n\n.button-outline {\n  color: #FFF;\n  background: transparent;\n  border: 2px solid #FFF;\n  border-radius: 40px;\n  padding: 5px 25px;\n  display: inline-block;\n  font-weight: bold; }\n\n.button-outline:hover {\n  color: #00b19d;\n  background: #FFF; }\n\n.button-outline a {\n  color: #FFF; }\n\n.button-outline a:hover {\n  color: #FFF; }\n\n.text-center {\n  text-align: center; }\n\n.color-text {\n  color: #8492a6; }\n\n.color-primary {\n  color: #00b19d; }\n\n.button {\n  border: 0;\n  border-radius: 4px;\n  outline: 0;\n  cursor: pointer; }\n\n.button {\n  color: white;\n  border: 2px solid rgba(255, 255, 255, 0.1);\n  border-radius: 5px;\n  background: #00b19d; }\n\n.button:hover {\n  color: #FFF;\n  border: 2px solid #00c3ad;\n  background: #00c3ad; }\n\n.button a:hover,\n.button a:focus {\n  color: #FFF;\n  border: 2px solid #00c3ad;\n  background: #00c3ad; }\n\n.button:focus {\n  color: #FFF;\n  border: 2px solid #00c3ad;\n  background: #00c3ad; }\n\n.button-circle {\n  width: 50px;\n  height: 50px;\n  margin-top: 10px;\n  display: inline-block;\n  border-radius: 50%; }\n\n.button-circle svg path {\n  fill: #00b19d; }\n\n.button-circle:hover {\n  background: #FFF; }\n\n@media screen and (max-width: 480px) {\n  .button-circle {\n    background: #FFF;\n    margin-left: 5px; }\n  .button-circle:hover {\n    background: #f2f2f2; } }\n\n.user-access {\n  text-align: center; }\n\n.user-access input:not([type=checkbox]):not([type=radio]) {\n  width: 100%;\n  height: 50px;\n  font-size: 18px;\n  margin-top: 15px;\n  border-radius: 4px;\n  outline: 0;\n  padding: 10px 10px;\n  transition: background-color .2s ease; }\n\n.user-access input[type=\"submit\"],\n.user-access input[type=\"button\"] {\n  color: white;\n  border: 2px solid rgba(255, 255, 255, 0.1);\n  border-radius: 5px;\n  background: #0a4858;\n  cursor: pointer;\n  font-weight: 100; }\n\n.user-access input[type=\"submit\"]:hover,\n.user-access input[type=\"button\"]:hover {\n  color: #0a4858;\n  border: 2px solid rgba(255, 255, 255, 0.1);\n  border-radius: 5px;\n  background: #fff; }\n\n.user-access select {\n  width: 100%;\n  background-color: white;\n  border: 1px solid rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n  display: block;\n  padding: 12px;\n  margin-top: 15px;\n  font-size: 18px;\n  color: #58595f;\n  box-sizing: border-box;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  cursor: pointer; }\n\n.user-access select {\n  background-image: linear-gradient(45deg, transparent 50%, gray 50%), linear-gradient(135deg, gray 50%, transparent 50%), linear-gradient(to right, #f2f2f2, #f2f2f2);\n  background-position: calc(100% - 20px) calc(1em - -2px), calc(100% - 15px) calc(1em - -2px), calc(100% - 2.5em) 0.5em;\n  background-size: 5px 5px, 5px 5px, 1px 1.5em;\n  background-repeat: no-repeat; }\n\n.user-access select:focus {\n  border-color: #00c3ad;\n  outline: 0; }\n\n.user-access select:-moz-focusring {\n  color: transparent;\n  text-shadow: 0 0 0 #000; }\n\n.coupon-link {\n  text-align: left;\n  font-size: 90%;\n  margin-top: 15px; }\n\n.input-wrapper {\n  position: relative; }\n\n.user-access span.close {\n  width: 28px;\n  height: 28px;\n  cursor: pointer;\n  display: block;\n  color: #8492a6;\n  font-size: 18px;\n  line-height: 25px;\n  background: #FFF;\n  padding: 0 5px;\n  border-radius: 50%;\n  border: 1px solid rgba(0, 0, 0, 0.2);\n  transition: all .2s ease;\n  position: absolute;\n  right: 11px;\n  top: 11px;\n  margin-top: 15px; }\n\n.user-access span.close:hover {\n  color: #8492a6;\n  background: #f7f7f7;\n  border: 1px solid rgba(0, 0, 0, 0.2); }\n\n.terms {\n  margin-top: 15px;\n  font-size: 80%; }\n\n.user-access .error-messages {\n  background: 0;\n  font-size: 85%;\n  margin: 0;\n  color: #e7515c;\n  background: #fff2f3;\n  padding: 5px; }\n\n.user-access .error-messages ul {\n  padding-left: 0; }\n\n.user-access .errors {\n  list-style: none;\n  color: #e7515c;\n  margin: 0; }\n\n.error-messages ul,\n.errors {\n  list-style-type: none; }\n\n.user-access .error {\n  color: #e7515c;\n  background: #fff2f3;\n  padding: 5px;\n  font-size: 85%; }\n\n.user-access .referer-info {\n  font-size: 90%; }\n\n.user-access .referer-info p {\n  font-size: 90%; }\n\n.logo {\n  width: 70% !important; }\n\n.logo-mini {\n  width: 20% !important;\n  padding-top: .5em; }\n\n.user-access .referer-logo {\n  margin-top: 15px; }\n\n.user-access .hidden {\n  display: none; }\n\n.bg-cover {\n  background-size: cover !important;\n  background-position: center !important;\n  background-repeat: no-repeat !important; }\n\n@media screen and (max-width: 480px) {\n  .register-quote {\n    margin-top: 190px; } }\n\n.slideshow {\n  position: relative; }\n\n.slideshow-list {\n  position: relative;\n  overflow: hidden; }\n\n.slideshow-item {\n  visibility: hidden;\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100vh; }\n\n@media screen and (max-width: 480px) {\n  .slideshow-item {\n    height: 100vh;\n    padding: 40px 80px; } }\n\n.slideshow-item-current {\n  visibility: visible;\n  position: relative; }\n\n.slideshow-fade .slideshow-item-hidePrevious,\n.slideshow-fade .slideshow-item-hideNext {\n  visibility: visible;\n  -webkit-animation: fadeOut 750ms cubic-bezier(0.455, 0.03, 0.515, 0.955) both;\n  animation: fadeOut 750ms cubic-bezier(0.455, 0.03, 0.515, 0.955) both; }\n\n.slideshow-fade .slideshow-item-hidePrevious,\n.slideshow-fade .slideshow-item-hideNext {\n  z-index: 2; }\n\n.slideshow-fade .slideshow-item-showPrevious,\n.slideshow-fade .slideshow-item-showNext {\n  z-index: 1; }\n\n@-webkit-keyframes fadeOut {\n  100% {\n    opacity: 0;\n    visibility: hidden; } }\n\n@keyframes fadeOut {\n  100% {\n    opacity: 0;\n    visibility: hidden; } }\n\n.slideshow-nav {\n  position: absolute;\n  bottom: 10%;\n  z-index: 9999;\n  margin-left: 70px; }\n\n.slideshow-pagination {\n  text-align: center; }\n\n.slideshow-dot {\n  text-indent: -9999px;\n  border: 0;\n  border-radius: 50%;\n  width: 10px;\n  height: 10px;\n  padding: 0;\n  margin: 5px;\n  background-color: rgba(255, 255, 255, 0.5);\n  -webkit-appearance: none; }\n\n.slideshow-dot-current {\n  background-color: white; }\n\n#checkbox-image {\n  display: inline-block;\n  width: 13px;\n  height: 13px;\n  background: url(https://cdn1.alegra.com/js/extjs2/resources/ext-theme-gray/images/form/checkbox.gif) -39px 0 no-repeat;\n  vertical-align: bottom;\n  margin-bottom: 10px; }\n\n#checkbox-image.checked {\n  background: url(https://cdn1.alegra.com/js/extjs2/resources/ext-theme-gray/images/form/checkbox.gif) 0 -26px no-repeat; }\n\n.td-loading {\n  background: white !important; }\n\n.mat-list-item.single-item {\n  height: 68px !important;\n  padding: 0 4px !important;\n  background: #FFF;\n  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-list-item.single-item.active {\n  height: 68px !important;\n  padding: 0 0px !important; }\n\n.mat-list-item.single-item:hover {\n  background: rgba(0, 0, 0, 0.04); }\n\n.link > a {\n  cursor: pointer;\n  color: #fff; }\n\n.circle:hover {\n  background: #fff; }\n\n.circle:hover .mat-icon {\n    color: #0a4858; }\n\n.menu:hover {\n  background: #fff; }\n\n.menu:hover .mat-icon {\n    color: #e86435; }\n\n.hide-div {\n  display: none !important; }\n\n/* -----*/\n\n.mat-input-element {\n  color: #215d78 !important;\n  font-size: 1.5em !important; }\n\n* {\n  box-sizing: border-box;\n  -moz-box-sizing: border-box; }\n\nhtml {\n  font-size: 62.5%;\n  height: 100%; }\n\nbody {\n  font-size: 14px;\n  font-family: 'Roboto', sans-serif;\n  color: #8492a6;\n  padding: 0;\n  margin: 0;\n  width: 100%;\n  height: 100%;\n  position: relative;\n  box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale; }\n\np {\n  font-family: 'Roboto', sans-serif;\n  color: #8492a6;\n  font-size: 14px; }\n\nlabel {\n  font-family: 'Roboto', sans-serif;\n  color: #8492a6;\n  font-size: 14px; }\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  font-family: 'Roboto', sans-serif;\n  color: #00b19d; }\n\na {\n  color: #00b19d;\n  transition: .2s;\n  text-decoration: none; }\n\na:hover {\n  color: #008e79;\n  transition: .2s; }\n\nhr {\n  border: 0;\n  height: 1px;\n  color: rgba(0, 0, 0, 0.1);\n  background-color: rgba(0, 0, 0, 0.1); }\n\ninput[type=\"text\"],\ninput[type=\"password\"] {\n  border-radius: 4px !important;\n  border: 1px solid rgba(0, 0, 0, 0.2) !important;\n  color: #58595f !important;\n  box-shadow: none !important;\n  height: 3rem !important;\n  outline: 0 !important;\n  padding: 5px 10px 0 5px !important;\n  margin-bottom: 5px !important;\n  margin-top: 5px !important;\n  width: 100% !important; }\n\ninput[type=\"text\"]:hover,\ninput[type=\"password\"]:hover {\n  box-shadow: none; }\n\ninput[type=\"text\"]:focus,\ninput[type=\"password\"]:focus {\n  box-shadow: none;\n  border: solid 1px #00c3ad; }\n\nbutton:focus {\n  outline: 0; }\n\n::-webkit-input-placeholder {\n  color: rgba(56, 56, 60, 0.4); }\n\n:-moz-placeholder {\n  color: rgba(56, 56, 60, 0.4);\n  opacity: 1; }\n\n::-moz-placeholder {\n  color: #38383c;\n  opacity: .4; }\n\n:-ms-input-placeholder {\n  color: rgba(56, 56, 60, 0.4); }\n\n::-ms-input-placeholder {\n  color: rgba(56, 56, 60, 0.4); }\n\n@-webkit-keyframes autofill {\n  to {\n    color: #58595f;\n    background: #FFF; } }\n\ninput:-webkit-autofill {\n  -webkit-animation-name: autofill;\n  -webkit-animation-fill-mode: both; }\n\n.main-container {\n  display: table;\n  width: 100%;\n  height: 100%; }\n\n.content {\n  display: table-cell;\n  height: 100%;\n  background-color: #eff2f7;\n  text-align: right;\n  vertical-align: top;\n  color: #FFF; }\n\n@media screen and (max-width: 480px) {\n  .content {\n    display: block;\n    overflow: auto;\n    text-align: center; } }\n\n.sidebar {\n  display: table-cell;\n  width: 160px;\n  height: 100%;\n  padding: 100px 80px;\n  background-color: #FFF; }\n\n@media screen and (max-width: 480px) {\n  .sidebar {\n    display: block;\n    width: 100%;\n    padding: 40px 40px; } }\n\n@media screen and (max-width: 480px) {\n  .sidebar-large {\n    display: block;\n    width: 100%;\n    padding: 40px 40px; } }\n\n.hero-login {\n  height: 100%; }\n\n@media screen and (max-width: 480px) {\n  .hero-login {\n    padding: 40px 40px; } }\n\n.intro-title {\n  font-size: 18px;\n  margin-bottom: 0; }\n\n.intro-summary {\n  color: #00b19d;\n  margin-top: 5px;\n  font-size: 97%; }\n\n.quote {\n  width: 450px;\n  color: #FFF;\n  font-size: 16px;\n  bottom: 0;\n  right: 0;\n  margin: 50px 80px 80px 0;\n  position: absolute; }\n\n@media screen and (max-width: 480px) {\n  .quote {\n    width: 100%;\n    float: left;\n    position: relative; } }\n\n.help-call {\n  padding-top: 15px;\n  margin-right: 70px; }\n\n@media screen and (max-width: 480px) {\n  .help-call {\n    margin-right: 0; } }\n\n.help-call p {\n  color: #FFF;\n  display: inline-block;\n  margin-right: 8px; }\n\n@media screen and (max-width: 480px) {\n  .help-call {\n    margin-right: 0; } }\n\n.button-outline {\n  color: #FFF;\n  background: transparent;\n  border: 2px solid #FFF;\n  border-radius: 40px;\n  padding: 5px 25px;\n  display: inline-block;\n  font-weight: bold; }\n\n.button-outline:hover {\n  color: #00b19d;\n  background: #FFF; }\n\n.button-outline a {\n  color: #FFF; }\n\n.button-outline a:hover {\n  color: #FFF; }\n\n.text-center {\n  text-align: center; }\n\n.color-text {\n  color: #8492a6; }\n\n.color-primary {\n  color: #00b19d; }\n\n.button {\n  border: 0;\n  border-radius: 4px;\n  outline: 0;\n  cursor: pointer; }\n\n.button {\n  color: white;\n  border: 2px solid rgba(255, 255, 255, 0.1);\n  border-radius: 5px;\n  background: #00b19d; }\n\n.button:hover {\n  color: #FFF;\n  border: 2px solid #00c3ad;\n  background: #00c3ad; }\n\n.button a:hover,\n.button a:focus {\n  color: #FFF;\n  border: 2px solid #00c3ad;\n  background: #00c3ad; }\n\n.button:focus {\n  color: #FFF;\n  border: 2px solid #00c3ad;\n  background: #00c3ad; }\n\n.button-circle {\n  width: 50px;\n  height: 50px;\n  margin-top: 10px;\n  display: inline-block;\n  border-radius: 50%; }\n\n.button-circle svg path {\n  fill: #00b19d; }\n\n.button-circle:hover {\n  background: #FFF; }\n\n@media screen and (max-width: 480px) {\n  .button-circle {\n    background: #FFF;\n    margin-left: 5px; }\n  .button-circle:hover {\n    background: #f2f2f2; } }\n\n.user-access {\n  text-align: center; }\n\n.user-access input:not([type=checkbox]):not([type=radio]) {\n  width: 100%;\n  height: 50px;\n  font-size: 18px;\n  margin-top: 15px;\n  border-radius: 4px;\n  outline: 0;\n  padding: 10px 10px;\n  transition: background-color .2s ease; }\n\n.user-access input[type=\"submit\"],\n.user-access input[type=\"button\"] {\n  color: white;\n  border: 2px solid rgba(255, 255, 255, 0.1);\n  border-radius: 5px;\n  background: #29647a;\n  cursor: pointer;\n  font-weight: bold; }\n\n.user-access input[type=\"submit\"]:hover,\n.user-access input[type=\"button\"]:hover {\n  color: white;\n  border: 2px solid rgba(255, 255, 255, 0.1);\n  border-radius: 5px;\n  background: #4a99c0; }\n\n.user-access select {\n  width: 100%;\n  background-color: white;\n  border: 1px solid rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n  display: block;\n  padding: 12px;\n  margin-top: 15px;\n  font-size: 18px;\n  color: #58595f;\n  box-sizing: border-box;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  cursor: pointer; }\n\n.user-access select {\n  background-image: linear-gradient(45deg, transparent 50%, gray 50%), linear-gradient(135deg, gray 50%, transparent 50%), linear-gradient(to right, #f2f2f2, #f2f2f2);\n  background-position: calc(100% - 20px) calc(1em - -2px), calc(100% - 15px) calc(1em - -2px), calc(100% - 2.5em) 0.5em;\n  background-size: 5px 5px, 5px 5px, 1px 1.5em;\n  background-repeat: no-repeat; }\n\n.user-access select:focus {\n  border-color: #00c3ad;\n  outline: 0; }\n\n.user-access select:-moz-focusring {\n  color: transparent;\n  text-shadow: 0 0 0 #000; }\n\n.coupon-link {\n  text-align: left;\n  font-size: 90%;\n  margin-top: 15px; }\n\n.input-wrapper {\n  position: relative; }\n\n.user-access span.close {\n  width: 28px;\n  height: 28px;\n  cursor: pointer;\n  display: block;\n  color: #8492a6;\n  font-size: 18px;\n  line-height: 25px;\n  background: #FFF;\n  padding: 0 5px;\n  border-radius: 50%;\n  border: 1px solid rgba(0, 0, 0, 0.2);\n  transition: all .2s ease;\n  position: absolute;\n  right: 11px;\n  top: 11px;\n  margin-top: 15px; }\n\n.user-access span.close:hover {\n  color: #8492a6;\n  background: #f7f7f7;\n  border: 1px solid rgba(0, 0, 0, 0.2); }\n\n.terms {\n  margin-top: 15px;\n  font-size: 80%; }\n\n.user-access .error-messages {\n  background: 0;\n  font-size: 85%;\n  margin: 0;\n  color: #e7515c;\n  background: #fff2f3;\n  padding: 5px; }\n\n.user-access .error-messages ul {\n  padding-left: 0; }\n\n.user-access .errors {\n  list-style: none;\n  color: #e7515c;\n  margin: 0; }\n\n.error-messages ul,\n.errors {\n  list-style-type: none; }\n\n.user-access .error {\n  color: #e7515c;\n  background: #fff2f3;\n  padding: 5px;\n  font-size: 85%; }\n\n.user-access .referer-info {\n  font-size: 90%; }\n\n.user-access .referer-info p {\n  font-size: 90%; }\n\n.logo {\n  width: 70% !important; }\n\n.user-access .referer-logo {\n  margin-top: 15px; }\n\n.user-access .hidden {\n  display: none; }\n\n.bg-cover {\n  background-size: cover !important;\n  background-position: center !important;\n  background-repeat: no-repeat !important; }\n\n@media screen and (max-width: 480px) {\n  .register-quote {\n    margin-top: 190px; } }\n\n.slideshow {\n  position: relative; }\n\n.slideshow-list {\n  position: relative;\n  overflow: hidden; }\n\n.slideshow-item {\n  visibility: hidden;\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100vh; }\n\n@media screen and (max-width: 480px) {\n  .slideshow-item {\n    height: 100vh;\n    padding: 40px 80px; } }\n\n.slideshow-item-current {\n  visibility: visible;\n  position: relative; }\n\n.slideshow-fade .slideshow-item-hidePrevious,\n.slideshow-fade .slideshow-item-hideNext {\n  visibility: visible;\n  -webkit-animation: fadeOut 750ms cubic-bezier(0.455, 0.03, 0.515, 0.955) both;\n  animation: fadeOut 750ms cubic-bezier(0.455, 0.03, 0.515, 0.955) both; }\n\n.slideshow-fade .slideshow-item-hidePrevious,\n.slideshow-fade .slideshow-item-hideNext {\n  z-index: 2; }\n\n.slideshow-fade .slideshow-item-showPrevious,\n.slideshow-fade .slideshow-item-showNext {\n  z-index: 1; }\n\n@-webkit-keyframes fadeOut {\n  100% {\n    opacity: 0;\n    visibility: hidden; } }\n\n@keyframes fadeOut {\n  100% {\n    opacity: 0;\n    visibility: hidden; } }\n\n.slideshow-nav {\n  position: absolute;\n  bottom: 10%;\n  z-index: 9999;\n  margin-left: 70px; }\n\n.slideshow-pagination {\n  text-align: center; }\n\n.slideshow-dot {\n  text-indent: -9999px;\n  border: 0;\n  border-radius: 50%;\n  width: 10px;\n  height: 10px;\n  padding: 0;\n  margin: 5px;\n  background-color: rgba(255, 255, 255, 0.5);\n  -webkit-appearance: none; }\n\n.slideshow-dot-current {\n  background-color: white; }\n\n#checkbox-image {\n  display: inline-block;\n  width: 13px;\n  height: 13px;\n  background: url(https://cdn1.alegra.com/js/extjs2/resources/ext-theme-gray/images/form/checkbox.gif) -39px 0 no-repeat;\n  vertical-align: bottom;\n  margin-bottom: 10px; }\n\n#checkbox-image.checked {\n  background: url(https://cdn1.alegra.com/js/extjs2/resources/ext-theme-gray/images/form/checkbox.gif) 0 -26px no-repeat; }\n\n.td-loading {\n  background: white !important; }\n\n.mat-list-item.single-item {\n  height: 68px !important;\n  padding: 0 4px !important;\n  background: #FFF;\n  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-list-item.single-item.active {\n  height: 68px !important;\n  padding: 0 0px !important; }\n\n.mat-list-item.single-item:hover {\n  background: rgba(0, 0, 0, 0.04); }\n\n.link > a {\n  cursor: pointer; }\n\n.ngx-charts {\n  height: 200px !important; }\n\n.enviar {\n  width: 105% !important; }\n\ninput:not([type]),\ninput[type=text]:not(.browser-default),\ninput[type=password]:not(.browser-default),\ninput[type=email]:not(.browser-default),\ninput[type=url]:not(.browser-default),\ninput[type=time]:not(.browser-default),\ninput[type=date]:not(.browser-default),\ninput[type=datetime]:not(.browser-default),\ninput[type=datetime-local]:not(.browser-default),\ninput[type=tel]:not(.browser-default),\ninput[type=number]:not(.browser-default),\ninput[type=search]:not(.browser-default),\ntextarea.materialize-textarea {\n  background-color: #fff !important; }\n\n.mat-column-select {\n  flex: 0 !important; }\n\n.sidenav-container {\n  display: none; }\n"

/***/ }),

/***/ "./src/app/paginas/ingreso/ingreso.component.ts":
/*!******************************************************!*\
  !*** ./src/app/paginas/ingreso/ingreso.component.ts ***!
  \******************************************************/
/*! exports provided: IngresoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IngresoComponent", function() { return IngresoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/auth.service */ "./src/app/core/auth.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../core/notify.service */ "./src/app/core/notify.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var config = {
    message: "Se ha enviado un enlace de restauración al correo electrónico",
    disableClose: false,
    title: "Atención",
    closeButton: "Aceptar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var errorConfig = {
    message: "Es necesario que ingrese un correo electrónico",
    disableClose: false,
    title: "Error",
    closeButton: "Cerrar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var IngresoComponent = /** @class */ (function () {
    function IngresoComponent(_route, _loadingService, _dialogService, _viewContainerRef, _auth, _notify) {
        this._route = _route;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._viewContainerRef = _viewContainerRef;
        this._auth = _auth;
        this._notify = _notify;
        this.item = {
            user: "",
            password: ""
        };
        this.showNavBar = false;
        this.showLogin = true;
    }
    IngresoComponent.prototype.ngOnInit = function () { };
    IngresoComponent.prototype.login = function () {
        var _this = this;
        this._auth.emailLogin(this.item.user, this.item.password).then(function () {
            _this._route.navigate(["inicio"]);
        })
            .catch(function (error) {
            _this._notify.errorMessage("Usuario o contrase\u00F1a invalidos", 2000);
        });
    };
    IngresoComponent.prototype.restart = function () {
        if (this.item.user) {
            this._loadingService.register();
            this._dialogService.openAlert(config);
            this._loadingService.resolve();
        }
        else {
            this._dialogService.openAlert(errorConfig);
        }
    };
    IngresoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-ingreso",
            template: __webpack_require__(/*! ./ingreso.component.html */ "./src/app/paginas/ingreso/ingreso.component.html"),
            styles: [__webpack_require__(/*! ./ingreso.component.scss */ "./src/app/paginas/ingreso/ingreso.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"],
            _core_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"]])
    ], IngresoComponent);
    return IngresoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/inicio/inicio.component.html":
/*!******************************************************!*\
  !*** ./src/app/paginas/inicio/inicio.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n\n\n"

/***/ }),

/***/ "./src/app/paginas/inicio/inicio.component.scss":
/*!******************************************************!*\
  !*** ./src/app/paginas/inicio/inicio.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor {\n  padding: 1em; }\n"

/***/ }),

/***/ "./src/app/paginas/inicio/inicio.component.ts":
/*!****************************************************!*\
  !*** ./src/app/paginas/inicio/inicio.component.ts ***!
  \****************************************************/
/*! exports provided: InicioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioComponent", function() { return InicioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InicioComponent = /** @class */ (function () {
    function InicioComponent() {
    }
    InicioComponent.prototype.ngOnInit = function () { };
    InicioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-inicio",
            template: __webpack_require__(/*! ./inicio.component.html */ "./src/app/paginas/inicio/inicio.component.html"),
            styles: [__webpack_require__(/*! ./inicio.component.scss */ "./src/app/paginas/inicio/inicio.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InicioComponent);
    return InicioComponent;
}());



/***/ }),

/***/ "./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Registro de categoria</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre en Español\" required>\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombreingles\" placeholder=\"Nombre en Inglés\">\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombreitaliano\" placeholder=\"Nombre en Italiano\">\n  </mat-form-field>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre\">Crear categoria</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.ts ***!
  \********************************************************************************/
/*! exports provided: CrearCategoriaDialogoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearCategoriaDialogoComponent", function() { return CrearCategoriaDialogoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CrearCategoriaDialogoComponent = /** @class */ (function () {
    function CrearCategoriaDialogoComponent(db, notify, dialogRef) {
        this.db = db;
        this.notify = notify;
        this.dialogRef = dialogRef;
        this.info = {};
    }
    CrearCategoriaDialogoComponent.prototype.ngOnInit = function () { };
    CrearCategoriaDialogoComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .add("categorias", this.info)
            .then(function () {
            _this.notify.update("Categoria creada exitosamente", "success");
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error creando registro", "error");
        });
    };
    CrearCategoriaDialogoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearCategoriaDialogoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear",
            template: __webpack_require__(/*! ./crear.component.html */ "./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.html"),
            styles: [__webpack_require__(/*! ./crear.component.scss */ "./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"]])
    ], CrearCategoriaDialogoComponent);
    return CrearCategoriaDialogoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar categoria</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre en Español\" required>\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombreingles\" placeholder=\"Nombre en Inglés\">\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombreitaliano\" placeholder=\"Nombre en Italiano\">\n  </mat-form-field>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre\">Modificar</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.ts ***!
  \****************************************************************************************/
/*! exports provided: ModificarCategoriaDialogoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarCategoriaDialogoComponent", function() { return ModificarCategoriaDialogoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var ModificarCategoriaDialogoComponent = /** @class */ (function () {
    function ModificarCategoriaDialogoComponent(db, notify, snackBar, dialogRef, data) {
        this.db = db;
        this.notify = notify;
        this.snackBar = snackBar;
        this.dialogRef = dialogRef;
        this.data = data;
        this.info = {};
        this.info = data.item;
    }
    ModificarCategoriaDialogoComponent.prototype.ngOnInit = function () { };
    ModificarCategoriaDialogoComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .update("categorias/" + this.info.id, this.info)
            .then(function () {
            _this.snackBar.open("Categoria modificado", "Cerrar", {
                duration: 2000,
                horizontalPosition: "left"
            });
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error modificando registro", "error");
        });
    };
    ModificarCategoriaDialogoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarCategoriaDialogoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar",
            template: __webpack_require__(/*! ./modificar.component.html */ "./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.html"),
            styles: [__webpack_require__(/*! ./modificar.component.scss */ "./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.scss")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], Object])
    ], ModificarCategoriaDialogoComponent);
    return ModificarCategoriaDialogoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/parametros-categorias/parametros-categorias.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/paginas/parametros-categorias/parametros-categorias.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <span class=\"titulo-seccion\">Categorias</span>\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" matTooltip=\"Crear nuevo registro\" mat-raised-button (click)=\"crear()\">\n          <mat-icon matListIcon class=\"small\">add</mat-icon>\n          Crear categoria\n        </button>\n      </div>\n    </mat-toolbar>\n  </div>\n\n\n  <div class=\"mat-elevation-z8\">\n    <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n\n      <!-- Name Column -->\n      <ng-container matColumnDef=\"nombre\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre categoria</mat-header-cell>\n        <mat-cell  (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.nombre}}</mat-cell>\n      </ng-container>\n\n      <!-- Name Column -->\n      <ng-container matColumnDef=\"nombreingles\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre Ingles</mat-header-cell>\n        <mat-cell  (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.nombreingles}}</mat-cell>\n      </ng-container>\n\n      <ng-container matColumnDef=\"nombreitaliano\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre Italiano</mat-header-cell>\n        <mat-cell  (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.nombreitaliano}}</mat-cell>\n      </ng-container>\n\n      <!-- Checkbox Column -->\n      <ng-container matColumnDef=\"opciones\">\n        <mat-header-cell *matHeaderCellDef></mat-header-cell>\n        <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n          <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n            <mat-icon>more_vert</mat-icon>\n          </button>\n          <mat-menu #menu=\"matMenu\">\n\n            <button mat-menu-item (click)=\"eliminar(row)\">Eliminar</button>\n          </mat-menu>\n        </mat-cell>\n      </ng-container>\n\n      <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n      <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n    </mat-table>\n\n    <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n    </mat-paginator>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/parametros-categorias/parametros-categorias.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/paginas/parametros-categorias/parametros-categorias.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column;\n  min-width: 300px; }\n\n.example-header {\n  min-height: 64px;\n  padding: 25px 24px 0;\n  background-color: #fff;\n  color: white; }\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%; }\n\n.mat-table {\n  overflow: auto;\n  height: 50vh; }\n\ninput.mat-input-element {\n  color: black; }\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n  max-height: 500px;\n  min-width: 300px; }\n\n.mat-table {\n  overflow: auto;\n  max-height: 500px; }\n\n.mat-column-select {\n  overflow: visible; }\n\n.mat-column-placa {\n  flex: 0 0 10%;\n  text-align: left; }\n\n.mat-column-propietario {\n  flex: 0 0 20%; }\n\n.mat-column-flota {\n  flex: 0 0 20%; }\n\n.mat-column-estado {\n  flex: 0 0 20%; }\n\n.label-active {\n  background: green;\n  color: white;\n  padding: .5em;\n  border-radius: 3px; }\n\n.label-inactive {\n  background: red;\n  color: white;\n  padding: .5em;\n  border-radius: 3px; }\n\n.estatus-cell {\n  overflow: initial; }\n\n.mat-cell {\n  color: #0091ae;\n  cursor: pointer; }\n\n.mat-cell:hover {\n  text-decoration: underline; }\n"

/***/ }),

/***/ "./src/app/paginas/parametros-categorias/parametros-categorias.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/paginas/parametros-categorias/parametros-categorias.component.ts ***!
  \**********************************************************************************/
/*! exports provided: ParametrosCategoriasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParametrosCategoriasComponent", function() { return ParametrosCategoriasComponent; });
/* harmony import */ var _dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dialogo/crear/crear.component */ "./src/app/paginas/parametros-categorias/dialogo/crear/crear.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dialogo/modificar/modificar.component */ "./src/app/paginas/parametros-categorias/dialogo/modificar/modificar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









// Configuración de confirm eliminar
var configDel = {
    message: '¿Está seguro de eliminar el registro?',
    disableClose: false,
    title: 'Confirmación',
    cancelButton: 'Cancelar',
    acceptButton: 'Aceptar',
    width: '500px',
};
var ParametrosCategoriasComponent = /** @class */ (function () {
    function ParametrosCategoriasComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = [
            "nombre",
            "nombreingles",
            "nombreitaliano",
            "opciones"
        ];
        this.lista = [];
    }
    ParametrosCategoriasComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    ParametrosCategoriasComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("categorias").subscribe(function (resp) {
            _this.lista = resp;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    ParametrosCategoriasComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_8__["ModificarCategoriaDialogoComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) { });
    };
    ParametrosCategoriasComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("categorias/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Categoria eliminada");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    ParametrosCategoriasComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_0__["CrearCategoriaDialogoComponent"], {
            width: "60%"
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ParametrosCategoriasComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], ParametrosCategoriasComponent.prototype, "sort", void 0);
    ParametrosCategoriasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-parametros-categorias",
            template: __webpack_require__(/*! ./parametros-categorias.component.html */ "./src/app/paginas/parametros-categorias/parametros-categorias.component.html"),
            styles: [__webpack_require__(/*! ./parametros-categorias.component.scss */ "./src/app/paginas/parametros-categorias/parametros-categorias.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_6__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_4__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_5__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_7__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], ParametrosCategoriasComponent);
    return ParametrosCategoriasComponent;
}());



/***/ }),

/***/ "./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.html":
/*!*****************************************************************************************************************************************!*\
  !*** ./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.html ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar sección de documento</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.seccion\" placeholder=\"Sección\" readonly>\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.idcarpeta\" placeholder=\"ID Carpeta\">\n  </mat-form-field>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\">Modificar</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.scss":
/*!*****************************************************************************************************************************************!*\
  !*** ./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.scss ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.ts":
/*!***************************************************************************************************************************************!*\
  !*** ./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.ts ***!
  \***************************************************************************************************************************************/
/*! exports provided: ModificarSeccionDocumentoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarSeccionDocumentoComponent", function() { return ModificarSeccionDocumentoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var ModificarSeccionDocumentoComponent = /** @class */ (function () {
    function ModificarSeccionDocumentoComponent(db, notify, snackBar, dialogRef, data) {
        this.db = db;
        this.notify = notify;
        this.snackBar = snackBar;
        this.dialogRef = dialogRef;
        this.data = data;
        this.info = {};
        this.info = data.item;
    }
    ModificarSeccionDocumentoComponent.prototype.ngOnInit = function () { };
    ModificarSeccionDocumentoComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .update("secciones-documentos/" + this.info.id, this.info)
            .then(function () {
            _this.snackBar.open("Sección modificada", "Cerrar", {
                duration: 2000,
                horizontalPosition: "left"
            });
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error modificando registro", "error");
        });
    };
    ModificarSeccionDocumentoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarSeccionDocumentoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar-seccion-documento",
            template: __webpack_require__(/*! ./modificar-seccion-documento.component.html */ "./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.html"),
            styles: [__webpack_require__(/*! ./modificar-seccion-documento.component.scss */ "./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.scss")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], Object])
    ], ModificarSeccionDocumentoComponent);
    return ModificarSeccionDocumentoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.html":
/*!********************************************************************************************************!*\
  !*** ./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <span class=\"titulo-seccion\">Secciones de documentos</span>\n    </mat-toolbar>\n  </div>\n\n\n  <div class=\"mat-elevation-z8\">\n    <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n\n      <!-- Name Column -->\n      <ng-container matColumnDef=\"seccion\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre sección</mat-header-cell>\n        <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.seccion}}</mat-cell>\n      </ng-container>\n\n      <!-- Name Column -->\n      <ng-container matColumnDef=\"idcarpeta\">\n        <mat-header-cell *matHeaderCellDef mat-sort-header>ID Carpeta</mat-header-cell>\n        <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.idcarpeta}}</mat-cell>\n      </ng-container>\n\n      <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n      <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n    </mat-table>\n\n    <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n    </mat-paginator>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.scss ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column;\n  min-width: 300px; }\n\n.example-header {\n  min-height: 64px;\n  padding: 25px 24px 0;\n  background-color: #fff;\n  color: white; }\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%; }\n\n.mat-table {\n  overflow: auto;\n  height: 50vh; }\n\ninput.mat-input-element {\n  color: black; }\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n  max-height: 500px;\n  min-width: 300px; }\n\n.mat-table {\n  overflow: auto;\n  max-height: 500px; }\n\n.mat-column-select {\n  overflow: visible; }\n\n.mat-column-placa {\n  flex: 0 0 10%;\n  text-align: left; }\n\n.mat-column-propietario {\n  flex: 0 0 20%; }\n\n.mat-column-flota {\n  flex: 0 0 20%; }\n\n.mat-column-estado {\n  flex: 0 0 20%; }\n\n.label-active {\n  background: green;\n  color: white;\n  padding: .5em;\n  border-radius: 3px; }\n\n.label-inactive {\n  background: red;\n  color: white;\n  padding: .5em;\n  border-radius: 3px; }\n\n.estatus-cell {\n  overflow: initial; }\n\n.mat-cell {\n  color: #0091ae;\n  cursor: pointer; }\n\n.mat-cell:hover {\n  text-decoration: underline; }\n"

/***/ }),

/***/ "./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: ParametrosSeccionesDocumentosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParametrosSeccionesDocumentosComponent", function() { return ParametrosSeccionesDocumentosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _dialogos_modificar_seccion_documento_modificar_seccion_documento_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dialogos/modificar-seccion-documento/modificar-seccion-documento.component */ "./src/app/paginas/parametros-secciones-documentos/dialogos/modificar-seccion-documento/modificar-seccion-documento.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: '¿Está seguro de eliminar el registro?',
    disableClose: false,
    title: 'Confirmación',
    cancelButton: 'Cancelar',
    acceptButton: 'Aceptar',
    width: '500px',
};
var ParametrosSeccionesDocumentosComponent = /** @class */ (function () {
    function ParametrosSeccionesDocumentosComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = [
            "seccion",
            "idcarpeta"
        ];
        this.lista = [];
    }
    ParametrosSeccionesDocumentosComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    ParametrosSeccionesDocumentosComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("secciones-documentos").subscribe(function (resp) {
            _this.lista = resp;
            _this.lista.forEach(function (element, index) {
                if (!element.seccion) {
                    _this.lista.splice(index, 1);
                }
            });
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    ParametrosSeccionesDocumentosComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogos_modificar_seccion_documento_modificar_seccion_documento_component__WEBPACK_IMPORTED_MODULE_7__["ModificarSeccionDocumentoComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) { });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], ParametrosSeccionesDocumentosComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], ParametrosSeccionesDocumentosComponent.prototype, "sort", void 0);
    ParametrosSeccionesDocumentosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-parametros-secciones-documentos',
            template: __webpack_require__(/*! ./parametros-secciones-documentos.component.html */ "./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.html"),
            styles: [__webpack_require__(/*! ./parametros-secciones-documentos.component.scss */ "./src/app/paginas/parametros-secciones-documentos/parametros-secciones-documentos.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_5__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], ParametrosSeccionesDocumentosComponent);
    return ParametrosSeccionesDocumentosComponent;
}());



/***/ }),

/***/ "./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Nuevo proyecto</h1>\n\n<div mat-dialog-content class=\"example-container\" style=\"    display: flex;\">\n\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de proyecto</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título\">\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n          <mat-option value=\"español\">Español</mat-option>\n          <mat-option value=\"ingles\">Ingles</mat-option>\n          <mat-option value=\"italiano\">Italiano</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <textarea matInput placeholder=\"Resumen\" [(ngModel)]=\"info.resumen\"></textarea>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.documentourl\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarPdf($event)\" color=\"primary\" accept=\".pdf\"\n          class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Cargar archivo</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-12 icon-pdf\" *ngIf=\"info.documentourl\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarPdf()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n\n\n  </div>\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.resumen || !info.idioma\">Crear proyecto</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n\n.icon-pdf {\n  background-image: url('icon-pdf.png');\n  background-size: contain;\n  background-position: center;\n  background-repeat: no-repeat;\n  width: 100%;\n  padding: 3em;\n  color: #fb7958 !important;\n  border: 1px dashed !important; }\n\n.icon-pdf button {\n    position: absolute;\n    right: 1em; }\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.ts ***!
  \***************************************************************************************/
/*! exports provided: CrearProyectoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearProyectoComponent", function() { return CrearProyectoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var CrearProyectoComponent = /** @class */ (function () {
    function CrearProyectoComponent(db, notify, upload, dialogRef, _loadingService, data, _upload) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.files = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
    }
    CrearProyectoComponent.prototype.ngOnInit = function () { };
    CrearProyectoComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.db
            .add("proyectos", this.info)
            .then(function () {
            _this.notify.update("Proyecto guardado", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    CrearProyectoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearProyectoComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    CrearProyectoComponent.prototype.cargarPdf = function (files) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadDocumento(currentUpload).then(function (url) {
                _this.info.documentourl = url;
            });
        }
    };
    CrearProyectoComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    CrearProyectoComponent.prototype.eliminarPdf = function () {
        this.info.documentourl = "";
    };
    CrearProyectoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear-proyecto",
            template: __webpack_require__(/*! ./crear-proyecto.component.html */ "./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.html"),
            styles: [__webpack_require__(/*! ./crear-proyecto.component.scss */ "./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], CrearProyectoComponent);
    return CrearProyectoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar proyecto</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.foto\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarImagen($event, 'imagen')\" color=\"primary\" accept=\"image/*\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Foto de proyecto</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-3 preview-image\" *ngIf=\"info.foto\" [ngStyle]=\"{'background-image': 'url(' + info.foto + ')'}\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFoto()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n\n    <div class=\"col-md-6\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título\">\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-6\">\n      <mat-form-field>\n        <mat-select placeholder=\"Idioma\" required [(ngModel)]=\"info.idioma\">\n          <mat-option value=\"español\">Español</mat-option>\n          <mat-option value=\"ingles\">Ingles</mat-option>\n          <mat-option value=\"italiano\">Italiano</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n    <div class=\"col-md-12\">\n      <mat-form-field>\n        <textarea matInput placeholder=\"Resumen\" [(ngModel)]=\"info.resumen\"></textarea>\n      </mat-form-field>\n    </div>\n\n    <div class=\"col-md-12\">\n      <div class=\"boton-agregar\" *ngIf=\"!info.documentourl\">\n        <td-file-input [(ngModel)]=\"files\" (select)=\"cargarPdf($event)\" color=\"primary\" accept=\".pdf\" class=\"suputamadre\">\n          <mat-icon>add</mat-icon>\n          <p>Cargar archivo</p>\n        </td-file-input>\n      </div>\n\n\n      <div class=\"col-md-12 icon-pdf\" *ngIf=\"info.documentourl\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarPdf()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </div>\n\n  </div>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo ||  !info.resumen || !info.idioma\">Guardar proyecto</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".icon-pdf {\n  background-image: url('icon-pdf.png');\n  background-size: contain;\n  background-position: center;\n  background-repeat: no-repeat;\n  width: 100%;\n  padding: 3em;\n  color: #fb7958 !important;\n  border: 1px dashed !important; }\n  .icon-pdf button {\n    position: absolute;\n    right: 1em; }\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: ModificarProyectoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarProyectoComponent", function() { return ModificarProyectoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var ModificarProyectoComponent = /** @class */ (function () {
    function ModificarProyectoComponent(db, notify, upload, dialogRef, _loadingService, data, _upload) {
        this.db = db;
        this.notify = notify;
        this.upload = upload;
        this.dialogRef = dialogRef;
        this._loadingService = _loadingService;
        this.data = data;
        this._upload = _upload;
        this.info = {};
        this.files = {};
        this.info = data.item;
    }
    ModificarProyectoComponent.prototype.ngOnInit = function () { };
    ModificarProyectoComponent.prototype.guardar = function () {
        var _this = this;
        this._loadingService.register();
        this.db
            .update("proyectos/" + this.info.id, this.info)
            .then(function () {
            _this.notify.update("Proyecto guardado", "success");
            _this._loadingService.resolve();
            _this.cerrar();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.notify.update("Error creando registro", "error");
        });
    };
    ModificarProyectoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarProyectoComponent.prototype.cargarImagen = function (files, tipo) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadPic(currentUpload).then(function (url) {
                _this.info.foto = url;
            });
        }
    };
    ModificarProyectoComponent.prototype.eliminarFoto = function () {
        this.info.foto = "";
    };
    ModificarProyectoComponent.prototype.cargarPdf = function (files) {
        var _this = this;
        if (files instanceof FileList) {
        }
        else {
            var currentUpload = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](files);
            this._upload.uploadDocumento(currentUpload).then(function (url) {
                _this.info.documentourl = url;
            });
        }
    };
    ModificarProyectoComponent.prototype.eliminarPdf = function () {
        this.info.documentourl = "";
    };
    ModificarProyectoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar-proyecto",
            template: __webpack_require__(/*! ./modificar-proyecto.component.html */ "./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.html"),
            styles: [__webpack_require__(/*! ./modificar-proyecto.component.scss */ "./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.scss")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"], Object, _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], ModificarProyectoComponent);
    return ModificarProyectoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/proyectos/proyectos.component.html":
/*!************************************************************!*\
  !*** ./src/app/paginas/proyectos/proyectos.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <button mat-button class=\"btn-ruta\">Proyecto</button>\n\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" (click)=\"crear()\">\n          <mat-icon matListIcon class=\"small\">add</mat-icon>\n          Crear proyecto\n        </button>\n      </div>\n\n    </mat-toolbar>\n  </div>\n\n  <tabla-proyectos></tabla-proyectos>\n\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/proyectos.component.scss":
/*!************************************************************!*\
  !*** ./src/app/paginas/proyectos/proyectos.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  min-height: calc(90vh - 60px);\n  padding: 0 1em; }\n  .row .col-md-3 {\n    background: #f5f8fa;\n    border: 1px solid #dfe3eb;\n    padding: 0; }\n  .row .col-md-3 .header {\n      border-bottom: 1px solid #dfe3eb;\n      padding: 16px 16px;\n      font-size: 12px;\n      -webkit-hyphens: none;\n          -ms-hyphens: none;\n              hyphens: none;\n      overflow: hidden !important;\n      text-overflow: ellipsis !important;\n      white-space: nowrap !important;\n      font-weight: 600;\n      text-transform: uppercase;\n      font-family: \"Roboto\"; }\n  .row .col-md-3 .tareas {\n      margin: 1em;\n      min-height: calc(90vh - 120px); }\n  .row .col-md-3 .tareas mat-card {\n        margin-bottom: 1em; }\n  .titulo {\n  margin: 0 !important;\n  color: #0091ae;\n  cursor: pointer; }\n  .titulo:hover {\n  font-weight: 500; }\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/proyectos.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/paginas/proyectos/proyectos.component.ts ***!
  \**********************************************************/
/*! exports provided: ProyectosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProyectosComponent", function() { return ProyectosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialogos_crear_proyecto_crear_proyecto_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dialogos/crear-proyecto/crear-proyecto.component */ "./src/app/paginas/proyectos/dialogos/crear-proyecto/crear-proyecto.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProyectosComponent = /** @class */ (function () {
    function ProyectosComponent(db, _dialogService, dialog, snackBar) {
        this.db = db;
        this._dialogService = _dialogService;
        this.dialog = dialog;
        this.snackBar = snackBar;
    }
    ProyectosComponent.prototype.ngOnInit = function () {
    };
    ProyectosComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogos_crear_proyecto_crear_proyecto_component__WEBPACK_IMPORTED_MODULE_4__["CrearProyectoComponent"], {
            width: "60%"
        });
    };
    ProyectosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-proyectos',
            template: __webpack_require__(/*! ./proyectos.component.html */ "./src/app/paginas/proyectos/proyectos.component.html"),
            styles: [__webpack_require__(/*! ./proyectos.component.scss */ "./src/app/paginas/proyectos/proyectos.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__["TdDialogService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], ProyectosComponent);
    return ProyectosComponent;
}());



/***/ }),

/***/ "./src/app/paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n    <!-- Id Column -->\n    <ng-container matColumnDef=\"titulo\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Titulo</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.titulo}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"resumen\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Resumen</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.resumen | slice:0:100}} ...</mat-cell>\n    </ng-container>\n\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"idioma\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Idioma</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.idioma}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"createdAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Creado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.createdAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"updatedAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Modificado el</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.updatedAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n    <!-- Checkbox Column -->\n    <ng-container matColumnDef=\"opciones\">\n      <mat-header-cell *matHeaderCellDef></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n\n          <button mat-menu-item (click)=\"eliminar(row)\">Eliminar</button>\n        </mat-menu>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n\n  <mat-paginator #paginator [length]=\"lista.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-cell {\n  color: #0091ae;\n  cursor: pointer; }\n\n.mat-cell:hover {\n  text-decoration: underline; }\n\n.mat-column-nombre {\n  flex: 0 0 30%;\n  text-align: left; }\n\n.mat-column-correo {\n  flex: 0 0 30%;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component.ts ***!
  \***************************************************************************************/
/*! exports provided: TablaProyectosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablaProyectosComponent", function() { return TablaProyectosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _dialogos_modificar_proyecto_modificar_proyecto_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../dialogos/modificar-proyecto/modificar-proyecto.component */ "./src/app/paginas/proyectos/dialogos/modificar-proyecto/modificar-proyecto.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: "¿Está seguro de eliminar el registro?",
    disableClose: false,
    title: "Confirmación",
    cancelButton: "Cancelar",
    acceptButton: "Aceptar",
    width: "500px" //OPTIONAL, defaults to 400px
};
var TablaProyectosComponent = /** @class */ (function () {
    function TablaProyectosComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = ["titulo", "resumen", "idioma", "createdAt", "updatedAt", "opciones"];
        this.lista = [];
    }
    TablaProyectosComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    TablaProyectosComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("proyectos").subscribe(function (resp) {
            _this.lista = resp;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    TablaProyectosComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogos_modificar_proyecto_modificar_proyecto_component__WEBPACK_IMPORTED_MODULE_7__["ModificarProyectoComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    TablaProyectosComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("proyectos/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Proyecto eliminado");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], TablaProyectosComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], TablaProyectosComponent.prototype, "sort", void 0);
    TablaProyectosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'tabla-proyectos',
            template: __webpack_require__(/*! ./tabla-proyectos.component.html */ "./src/app/paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component.html"),
            styles: [__webpack_require__(/*! ./tabla-proyectos.component.scss */ "./src/app/paginas/proyectos/tablas/tabla-proyectos/tabla-proyectos.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_5__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_4__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], TablaProyectosComponent);
    return TablaProyectosComponent;
}());



/***/ }),

/***/ "./src/app/paginas/tareas/dialogo/crear/crear.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/paginas/tareas/dialogo/crear/crear.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Crear tarea</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <mat-form-field>\n    <mat-select placeholder=\"Estado\" required [(ngModel)]=\"info.estado\">\n      <mat-option value=\"pendiente\">Pendiente</mat-option>\n      <mat-option value=\"proceso\">En proceso</mat-option>\n      <mat-option value=\"revision\">En revisión</mat-option>\n      <mat-option value=\"terminada\">Terminado</mat-option>\n    </mat-select>\n  </mat-form-field>\n\n\n  <mat-tab-group style=\"margin-top: 1em;\">\n    <mat-tab label=\"Español\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título de la tarea\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Notas\" [(ngModel)]=\"info.notas\"></textarea>\n      </mat-form-field>\n      <td-file-upload #singleFileUpload [(ngModel)]=\"file\" (select)=\"uploadEvent($event)\" (upload)=\"uploadEvent($event)\"\n        (cancel)=\"cancelEvent()\" required>\n        <mat-icon>file_upload</mat-icon>\n        <span>{{ singleFileUpload.value?.name }}</span>\n        <ng-template td-file-input-label>\n          <mat-icon>attach_file</mat-icon>\n          <span>\n            Seleccione archivo\n            <span [hidden]=\"!singleFileUpload?.required\">*</span>\n          </span>\n        </ng-template>\n      </td-file-upload>\n    </mat-tab>\n    <mat-tab label=\"Ingles\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloingles\" placeholder=\"Título ingles\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Notas ingles\" [(ngModel)]=\"info.notasingles\"></textarea>\n      </mat-form-field>\n      <td-file-upload #singleFileUploadIngles [(ngModel)]=\"fileingles\" (select)=\"uploadEventIngles($event)\" (upload)=\"uploadEventIngles($event)\"\n        (cancel)=\"cancelEvent()\" required>\n        <mat-icon>file_upload</mat-icon>\n        <span>{{ singleFileUploadIngles.value?.name }}</span>\n        <ng-template td-file-input-label>\n          <mat-icon>attach_file</mat-icon>\n          <span>\n            Seleccione archivo\n            <span [hidden]=\"!singleFileUploadIngles?.required\">*</span>\n          </span>\n        </ng-template>\n      </td-file-upload>\n    </mat-tab>\n    <mat-tab label=\"Italiano\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloitaliano\" placeholder=\"Título italiano\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Notas italiano\" [(ngModel)]=\"info.notasitaliano\"></textarea>\n      </mat-form-field>\n      <td-file-upload #singleFileUploadItaliano [(ngModel)]=\"fileitaliano\" (select)=\"uploadEventItaliano($event)\" (upload)=\"uploadEventItaliano($event)\"\n        (cancel)=\"cancelEvent()\" required>\n        <mat-icon>file_upload</mat-icon>\n        <span>{{ singleFileUploadItaliano.value?.name }}</span>\n        <ng-template td-file-input-label>\n          <mat-icon>attach_file</mat-icon>\n          <span>\n            Seleccione archivo\n            <span [hidden]=\"!singleFileUploadItaliano?.required\">*</span>\n          </span>\n        </ng-template>\n      </td-file-upload>\n    </mat-tab>\n  </mat-tab-group>\n\n\n\n\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.notas || !info.estado\">Crear tarea</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/tareas/dialogo/crear/crear.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/paginas/tareas/dialogo/crear/crear.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/tareas/dialogo/crear/crear.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/paginas/tareas/dialogo/crear/crear.component.ts ***!
  \*****************************************************************/
/*! exports provided: CrearTareaDialogoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearTareaDialogoComponent", function() { return CrearTareaDialogoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CrearTareaDialogoComponent = /** @class */ (function () {
    function CrearTareaDialogoComponent(db, notify, dialogRef, _uploadService, _loadingService) {
        this.db = db;
        this.notify = notify;
        this.dialogRef = dialogRef;
        this._uploadService = _uploadService;
        this._loadingService = _loadingService;
        this.info = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
    }
    CrearTareaDialogoComponent.prototype.ngOnInit = function () { };
    CrearTareaDialogoComponent.prototype.guardar = function () {
        var _this = this;
        // Consulto en que index esta la ultima tarea
        var query = this.db
            .colWithIds$("tareas", function (ref) {
            return ref.where("estado", "==", _this.info.estado).orderBy("index", "asc");
        })
            .subscribe(function (resp) {
            query.unsubscribe();
            var index = 0;
            if (resp.length > 0) {
                var ultimo = resp[resp.length - 1];
                if (ultimo.index) {
                    index = ultimo.index + 1;
                }
                else {
                    index = 1;
                }
            }
            else {
                index = 1;
            }
            _this.info.index = index;
            _this.db
                .add("tareas", _this.info)
                .then(function () {
                _this.notify.update("Tarea creada exitosamente", "success");
                _this.cerrar();
            })
                .catch(function (err) {
                _this.notify.update("Error creando registro", "error");
            });
        });
    };
    CrearTareaDialogoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearTareaDialogoComponent.prototype.uploadEvent = function (file) {
        var _this = this;
        this.fileUploadMsg = file.name;
        this.file = file;
        this._loadingService.register();
        var archivo = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](this.file);
        this._uploadService
            .uploadDocumento(archivo)
            .then(function (url) {
            _this.info.url = url;
            _this._loadingService.resolve();
        });
    };
    CrearTareaDialogoComponent.prototype.uploadEventIngles = function (file) {
        var _this = this;
        this.fileUploadMsg = file.name;
        this.fileingles = file;
        this._loadingService.register();
        var archivo = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](this.fileingles);
        this._uploadService
            .uploadDocumento(archivo)
            .then(function (url) {
            _this.info.urlingles = url;
            _this._loadingService.resolve();
        });
    };
    CrearTareaDialogoComponent.prototype.uploadEventItaliano = function (file) {
        var _this = this;
        this.fileUploadMsg = file.name;
        this.fileitaliano = file;
        this._loadingService.register();
        var archivo = new _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](this.fileitaliano);
        this._uploadService
            .uploadDocumento(archivo)
            .then(function (url) {
            _this.info.urlitaliano = url;
            _this._loadingService.resolve();
        });
    };
    CrearTareaDialogoComponent.prototype.cancelEvent = function () {
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.info.file = null;
    };
    CrearTareaDialogoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear",
            template: __webpack_require__(/*! ./crear.component.html */ "./src/app/paginas/tareas/dialogo/crear/crear.component.html"),
            styles: [__webpack_require__(/*! ./crear.component.scss */ "./src/app/paginas/tareas/dialogo/crear/crear.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_5__["TdLoadingService"]])
    ], CrearTareaDialogoComponent);
    return CrearTareaDialogoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/tareas/dialogo/modificar/modificar.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/paginas/tareas/dialogo/modificar/modificar.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar tarea</h1>\n\n<div mat-dialog-content class=\"example-container\">\n\n  <mat-form-field>\n    <mat-select placeholder=\"Estado\" required [(ngModel)]=\"info.estado\">\n      <mat-option value=\"pendiente\">Pendiente</mat-option>\n      <mat-option value=\"proceso\">En proceso</mat-option>\n      <mat-option value=\"revision\">En revisión</mat-option>\n      <mat-option value=\"terminada\">Terminado</mat-option>\n    </mat-select>\n  </mat-form-field>\n\n  <mat-tab-group>\n    <mat-tab label=\"Español\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.titulo\" placeholder=\"Título de la tarea\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Notas\" [(ngModel)]=\"info.notas\"></textarea>\n      </mat-form-field>\n      <td-file-upload #singleFileUpload [(ngModel)]=\"file\" (select)=\"uploadEvent($event)\" (upload)=\"uploadEvent($event)\"\n        (cancel)=\"cancelEvent()\" required *ngIf=\"!info.url\">\n        <mat-icon>file_upload</mat-icon>\n        <span>{{ singleFileUpload.value?.name }}</span>\n        <ng-template td-file-input-label>\n          <mat-icon>attach_file</mat-icon>\n          <span>\n            Seleccione archivo\n            <span [hidden]=\"!singleFileUpload?.required\">*</span>\n          </span>\n        </ng-template>\n      </td-file-upload>\n      <div class=\"col-md-12 icon-pdf\" *ngIf=\"info.url\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFile()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </mat-tab>\n    <mat-tab label=\"Ingles\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloingles\" placeholder=\"Título ingles\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Notas ingles\" [(ngModel)]=\"info.notasingles\"></textarea>\n      </mat-form-field>\n      <td-file-upload #singleFileUploadIngles [(ngModel)]=\"fileingles\" (select)=\"uploadEventIngles($event)\" (upload)=\"uploadEventIngles($event)\"\n        (cancel)=\"cancelEvent()\" required *ngIf=\"!info.urlingles\">\n        <mat-icon>file_upload</mat-icon>\n        <span>{{ singleFileUploadIngles.value?.name }}</span>\n        <ng-template td-file-input-label>\n          <mat-icon>attach_file</mat-icon>\n          <span>\n            Seleccione archivo\n            <span [hidden]=\"!singleFileUploadIngles?.required\">*</span>\n          </span>\n        </ng-template>\n      </td-file-upload>\n      <div class=\"col-md-12 icon-pdf\" *ngIf=\"info.urlingles\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFileIngles()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </mat-tab>\n    <mat-tab label=\"Italiano\">\n      <mat-form-field required>\n        <input type=\"text\" matInput [(ngModel)]=\"info.tituloitaliano\" placeholder=\"Título italiano\">\n      </mat-form-field>\n      <mat-form-field>\n        <textarea matInput placeholder=\"Notas italiano\" [(ngModel)]=\"info.notasitaliano\"></textarea>\n      </mat-form-field>\n      <td-file-upload #singleFileUploadItaliano [(ngModel)]=\"fileitaliano\" (select)=\"uploadEventItaliano($event)\"\n        (upload)=\"uploadEventItaliano($event)\" (cancel)=\"cancelEvent()\" required *ngIf=\"!info.urlitaliano\">\n        <mat-icon>file_upload</mat-icon>\n        <span>{{ singleFileUploadItaliano.value?.name }}</span>\n        <ng-template td-file-input-label>\n          <mat-icon>attach_file</mat-icon>\n          <span>\n            Seleccione archivo\n            <span [hidden]=\"!singleFileUploadItaliano?.required\">*</span>\n          </span>\n        </ng-template>\n      </td-file-upload>\n      <div class=\"col-md-12 icon-pdf\" *ngIf=\"info.urlitaliano\">\n        <button mat-mini-fab color=\"warn\" (click)=\"eliminarFileItaliano()\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </div>\n    </mat-tab>\n  </mat-tab-group>\n\n\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.titulo || !info.notas || !info.estado\">Guardar</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/tareas/dialogo/modificar/modificar.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/paginas/tareas/dialogo/modificar/modificar.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n\n.icon-pdf {\n  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAW0ElEQVR42u2df4gd13XHz7z3dmVZK3lVWVLsGLxuaJs0pVJicBNKyDql/9SUKKUQFwzeUgr9p7aCwUS2064gbkmQLdkUUkpJ1qWm0NJYwSVJsSQ/0R+0BSdSm4Sowck6FLuyrUq2d/Vz923veW9m9723M29m3sydc+fc7wdGs6vdfXPfsvcz95x77p2AQG156q9/a9qc9oefzobnmfBg+Ov7Cl7mrDkuhR8vhgfTDs9nHnng65fyvSRwhUC6ASAd09G5k89Qr7PzwR37k9LtGuI09URxJjwWjRjOSDcKjAYCcIyws8/SRmcvegeXhkcQkRTakIJbQADCmA4/S70Oz4drd3Vb8GihTT0htKUb4zMQQMX03eEPkD8dPg0WwnHCCKFyIIAKMJ2eO/ss9Tr9ndLtcZzXaEMGx6Ubox0IwBJhp4+OW6TbU1PeoZ4MjkMGdoAASiQc3h8kdHobRDI4hjChPCCAgoRz8XPU6/gY3lcDhwnHzLGAGoRiQABjEmbv58zxoHRbPOc56omgLd2QOgIB5MR0/Dlzmifc7V2DRwXzRgQL0g2pExBABsJh/sHwQGzvNpwr4PDgGMKDdCCAEZiOP0O9Tj9H6Ph1g0WwQD0RLEo3xlUggBjCjj9PiO+1wHmCeYhgMxBAH31D/T+WbguwwmFCaDAABECI8T0DOYI+vBcAsvreglkD8lgAYdUe3wmwIMdveCHSQV+rC70TQDjcnzfHw9JtAU7xDPVGBF6FBV4JIFygs0CI80E8nB+Y82nhkRcCCKf1FgjDfZANDgvmfJg2VC8A0/k5sz9PuOuDfPBogEOCY9INsYlaAYSxPg/lcNcHReDRwAGtuQGVAkCsD0pGbW5AlQCQ4QeWUTdToEYA4bz+AtV/G23gNrzN+ZyWugEVAsCQH1SMmpCg9gIwnZ+ztBjyAwmeMRI4KN2IItRWAMjyA0eo9SxBLQUQxvvc+bGAB7hAbfMCtRNAuBknd37E+8AlOC9woG6bk9ZKAOHS3a9JtwOAEfxunZYY10YApvPPE3bqAfXgsJHAvHQjslALAZjOv0DYnw/Ui+eMBOakG5GG8wJA5wc1xnkJOC0AdH6gAKcl4KQAwjn+NqGsF+iApwlnXawVcE4A6PxAKU5KwEUBcDEFOj/QyFkjgP3SjejHKQEg5gce4FROwBkBoPMDj3BGAk4IAJ0feIgTEhAXACr8gMeIVwyKCgC1/QDIrh0QE0C4qu9lqesD4BD3Sq0iFBFAuJ6f3zCW9ALQW0o8K7GfQOUCQKEPALGIFApJCKBNirfx2tEi2t5ck25G5Vyb3Etvv/emdDPqzmkjgNkqL1ipALRu4Pn+LWv0oW1r3fP2pnRrhPj48/T8t56gi0s/lW5J3al0o9HKBBBu3f1CVderAu7w9+zodXzfmfzkcbp6bZn+5tuQQAl8pqotxysRgMak3yemO7RvCh0/ggXAQAKlUFlS0LoAtCX9tjSMnnd36NYJdP5+IgEwkEApVJIUrEIAauJ+dP5k+gXAQAKlYD0fYFUA2uL++3Z16K6t6PxxDAuAgQRKwWo+wJoAwqH/IimJ+zne57gfxBMnAAYSKAznA2ZshQI2BdAmJfP9PPR/8H2rNNmQbom7JAmAgQQKY60+wIoATOfnuOWozd9Ildyzo9Od7gPJjBIAAwkU5nNGAsfKftHSBWA6/4w58fSFiqE/8+Btq/4W+GQkTQAMJFAIDgX2GwkslvmiNgTQJiVDf4Yz/vfvReyfRhYBMJBAIUoPBUoVgLasP4PhfzayCoCBBApR6qxAaQLQlvWPwNRfNvIIgIEExqbUWYEyBaCm4KcfLvxBrX86eQXAQAJjU1qBUCkCCGv9vyv6K7EEBJCNcQTAQAJj85Ey1gqUJYA2KUr89QMBZGNcATCQwFiUkhAsLADtG3tCANkoIgAGEhiLwhuKFhJAmPjjYcid0r8JW0AA2SgqAAYSyM1r1KsNGDshWFQA86R8T38IIBtlCICBBHJT6NkCYwtA67TfMBBANsoSAAMJ5KLQtGARAcyT8rs/AwFko0wBMJBALsYeBYwlgLDe/yfS77oKIIBslC0ABhLIxV3jrBMYVwAL5MnDPCGAbNgQAAMJZGash43mFoBPd38GAsiGLQEwkEBmco8CxhHAAnly92cggGzYFAADCWQid4lwLgH4kvnvBwLIhm0BMJBAKrlnBPIKYJ48yPz3AwFkowoBMJBAKrlmBPIKgM3izd2fgQCyUZUAGEhgJO8YAUxn/ebMAtBe858EBJCNKgXAQAIjybxGII8AFklxzX8SEEA2qhYAAwkk8poRwEyWb8wkANP5Z83pZel3JQEEkA0JATCQQCL3Ggm0074pqwAWyKOpv34ggGxICYCBBGLJVBiUKoBw6u+i9LuRAgLIhqQAGEgglp1pU4JZBKDqIR95gQCyIS0ABhLYROrDRLIIYJE8TP5FQADZcEEADCQwQGoycKQANG/2mRUIIBuuCIBhCfztiT+ity8tSjfFBUZuHpomgAXyNPkXAQFkwyUBMJDAOiOTgWkC8K7ybxgIIBuuCYCBBLqMrAxMFIDGx3yNAwSQDRcFwEACXRIfJzZKAAvk+fCfgQCy8e4HPk+33vEx6WbEAgkkhwGjBOD98J+BALKxsv3DdPNHn5RuRiKeSyAxDIgVAIb/G0AA2bn+gYdo6o5PSTcjEc8lEBsGJAlA5YM+xwECyMeV2z9Lt/zc70g3IxGPJRC7W1CSABbJ4+KffiCA/AQ37aGr0/fQNdpGF691pJuzidWVZXrxv75FzVZDuilVElsUtEkAKP4ZBALQyaF/WaLdt03R5GRTuilVsqkoKE4AXtf+DwMB6OTz//QeBY3ANwlsWhsQJ4A2KX3U9zhAADphATCeSWDTI8XjBIC/9j4gAJ1EAmBYAtO7ttK2qUnpZlnHCGCgzw984vPOP0lAADrpF0DEzt03+yCBgZ2ChgUwT55t+50GBKCTOAEwHkhgYNvwYQG0CfH/ABCATpIEwCiXwEAeYFgA+EsfAgLQySgBMDcbAfyMEYFG+vMA6x9g/j8eCEAnaQJgFEtgvR6gXwCY/48BAtBJFgEwSiWwXg/QL4AFwvLfTbggAC6t5aMqOks/IVpZFn3PtskqAGbrzRPdvECjkfth2q6yvjy4XwA8JNgn3TLXcEEAE/uepGD6w5Vdb+3S9+nG2cdF37Nt8giAmZhs0O7btmuRwFkjAA75BwSAcW4M0gIIpu6iiburj8yunz4g9p6rIK8AGE0SiBKB3X+QAExGWgCN6V+i1r4vVn5dCCAeRRLoJgIjAWADkAQgAJ2MKwCGlxHv2rut7usHuhuERAKYJ1QAxgIB6KSIABgFi4i6FYGRAHiroE9Lt8hFIACdFBUAU3MJfMMI4EAkgDahBDgWCEAnZQiAqbEEuiXBkQAwA5CAtAAwC2CHsgQQUcf1AzwTEPj++O80pAXAtD74EDX2Vrfbbuf8KVr54bOi79k2ZQuAqaEEdgbYA2A0LggAlI8NATA1k8C9EEAKEIBObAmA2TF9E+3YeZP0W8xCVwDzhCnARCAAndgUAFOTRUSHIYAUIACd2BYAUwMJdAWwQFgFmAgEoJMqBMA4LoHnAtQAjAYC0ElVAmC2bG3Rrj3bXFw/cBoCSAEC0EmVAmAcXUTUFQD2ARgBBKCTqgXAOCiBswGqAEcDAehEQgAMS+DWvVPOPJgUAkjBCQG0tlHrFx6ioDUl/esojZVX/5LWeOsxIaQEwLi0fgACSEFcAKbzd7cEm5qR/lWUy8pluvHKQVq7+qbI5SUFwLgiAQggBWkBSK0GrIKVs09Q59L3RK4tLQDGBQlAAClAAPbwXQAMS4DrBHjnYZHrQwCjgQDsAQFsILWICAJIAQKwBwQwiIQEIIAUIAB7QACbqVoCEEAKEIA9IIB4pnZsoeldWyu5FgSQgrQAmKp3BKqCtaXF3tOHhB5B5rIAmKoWEUEAKbggAKZ5x292awK0sPo/L4o+f9B1ATBVSAACSMEVAYByqYMAGJYAhwO21g9AAClAADqpiwAYm4uIIIAUIACd1EkAjC0JQAApQAA6qZsAGBsSgABSgAB0UkcBMCyBnbvLezApNgRJAQLQSV0FwJS4iOgstgRLAQLQSZ0FwJQkAewJmAYEoJO6C4ApQQKnsS14ChCATjQIIKLA+oHn8GCQFFwQQPcJwfueJGo5u798blbOPUud/z0ldn1NAmDGlACeDJSGtACCm/bQxN3HVHX+iBuvfE5sX0BtAmDGkMBhPBw0BWkBYDWgHTQKgOGyYV5NmBE8HTgNCMAeEIAdciwi6gpg2nxwUbrRrgIB2AMCsEdGCezs1hSiGjAZCMAeEIBd0iTwyANfDyIBtAm1ALFAAPaAAOzDuw1zcjBm/cBpI4DZSADHzenT0o11EWkBdKcA7z4q/WuwAgRQDQmLiL5hBHAgEsA8YSowFmkBMI33far7aDBNdM6fopUfPit2fZ8EwMRI4LARwHwkgAPm9IJ0I13EBQEwPBIIFG0JJnXnj/BNAMyQBD5jBHA8EsB+c/qudANdxBUBgHLxUQBM3/qBjxgBnFkfD2AmIB4IQCe+CoBhCTz9+Mlu3+8XAPYFiAEC0InPAjC9/tzRJ059sPdhCFYFxgMB6MRnATSawd8/9djJ3+aP+wVw0Jx0zjcVAALQic8CaLYaXzhy6ES3uKRfAEgExgAB6MRrATSDjx957OS/8ccDlQFIBG4GAtCJzwI4+oVT6/1+WABtQknwABCATnwVQNAI/uPpx0/+yvrn/V9EReBmXBEACoHKxVcBNJrB0089dvKR6PNhAcwS9gYYwAUBoBS4fHwVQLPVuO/IoRPfjD7ftEQIeYBBpAWAxUB28FUA/fE/EyeANiEPsI60ALAc2A4+CmA4/u/+3/A3oR5gEAjAHhBAtfTP/0fECQD1AH1AAPaAAKqlf/4/IvYxo0YCi+Z0p3SDXQACsAcEUCEBvX70iVPv3/zfMRgBHDOnh6Xb7AIQgD0ggOpoNIOvPvXYyd8b/v8kAWCDkBBpAeDBIHbwTQAm/n/AxP/PD/9/kPQDRgKXzOkW6YZLIy0ABo8GKx+vBBDQkhn+b4//UgJYHtzDBQGA8vFJAP3Lf4cZJQCEAQQBaMUnASQN/5lg1A8iDIAAtOKNAEYM/3tfHgHCAAhAK74IYNTwn0kTgPdFQRCATnwRQFzxTz9B2gv4XhQEAejECwEkFP8MfksKvq8NgAB04oMA4mr/h8kiAK8fHw4B6MQHAUxuad76pUdfujDqe1IFwPicDIQAdKJdAGnJv4isApglT3cKggB0ol0Awzv/JJFJAIyvyUAIQCeqBZAh+bfxrRkxApgzp69Jv7eqgQB0olkA5u7/h+bu/2dZvjezABgfKwMhAJ2oFUBK5d/mb8+Bj9uGQwA60SqA4W2/08grAJ4SXCSPRgEQgE5UCsDc/ScnmzNpU3+DP5IT33YLggB0olEASbv+jGIcAcyYk8w2LgJAAPp4Y7lDz3xnWboZpTOxpfnzX370pR/l+ZncAmB8Kgz6xHSH9k1BAJr4wYUV+qsfXJFuRqlkLfwZZlwBzJAno4APbVujX9vZkW4GKJETP71OJ167Jt2M8jCx/8Rk86N57/69Hx0TX2YEtjSIfv/2VelmgBJ51gz/X1/WI/W8mf9+igjAmxmB+3Z16K6tCAM0oC7+HyPzP/jjBfBlFMBJQE4Ggvrzd/99lV45f0O6GaVR5O7PFBUAjwLOkAdrBDAKqD8K7/6vm7v/L4979++9REF8WSOwo0V0/55VmmxItwSMi7bYP0/NfxKFBcD48khxhAL1RdvQP+5R32O9ThmN8WnzUEwL1g9tnZ9J2+wzK6UIgPGpRPhnt/YksAXhgNNcXVmjF398TV3nH6fkN4kyBeDNtCAz1Vij2akbNLOjKd0UEMOP31mlf3j1qqqYv0vBab/NL1civj1OrNNZo6nly/SxPS36xV0t6eYA6nV8vuNru+tHjHrM1ziUKgDGl4RgBEvgrTfeMyMCExrc0qTbtzXotimMCqrk4tVOd4rv+xdWuh9rpazE38Brlt3IcJ0A1wZ4EQowkQRuXNf7xweEKVDvP/plLeDjw0RYApcuXKHLS9elmwIUkuUhH+NgRQCMb6FAxP+9dRkSAKViY+i//tq2Gu3brEA/kAAojZKz/ptf3iK+zQr0AwmAMig76z+MVQEwPhUIDcM5gaV3FW08ASqlzIKfJKoQAIcCbXPss30tF1k2o4CLZjQAQC4COmeG/r9qa+i/cZkKCNcKtMnDfAADCYBcmLi/2Qh+vYxa//RLVYTP+QAGEgBZsR3391OZABif8wEMJADSqCLu76dSATC+1gdEXLu6Qm+fX6a1DnYXAoPYnO9PvGbVb9L3pCBz/foqvfXGEiQANqgo6bf5sgL4nhRkIAGwToVJv82XFsJIYNacXpa6vgtAAoBpthr3HTl04psS1xYTAOPLhqKjYAlcOL9MqytYSegjZWzsWQRRATC+PFtgFFhO7CdF9/QvA3EBMD49bDQJSMAvxn2YZ9k4IQAGEoAEfMGVzs84IwAGEoAEtONS52ecEgBjJMDbiXlbI8CwBLhi8MplnRtb+opEoU9qm6QbMAwKhTbAngKKECr0SW+Wg0ACG0ACCnC08/ea5jDICfSABOqLazH/ME4LgIEEekAC9cP1zs84LwAGEuiB5cT1oQ6dn6mFABhUDPaABNzHhQq/rNRGAAzWDvSABNxFurY/L7USABOuIjxOHi8lZiABx+Alvc3GZ6VW9Y3f7BoS7iewQJ5PE3KhECcHsZxYmIDONRvBnMR6/uJNrylhrQCPBLzdXozBngKycHXfxETjN1yc48/UfukGFMX3jUYZSECGqjfwtEHtBcCEW44vkMd5AUigQnrx/h9UtXW33beiBOQFIIFKqHG8H/92FBHmBebJ45AAy4ntwUP+VqvxaF3j/ThUCSDC95AAEigZRUP+zW9NKb7PEkAC5RA0ghMTE437Nd31B96fdANsY0RwkHphgXejAUigAL27/pfMXf+L0k2x+zY9wEhghnohgXejAZbAhTeX6dqVFemm1Aae229NNB748qMv/Ui6Ldbfq3QDqsTn3ACWE2dAcayf/JY9w+eZAkggGY0Z/ix4J4CIsG6Aqwi9CgsggUF4uN8I6GEt8/q53790A6QJlxjPm+NO6bZUBSRA/Jf/uhnu/2mdlu7a+TWAKCw4GB5e5AeW3r1Gly5ckW5G9Zg4v9EI/sIM9//Et+F+/K8DrNMnAi92HvJqTwF0/FgggBjCacN58mAfQh8kwPvzNVuNQz5M6+UFAhhBKAIeEcyR4tBApQTCO77p+H+Ojp8MBJABH3IELAHOCdR+JSGG+rmAAHKiedag1suJkdUfCwhgTMLNSedIWZ6gbhLg+D4Igq/WbTNOV4AAChKGB3PUCw9UjAqcl0Dvbv+VZjP4Cob5xYAASiSsLmQR8JqDWucKnJNAL7b/R/MHe8TXqj0bQACWCBceRUctZbC60qG3zy/JLSeOOn0QvODTAp0qgQAqIJTBLPVkUKswofI9Bczw3nT6b5tOfwqd3j4QQMWEYcIs9WRQi4VItiXQXZDTCF6ktbUTGN5XCwQgTDibEB3OCqFMCXCHDwL6Z3OXP4nsvSwQgGP0jRD2h4cz25yPJYGAzpm7+/dMZz+DO7x7QAA1IJTCDG1IgacexUYLccuJ+a5uTu+aO/t/ms7+HdPZX0Vndx8IoMaENQj7w09nw/NMeDD89aIjiLPmuBR+vBgeXDb8r1cu32g1m8G/Yy6+vvw/uAP8/OUkeMwAAAAASUVORK5CYII=);\n  background-size: contain;\n  background-position: center;\n  background-repeat: no-repeat;\n  width: 100%;\n  padding: 3em;\n  color: #fb7958 !important;\n  border: 1px dashed !important; }\n\n.icon-pdf button {\n    position: absolute;\n    right: 1em; }\n"

/***/ }),

/***/ "./src/app/paginas/tareas/dialogo/modificar/modificar.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/paginas/tareas/dialogo/modificar/modificar.component.ts ***!
  \*************************************************************************/
/*! exports provided: ModificarTareaDialogoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarTareaDialogoComponent", function() { return ModificarTareaDialogoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/upload.service */ "./src/app/core/upload.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var ModificarTareaDialogoComponent = /** @class */ (function () {
    function ModificarTareaDialogoComponent(db, notify, snackBar, dialogRef, data, _loadingService, _uploadService) {
        this.db = db;
        this.notify = notify;
        this.snackBar = snackBar;
        this.dialogRef = dialogRef;
        this.data = data;
        this._loadingService = _loadingService;
        this._uploadService = _uploadService;
        this.info = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
        this.info = data.item;
    }
    ModificarTareaDialogoComponent.prototype.ngOnInit = function () { };
    ModificarTareaDialogoComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .update("tareas/" + this.info.id, this.info)
            .then(function () {
            _this.snackBar.open("Tarea modificado", "Cerrar", {
                duration: 2000,
                horizontalPosition: "left"
            });
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error modificando registro", "error");
        });
    };
    ModificarTareaDialogoComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarTareaDialogoComponent.prototype.uploadEvent = function (file) {
        var _this = this;
        this.fileUploadMsg = file.name;
        this.file = file;
        this._loadingService.register();
        var archivo = new _core_upload_service__WEBPACK_IMPORTED_MODULE_5__["Upload"](this.file);
        this._uploadService.uploadDocumento(archivo).then(function (url) {
            _this.info.url = url;
            _this._loadingService.resolve();
        });
    };
    ModificarTareaDialogoComponent.prototype.uploadEventIngles = function (file) {
        var _this = this;
        this.fileUploadMsg = file.name;
        this.fileingles = file;
        this._loadingService.register();
        var archivo = new _core_upload_service__WEBPACK_IMPORTED_MODULE_5__["Upload"](this.fileingles);
        this._uploadService.uploadDocumento(archivo).then(function (url) {
            _this.info.urlingles = url;
            _this._loadingService.resolve();
        });
    };
    ModificarTareaDialogoComponent.prototype.uploadEventItaliano = function (file) {
        var _this = this;
        this.fileUploadMsg = file.name;
        this.fileitaliano = file;
        this._loadingService.register();
        var archivo = new _core_upload_service__WEBPACK_IMPORTED_MODULE_5__["Upload"](this.fileitaliano);
        this._uploadService.uploadDocumento(archivo).then(function (url) {
            _this.info.urlitaliano = url;
            _this._loadingService.resolve();
        });
    };
    ModificarTareaDialogoComponent.prototype.eliminarFile = function () {
        this.info.url = "";
    };
    ModificarTareaDialogoComponent.prototype.eliminarFileIngles = function () {
        this.info.urlingles = "";
    };
    ModificarTareaDialogoComponent.prototype.eliminarFileItaliano = function () {
        this.info.urlitaliano = "";
    };
    ModificarTareaDialogoComponent.prototype.cancelEvent = function () {
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.info.file = null;
    };
    ModificarTareaDialogoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar",
            template: __webpack_require__(/*! ./modificar.component.html */ "./src/app/paginas/tareas/dialogo/modificar/modificar.component.html"),
            styles: [__webpack_require__(/*! ./modificar.component.scss */ "./src/app/paginas/tareas/dialogo/modificar/modificar.component.scss")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], Object, _covalent_core_loading__WEBPACK_IMPORTED_MODULE_4__["TdLoadingService"],
            _core_upload_service__WEBPACK_IMPORTED_MODULE_5__["UploadService"]])
    ], ModificarTareaDialogoComponent);
    return ModificarTareaDialogoComponent;
}());



/***/ }),

/***/ "./src/app/paginas/tareas/tareas.component.html":
/*!******************************************************!*\
  !*** ./src/app/paginas/tareas/tareas.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n\n<div class=\"cabecera-seccion\">\n  <mat-toolbar class=\"opciones-cabecera\">\n\n    <button mat-button  class=\"btn-ruta\">Tareas</button>\n\n    <div class=\"button-row md-button-right\">\n      <button mat-button class=\"btn-crear\" (click)=\"crear()\">\n        <mat-icon matListIcon class=\"small\">add</mat-icon>\n        Crear tarea\n      </button>\n    </div>\n  </mat-toolbar>\n</div>\n\n  <div class=\"row\">\n    <div class=\"col-md-3 red\" >\n      <div class=\"header\">\n        SIN INICIAR\n      </div>\n      <div class=\"tareas\" droppable (onDrop)=\"onItemPendiente($event)\" dnd-sortable-container [sortableData]=\"tareasPendientes\">\n        <mat-card draggable *ngFor=\"let item of tareasPendientes; let i = index\" [dragData]=\"item\" class=\"tarjeta\" (click)=\"ir(item)\">\n          <mat-card-header>\n            <mat-card-title class=\"titulo\">{{item.titulo}}</mat-card-title>\n          </mat-card-header>\n          <mat-card-content>\n            <p>\n              {{item.notas}}\n            </p>\n          </mat-card-content>\n          <mat-card-actions style=\"min-height: 40px;\">\n            <mat-icon class=\"btn-icon-position\" (click)=\"upIndex(tareasPendientes,i)\" *ngIf=\"i != 0\">keyboard_arrow_up</mat-icon>\n            <mat-icon class=\"btn-icon-position\" (click)=\"downIndex(tareasPendientes,i)\" *ngIf=\"i != tareasPendientes.length-1\">keyboard_arrow_down</mat-icon>\n            <mat-icon class=\"btn-icon-eliminar\" (click)=\"eliminar(item)\">delete</mat-icon>\n          </mat-card-actions>\n        </mat-card>\n      </div>\n    </div>\n\n    <div class=\"col-md-3 red\">\n      <div class=\"header\">\n        EN PROCESO\n      </div>\n      <div class=\"tareas\" droppable (onDrop)=\"onItemProceso($event)\">\n        <mat-card draggable *ngFor=\"let item of tareasEnProceso; let i = index;\" [dragData]=\"item\" class=\"tarjeta\" (click)=\"ir(item)\">\n          <mat-card-header>\n            <mat-card-title class=\"titulo\">{{item.titulo}}</mat-card-title>\n          </mat-card-header>\n          <mat-card-content>\n            <p>\n              {{item.notas}}\n            </p>\n          </mat-card-content>\n          <mat-card-actions style=\"min-height: 40px;\">\n            <mat-icon class=\"btn-icon-position\" (click)=\"upIndex(tareasEnProceso,i)\" *ngIf=\"i != 0\">keyboard_arrow_up</mat-icon>\n            <mat-icon class=\"btn-icon-position\" (click)=\"downIndex(tareasEnProceso,i)\" *ngIf=\"i != tareasEnProceso.length-1\">keyboard_arrow_down</mat-icon>\n            <mat-icon class=\"btn-icon-eliminar\" (click)=\"eliminar(i)\">delete</mat-icon>\n          </mat-card-actions>\n        </mat-card>\n      </div>\n    </div>\n\n    <div class=\"col-md-3 red\">\n      <div class=\"header\">\n        EN REVISIÓN\n      </div>\n      <div class=\"tareas\" droppable (onDrop)=\"onItemRevision($event)\">\n        <mat-card draggable *ngFor=\"let item of tareasEnRevision;let i = index;\" [dragData]=\"item\" class=\"tarjeta\" (click)=\"ir(item)\">\n          <mat-card-header>\n            <mat-card-title class=\"titulo\">{{item.titulo}}</mat-card-title>\n          </mat-card-header>\n          <mat-card-content>\n            <p>\n              {{item.notas}}\n            </p>\n          </mat-card-content>\n          <mat-card-actions style=\"min-height: 40px;\">\n            <mat-icon class=\"btn-icon-position\" (click)=\"upIndex(tareasEnRevision,i)\" *ngIf=\"i != 0\">keyboard_arrow_up</mat-icon>\n            <mat-icon class=\"btn-icon-position\" (click)=\"downIndex(tareasEnRevision,i)\" *ngIf=\"i != tareasEnRevision.length-1\">keyboard_arrow_down</mat-icon>\n            <mat-icon class=\"btn-icon-eliminar\" (click)=\"eliminar(item)\">delete</mat-icon>\n          </mat-card-actions>\n        </mat-card>\n      </div>\n    </div>\n\n    <div class=\"col-md-3 red\">\n      <div class=\"header\">\n        TERMINADO\n      </div>\n      <div class=\"tareas\" droppable (onDrop)=\"onItemTerminado($event)\">\n        <mat-card draggable *ngFor=\"let item of tareasTerminadas;let i = index;\" [dragData]=\"item\" class=\"tarjeta\" (click)=\"ir(item)\">\n          <mat-card-header>\n            <mat-card-title class=\"titulo\">{{item.titulo}}</mat-card-title>\n          </mat-card-header>\n          <mat-card-content>\n            <p>\n              {{item.notas}}\n            </p>\n          </mat-card-content>\n          <mat-card-actions style=\"min-height: 40px;\">\n            <mat-icon class=\"btn-icon-position\" (click)=\"upIndex(tareasTerminadas,i)\" *ngIf=\"i != 0\">keyboard_arrow_up</mat-icon>\n            <mat-icon class=\"btn-icon-position\" (click)=\"downIndex(tareasTerminadas,i)\" *ngIf=\"i != tareasTerminadas.length-1\">keyboard_arrow_down</mat-icon>\n            <mat-icon class=\"btn-icon-eliminar\" (click)=\"eliminar(item)\" matTooltip=\"Eliminar tarea\">delete</mat-icon>\n          </mat-card-actions>\n        </mat-card>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--<h4>Simple sortable handle</h4>\n<div class=\"row\">\n  <div class=\"col-sm-3\">\n    <div class=\"panel panel-success\">\n      <div class=\"panel-heading\">\n        Favorite drinks\n      </div>\n      <div class=\"panel-body\">\n        <ul class=\"list-group\" dnd-sortable-container [sortableData]=\"listOne\">\n          <li *ngFor=\"let item of listOne; let i = index\" class=\"list-group-item\" [dragData]=\"item\" dnd-sortable (onDragSuccess)=\"orderedProduct($event, i)\"\n            [sortableIndex]=\"i\">\n            <span dnd-sortable-handle>=</span>&nbsp; {{item}}\n          </li>\n        </ul>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-sm-6\">\n    <div class=\"panel panel-default\">\n      <div class=\"panel-body\">\n        My prefences:\n        <br/>\n        <span *ngFor=\"let item of listOne; let i = index\">{{i + 1}}) {{item}}\n          <br/>\n        </span>\n      </div>\n    </div>\n  </div>\n</div>-->\n"

/***/ }),

/***/ "./src/app/paginas/tareas/tareas.component.scss":
/*!******************************************************!*\
  !*** ./src/app/paginas/tareas/tareas.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  min-height: calc(90vh - 60px);\n  padding: 0 1em; }\n  .row .col-md-3 {\n    background: #f5f8fa;\n    border: 1px solid #dfe3eb;\n    padding: 0; }\n  .row .col-md-3 .header {\n      border-bottom: 1px solid #dfe3eb;\n      padding: 16px 16px;\n      font-size: 12px;\n      -webkit-hyphens: none;\n          -ms-hyphens: none;\n              hyphens: none;\n      overflow: hidden !important;\n      text-overflow: ellipsis !important;\n      white-space: nowrap !important;\n      font-weight: 600;\n      text-transform: uppercase;\n      font-family: \"Roboto\"; }\n  .row .col-md-3 .tareas {\n      margin: 1em;\n      min-height: calc(90vh - 120px); }\n  .row .col-md-3 .tareas mat-card {\n        margin-bottom: 1em; }\n  .titulo {\n  margin: 0 !important;\n  color: #0091ae;\n  cursor: pointer; }\n  .titulo:hover {\n  font-weight: 500; }\n"

/***/ }),

/***/ "./src/app/paginas/tareas/tareas.component.ts":
/*!****************************************************!*\
  !*** ./src/app/paginas/tareas/tareas.component.ts ***!
  \****************************************************/
/*! exports provided: TareasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TareasComponent", function() { return TareasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dialogo/crear/crear.component */ "./src/app/paginas/tareas/dialogo/crear/crear.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dialogo/modificar/modificar.component */ "./src/app/paginas/tareas/dialogo/modificar/modificar.component.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TareasComponent = /** @class */ (function () {
    function TareasComponent(db, _dialogService, dialog, snackBar) {
        this.db = db;
        this._dialogService = _dialogService;
        this.dialog = dialog;
        this.snackBar = snackBar;
        this.tareasPendientes = [];
        this.tareasEnProceso = [];
        this.tareasEnRevision = [];
        this.tareasTerminadas = [];
    }
    TareasComponent.prototype.orderedProduct = function ($event, i) {
        console.log('Evento', $event);
        console.log('i', i);
    };
    TareasComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.db
            .colWithIds$("tareas", function (ref) {
            return ref.where("estado", "==", "pendiente").orderBy("index", "asc");
        })
            .subscribe(function (resp) {
            _this.tareasPendientes = resp;
        });
        this.db
            .colWithIds$("tareas", function (ref) {
            return ref.where("estado", "==", "proceso").orderBy("index", "asc");
        })
            .subscribe(function (resp) {
            _this.tareasEnProceso = resp;
        });
        this.db
            .colWithIds$("tareas", function (ref) {
            return ref.where("estado", "==", "revision").orderBy("index", "asc");
        })
            .subscribe(function (resp) {
            _this.tareasEnRevision = resp;
        });
        this.db
            .colWithIds$("tareas", function (ref) {
            return ref.where("estado", "==", "terminada").orderBy("index", "asc");
        })
            .subscribe(function (resp) {
            _this.tareasTerminadas = resp;
        });
    };
    TareasComponent.prototype.onItemPendiente = function (e) {
        e.dragData.index = this.tareasPendientes.length + 1;
        this.db.update("tareas/" + e.dragData.id, {
            estado: "pendiente",
            index: e.dragData.index
        });
    };
    TareasComponent.prototype.onItemProceso = function (e) {
        e.dragData.index = this.tareasEnProceso.length + 1;
        this.db.update("tareas/" + e.dragData.id, {
            estado: "proceso",
            index: e.dragData.index
        });
    };
    TareasComponent.prototype.onItemRevision = function (e) {
        e.dragData.index = this.tareasEnRevision.length + 1;
        this.db.update("tareas/" + e.dragData.id, {
            estado: "revision",
            index: e.dragData.index
        });
    };
    TareasComponent.prototype.onItemTerminado = function (e) {
        e.dragData.index = this.tareasTerminadas.length + 1;
        this.db.update("tareas/" + e.dragData.id, {
            estado: "terminada",
            index: e.dragData.index
        });
    };
    TareasComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_3__["CrearTareaDialogoComponent"], {
            width: "60%"
        });
    };
    TareasComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_5__["ModificarTareaDialogoComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) { });
    };
    TareasComponent.prototype.eliminar = function (item) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default()({
            title: "Confirmación de borrado",
            text: "¿Está seguro de borrar esta tarea?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar"
        }).then(function (result) {
            if (result.value) {
                _this.db.delete("tareas/" + item.id).then(function () {
                    _this.snackBar.open("Tarea eliminada", "Cerrar", {
                        duration: 2000,
                        horizontalPosition: "left"
                    });
                });
            }
        });
    };
    TareasComponent.prototype.upIndex = function (lista, index) {
        if (index != 0) {
            var anterior = lista[index - 1];
            var actual = lista[index];
            anterior.index = actual.index;
            actual.index -= 1;
            // Actualizo
            this.db.update("tareas/" + anterior.id, {
                index: anterior.index
            });
            this.db.update("tareas/" + actual.id, {
                index: actual.index
            });
        }
    };
    TareasComponent.prototype.downIndex = function (lista, index) {
        if (index != lista.length - 1) {
            var siguiente = lista[index + 1];
            var actual = lista[index];
            siguiente.index = actual.index;
            actual.index += 1;
            // Actualizo
            this.db.update("tareas/" + siguiente.id, {
                index: siguiente.index
            });
            this.db.update("tareas/" + actual.id, {
                index: actual.index
            });
        }
    };
    TareasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-tareas",
            template: __webpack_require__(/*! ./tareas.component.html */ "./src/app/paginas/tareas/tareas.component.html"),
            styles: [__webpack_require__(/*! ./tareas.component.scss */ "./src/app/paginas/tareas/tareas.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_2__["TdDialogService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"]])
    ], TareasComponent);
    return TareasComponent;
}());



/***/ }),

/***/ "./src/app/paginas/usuarios/dialogo/crear/crear.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/paginas/usuarios/dialogo/crear/crear.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Registro de usuario</h1>\n\n<div mat-dialog-content class=\"example-container\">\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre completo\" required>\n  </mat-form-field>\n\n\n  <mat-form-field required>\n    <input type=\"email\" matInput [(ngModel)]=\"info.correo\" placeholder=\"Correo electrónico\">\n  </mat-form-field>\n\n  <mat-form-field>\n    <input matInput type=\"password\" [(ngModel)]=\"info.contrasena\" placeholder=\"Contraseña\" required>\n  </mat-form-field>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre || !info.correo || !info.contrasena \">Crear usuario</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/usuarios/dialogo/crear/crear.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/paginas/usuarios/dialogo/crear/crear.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/usuarios/dialogo/crear/crear.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/paginas/usuarios/dialogo/crear/crear.component.ts ***!
  \*******************************************************************/
/*! exports provided: CrearUsuarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearUsuarioComponent", function() { return CrearUsuarioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CrearUsuarioComponent = /** @class */ (function () {
    function CrearUsuarioComponent(db, notify, dialogRef) {
        this.db = db;
        this.notify = notify;
        this.dialogRef = dialogRef;
        this.info = {};
    }
    CrearUsuarioComponent.prototype.ngOnInit = function () { };
    CrearUsuarioComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .add("usuarios-sistema", this.info)
            .then(function () {
            _this.notify.update('Usuario creado exitosamente', 'success');
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error creando registro", "error");
        });
    };
    CrearUsuarioComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    CrearUsuarioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-crear",
            template: __webpack_require__(/*! ./crear.component.html */ "./src/app/paginas/usuarios/dialogo/crear/crear.component.html"),
            styles: [__webpack_require__(/*! ./crear.component.scss */ "./src/app/paginas/usuarios/dialogo/crear/crear.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"]])
    ], CrearUsuarioComponent);
    return CrearUsuarioComponent;
}());



/***/ }),

/***/ "./src/app/paginas/usuarios/dialogo/modificar/modificar.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/paginas/usuarios/dialogo/modificar/modificar.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Modificar usuario</h1>\n\n<div mat-dialog-content class=\"example-container\">\n  <mat-form-field>\n    <input matInput [(ngModel)]=\"info.nombre\" placeholder=\"Nombre completo\" required>\n  </mat-form-field>\n\n\n  <mat-form-field required>\n    <input type=\"email\" matInput [(ngModel)]=\"info.correo\" placeholder=\"Correo electrónico\">\n  </mat-form-field>\n\n\n</div>\n\n<div mat-dialog-actions align=\"end\">\n  <div fxFlex></div>\n  <button mat-button class=\"btn-cancelar\" (click)=\"cerrar()\" cdkFocusInitial>Cancelar</button>\n  <button mat-button class=\"btn-crear\" (click)=\"guardar()\" [disabled]=\"!info.nombre || !info.correo\">Modificar</button>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/usuarios/dialogo/modificar/modificar.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/paginas/usuarios/dialogo/modificar/modificar.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flow-root;\n  flex-direction: column;\n  -webkit-column-count: 1;\n          column-count: 1; }\n\n.example-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/paginas/usuarios/dialogo/modificar/modificar.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/paginas/usuarios/dialogo/modificar/modificar.component.ts ***!
  \***************************************************************************/
/*! exports provided: ModificarUsuarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificarUsuarioComponent", function() { return ModificarUsuarioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var ModificarUsuarioComponent = /** @class */ (function () {
    function ModificarUsuarioComponent(db, notify, snackBar, dialogRef, data) {
        this.db = db;
        this.notify = notify;
        this.snackBar = snackBar;
        this.dialogRef = dialogRef;
        this.data = data;
        this.info = {};
        this.info = data.item;
    }
    ModificarUsuarioComponent.prototype.ngOnInit = function () { };
    ModificarUsuarioComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .update("usuarios-sistema/" + this.info.id, this.info)
            .then(function () {
            _this.snackBar.open('Usuario modificado', 'Cerrar', {
                duration: 2000,
                horizontalPosition: "left"
            });
            _this.cerrar();
        })
            .catch(function (err) {
            _this.notify.update("Error modificando registro", "error");
        });
    };
    ModificarUsuarioComponent.prototype.cerrar = function () {
        this.dialogRef.close();
    };
    ModificarUsuarioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-modificar",
            template: __webpack_require__(/*! ./modificar.component.html */ "./src/app/paginas/usuarios/dialogo/modificar/modificar.component.html"),
            styles: [__webpack_require__(/*! ./modificar.component.scss */ "./src/app/paginas/usuarios/dialogo/modificar/modificar.component.scss")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_2__["NotifyService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], Object])
    ], ModificarUsuarioComponent);
    return ModificarUsuarioComponent;
}());



/***/ }),

/***/ "./src/app/paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-cell {\n    color: #0091ae;\n    cursor: pointer;\n}\n\n.mat-cell:hover {\n    text-decoration: underline;\n}\n\n.mat-column-nombre {\n    flex: 0 0 30%;\n    text-align: left;\n}\n\n.mat-column-correo {\n    flex: 0 0 30%;\n    text-align: left;\n}\n"

/***/ }),

/***/ "./src/app/paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"dataSource\" matSort aria-label=\"Elements\">\n\n    <!-- Id Column -->\n    <ng-container matColumnDef=\"correo\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>email</mat-header-cell>\n      <mat-cell (click)=\"ir(row)\" *matCellDef=\"let row\">{{row.correo}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"nombre\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Nombre usuario</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.nombre}}</mat-cell>\n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"createdAt\">\n      <mat-header-cell *matHeaderCellDef mat-sort-header>Fecha creación</mat-header-cell>\n      <mat-cell *matCellDef=\"let row\">{{row.createdAt | date: 'dd/MM/yyyy'}}</mat-cell>\n    </ng-container>\n\n    <!-- Checkbox Column -->\n    <ng-container matColumnDef=\"opciones\">\n      <mat-header-cell *matHeaderCellDef></mat-header-cell>\n      <mat-cell *matCellDef=\"let row\" class=\"pull-right-menu\">\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n\n          <button mat-menu-item (click)=\"eliminar(row)\">Eliminar</button>\n        </mat-menu>\n      </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n\n  <mat-paginator #paginator\n    [length]=\"lista.length\"\n    [pageIndex]=\"0\"\n    [pageSize]=\"50\"\n    [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./src/app/paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component.ts ***!
  \************************************************************************************/
/*! exports provided: TablaUsuariosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablaUsuariosComponent", function() { return TablaUsuariosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_notify_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/notify.service */ "./src/app/core/notify.service.ts");
/* harmony import */ var _dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../dialogo/modificar/modificar.component */ "./src/app/paginas/usuarios/dialogo/modificar/modificar.component.ts");
/* harmony import */ var _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @covalent/core/dialogs */ "./node_modules/@covalent/core/esm5/covalent-core-dialogs.js");
/* harmony import */ var _node_modules_covalent_core_loading__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../node_modules/@covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// Configuración de confirm eliminar
var configDel = {
    message: '¿Está seguro de eliminar el registro?',
    disableClose: false,
    title: 'Confirmación',
    cancelButton: 'Cancelar',
    acceptButton: 'Aceptar',
    width: '500px',
};
var TablaUsuariosComponent = /** @class */ (function () {
    function TablaUsuariosComponent(db, _loadingService, _dialogService, _notify, _router, dialog) {
        this.db = db;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._notify = _notify;
        this._router = _router;
        this.dialog = dialog;
        this.displayedColumns = ["correo", "nombre", "createdAt", "opciones"];
        this.lista = [];
    }
    TablaUsuariosComponent.prototype.ngOnInit = function () {
        this.cargarLista();
    };
    TablaUsuariosComponent.prototype.cargarLista = function () {
        var _this = this;
        this.db.colWithIds$("usuarios-sistema").subscribe(function (resp) {
            _this.lista = resp;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.lista);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    TablaUsuariosComponent.prototype.ir = function (item) {
        var dialogRef = this.dialog.open(_dialogo_modificar_modificar_component__WEBPACK_IMPORTED_MODULE_5__["ModificarUsuarioComponent"], {
            width: "60%",
            data: { item: item }
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    TablaUsuariosComponent.prototype.eliminar = function (item) {
        var _this = this;
        this._dialogService
            .openConfirm(configDel)
            .afterClosed()
            .subscribe(function (accept) {
            if (accept) {
                _this.db
                    .delete("usuarios-sistema/" + item.id)
                    .then(function (res) {
                    _this._notify.okMessage("Usuario eliminado");
                })
                    .catch(function (err) {
                    _this._notify.errorMessage(err);
                });
            }
            else {
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], TablaUsuariosComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], TablaUsuariosComponent.prototype, "sort", void 0);
    TablaUsuariosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "tabla-usuarios",
            template: __webpack_require__(/*! ./tabla-usuarios.component.html */ "./src/app/paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component.html"),
            styles: [__webpack_require__(/*! ./tabla-usuarios.component.css */ "./src/app/paginas/usuarios/tablas/tabla-usuarios/tabla-usuarios.component.css")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_3__["ResourcesService"],
            _node_modules_covalent_core_loading__WEBPACK_IMPORTED_MODULE_7__["TdLoadingService"],
            _covalent_core_dialogs__WEBPACK_IMPORTED_MODULE_6__["TdDialogService"],
            _core_notify_service__WEBPACK_IMPORTED_MODULE_4__["NotifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], TablaUsuariosComponent);
    return TablaUsuariosComponent;
}());



/***/ }),

/***/ "./src/app/paginas/usuarios/usuarios.component.html":
/*!**********************************************************!*\
  !*** ./src/app/paginas/usuarios/usuarios.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor\">\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <span class=\"titulo-seccion\">Usuarios del sistema</span>\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" matTooltip=\"Crear nuevo registro\" mat-raised-button (click)=\"crear()\">\n          <mat-icon matListIcon class=\"small\">add</mat-icon>\n          Crear usuario\n        </button>\n      </div>\n    </mat-toolbar>\n  </div>\n  <tabla-usuarios></tabla-usuarios>\n</div>\n\n"

/***/ }),

/***/ "./src/app/paginas/usuarios/usuarios.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/paginas/usuarios/usuarios.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column;\n  min-width: 300px; }\n\n.example-header {\n  min-height: 64px;\n  padding: 25px 24px 0;\n  background-color: #fff;\n  color: white; }\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%; }\n\n.mat-table {\n  overflow: auto;\n  height: 50vh; }\n\ninput.mat-input-element {\n  color: black; }\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n  max-height: 500px;\n  min-width: 300px; }\n\n.mat-table {\n  overflow: auto;\n  max-height: 500px; }\n\n.mat-column-select {\n  overflow: visible; }\n\n.mat-column-placa {\n  flex: 0 0 10%;\n  text-align: left; }\n\n.mat-column-propietario {\n  flex: 0 0 20%; }\n\n.mat-column-flota {\n  flex: 0 0 20%; }\n\n.mat-column-estado {\n  flex: 0 0 20%; }\n\n.label-active {\n  background: green;\n  color: white;\n  padding: .5em;\n  border-radius: 3px; }\n\n.label-inactive {\n  background: red;\n  color: white;\n  padding: .5em;\n  border-radius: 3px; }\n\n.estatus-cell {\n  overflow: initial; }\n"

/***/ }),

/***/ "./src/app/paginas/usuarios/usuarios.component.ts":
/*!********************************************************!*\
  !*** ./src/app/paginas/usuarios/usuarios.component.ts ***!
  \********************************************************/
/*! exports provided: UsuariosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuariosComponent", function() { return UsuariosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dialogo/crear/crear.component */ "./src/app/paginas/usuarios/dialogo/crear/crear.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UsuariosComponent = /** @class */ (function () {
    function UsuariosComponent(dialog) {
        this.dialog = dialog;
    }
    UsuariosComponent.prototype.ngOnInit = function () {
    };
    UsuariosComponent.prototype.crear = function () {
        var dialogRef = this.dialog.open(_dialogo_crear_crear_component__WEBPACK_IMPORTED_MODULE_2__["CrearUsuarioComponent"], {
            width: "60%",
        });
    };
    UsuariosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-usuarios',
            template: __webpack_require__(/*! ./usuarios.component.html */ "./src/app/paginas/usuarios/usuarios.component.html"),
            styles: [__webpack_require__(/*! ./usuarios.component.scss */ "./src/app/paginas/usuarios/usuarios.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], UsuariosComponent);
    return UsuariosComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var froala_editor_js_froala_editor_pkgd_min_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! froala-editor/js/froala_editor.pkgd.min.js */ "./node_modules/froala-editor/js/froala_editor.pkgd.min.js");
/* harmony import */ var froala_editor_js_froala_editor_pkgd_min_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(froala_editor_js_froala_editor_pkgd_min_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_5__);






window["$"] = jquery__WEBPACK_IMPORTED_MODULE_5__;
window["jQuery"] = jquery__WEBPACK_IMPORTED_MODULE_5__;
if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ "./src/paginas/mensaje-papa/mensaje-papa.component.html":
/*!**************************************************************!*\
  !*** ./src/paginas/mensaje-papa/mensaje-papa.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n\n<div class=\"contenedor\">\n\n  <div class=\"cabecera-seccion\">\n    <mat-toolbar class=\"opciones-cabecera\">\n\n      <button mat-button class=\"btn-ruta\">Mensaje del Papa</button>\n      <div class=\"button-row md-button-right\">\n        <button mat-button class=\"btn-crear\" (click)=\"guardar()\">\n          Guardar\n        </button>\n      </div>\n\n    </mat-toolbar>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-12 red\">\n      <mat-tab-group>\n        <mat-tab label=\"Español\">\n          <td-file-upload #singleFileUpload [(ngModel)]=\"file\" (select)=\"uploadEvent($event)\" (upload)=\"uploadEvent($event)\"\n            (cancel)=\"cancelEvent()\" required *ngIf=\"!info.url\">\n            <mat-icon>file_upload</mat-icon>\n            <span>{{ singleFileUpload.value?.name }}</span>\n            <ng-template td-file-input-label>\n              <mat-icon>attach_file</mat-icon>\n              <span>\n                Seleccione archivo\n                <span [hidden]=\"!singleFileUpload?.required\">*</span>\n              </span>\n            </ng-template>\n          </td-file-upload>\n          <div class=\"col-md-12 icon-pdf\" *ngIf=\"info.url\">\n            <button mat-mini-fab color=\"warn\" (click)=\"eliminarFile()\">\n              <mat-icon>delete</mat-icon>\n            </button>\n          </div>\n          <br><br>\n          <span style=\"color: #7a7a7a; margin-top: 2em;\">Mensaje</span>\n          <div style=\"margin-top: 1em;\" [froalaEditor] [(froalaModel)]=\"info.contenido\"></div>\n        </mat-tab>\n        <mat-tab label=\"Ingles\">\n          <td-file-upload #singleFileUploadIngles [(ngModel)]=\"fileingles\" (select)=\"uploadEventIngles($event)\"\n            (upload)=\"uploadEventIngles($event)\" (cancel)=\"cancelEvent()\" required *ngIf=\"!info.urlingles\">\n            <mat-icon>file_upload</mat-icon>\n            <span>{{ singleFileUploadIngles.value?.name }}</span>\n            <ng-template td-file-input-label>\n              <mat-icon>attach_file</mat-icon>\n              <span>\n                Seleccione archivo\n                <span [hidden]=\"!singleFileUploadIngles?.required\">*</span>\n              </span>\n            </ng-template>\n          </td-file-upload>\n          <div class=\"col-md-12 icon-pdf\" *ngIf=\"info.urlingles\">\n            <button mat-mini-fab color=\"warn\" (click)=\"eliminarFileIngles()\">\n              <mat-icon>delete</mat-icon>\n            </button>\n          </div>\n          <br><br>\n          <span style=\"color: #7a7a7a; margin-top: 2em;\">Mensaje</span>\n          <div style=\"margin-top: 1em;\" [froalaEditor] [(froalaModel)]=\"info.contenidoenglish\"></div>\n        </mat-tab>\n        <mat-tab label=\"Italiano\">\n          <td-file-upload #singleFileUploadItaliano [(ngModel)]=\"fileitaliano\" (select)=\"uploadEventItaliano($event)\"\n            (upload)=\"uploadEventItaliano($event)\" (cancel)=\"cancelEvent()\" required *ngIf=\"!info.urlitaliano\">\n            <mat-icon>file_upload</mat-icon>\n            <span>{{ singleFileUploadItaliano.value?.name }}</span>\n            <ng-template td-file-input-label>\n              <mat-icon>attach_file</mat-icon>\n              <span>\n                Seleccione archivo\n                <span [hidden]=\"!singleFileUploadItaliano?.required\">*</span>\n              </span>\n            </ng-template>\n          </td-file-upload>\n          <div class=\"col-md-12 icon-pdf\" *ngIf=\"info.urlitaliano\">\n            <button mat-mini-fab color=\"warn\" (click)=\"eliminarFileItaliano()\">\n              <mat-icon>delete</mat-icon>\n            </button>\n          </div>\n          <br><br>\n          <span style=\"color: #7a7a7a; margin-top: 2em;\">Mensaje</span>\n          <div style=\"margin-top: 1em;\" [froalaEditor] [(froalaModel)]=\"info.contenidoitaliano\"></div>\n        </mat-tab>\n      </mat-tab-group>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/paginas/mensaje-papa/mensaje-papa.component.scss":
/*!**************************************************************!*\
  !*** ./src/paginas/mensaje-papa/mensaje-papa.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  min-height: calc(90vh - 60px);\n  padding: 0 1em; }\n  .row .col-md-3 {\n    background: #f5f8fa;\n    border: 1px solid #dfe3eb;\n    padding: 0; }\n  .row .col-md-3 .header {\n      border-bottom: 1px solid #dfe3eb;\n      padding: 16px 16px;\n      font-size: 12px;\n      -webkit-hyphens: none;\n          -ms-hyphens: none;\n              hyphens: none;\n      overflow: hidden !important;\n      text-overflow: ellipsis !important;\n      white-space: nowrap !important;\n      font-weight: 600;\n      text-transform: uppercase;\n      font-family: \"Roboto\"; }\n  .row .col-md-3 .tareas {\n      margin: 1em;\n      min-height: calc(90vh - 120px); }\n  .row .col-md-3 .tareas mat-card {\n        margin-bottom: 1em; }\n  .titulo {\n  margin: 0 !important;\n  color: #0091ae;\n  cursor: pointer; }\n  .titulo:hover {\n  font-weight: 500; }\n  .icon-pdf {\n  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAW0ElEQVR42u2df4gd13XHz7z3dmVZK3lVWVLsGLxuaJs0pVJicBNKyDql/9SUKKUQFwzeUgr9p7aCwUS2064gbkmQLdkUUkpJ1qWm0NJYwSVJsSQ/0R+0BSdSm4Sowck6FLuyrUq2d/Vz923veW9m9723M29m3sydc+fc7wdGs6vdfXPfsvcz95x77p2AQG156q9/a9qc9oefzobnmfBg+Ov7Cl7mrDkuhR8vhgfTDs9nHnng65fyvSRwhUC6ASAd09G5k89Qr7PzwR37k9LtGuI09URxJjwWjRjOSDcKjAYCcIyws8/SRmcvegeXhkcQkRTakIJbQADCmA4/S70Oz4drd3Vb8GihTT0htKUb4zMQQMX03eEPkD8dPg0WwnHCCKFyIIAKMJ2eO/ss9Tr9ndLtcZzXaEMGx6Ubox0IwBJhp4+OW6TbU1PeoZ4MjkMGdoAASiQc3h8kdHobRDI4hjChPCCAgoRz8XPU6/gY3lcDhwnHzLGAGoRiQABjEmbv58zxoHRbPOc56omgLd2QOgIB5MR0/Dlzmifc7V2DRwXzRgQL0g2pExBABsJh/sHwQGzvNpwr4PDgGMKDdCCAEZiOP0O9Tj9H6Ph1g0WwQD0RLEo3xlUggBjCjj9PiO+1wHmCeYhgMxBAH31D/T+WbguwwmFCaDAABECI8T0DOYI+vBcAsvreglkD8lgAYdUe3wmwIMdveCHSQV+rC70TQDjcnzfHw9JtAU7xDPVGBF6FBV4JIFygs0CI80E8nB+Y82nhkRcCCKf1FgjDfZANDgvmfJg2VC8A0/k5sz9PuOuDfPBogEOCY9INsYlaAYSxPg/lcNcHReDRwAGtuQGVAkCsD0pGbW5AlQCQ4QeWUTdToEYA4bz+AtV/G23gNrzN+ZyWugEVAsCQH1SMmpCg9gIwnZ+ztBjyAwmeMRI4KN2IItRWAMjyA0eo9SxBLQUQxvvc+bGAB7hAbfMCtRNAuBknd37E+8AlOC9woG6bk9ZKAOHS3a9JtwOAEfxunZYY10YApvPPE3bqAfXgsJHAvHQjslALAZjOv0DYnw/Ui+eMBOakG5GG8wJA5wc1xnkJOC0AdH6gAKcl4KQAwjn+NqGsF+iApwlnXawVcE4A6PxAKU5KwEUBcDEFOj/QyFkjgP3SjejHKQEg5gce4FROwBkBoPMDj3BGAk4IAJ0feIgTEhAXACr8gMeIVwyKCgC1/QDIrh0QE0C4qu9lqesD4BD3Sq0iFBFAuJ6f3zCW9ALQW0o8K7GfQOUCQKEPALGIFApJCKBNirfx2tEi2t5ck25G5Vyb3Etvv/emdDPqzmkjgNkqL1ipALRu4Pn+LWv0oW1r3fP2pnRrhPj48/T8t56gi0s/lW5J3al0o9HKBBBu3f1CVderAu7w9+zodXzfmfzkcbp6bZn+5tuQQAl8pqotxysRgMak3yemO7RvCh0/ggXAQAKlUFlS0LoAtCX9tjSMnnd36NYJdP5+IgEwkEApVJIUrEIAauJ+dP5k+gXAQAKlYD0fYFUA2uL++3Z16K6t6PxxDAuAgQRKwWo+wJoAwqH/IimJ+zne57gfxBMnAAYSKAznA2ZshQI2BdAmJfP9PPR/8H2rNNmQbom7JAmAgQQKY60+wIoATOfnuOWozd9Ildyzo9Od7gPJjBIAAwkU5nNGAsfKftHSBWA6/4w58fSFiqE/8+Btq/4W+GQkTQAMJFAIDgX2GwkslvmiNgTQJiVDf4Yz/vfvReyfRhYBMJBAIUoPBUoVgLasP4PhfzayCoCBBApR6qxAaQLQlvWPwNRfNvIIgIEExqbUWYEyBaCm4KcfLvxBrX86eQXAQAJjU1qBUCkCCGv9vyv6K7EEBJCNcQTAQAJj85Ey1gqUJYA2KUr89QMBZGNcATCQwFiUkhAsLADtG3tCANkoIgAGEhiLwhuKFhJAmPjjYcid0r8JW0AA2SgqAAYSyM1r1KsNGDshWFQA86R8T38IIBtlCICBBHJT6NkCYwtA67TfMBBANsoSAAMJ5KLQtGARAcyT8rs/AwFko0wBMJBALsYeBYwlgLDe/yfS77oKIIBslC0ABhLIxV3jrBMYVwAL5MnDPCGAbNgQAAMJZGash43mFoBPd38GAsiGLQEwkEBmco8CxhHAAnly92cggGzYFAADCWQid4lwLgH4kvnvBwLIhm0BMJBAKrlnBPIKYJ48yPz3AwFkowoBMJBAKrlmBPIKgM3izd2fgQCyUZUAGEhgJO8YAUxn/ebMAtBe858EBJCNKgXAQAIjybxGII8AFklxzX8SEEA2qhYAAwkk8poRwEyWb8wkANP5Z83pZel3JQEEkA0JATCQQCL3Ggm0074pqwAWyKOpv34ggGxICYCBBGLJVBiUKoBw6u+i9LuRAgLIhqQAGEgglp1pU4JZBKDqIR95gQCyIS0ABhLYROrDRLIIYJE8TP5FQADZcEEADCQwQGoycKQANG/2mRUIIBuuCIBhCfztiT+ity8tSjfFBUZuHpomgAXyNPkXAQFkwyUBMJDAOiOTgWkC8K7ybxgIIBuuCYCBBLqMrAxMFIDGx3yNAwSQDRcFwEACXRIfJzZKAAvk+fCfgQCy8e4HPk+33vEx6WbEAgkkhwGjBOD98J+BALKxsv3DdPNHn5RuRiKeSyAxDIgVAIb/G0AA2bn+gYdo6o5PSTcjEc8lEBsGJAlA5YM+xwECyMeV2z9Lt/zc70g3IxGPJRC7W1CSABbJ4+KffiCA/AQ37aGr0/fQNdpGF691pJuzidWVZXrxv75FzVZDuilVElsUtEkAKP4ZBALQyaF/WaLdt03R5GRTuilVsqkoKE4AXtf+DwMB6OTz//QeBY3ANwlsWhsQJ4A2KX3U9zhAADphATCeSWDTI8XjBIC/9j4gAJ1EAmBYAtO7ttK2qUnpZlnHCGCgzw984vPOP0lAADrpF0DEzt03+yCBgZ2ChgUwT55t+50GBKCTOAEwHkhgYNvwYQG0CfH/ABCATpIEwCiXwEAeYFgA+EsfAgLQySgBMDcbAfyMEYFG+vMA6x9g/j8eCEAnaQJgFEtgvR6gXwCY/48BAtBJFgEwSiWwXg/QL4AFwvLfTbggAC6t5aMqOks/IVpZFn3PtskqAGbrzRPdvECjkfth2q6yvjy4XwA8JNgn3TLXcEEAE/uepGD6w5Vdb+3S9+nG2cdF37Nt8giAmZhs0O7btmuRwFkjAA75BwSAcW4M0gIIpu6iiburj8yunz4g9p6rIK8AGE0SiBKB3X+QAExGWgCN6V+i1r4vVn5dCCAeRRLoJgIjAWADkAQgAJ2MKwCGlxHv2rut7usHuhuERAKYJ1QAxgIB6KSIABgFi4i6FYGRAHiroE9Lt8hFIACdFBUAU3MJfMMI4EAkgDahBDgWCEAnZQiAqbEEuiXBkQAwA5CAtAAwC2CHsgQQUcf1AzwTEPj++O80pAXAtD74EDX2Vrfbbuf8KVr54bOi79k2ZQuAqaEEdgbYA2A0LggAlI8NATA1k8C9EEAKEIBObAmA2TF9E+3YeZP0W8xCVwDzhCnARCAAndgUAFOTRUSHIYAUIACd2BYAUwMJdAWwQFgFmAgEoJMqBMA4LoHnAtQAjAYC0ElVAmC2bG3Rrj3bXFw/cBoCSAEC0EmVAmAcXUTUFQD2ARgBBKCTqgXAOCiBswGqAEcDAehEQgAMS+DWvVPOPJgUAkjBCQG0tlHrFx6ioDUl/esojZVX/5LWeOsxIaQEwLi0fgACSEFcAKbzd7cEm5qR/lWUy8pluvHKQVq7+qbI5SUFwLgiAQggBWkBSK0GrIKVs09Q59L3RK4tLQDGBQlAAClAAPbwXQAMS4DrBHjnYZHrQwCjgQDsAQFsILWICAJIAQKwBwQwiIQEIIAUIAB7QACbqVoCEEAKEIA9IIB4pnZsoeldWyu5FgSQgrQAmKp3BKqCtaXF3tOHhB5B5rIAmKoWEUEAKbggAKZ5x292awK0sPo/L4o+f9B1ATBVSAACSMEVAYByqYMAGJYAhwO21g9AAClAADqpiwAYm4uIIIAUIACd1EkAjC0JQAApQAA6qZsAGBsSgABSgAB0UkcBMCyBnbvLezApNgRJAQLQSV0FwJS4iOgstgRLAQLQSZ0FwJQkAewJmAYEoJO6C4ApQQKnsS14ChCATjQIIKLA+oHn8GCQFFwQQPcJwfueJGo5u798blbOPUud/z0ldn1NAmDGlACeDJSGtACCm/bQxN3HVHX+iBuvfE5sX0BtAmDGkMBhPBw0BWkBYDWgHTQKgOGyYV5NmBE8HTgNCMAeEIAdciwi6gpg2nxwUbrRrgIB2AMCsEdGCezs1hSiGjAZCMAeEIBd0iTwyANfDyIBtAm1ALFAAPaAAOzDuw1zcjBm/cBpI4DZSADHzenT0o11EWkBdKcA7z4q/WuwAgRQDQmLiL5hBHAgEsA8YSowFmkBMI33far7aDBNdM6fopUfPit2fZ8EwMRI4LARwHwkgAPm9IJ0I13EBQEwPBIIFG0JJnXnj/BNAMyQBD5jBHA8EsB+c/qudANdxBUBgHLxUQBM3/qBjxgBnFkfD2AmIB4IQCe+CoBhCTz9+Mlu3+8XAPYFiAEC0InPAjC9/tzRJ059sPdhCFYFxgMB6MRnATSawd8/9djJ3+aP+wVw0Jx0zjcVAALQic8CaLYaXzhy6ES3uKRfAEgExgAB6MRrATSDjx957OS/8ccDlQFIBG4GAtCJzwI4+oVT6/1+WABtQknwABCATnwVQNAI/uPpx0/+yvrn/V9EReBmXBEACoHKxVcBNJrB0089dvKR6PNhAcwS9gYYwAUBoBS4fHwVQLPVuO/IoRPfjD7ftEQIeYBBpAWAxUB28FUA/fE/EyeANiEPsI60ALAc2A4+CmA4/u/+3/A3oR5gEAjAHhBAtfTP/0fECQD1AH1AAPaAAKqlf/4/IvYxo0YCi+Z0p3SDXQACsAcEUCEBvX70iVPv3/zfMRgBHDOnh6Xb7AIQgD0ggOpoNIOvPvXYyd8b/v8kAWCDkBBpAeDBIHbwTQAm/n/AxP/PD/9/kPQDRgKXzOkW6YZLIy0ABo8GKx+vBBDQkhn+b4//UgJYHtzDBQGA8vFJAP3Lf4cZJQCEAQQBaMUnASQN/5lg1A8iDIAAtOKNAEYM/3tfHgHCAAhAK74IYNTwn0kTgPdFQRCATnwRQFzxTz9B2gv4XhQEAejECwEkFP8MfksKvq8NgAB04oMA4mr/h8kiAK8fHw4B6MQHAUxuad76pUdfujDqe1IFwPicDIQAdKJdAGnJv4isApglT3cKggB0ol0Awzv/JJFJAIyvyUAIQCeqBZAh+bfxrRkxApgzp69Jv7eqgQB0olkA5u7/h+bu/2dZvjezABgfKwMhAJ2oFUBK5d/mb8+Bj9uGQwA60SqA4W2/08grAJ4SXCSPRgEQgE5UCsDc/ScnmzNpU3+DP5IT33YLggB0olEASbv+jGIcAcyYk8w2LgJAAPp4Y7lDz3xnWboZpTOxpfnzX370pR/l+ZncAmB8Kgz6xHSH9k1BAJr4wYUV+qsfXJFuRqlkLfwZZlwBzJAno4APbVujX9vZkW4GKJETP71OJ167Jt2M8jCx/8Rk86N57/69Hx0TX2YEtjSIfv/2VelmgBJ51gz/X1/WI/W8mf9+igjAmxmB+3Z16K6tCAM0oC7+HyPzP/jjBfBlFMBJQE4Ggvrzd/99lV45f0O6GaVR5O7PFBUAjwLOkAdrBDAKqD8K7/6vm7v/L4979++9REF8WSOwo0V0/55VmmxItwSMi7bYP0/NfxKFBcD48khxhAL1RdvQP+5R32O9ThmN8WnzUEwL1g9tnZ9J2+wzK6UIgPGpRPhnt/YksAXhgNNcXVmjF398TV3nH6fkN4kyBeDNtCAz1Vij2akbNLOjKd0UEMOP31mlf3j1qqqYv0vBab/NL1civj1OrNNZo6nly/SxPS36xV0t6eYA6nV8vuNru+tHjHrM1ziUKgDGl4RgBEvgrTfeMyMCExrc0qTbtzXotimMCqrk4tVOd4rv+xdWuh9rpazE38Brlt3IcJ0A1wZ4EQowkQRuXNf7xweEKVDvP/plLeDjw0RYApcuXKHLS9elmwIUkuUhH+NgRQCMb6FAxP+9dRkSAKViY+i//tq2Gu3brEA/kAAojZKz/ptf3iK+zQr0AwmAMig76z+MVQEwPhUIDcM5gaV3FW08ASqlzIKfJKoQAIcCbXPss30tF1k2o4CLZjQAQC4COmeG/r9qa+i/cZkKCNcKtMnDfAADCYBcmLi/2Qh+vYxa//RLVYTP+QAGEgBZsR3391OZABif8wEMJADSqCLu76dSATC+1gdEXLu6Qm+fX6a1DnYXAoPYnO9PvGbVb9L3pCBz/foqvfXGEiQANqgo6bf5sgL4nhRkIAGwToVJv82XFsJIYNacXpa6vgtAAoBpthr3HTl04psS1xYTAOPLhqKjYAlcOL9MqytYSegjZWzsWQRRATC+PFtgFFhO7CdF9/QvA3EBMD49bDQJSMAvxn2YZ9k4IQAGEoAEfMGVzs84IwAGEoAEtONS52ecEgBjJMDbiXlbI8CwBLhi8MplnRtb+opEoU9qm6QbMAwKhTbAngKKECr0SW+Wg0ACG0ACCnC08/ea5jDICfSABOqLazH/ME4LgIEEekAC9cP1zs84LwAGEuiB5cT1oQ6dn6mFABhUDPaABNzHhQq/rNRGAAzWDvSABNxFurY/L7USABOuIjxOHi8lZiABx+Alvc3GZ6VW9Y3f7BoS7iewQJ5PE3KhECcHsZxYmIDONRvBnMR6/uJNrylhrQCPBLzdXozBngKycHXfxETjN1yc48/UfukGFMX3jUYZSECGqjfwtEHtBcCEW44vkMd5AUigQnrx/h9UtXW33beiBOQFIIFKqHG8H/92FBHmBebJ45AAy4ntwUP+VqvxaF3j/ThUCSDC95AAEigZRUP+zW9NKb7PEkAC5RA0ghMTE437Nd31B96fdANsY0RwkHphgXejAUigAL27/pfMXf+L0k2x+zY9wEhghnohgXejAZbAhTeX6dqVFemm1Aae229NNB748qMv/Ui6Ldbfq3QDqsTn3ACWE2dAcayf/JY9w+eZAkggGY0Z/ix4J4CIsG6Aqwi9CgsggUF4uN8I6GEt8/q53790A6QJlxjPm+NO6bZUBSRA/Jf/uhnu/2mdlu7a+TWAKCw4GB5e5AeW3r1Gly5ckW5G9Zg4v9EI/sIM9//Et+F+/K8DrNMnAi92HvJqTwF0/FgggBjCacN58mAfQh8kwPvzNVuNQz5M6+UFAhhBKAIeEcyR4tBApQTCO77p+H+Ojp8MBJABH3IELAHOCdR+JSGG+rmAAHKiedag1suJkdUfCwhgTMLNSedIWZ6gbhLg+D4Igq/WbTNOV4AAChKGB3PUCw9UjAqcl0Dvbv+VZjP4Cob5xYAASiSsLmQR8JqDWucKnJNAL7b/R/MHe8TXqj0bQACWCBceRUctZbC60qG3zy/JLSeOOn0QvODTAp0qgQAqIJTBLPVkUKswofI9Bczw3nT6b5tOfwqd3j4QQMWEYcIs9WRQi4VItiXQXZDTCF6ktbUTGN5XCwQgTDibEB3OCqFMCXCHDwL6Z3OXP4nsvSwQgGP0jRD2h4cz25yPJYGAzpm7+/dMZz+DO7x7QAA1IJTCDG1IgacexUYLccuJ+a5uTu+aO/t/ms7+HdPZX0Vndx8IoMaENQj7w09nw/NMeDD89aIjiLPmuBR+vBgeXDb8r1cu32g1m8G/Yy6+vvw/uAP8/OUkeMwAAAAASUVORK5CYII=);\n  background-size: contain;\n  background-position: center;\n  background-repeat: no-repeat;\n  width: 100%;\n  padding: 3em;\n  color: #fb7958 !important;\n  border: 1px dashed !important; }\n  .icon-pdf button {\n    position: absolute;\n    right: 1em; }\n"

/***/ }),

/***/ "./src/paginas/mensaje-papa/mensaje-papa.component.ts":
/*!************************************************************!*\
  !*** ./src/paginas/mensaje-papa/mensaje-papa.component.ts ***!
  \************************************************************/
/*! exports provided: MensajePapaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MensajePapaComponent", function() { return MensajePapaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../app/core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _app_core_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app/core/upload.service */ "./src/app/core/upload.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MensajePapaComponent = /** @class */ (function () {
    function MensajePapaComponent(db, snackBar, _loadingService, _uploadService) {
        var _this = this;
        this.db = db;
        this.snackBar = snackBar;
        this._loadingService = _loadingService;
        this._uploadService = _uploadService;
        this.info = {};
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.disabled = false;
        this.db
            .docWithRefs$("secciones-documentos/mensaje-papa")
            .subscribe(function (resp) {
            _this.info = resp;
        });
    }
    MensajePapaComponent.prototype.ngOnInit = function () { };
    MensajePapaComponent.prototype.guardar = function () {
        var _this = this;
        this.db
            .set("secciones-documentos/mensaje-papa", this.info)
            .then(function () {
            _this.snackBar.open("Datos guardados", "Cerrar", {
                duration: 2000
            });
            _this._loadingService.resolve();
        })
            .catch(function (err) {
            _this._loadingService.resolve();
            _this.snackBar.open("Error guardando cambios", "Cerrar", {
                duration: 2000
            });
        });
    };
    MensajePapaComponent.prototype.uploadEvent = function (file) {
        var _this = this;
        this.fileUploadMsg = file.name;
        this.file = file;
        this._loadingService.register();
        var archivo = new _app_core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](this.file);
        this._uploadService.uploadDocumento(archivo).then(function (url) {
            _this.info.url = url;
            _this._loadingService.resolve();
        });
    };
    MensajePapaComponent.prototype.uploadEventIngles = function (file) {
        var _this = this;
        this.fileUploadMsg = file.name;
        this.fileingles = file;
        this._loadingService.register();
        var archivo = new _app_core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](this.fileingles);
        this._uploadService.uploadDocumento(archivo).then(function (url) {
            _this.info.urlingles = url;
            _this._loadingService.resolve();
        });
    };
    MensajePapaComponent.prototype.uploadEventItaliano = function (file) {
        var _this = this;
        this.fileUploadMsg = file.name;
        this.fileitaliano = file;
        this._loadingService.register();
        var archivo = new _app_core_upload_service__WEBPACK_IMPORTED_MODULE_4__["Upload"](this.fileitaliano);
        this._uploadService.uploadDocumento(archivo).then(function (url) {
            _this.info.urlitaliano = url;
            _this._loadingService.resolve();
        });
    };
    MensajePapaComponent.prototype.eliminarFile = function () {
        this.info.url = "";
    };
    MensajePapaComponent.prototype.eliminarFileIngles = function () {
        this.info.urlingles = "";
    };
    MensajePapaComponent.prototype.eliminarFileItaliano = function () {
        this.info.urlitaliano = "";
    };
    MensajePapaComponent.prototype.cancelEvent = function () {
        this.fileSelectMsg = "No file selected yet.";
        this.fileUploadMsg = "No file uploaded yet.";
        this.info.file = null;
    };
    MensajePapaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-mensaje-papa",
            template: __webpack_require__(/*! ./mensaje-papa.component.html */ "./src/paginas/mensaje-papa/mensaje-papa.component.html"),
            styles: [__webpack_require__(/*! ./mensaje-papa.component.scss */ "./src/paginas/mensaje-papa/mensaje-papa.component.scss")]
        }),
        __metadata("design:paramtypes", [_app_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_2__["TdLoadingService"],
            _app_core_upload_service__WEBPACK_IMPORTED_MODULE_4__["UploadService"]])
    ], MensajePapaComponent);
    return MensajePapaComponent;
}());



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/checho/Trabajo/Trusty/Proyectos/Pasionistas/administrador/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map