(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: firebaseConfig, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "firebaseConfig", function() { return firebaseConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _componentes_banner_carousel_banner_carousel_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./componentes/banner-carousel/banner-carousel.component */ "./src/app/componentes/banner-carousel/banner-carousel.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _componentes_barra_navegacion_barra_navegacion_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./componentes/barra-navegacion/barra-navegacion.component */ "./src/app/componentes/barra-navegacion/barra-navegacion.component.ts");
/* harmony import */ var _componentes_hero_banner_hero_banner_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./componentes/hero-banner/hero-banner.component */ "./src/app/componentes/hero-banner/hero-banner.component.ts");
/* harmony import */ var _componentes_mensaje_papa_mensaje_papa_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./componentes/mensaje-papa/mensaje-papa.component */ "./src/app/componentes/mensaje-papa/mensaje-papa.component.ts");
/* harmony import */ var _componentes_agenda_agenda_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./componentes/agenda/agenda.component */ "./src/app/componentes/agenda/agenda.component.ts");
/* harmony import */ var _componentes_liturgias_liturgias_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./componentes/liturgias/liturgias.component */ "./src/app/componentes/liturgias/liturgias.component.ts");
/* harmony import */ var _componentes_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./componentes/cronicas/cronicas.component */ "./src/app/componentes/cronicas/cronicas.component.ts");
/* harmony import */ var _componentes_biblioteca_biblioteca_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./componentes/biblioteca/biblioteca.component */ "./src/app/componentes/biblioteca/biblioteca.component.ts");
/* harmony import */ var _componentes_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./componentes/proyectos/proyectos.component */ "./src/app/componentes/proyectos/proyectos.component.ts");
/* harmony import */ var _paginas_asistentes_asistentes_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./paginas/asistentes/asistentes.component */ "./src/app/paginas/asistentes/asistentes.component.ts");
/* harmony import */ var _paginas_home_home_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./paginas/home/home.component */ "./src/app/paginas/home/home.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./app.routing.module */ "./src/app/app.routing.module.ts");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_auth_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./core/auth.service */ "./src/app/core/auth.service.ts");
/* harmony import */ var _core_upload_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./core/upload.service */ "./src/app/core/upload.service.ts");
/* harmony import */ var ngx_store__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ngx-store */ "./node_modules/ngx-store/ngx-store.js");
/* harmony import */ var angular2_fontawesome_angular2_fontawesome__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! angular2-fontawesome/angular2-fontawesome */ "./node_modules/angular2-fontawesome/angular2-fontawesome.js");
/* harmony import */ var angular2_fontawesome_angular2_fontawesome__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(angular2_fontawesome_angular2_fontawesome__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/ngx-spinner.umd.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(ngx_spinner__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var _node_modules_angular_forms__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../../node_modules/@angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js");
/* harmony import */ var _paginas_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./paginas/inicio/inicio.component */ "./src/app/paginas/inicio/inicio.component.ts");
/* harmony import */ var _paginas_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./paginas/proyectos/proyectos.component */ "./src/app/paginas/proyectos/proyectos.component.ts");
/* harmony import */ var _paginas_proyecto_detalle_proyecto_detalle_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./paginas/proyecto-detalle/proyecto-detalle.component */ "./src/app/paginas/proyecto-detalle/proyecto-detalle.component.ts");
/* harmony import */ var _paginas_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./paginas/cronicas/cronicas.component */ "./src/app/paginas/cronicas/cronicas.component.ts");
/* harmony import */ var _paginas_cronica_detalle_cronica_detalle_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./paginas/cronica-detalle/cronica-detalle.component */ "./src/app/paginas/cronica-detalle/cronica-detalle.component.ts");
/* harmony import */ var _componentes_encuestra_encuestra_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./componentes/encuestra/encuestra.component */ "./src/app/componentes/encuestra/encuestra.component.ts");
/* harmony import */ var _paginas_encuestas_encuestas_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./paginas/encuestas/encuestas.component */ "./src/app/paginas/encuestas/encuestas.component.ts");
/* harmony import */ var _pipes_order_by_pipe__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./pipes/order-by.pipe */ "./src/app/pipes/order-by.pipe.ts");
/* harmony import */ var _paginas_foros_foros_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./paginas/foros/foros.component */ "./src/app/paginas/foros/foros.component.ts");
/* harmony import */ var _paginas_foro_detalle_foro_detalle_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./paginas/foro-detalle/foro-detalle.component */ "./src/app/paginas/foro-detalle/foro-detalle.component.ts");
/* harmony import */ var _paginas_grupos_grupos_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./paginas/grupos/grupos.component */ "./src/app/paginas/grupos/grupos.component.ts");
/* harmony import */ var _paginas_asistentes_grupos_asistentes_grupos_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./paginas/asistentes-grupos/asistentes-grupos.component */ "./src/app/paginas/asistentes-grupos/asistentes-grupos.component.ts");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









































var firebaseConfig = {
    apiKey: "AIzaSyAaLH5P2QXTeSNN2S8wLkYEftf_3qj9UTw",
    authDomain: "pasionistas-conferencia.firebaseapp.com",
    databaseURL: "https://pasionistas-conferencia.firebaseio.com",
    projectId: "pasionistas-conferencia",
    storageBucket: "pasionistas-conferencia.appspot.com",
    messagingSenderId: "799598907957"
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _componentes_banner_carousel_banner_carousel_component__WEBPACK_IMPORTED_MODULE_4__["BannerCarouselComponent"],
                _componentes_barra_navegacion_barra_navegacion_component__WEBPACK_IMPORTED_MODULE_6__["BarraNavegacionComponent"],
                _componentes_hero_banner_hero_banner_component__WEBPACK_IMPORTED_MODULE_7__["HeroBannerComponent"],
                _componentes_mensaje_papa_mensaje_papa_component__WEBPACK_IMPORTED_MODULE_8__["MensajePapaComponent"],
                _componentes_agenda_agenda_component__WEBPACK_IMPORTED_MODULE_9__["AgendaComponent"],
                _componentes_liturgias_liturgias_component__WEBPACK_IMPORTED_MODULE_10__["LiturgiasComponent"],
                _componentes_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_11__["CronicasComponent"],
                _componentes_biblioteca_biblioteca_component__WEBPACK_IMPORTED_MODULE_12__["BibliotecaComponent"],
                _componentes_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_13__["ProyectosComponent"],
                _paginas_asistentes_asistentes_component__WEBPACK_IMPORTED_MODULE_14__["AsistentesComponent"],
                _paginas_home_home_component__WEBPACK_IMPORTED_MODULE_15__["HomeComponent"],
                _paginas_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_25__["InicioComponent"],
                _paginas_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_26__["ProyectosPaginaComponent"],
                _paginas_proyecto_detalle_proyecto_detalle_component__WEBPACK_IMPORTED_MODULE_27__["ProyectoDetalleComponent"],
                _paginas_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_28__["CronicasPaginaComponent"],
                _paginas_cronica_detalle_cronica_detalle_component__WEBPACK_IMPORTED_MODULE_29__["CronicaDetalleComponent"],
                _componentes_encuestra_encuestra_component__WEBPACK_IMPORTED_MODULE_30__["EncuestraComponent"],
                _paginas_encuestas_encuestas_component__WEBPACK_IMPORTED_MODULE_31__["EncuestasPaginaComponent"],
                _pipes_order_by_pipe__WEBPACK_IMPORTED_MODULE_32__["OrderByPipe"],
                _paginas_foros_foros_component__WEBPACK_IMPORTED_MODULE_33__["ForosComponent"],
                _paginas_foro_detalle_foro_detalle_component__WEBPACK_IMPORTED_MODULE_34__["ForoDetalleComponent"],
                _paginas_grupos_grupos_component__WEBPACK_IMPORTED_MODULE_35__["GruposComponent"],
                _paginas_asistentes_grupos_asistentes_grupos_component__WEBPACK_IMPORTED_MODULE_36__["AsistentesGruposComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_16__["AppRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"].forRoot(),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _angular_fire_auth__WEBPACK_IMPORTED_MODULE_40__["AngularFireAuthModule"],
                _angular_fire__WEBPACK_IMPORTED_MODULE_37__["AngularFireModule"].initializeApp(firebaseConfig),
                _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_38__["AngularFirestoreModule"].enablePersistence(),
                _angular_fire_storage__WEBPACK_IMPORTED_MODULE_39__["AngularFireStorageModule"],
                ngx_store__WEBPACK_IMPORTED_MODULE_20__["WebStorageModule"],
                angular2_fontawesome_angular2_fontawesome__WEBPACK_IMPORTED_MODULE_21__["Angular2FontawesomeModule"],
                ngx_spinner__WEBPACK_IMPORTED_MODULE_22__["NgxSpinnerModule"],
                _node_modules_angular_forms__WEBPACK_IMPORTED_MODULE_23__["FormsModule"],
                ng2_search_filter__WEBPACK_IMPORTED_MODULE_24__["Ng2SearchPipeModule"],
            ],
            providers: [_core_resources_service__WEBPACK_IMPORTED_MODULE_17__["ResourcesService"], _core_auth_service__WEBPACK_IMPORTED_MODULE_18__["AuthService"], _core_upload_service__WEBPACK_IMPORTED_MODULE_19__["UploadService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app.routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _paginas_home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./paginas/home/home.component */ "./src/app/paginas/home/home.component.ts");
/* harmony import */ var _paginas_asistentes_asistentes_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./paginas/asistentes/asistentes.component */ "./src/app/paginas/asistentes/asistentes.component.ts");
/* harmony import */ var _paginas_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./paginas/inicio/inicio.component */ "./src/app/paginas/inicio/inicio.component.ts");
/* harmony import */ var _paginas_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./paginas/proyectos/proyectos.component */ "./src/app/paginas/proyectos/proyectos.component.ts");
/* harmony import */ var _paginas_proyecto_detalle_proyecto_detalle_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./paginas/proyecto-detalle/proyecto-detalle.component */ "./src/app/paginas/proyecto-detalle/proyecto-detalle.component.ts");
/* harmony import */ var _paginas_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./paginas/cronicas/cronicas.component */ "./src/app/paginas/cronicas/cronicas.component.ts");
/* harmony import */ var _paginas_cronica_detalle_cronica_detalle_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./paginas/cronica-detalle/cronica-detalle.component */ "./src/app/paginas/cronica-detalle/cronica-detalle.component.ts");
/* harmony import */ var _paginas_encuestas_encuestas_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./paginas/encuestas/encuestas.component */ "./src/app/paginas/encuestas/encuestas.component.ts");
/* harmony import */ var _paginas_foros_foros_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./paginas/foros/foros.component */ "./src/app/paginas/foros/foros.component.ts");
/* harmony import */ var _paginas_foro_detalle_foro_detalle_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./paginas/foro-detalle/foro-detalle.component */ "./src/app/paginas/foro-detalle/foro-detalle.component.ts");
/* harmony import */ var _paginas_grupos_grupos_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./paginas/grupos/grupos.component */ "./src/app/paginas/grupos/grupos.component.ts");
/* harmony import */ var _paginas_asistentes_grupos_asistentes_grupos_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./paginas/asistentes-grupos/asistentes-grupos.component */ "./src/app/paginas/asistentes-grupos/asistentes-grupos.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var options = {
    useHash: true
};
var routes = [
    { path: "", component: _paginas_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_4__["InicioComponent"] },
    { path: "inicio", component: _paginas_home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] },
    { path: "participants", component: _paginas_asistentes_asistentes_component__WEBPACK_IMPORTED_MODULE_3__["AsistentesComponent"] },
    { path: "proyectos", component: _paginas_proyectos_proyectos_component__WEBPACK_IMPORTED_MODULE_5__["ProyectosPaginaComponent"] },
    { path: "proyecto-detalle/:id", component: _paginas_proyecto_detalle_proyecto_detalle_component__WEBPACK_IMPORTED_MODULE_6__["ProyectoDetalleComponent"] },
    { path: "cronicas", component: _paginas_cronicas_cronicas_component__WEBPACK_IMPORTED_MODULE_7__["CronicasPaginaComponent"] },
    { path: "cronica-detalle/:id", component: _paginas_cronica_detalle_cronica_detalle_component__WEBPACK_IMPORTED_MODULE_8__["CronicaDetalleComponent"] },
    { path: "votaciones", component: _paginas_encuestas_encuestas_component__WEBPACK_IMPORTED_MODULE_9__["EncuestasPaginaComponent"] },
    { path: "foros", component: _paginas_foros_foros_component__WEBPACK_IMPORTED_MODULE_10__["ForosComponent"] },
    { path: "foro-detalle/:id", component: _paginas_foro_detalle_foro_detalle_component__WEBPACK_IMPORTED_MODULE_11__["ForoDetalleComponent"] },
    { path: "groups", component: _paginas_grupos_grupos_component__WEBPACK_IMPORTED_MODULE_12__["GruposComponent"] },
    { path: "groups/participants/:id", component: _paginas_asistentes_grupos_asistentes_grupos_component__WEBPACK_IMPORTED_MODULE_13__["AsistentesGruposComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, options)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/componentes/agenda/agenda.component.html":
/*!**********************************************************!*\
  !*** ./src/app/componentes/agenda/agenda.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<section class=\"actividades\">\n  <ul class=\"opciones-actividades\">\n    <li class=\"active\" [ngClass]=\"{active: opcion == 'calendario'}\">\n      <a class=\"tappable\" (click)=\"opcion='calendario'\" *ngIf=\"idioma=='español'\">Calendario </a>\n      <a class=\"tappable\" (click)=\"opcion='calendario'\" *ngIf=\"idioma=='english'\">Schedule </a>\n      <a class=\"tappable\" (click)=\"opcion='calendario'\" *ngIf=\"idioma=='italiano'\">Programma </a>\n    </li>\n    <li>/</li>\n    <li [ngClass]=\"{active: opcion == 'actividades'}\">\n      <a class=\"tappable\" (click)=\"opcion='actividades'\" *ngIf=\"idioma=='español'\">Actividades </a>\n      <a class=\"tappable\" (click)=\"opcion='actividades'\" *ngIf=\"idioma=='english'\">Activities </a>\n      <a class=\"tappable\" (click)=\"opcion='actividades'\" *ngIf=\"idioma=='italiano'\">Attività </a>\n    </li>\n  </ul>\n\n  <ngb-tabset orientation=\"horizontal\" *ngIf=\"opcion == 'calendario'\">\n    <ngb-tab title=\"{{item.fecha.toDate() | date:'MMMM dd'}}\" *ngFor=\"let item of actividades\">\n      <ng-template ngbTabContent>\n        <div class=\"card event-card\" style=\"width: 100%;min-width: 75vw;\" *ngFor=\"let actividad of item.actividades\"\n\n        [ngClass]=\"{ 'card-inicio': actividad.actividad == 'inicio', 'card-estado': actividad.actividad == 'estado', 'card-identidad': actividad.actividad == 'identidad',\n                     'card-solidaridad': actividad.actividad == 'solidaridad', 'card-comision': actividad.actividad == 'comision',\n                     'card-nuevos': actividad.actividad == 'nuevos', 'card-liderazgo': actividad.actividad == 'liderazgo',\n                    'card-propuesta': actividad.actividad == 'propuesta'}\" >\n\n          <div class=\"card-body\">\n\n            <h6 class=\"card-subtitle mb-2 text-muted hour\" *ngIf=\"idioma=='español'\">{{actividad.horainicio.toDate() | date:'HH:mm':'UTC-5'}} -\n              {{actividad.horafin.toDate() | date:'HH:mm':'UTC-5'}}</h6>\n            <h6 class=\"card-subtitle mb-2 text-muted hour\" *ngIf=\"idioma=='english'\">{{actividad.horainicio.toDate() | date:'HH:mm':'UTC-5'}} -\n              {{actividad.horafin.toDate() | date:'HH:mm':'UTC-5'}}</h6>\n            <h6 class=\"card-subtitle mb-2 text-muted hour\" *ngIf=\"idioma=='italiano'\">{{actividad.horainicio.toDate() | date:'HH:mm':'UTC-5'}} -\n              {{actividad.horafin.toDate() | date:'HH:mm':'UTC-5'}}</h6>\n\n\n            <h6 class=\"card-subtitle mb-2 text-muted hour tipo-actividad\" *ngIf=\"idioma=='español'\">\n              <span *ngIf=\"actividad.actividad == 'inicio'\">Bienvenida, presentación, inicio</span>\n              <span *ngIf=\"actividad.actividad == 'estado'\">Estado de la congregación, compartiendo en configuraciones</span>\n              <span *ngIf=\"actividad.actividad == 'identidad'\">Identidad pasionista, formación, Vida de comunidad y visión</span>\n              <span *ngIf=\"actividad.actividad == 'solidaridad'\">Solidaridad y finanzas</span>\n              <span *ngIf=\"actividad.actividad == 'comision'\">Comisión juridica</span>\n              <span *ngIf=\"actividad.actividad == 'nuevos'\">Nuevos puntos, nuevas fronteras, trabajos laicos</span>\n              <span *ngIf=\"actividad.actividad == 'liderazgo'\">Liderazgo, elecciones</span>\n              <span *ngIf=\"actividad.actividad == 'propuesta'\">Propuestas formuladas y votaciones</span>\n            </h6>\n\n            <h6 class=\"card-subtitle mb-2 text-muted hour tipo-actividad\" *ngIf=\"idioma=='english'\">\n              <span *ngIf=\"actividad.actividad == 'inicio'\">Welcome, presentation, home</span>\n              <span *ngIf=\"actividad.actividad == 'estado'\">State of the congregation, sharing in configurations</span>\n              <span *ngIf=\"actividad.actividad == 'identidad'\">Passionist identity, formation, Community life and vision</span>\n              <span *ngIf=\"actividad.actividad == 'solidaridad'\">Solidarity and finances</span>\n              <span *ngIf=\"actividad.actividad == 'comision'\">Legal Commission</span>\n              <span *ngIf=\"actividad.actividad == 'nuevos'\">New points, new frontiers, lay jobs</span>\n              <span *ngIf=\"actividad.actividad == 'liderazgo'\">Leadership, elections</span>\n              <span *ngIf=\"actividad.actividad == 'propuesta'\">Proposals formulated and votes</span>\n            </h6>\n\n            <h6 class=\"card-subtitle mb-2 text-muted hour tipo-actividad\" *ngIf=\"idioma=='italiano'\">\n              <span *ngIf=\"actividad.actividad == 'inicio'\">Benvenuto, presentazione, casa</span>\n              <span *ngIf=\"actividad.actividad == 'estado'\">Stato della congregazione, condivisione in configurazioni</span>\n              <span *ngIf=\"actividad.actividad == 'identidad'\">Identità passionista, formazione, vita comunitaria e visione</span>\n              <span *ngIf=\"actividad.actividad == 'solidaridad'\">Solidarietà e finanze</span>\n              <span *ngIf=\"actividad.actividad == 'comision'\">Commissione legale</span>\n              <span *ngIf=\"actividad.actividad == 'nuevos'\">Nuovi punti, nuove frontiere, posti di lavoro</span>\n              <span *ngIf=\"actividad.actividad == 'liderazgo'\">Leadership, elezioni</span>\n              <span *ngIf=\"actividad.actividad == 'propuesta'\">Proposte formulate e voti</span>\n            </h6>\n\n\n            <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='español'\">{{actividad.titulo}}</h6>\n            <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='english'\">{{actividad.tituloingles}}</h6>\n            <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='italiano'\">{{actividad.tituloitaliano}}</h6>\n\n            <p class=\"card-text content-event\" *ngIf=\"idioma=='español'\">{{actividad.descripcion}}</p>\n            <p class=\"card-text content-event\" *ngIf=\"idioma=='english'\">{{actividad.descripcioningles}}</p>\n            <p class=\"card-text content-event\" *ngIf=\"idioma=='italiano'\">{{actividad.descripcionitaliano}}</p>\n          </div>\n        </div>\n\n      </ng-template>\n    </ngb-tab>\n  </ngb-tabset>\n\n  <div class=\"container-actividades\" *ngIf=\"opcion == 'actividades'\">\n    <div class=\"card contenedor\">\n      <div class=\"card-body\">\n\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='español'\">Sin iniciar</h6>\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='english'\">Without starting</h6>\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='italiano'\">Senza iniziare</h6>\n\n        <div class=\"card actividad\" *ngFor=\"let item of tareasPendientes\">\n          <div class=\"card-body\">\n            <h6 class=\"card-title\" *ngIf=\"idioma=='español'\">{{item.titulo}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='español'\">{{item.notas}}</p>\n            <h6 class=\"card-title\" *ngIf=\"idioma=='english'\">{{item.tituloingles}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='english'\">{{item.notasingles}}</p>\n            <h6 class=\"card-title\" *ngIf=\"idioma=='italiano'\">{{item.tituloitaliano}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='italiano'\">{{item.notasitaliano}}</p>\n            <a target=\"blank\" [href]=\"item.url\" class=\"attach-button\" *ngIf=\"idioma=='español' && item.url\">\n              <i class=\"fal fa-paperclip\"></i> Ver adjunto\n            </a>\n            <a target=\"blank\" [href]=\"item.urlingles\" class=\"attach-button\" *ngIf=\"idioma=='english' && item.urlingles\">\n              <i class=\"fal fa-paperclip\"></i> See attached\n            </a>\n            <a target=\"blank\" [href]=\"item.urlitaliano\" class=\"attach-button\" *ngIf=\"idioma=='italiano' && item.urlitaliano\">\n              <i class=\"fal fa-paperclip\"></i> Vedi allegato\n            </a>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"card contenedor\">\n      <div class=\"card-body\">\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='español'\">En proceso</h6>\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='english'\">In process</h6>\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='italiano'\">In corso</h6>\n\n        <div class=\"card actividad\" *ngFor=\"let item of tareasEnProceso\">\n          <div class=\"card-body\">\n            <h6 class=\"card-title\" *ngIf=\"idioma=='español'\">{{item.titulo}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='español'\">{{item.notas}}</p>\n            <h6 class=\"card-title\" *ngIf=\"idioma=='english'\">{{item.tituloingles}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='english'\">{{item.notasingles}}</p>\n            <h6 class=\"card-title\" *ngIf=\"idioma=='italiano'\">{{item.tituloitaliano}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='italiano'\">{{item.notasitaliano}}</p>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"card contenedor\">\n      <div class=\"card-body\">\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='español'\">En revisión</h6>\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='english'\">In review</h6>\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='italiano'\">In revisione</h6>\n        <div class=\"card actividad\" *ngFor=\"let item of tareasEnRevision\">\n          <div class=\"card-body\">\n            <h6 class=\"card-title\" *ngIf=\"idioma=='español'\">{{item.titulo}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='español'\">{{item.notas}}</p>\n            <h6 class=\"card-title\" *ngIf=\"idioma=='english'\">{{item.tituloingles}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='english'\">{{item.notasingles}}</p>\n            <h6 class=\"card-title\" *ngIf=\"idioma=='italiano'\">{{item.tituloitaliano}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='italiano'\">{{item.notasitaliano}}</p>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"card contenedor\">\n      <div class=\"card-body\">\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='español'\">Terminadas</h6>\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='english'\">Finished</h6>\n        <h6 class=\"card-title line event-title\" *ngIf=\"idioma=='italiano'\">Finito</h6>\n        <div class=\"card actividad\" *ngFor=\"let item of tareasTerminadas\">\n          <div class=\"card-body\">\n            <h6 class=\"card-title\" *ngIf=\"idioma=='español'\">{{item.titulo}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='español'\">{{item.notas}}</p>\n            <h6 class=\"card-title\" *ngIf=\"idioma=='english'\">{{item.tituloingles}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='english'\">{{item.notasingles}}</p>\n            <h6 class=\"card-title\" *ngIf=\"idioma=='italiano'\">{{item.tituloitaliano}}</h6>\n            <p class=\"card-text\" *ngIf=\"idioma=='italiano'\">{{item.notasitaliano}}</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n</section>\n"

/***/ }),

/***/ "./src/app/componentes/agenda/agenda.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/componentes/agenda/agenda.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: .4em;\n    font-size: 1.7em;\n    border-bottom: 4px solid transparent; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  .opciones-actividades li.active {\n    border-bottom: 4px solid #9F2820;\n    transition: 1s; }\n  .actividades {\n  padding: 4em 5em 2em; }\n  .event-card {\n  border: 0px solid rgba(0, 0, 0, 0.1);\n  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.5);\n  margin-bottom: .5em; }\n  .line {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.2);\n  padding-bottom: .5em; }\n  .hour {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .event-title {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .content-event {\n  font-size: 16px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 300; }\n  .container-actividades {\n  display: flex;\n  height: 600px;\n  border: 1px solid rgba(0, 0, 0, 0.3);\n  overflow-y: scroll;\n  margin-left: 1px;\n  border-radius: 5px;\n  box-shadow: 0 4px 8px #dedede;\n  padding: 1em;\n  background-color: #9F2820; }\n  .container-actividades .card {\n    margin: 3px; }\n  .card-inicio {\n  background-color: #95f893 !important; }\n  .card-inicio .card-subtitle,\n  .card-inicio .card-title,\n  .card-inicio .card-text {\n    color: #000 !important; }\n  .card-estado {\n  background-color: #bed5f0 !important; }\n  .card-estado .card-subtitle,\n  .card-estado .card-title,\n  .card-estado .card-text {\n    color: #000 !important; }\n  .card-identidad {\n  background-color: #f77c79 !important; }\n  .card-identidad .card-subtitle,\n  .card-identidad .card-title,\n  .card-identidad .card-text {\n    color: #fff !important; }\n  .card-solidaridad {\n  background-color: #fde68f !important; }\n  .card-comision {\n  background-color: #8fa9df !important; }\n  .card-comision .card-subtitle,\n  .card-comision .card-title,\n  .card-comision .card-text {\n    color: #fff !important; }\n  .card-nuevos {\n  background-color: #c09839 !important; }\n  .card-nuevos .card-subtitle,\n  .card-nuevos .card-title,\n  .card-nuevos .card-text {\n    color: #fff !important; }\n  .card-liderazgo {\n  background-color: #d0cece !important; }\n  .card-liderazgo .card-subtitle,\n  .card-liderazgo .card-title,\n  .card-liderazgo .card-text {\n    color: #000 !important; }\n  .card-propuesta {\n  background-color: #f1b5ed !important; }\n  .card-propuesta .card-subtitle,\n  .card-propuesta .card-title,\n  .card-propuesta .card-text {\n    color: #fff !important;\n    text-shadow: 0 4px 8px rgba(0, 0, 0, 0.5); }\n  .tipo-actividad {\n  position: absolute;\n  top: 1.5em;\n  right: 2em; }\n  .attach-button {\n  cursor: pointer; }\n  .attach-button:hover {\n    font-weight: bold; }\n"

/***/ }),

/***/ "./src/app/componentes/agenda/agenda.component.ts":
/*!********************************************************!*\
  !*** ./src/app/componentes/agenda/agenda.component.ts ***!
  \********************************************************/
/*! exports provided: AgendaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendaComponent", function() { return AgendaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AgendaComponent = /** @class */ (function () {
    function AgendaComponent(db, localstorage) {
        var _this = this;
        this.db = db;
        this.localstorage = localstorage;
        this.opcion = "calendario";
        this.idioma = 'español';
        this.tareasEnRevision = [];
        this.tareasPendientes = [];
        this.tareasEnProceso = [];
        this.tareasTerminadas = [];
        this.actividades = [];
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
        this.db
            .colWithIds$("tareas", function (ref) {
            return ref.where("estado", "==", "pendiente").orderBy("index", "asc");
        })
            .subscribe(function (resp) {
            _this.tareasPendientes = resp;
        });
        this.db
            .colWithIds$("tareas", function (ref) {
            return ref.where("estado", "==", "proceso").orderBy("index", "asc");
        })
            .subscribe(function (resp) {
            _this.tareasEnProceso = resp;
        });
        this.db
            .colWithIds$("tareas", function (ref) {
            return ref.where("estado", "==", "revision").orderBy("index", "asc");
        })
            .subscribe(function (resp) {
            _this.tareasEnRevision = resp;
        });
        this.db
            .colWithIds$("tareas", function (ref) {
            return ref.where("estado", "==", "terminada").orderBy("index", "asc");
        })
            .subscribe(function (resp) {
            _this.tareasTerminadas = resp;
        });
        this.db
            .colWithIds$("actividades", function (ref) {
            return ref.orderBy("dia", "asc").orderBy("horainicio", "asc");
        })
            .subscribe(function (resp) {
            _this.actividades = [];
            resp.forEach(function (element) {
                var dia;
                var indexfind;
                _this.actividades.forEach(function (item, index) {
                    if (item.dia == element.dia) {
                        dia = item;
                        indexfind = index;
                    }
                });
                //console.log(dia)
                if (!dia) {
                    _this.actividades.push({
                        dia: element.dia,
                        fecha: element.fecha,
                        actividades: [
                            element
                        ]
                    });
                }
                else {
                    _this.actividades[indexfind].actividades.push(element);
                }
            });
            //this.actividades = resp;
        });
    }
    AgendaComponent.prototype.ngOnInit = function () { };
    AgendaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "agenda",
            template: __webpack_require__(/*! ./agenda.component.html */ "./src/app/componentes/agenda/agenda.component.html"),
            styles: [__webpack_require__(/*! ./agenda.component.scss */ "./src/app/componentes/agenda/agenda.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__["LocalstorageService"]])
    ], AgendaComponent);
    return AgendaComponent;
}());



/***/ }),

/***/ "./src/app/componentes/banner-carousel/banner-carousel.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/componentes/banner-carousel/banner-carousel.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ngb-carousel *ngIf=\"images\">\n  <ng-template ngbSlide>\n    <img [src]=\"images[0]\" alt=\"Random first slide\">\n    <div class=\"carousel-caption\">\n      <h3>First slide label</h3>\n      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>\n    </div>\n  </ng-template>\n</ngb-carousel>\n"

/***/ }),

/***/ "./src/app/componentes/banner-carousel/banner-carousel.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/componentes/banner-carousel/banner-carousel.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "img {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/componentes/banner-carousel/banner-carousel.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/componentes/banner-carousel/banner-carousel.component.ts ***!
  \**************************************************************************/
/*! exports provided: BannerCarouselComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BannerCarouselComponent", function() { return BannerCarouselComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BannerCarouselComponent = /** @class */ (function () {
    function BannerCarouselComponent(_http, localstorage) {
        var _this = this;
        this._http = _http;
        this.localstorage = localstorage;
        this.images = [
            'assets/background-banner.JPG'
        ];
        this.idioma = 'español';
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
    }
    BannerCarouselComponent.prototype.ngOnInit = function () { };
    BannerCarouselComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'banner-carousel',
            template: __webpack_require__(/*! ./banner-carousel.component.html */ "./src/app/componentes/banner-carousel/banner-carousel.component.html"),
            styles: [__webpack_require__(/*! ./banner-carousel.component.scss */ "./src/app/componentes/banner-carousel/banner-carousel.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__["LocalstorageService"]])
    ], BannerCarouselComponent);
    return BannerCarouselComponent;
}());



/***/ }),

/***/ "./src/app/componentes/barra-navegacion/barra-navegacion.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/componentes/barra-navegacion/barra-navegacion.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light navbar-fixed-top\">\n  <a class=\"navbar-brand\" href=\"#\">\n\n    <img src=\"assets/LogoEsp.svg\" width=\"120\" *ngIf=\"idioma == 'español'\">\n    <img src=\"assets/LogoEng.svg\" width=\"120\" *ngIf=\"idioma == 'english'\">\n    <img src=\"assets/LogoIta.svg\" width=\"120\" *ngIf=\"idioma == 'italiano'\">\n\n  </a>\n\n  <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed = !isCollapsed\" [attr.aria-expanded]=\"!isCollapsed\"\n    aria-controls=\"navbarsExampleDefault\" aria-label=\"Toggle\n    navigation\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n  <!--<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarText\" aria-controls=\"navbarText\"\n    aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>-->\n\n  <div class=\"collapse navbar-collapse\" id=\"navbarText\" [ngbCollapse]=\"isCollapsed\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item active\">\n        <a class=\"nav-link\" href=\"#/inicio\">\n          <span *ngIf=\"idioma=='español'\">INICIO</span>\n          <span *ngIf=\"idioma=='english'\">HOME</span>\n          <span *ngIf=\"idioma=='italiano'\">PRINCIPALE</span>\n          <span class=\"sr-only\">(current)</span>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" [routerLink]=\"['/inicio']\" fragment=\"agenda\">\n          <span *ngIf=\"idioma=='español'\">AGENDA</span>\n          <span *ngIf=\"idioma=='english'\">SCHEDULE</span>\n          <span *ngIf=\"idioma=='italiano'\">PROGRAMMA</span>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" [routerLink]=\"['/inicio']\" fragment=\"articulos\">\n          <span *ngIf=\"idioma=='español'\">BIBLIOTECA</span>\n          <span *ngIf=\"idioma=='english'\">LIBRARY</span>\n          <span *ngIf=\"idioma=='italiano'\">BIBLIOTECA</span>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" [routerLink]=\"['/inicio']\" fragment=\"cronicas\">\n          <span *ngIf=\"idioma=='español'\">CRÓNICAS</span>\n          <span *ngIf=\"idioma=='english'\">CHRONICLES</span>\n          <span *ngIf=\"idioma=='italiano'\">CRONACHE</span>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" [routerLink]=\"['/inicio']\" fragment=\"proyectos\">\n          <span *ngIf=\"idioma=='español'\">PROYECTOS</span>\n          <span *ngIf=\"idioma=='english'\">PROJECTS</span>\n          <span *ngIf=\"idioma=='italiano'\">PROGETTI</span>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" [routerLink]=\"['/foros']\" >\n          <span *ngIf=\"idioma=='español'\">FOROS</span>\n          <span *ngIf=\"idioma=='english'\">FORUMS</span>\n          <span *ngIf=\"idioma=='italiano'\">FORUM</span>\n        </a>\n      </li>\n    </ul>\n    <span class=\"navbar-text\">\n      <ul class=\"navbar-nav mr-auto right-options\">\n        <div ngbDropdown>\n          <button class=\"btn\" id=\"dropDownIdioma\" ngbDropdownToggle>{{idioma|uppercase}}</button>\n          <div ngbDropdownMenu aria-labelledby=\"dropDownIdioma\">\n            <button class=\"dropdown-item\" (click)=\"changeIdioma('español')\">ESPAÑOL</button>\n            <button class=\"dropdown-item\" (click)=\"changeIdioma('english')\">ENGLISH</button>\n            <button class=\"dropdown-item\" (click)=\"changeIdioma('italiano')\">ITALIANO</button>\n          </div>\n        </div>\n        <div ngbDropdown>\n          <button class=\"btn\" id=\"dropDownIdioma\" ngbDropdownToggle>{{user.nombre | slice:0:10 | uppercase}}</button>\n          <div ngbDropdownMenu aria-labelledby=\"dropDownIdioma\">\n            <button class=\"dropdown-item\" (click)=\"close()\" *ngIf=\"idioma=='español'\">Salir</button>\n            <button class=\"dropdown-item\" (click)=\"close()\" *ngIf=\"idioma=='english'\">Sign out</button>\n            <button class=\"dropdown-item\" (click)=\"close()\" *ngIf=\"idioma=='italiano'\">Disconnessione</button>\n\n          </div>\n        </div>\n      </ul>\n    </span>\n  </div>\n</nav>\n\n\n"

/***/ }),

/***/ "./src/app/componentes/barra-navegacion/barra-navegacion.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/componentes/barra-navegacion/barra-navegacion.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar {\n  padding-left: 2em;\n  padding-right: 2em;\n  background-color: #fff !important;\n  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3);\n  position: fixed;\n  left: 0;\n  right: 0;\n  top: 0;\n  z-index: 400; }\n  .navbar .navbar-brand {\n    margin-right: 3em; }\n  .navbar-nav .nav-item .nav-link::after {\n  content: '|';\n  padding-left: 1em; }\n  .navbar-nav .nav-item:last-child .nav-link::after {\n  content: '';\n  padding-left: 1em; }\n  .right-options {\n  display: inline-flex;\n  justify-content: center;\n  align-items: center; }\n  #dropDownIdioma {\n  background-color: transparent; }\n  .btn-ingresar::before {\n  content: '|';\n  padding-right: 1em; }\n  @media screen and (max-width: 992px) {\n  .navbar-brand img {\n    width: 140px; }\n  .nav-link {\n    padding-top: 15px; }\n  .navbar-text {\n    width: 100%; }\n  .nav-item {\n    width: 100%;\n    text-align: center;\n    border-bottom: 1px solid rgba(0, 0, 0, 0.3);\n    padding-bottom: 10px; }\n    .nav-item .btn {\n      margin-top: 1em;\n      width: 100%; }\n  .pipe-menu {\n    display: none; }\n  .active {\n    border-bottom: none; }\n  .right-options {\n    width: 100%; }\n  .nav-item .nav-link::after {\n    content: '' !important;\n    padding-left: 0 !important; }\n  .nav-item:last-child .nav-link::after {\n    content: '';\n    padding-left: 0 !important; }\n  navbar-light .navbar-nav .nav-link {\n    color: rgba(0, 0, 0, 0.5);\n    font-size: 10px;\n    margin: 0em; }\n  #dropDownIdioma {\n    display: none !important; } }\n"

/***/ }),

/***/ "./src/app/componentes/barra-navegacion/barra-navegacion.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/componentes/barra-navegacion/barra-navegacion.component.ts ***!
  \****************************************************************************/
/*! exports provided: BarraNavegacionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarraNavegacionComponent", function() { return BarraNavegacionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _core_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/auth.service */ "./src/app/core/auth.service.ts");
/* harmony import */ var _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/@angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BarraNavegacionComponent = /** @class */ (function () {
    function BarraNavegacionComponent(localstorage, modalService, auth, _router) {
        var _this = this;
        this.localstorage = localstorage;
        this.modalService = modalService;
        this.auth = auth;
        this._router = _router;
        this.idioma = "español";
        this.user = {};
        this.isCollapsed = true;
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
        console.log(this.idioma);
        if (!this.idioma) {
            this.idioma = "español";
        }
        this.auth.getCurrentUser().subscribe(function (user) {
            if (user) {
                if (user.length > 0) {
                    _this.user = user[0];
                }
                else {
                    _this._router.navigate(['/']);
                }
            }
            else {
                _this._router.navigate(['/']);
            }
        });
    }
    BarraNavegacionComponent.prototype.open = function (content) {
        this.modalService.open(content, {
            windowClass: "dark-modal",
            backdropClass: "dark-back"
        });
    };
    BarraNavegacionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.idioma = this.localstorage.getActualIdioma();
        this._router.events.subscribe(function (event) {
            if (event instanceof _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
                var tree = _this._router.parseUrl(_this._router.url);
                if (tree.fragment) {
                    var element = document.querySelector("#" + tree.fragment);
                    if (element) {
                        element.scrollIntoView();
                    }
                }
            }
        });
    };
    BarraNavegacionComponent.prototype.changeIdioma = function (idioma) {
        this.idioma = idioma;
        this.localstorage.setIdioma(idioma);
    };
    BarraNavegacionComponent.prototype.close = function () {
        this.auth.signOut();
        this._router.navigate(['/']);
    };
    BarraNavegacionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "barra-navegacion",
            template: __webpack_require__(/*! ./barra-navegacion.component.html */ "./src/app/componentes/barra-navegacion/barra-navegacion.component.html"),
            styles: [__webpack_require__(/*! ./barra-navegacion.component.scss */ "./src/app/componentes/barra-navegacion/barra-navegacion.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__["LocalstorageService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"],
            _core_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], BarraNavegacionComponent);
    return BarraNavegacionComponent;
}());



/***/ }),

/***/ "./src/app/componentes/biblioteca/biblioteca.component.html":
/*!******************************************************************!*\
  !*** ./src/app/componentes/biblioteca/biblioteca.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\">\n\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a class=\"tappable\" *ngIf=\"idioma=='español'\">Biblioteca </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='english'\">Library </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='italiano'\">Biblioteca </a>\n        </li>\n        <li>\n      </ul>\n      <!--<p class=\"introduccion\" *ngIf=\"idioma=='español'\">\n        ESPAÑOL Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='english'\">\n        ENGLISH Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='italiano'\">\n        ITALIANO Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>-->\n\n      <div class=\"row\">\n        <div class=\"row\" [ngClass]=\"{'col-sm-12': !carpetaLiturgiaSeleccionada, 'col-sm-6': carpetaLiturgiaSeleccionada}\">\n          <div class=\"card-liturgia\" [ngClass]=\"{'col-sm-2': !carpetaLiturgiaSeleccionada, 'col-sm-4': carpetaLiturgiaSeleccionada}\"\n            *ngFor=\"let item of carpetas; let i = index;\" (click)=\"seleccionarCarpeta(item)\">\n            <div class=\"card liturgias\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\" *ngIf=\"item.foto\">\n              <div class=\"card-body\">\n                <h6 class=\"card-title\" *ngIf=\"idioma=='español'\">{{item.nombre}}</h6>\n                <h6 class=\"card-title\" *ngIf=\"idioma=='english'\">{{item.nombreingles}}</h6>\n                <h6 class=\"card-title\" *ngIf=\"idioma=='italiano'\">{{item.nombreitaliano}}</h6>\n              </div>\n            </div>\n            <div class=\"card liturgias\" style=\"background-image: url('../../../assets/mock-background-liturgia.png')\" *ngIf=\"!item.foto\">\n              <div class=\"card-body\">\n                <h6 class=\"card-title\" *ngIf=\"idioma=='español'\">{{item.nombre}}</h6>\n                <h6 class=\"card-title\" *ngIf=\"idioma=='english'\">{{item.nombreingles}}</h6>\n                <h6 class=\"card-title\" *ngIf=\"idioma=='italiano'\">{{item.nombreitaliano}}</h6>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-sm-6 contenedor-liturgias\" *ngIf=\"carpetaLiturgiaSeleccionada\">\n          <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"cerrarLiturgia()\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n\n          <h5 style=\"color: #b1504d;margin-bottom:1em;\" *ngIf=\"idioma=='español'\">\n            <fa [name]=\"'chevron-left'\" *ngIf=\"carpetaLiturgiaSeleccionada.padre != carpetaliturgiaid\" class=\"tappable\" (click)=\"seleccionarCarpetaVolver(carpetaLiturgiaSeleccionada)\"></fa>\n            {{carpetaLiturgiaSeleccionada.nombre}}\n          </h5>\n\n          <h5 style=\"color: #b1504d;margin-bottom:1em;\" *ngIf=\"idioma=='english'\">\n            <fa [name]=\"'chevron-left'\" *ngIf=\"carpetaLiturgiaSeleccionada.padre != carpetaliturgiaid\" class=\"tappable\" (click)=\"seleccionarCarpetaVolver(carpetaLiturgiaSeleccionada)\"></fa>\n            {{carpetaLiturgiaSeleccionada.nombreingles}}\n          </h5>\n\n          <h5 style=\"color: #b1504d;margin-bottom:1em;\" *ngIf=\"idioma=='italiano'\">\n            <fa [name]=\"'chevron-left'\" *ngIf=\"carpetaLiturgiaSeleccionada.padre != carpetaliturgiaid\" class=\"tappable\" (click)=\"seleccionarCarpetaVolver(carpetaLiturgiaSeleccionada)\"></fa>\n            {{carpetaLiturgiaSeleccionada.nombreitaliano}}\n          </h5>\n\n          <div class=\"alert alert-danger\" role=\"alert\"\n               *ngIf=\"idioma=='español' && carpetasLiturgiaSeleccionanda.length <= 0 && documentos.length <= 0\">\n            <fa [name]=\"'exclamation-circle'\"></fa> Esta biblioteca no contiene carpetas ni documentos\n          </div>\n          <div class=\"alert alert-danger\" role=\"alert\"\n               *ngIf=\"idioma=='english' && carpetasLiturgiaSeleccionanda.length <= 0 && documentos.length <= 0\">\n            <fa [name]=\"'exclamation-circle'\"></fa> This library does not contain folders or documents\n          </div>\n          <div class=\"alert alert-danger\" role=\"alert\"\n               *ngIf=\"idioma=='italiano' && carpetasLiturgiaSeleccionanda.length <= 0 && documentos.length <= 0\">\n            <fa [name]=\"'exclamation-circle'\"></fa> Questa libreria non contiene cartelle o documenti\n          </div>\n\n          <h6 class=\"titulo\" *ngIf=\"carpetasLiturgiaSeleccionanda.length > 0\">\n            <span *ngIf=\"idioma=='español'\">\n              Carpetas\n            </span>\n            <span *ngIf=\"idioma=='english'\">\n              Folders\n            </span>\n            <span *ngIf=\"idioma=='italiano'\">\n              Cartelle\n            </span>\n          </h6>\n          <table class=\"table table-hover \">\n            <tbody>\n              <tr *ngFor=\"let item of carpetasLiturgiaSeleccionanda\" class=\"tappable\">\n                <td (click)=\"seleccionarCarpetaHijo(item)\">\n                  <fa style=\"color: #b1504d;margin-right:.3em;\" [name]=\"'folder'\"></fa>\n                  <span *ngIf=\"idioma=='español'\">\n                    {{item.nombre}}\n                  </span>\n                  <span *ngIf=\"idioma=='english'\">\n                    {{item.nombreingles}}\n                  </span>\n                  <span *ngIf=\"idioma=='italiano'\">\n                    {{item.nombreitaliano}}\n                  </span>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n\n          <h6 class=\"titulo\" *ngIf=\"documentos.length > 0\">\n            <span *ngIf=\"idioma=='español'\">\n              Documentos\n            </span>\n            <span *ngIf=\"idioma=='english'\">\n              Documents\n            </span>\n            <span *ngIf=\"idioma=='italiano'\">\n              Documentazione\n            </span>\n          </h6>\n          <table class=\"table table-hover \">\n            <tbody>\n              <tr *ngFor=\"let item of documentos\">\n                <td class=\"tappable\" (click)=\"selecturl(item,content)\" *ngIf=\"idioma == 'español' && item.idioma == 'español' || idioma == 'english' && item.idioma == 'ingles' || idioma == 'italiano' && item.idioma == 'italiano'\">\n                  <fa style=\"color: #b1504d;margin-right:.3em;\" [name]=\"'file'\"></fa>\n                  <span>\n                    {{item.nombre}}\n                  </span>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n\n<ng-template #content let-c=\"close\" let-d=\"dismiss\">\n  <div class=\"modal-header\" style=\"border:none;\">\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <iframe [src]='urlselect' style=\"height:80vh; width: 100%;\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen>\n    </iframe>\n  </div>\n</ng-template>\n\n<ngx-spinner *ngIf=\"idioma=='español'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"Un momento por favor\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='english'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"One moment, please\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='italiano'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"Un momento, per favore\"></ngx-spinner>\n\n\n"

/***/ }),

/***/ "./src/app/componentes/biblioteca/biblioteca.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/componentes/biblioteca/biblioteca.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 1em 2em 3em;\n  background-color: #ECEBEB; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer;\n  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.4); }\n  .contenedor-liturgias {\n  background-color: #fff;\n  margin-left: 1em;\n  padding: 1em;\n  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.4); }\n  .contenedor-liturgias .titulo {\n    color: rgba(0, 0, 0, 0.4); }\n  .tappable {\n  cursor: pointer; }\n  .col-sm-2.card-liturgia,\n.col-sm-3.card-liturgia,\n.col-sm-4.card-liturgia,\n.col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-2.card-liturgia .card-title,\n  .col-sm-3.card-liturgia .card-title,\n  .col-sm-4.card-liturgia .card-title,\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .modal-dialog {\n  width: 100%;\n  height: 95%;\n  margin: 0;\n  padding: 0; }\n  .modal-content {\n  height: auto;\n  min-height: 95%;\n  border-radius: 0; }\n"

/***/ }),

/***/ "./src/app/componentes/biblioteca/biblioteca.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/componentes/biblioteca/biblioteca.component.ts ***!
  \****************************************************************/
/*! exports provided: BibliotecaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BibliotecaComponent", function() { return BibliotecaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _node_modules_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/@ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/ngx-spinner.umd.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ngx_spinner__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var BibliotecaComponent = /** @class */ (function () {
    function BibliotecaComponent(localstorage, db, sanitizer, modalService, spinner) {
        var _this = this;
        this.localstorage = localstorage;
        this.db = db;
        this.sanitizer = sanitizer;
        this.modalService = modalService;
        this.spinner = spinner;
        this.idioma = "español";
        this.urlselect = "";
        this.type = "application/pdf";
        this.tituloDocumento = "";
        this.documentos = [];
        this.carpetaliturgiaid = "0";
        this.carpetas = [];
        this.carpetasLiturgiaSeleccionanda = [];
        this.carpetaAnterior = {};
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
        this.db
            .docWithRefs$("secciones-documentos/Tndljx20gLXnCX0htMZ0")
            .subscribe(function (resp) {
            if (resp.idcarpeta) {
                _this.carpetaliturgiaid = resp.idcarpeta;
                _this.db
                    .colWithIds$("rutas", function (ref) {
                    return ref.where("padre", "==", resp.idcarpeta);
                })
                    .subscribe(function (resp) {
                    _this.carpetas = resp;
                });
            }
        });
    }
    BibliotecaComponent.prototype.ngOnInit = function () { };
    BibliotecaComponent.prototype.seleccionarCarpeta = function (item) {
        var _this = this;
        this.carpetaLiturgiaSeleccionada = item;
        this.db
            .colWithIds$("documentos", function (ref) { return ref.where("ruta", "==", item.id); })
            .subscribe(function (documentos) {
            _this.documentos = documentos;
        });
        this.db
            .docWithRefs$("secciones-documentos/Tndljx20gLXnCX0htMZ0")
            .subscribe(function (resp) {
            if (resp.idcarpeta) {
                _this.db
                    .colWithIds$("rutas", function (ref) { return ref.where("padre", "==", item.id); })
                    .subscribe(function (resp) {
                    _this.carpetasLiturgiaSeleccionanda = resp;
                });
            }
        });
    };
    BibliotecaComponent.prototype.seleccionarCarpetaHijo = function (item) {
        var _this = this;
        this.carpetaLiturgiaSeleccionada = item;
        this.db
            .colWithIds$("documentos", function (ref) { return ref.where("ruta", "==", item.id); })
            .subscribe(function (documentos) {
            _this.documentos = documentos;
        });
        this.db
            .docWithRefs$("secciones-documentos/Tndljx20gLXnCX0htMZ0")
            .subscribe(function (resp) {
            if (resp.idcarpeta) {
                _this.db
                    .colWithIds$("rutas", function (ref) { return ref.where("padre", "==", item.id); })
                    .subscribe(function (resp) {
                    _this.carpetasLiturgiaSeleccionanda = resp;
                });
            }
        });
    };
    BibliotecaComponent.prototype.seleccionarCarpetaVolver = function (item) {
        var _this = this;
        console.log(item);
        this.db.docWithRefs$("rutas/" + item.padre).subscribe(function (carpetaant) {
            carpetaant.id = item.padre;
            _this.carpetaAnterior = carpetaant;
            _this.seleccionarCarpetaHijo(carpetaant);
        });
    };
    BibliotecaComponent.prototype.cerrarLiturgia = function () {
        this.carpetaLiturgiaSeleccionada = null;
    };
    BibliotecaComponent.prototype.selecturl = function (item, content) {
        var _this = this;
        this.urlselect = this.sanitizer.bypassSecurityTrustResourceUrl(item.url);
        this.type = item.type;
        this.tituloDocumento = item.nombre;
        this.spinner.show();
        setTimeout(function () {
            _this.spinner.hide();
        }, 3000);
        this.modalService
            .open(content, { size: "lg", centered: false })
            .result.then(function (result) { }, function (reason) { });
    };
    BibliotecaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'biblioteca',
            template: __webpack_require__(/*! ./biblioteca.component.html */ "./src/app/componentes/biblioteca/biblioteca.component.html"),
            styles: [__webpack_require__(/*! ./biblioteca.component.scss */ "./src/app/componentes/biblioteca/biblioteca.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__["LocalstorageService"],
            _core_resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"],
            _node_modules_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"]])
    ], BibliotecaComponent);
    return BibliotecaComponent;
}());



/***/ }),

/***/ "./src/app/componentes/cronicas/cronicas.component.html":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cronicas/cronicas.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-5\">\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a class=\"tappable\" *ngIf=\"idioma=='español'\">Cronicas </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='english'\">Chronicles </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='italiano'\">Cronache </a>\n        </li>\n        <li>\n      </ul>\n      <!--<p class=\"introduccion\" *ngIf=\"idioma=='español'\">\n        ESPAÑOL Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='english'\">\n        ENGLISH Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='italiano'\">\n        ITALIANO Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>-->\n\n    </div>\n    <div class=\"col-md-12\">\n      <div class=\"row\">\n        <div class=\"col-md-4 col-sm-12\" *ngFor=\"let item of cronicas | slice:0:3\">\n          <div class=\"card articulo\" style=\"width: 100%;\">\n            <a href=\"#/cronica-detalle/{{item.id}}\" class=\"image-card\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\"></a>\n            <div class=\"card-body\">\n              <h5 class=\"card-title titulo\">{{item.titulo}}</h5>\n              <h6 class=\"card-subtitle mb-2 text-muted fecha\">{{item.createdAt.toDate() | date: 'mediumDate'}}</h6>\n              <p class=\"card-text\">{{item.resumen | slice:0:100}} ...</p>\n              <a class=\"link-ver\" *ngIf=\"idioma=='español'\" href=\"#/cronica-detalle/{{item.id}}\">Leer más</a>\n              <a class=\"link-ver\" *ngIf=\"idioma=='english'\" href=\"#/cronica-detalle/{{item.id}}\">Read more</a>\n              <a class=\"link-ver\" *ngIf=\"idioma=='italiano'\" href=\"#/cronica-detalle/{{item.id}}\">Leggi di più</a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <br>\n\n    <div class=\"col-md-12\" style=\"margin-top:2em;\">\n      <a class=\"btn btn-danger btn-lg btn-ver\" href=\"#/cronicas\">\n        <span *ngIf=\"idioma=='español'\">Ver más</span>\n        <span *ngIf=\"idioma=='english'\">See more</span>\n        <span *ngIf=\"idioma=='italiano'\">Vedi di più</span>\n      </a>\n    </div>\n  </div>\n\n</section>\n"

/***/ }),

/***/ "./src/app/componentes/cronicas/cronicas.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cronicas/cronicas.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  .image-card {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position-y: top;\n  background-position-x: center;\n  height: 250px;\n  width: 100%;\n  cursor: pointer; }\n  .image-card:hover {\n  opacity: .8; }\n  .card-body {\n  min-height: 210px; }\n  .card {\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.3); }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em;\n  background-color: #9F2820 important; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  @media screen and (max-width: 992px) {\n  .col-md-4 {\n    width: 50% !important;\n    max-width: 50% !important;\n    flex: 0 0 50%; } }\n"

/***/ }),

/***/ "./src/app/componentes/cronicas/cronicas.component.ts":
/*!************************************************************!*\
  !*** ./src/app/componentes/cronicas/cronicas.component.ts ***!
  \************************************************************/
/*! exports provided: CronicasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CronicasComponent", function() { return CronicasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CronicasComponent = /** @class */ (function () {
    function CronicasComponent(localstorage, db) {
        var _this = this;
        this.localstorage = localstorage;
        this.db = db;
        this.idioma = "español";
        this.idioma2 = "español";
        this.cronicas = [];
        this.idioma = this.localstorage.getActualIdioma();
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
            if (event.newValue == 'english') {
                _this.idioma2 = 'ingles';
            }
            else {
                _this.idioma2 = event.newValue;
            }
            _this.db
                .colWithIds$("cronicas", function (ref) {
                return ref.where("idioma", "==", _this.idioma2);
            })
                .subscribe(function (cronicas) {
                _this.cronicas = cronicas;
            });
        });
        if (this.idioma == "english") {
            this.idioma2 = "ingles";
        }
        else {
            this.idioma2 = this.idioma;
        }
        this.db
            .colWithIds$("cronicas", function (ref) {
            return ref.where("idioma", "==", _this.idioma2);
        })
            .subscribe(function (cronicas) {
            _this.cronicas = cronicas;
        });
    }
    CronicasComponent.prototype.ngOnInit = function () { };
    CronicasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "cronicas",
            template: __webpack_require__(/*! ./cronicas.component.html */ "./src/app/componentes/cronicas/cronicas.component.html"),
            styles: [__webpack_require__(/*! ./cronicas.component.scss */ "./src/app/componentes/cronicas/cronicas.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__["LocalstorageService"],
            _core_resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"]])
    ], CronicasComponent);
    return CronicasComponent;
}());



/***/ }),

/***/ "./src/app/componentes/encuestra/encuestra.component.html":
/*!****************************************************************!*\
  !*** ./src/app/componentes/encuestra/encuestra.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card text-center\" style=\"margin-bottom:1em;\">\n  <div class=\"card-header\" style=\"background: #fff; color: #000; font-size: 20px; font-weight: 700;\">\n    <span *ngIf=\"idioma=='español'\">{{info.titulo}} </span>\n    <span *ngIf=\"idioma=='english'\">{{info.tituloingles}} </span>\n    <span *ngIf=\"idioma=='italiano'\">{{info.tituloitaliano}} </span>\n  </div>\n  <div class=\"card-body\" *ngIf=\"!contestada\">\n    <h5 class=\"card-title\" style=\"margin-bottom: 2em;\">\n      <span *ngIf=\"idioma=='español'\">{{info.pregunta}} </span>\n      <span *ngIf=\"idioma=='english'\">{{info.preguntaingles}} </span>\n      <span *ngIf=\"idioma=='italiano'\">{{info.preguntaitaliano}} </span>\n    </h5>\n    <button class=\"btn btn-success btn-lg\" style=\"width: 26%; margin-right: 2px;\" (click)=\"responder('si')\">\n      <span *ngIf=\"idioma=='español'\">Si </span>\n      <span *ngIf=\"idioma=='english'\">Yes </span>\n      <span *ngIf=\"idioma=='italiano'\">Se </span>\n    </button>\n    <button class=\"btn btn-danger btn-lg\" style=\"width: 26%; margin-left: 2px;\" (click)=\"responder('no')\">\n      <span *ngIf=\"idioma=='español'\">No </span>\n      <span *ngIf=\"idioma=='english'\">No </span>\n      <span *ngIf=\"idioma=='italiano'\">Non </span>\n    </button>\n\n    <button class=\"btn btn-warning btn-lg\" style=\"width: 26%; margin-left: 2px;\" (click)=\"responder('blanco')\">\n      <span *ngIf=\"idioma=='español'\">Blanco </span>\n      <span *ngIf=\"idioma=='english'\">Blank vote </span>\n      <span *ngIf=\"idioma=='italiano'\">Vota vuoto</span>\n    </button>\n  </div>\n  <div class=\"card-body\" *ngIf=\"contestada\">\n\n    <h5 class=\"card-title\" style=\"margin-bottom: 2em;\">\n      <span *ngIf=\"idioma=='español'\">{{info.pregunta}} </span>\n      <span *ngIf=\"idioma=='english'\">{{info.preguntaingles}} </span>\n      <span *ngIf=\"idioma=='italiano'\">{{info.preguntaitaliano}} </span>\n    </h5>\n\n    <div class=\"row\" style=\"margin-bottom: 1em;\">\n      <div class=\"col-sm-2\">\n        <div class=\"progress\" style=\"height: 50px;\">\n          <div class=\"progress-bar bg-success\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%\">\n            <span *ngIf=\"idioma=='español'\">Si </span>\n            <span *ngIf=\"idioma=='english'\">Yes </span>\n            <span *ngIf=\"idioma=='italiano'\">Se </span>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-sm-10\" style=\"display: inline-flex; align-items: center;\">\n        <strong style=\"font-size: 18px\">\n           {{totalsi | number:'0.0-0'}}\n            <span *ngIf=\"idioma=='español'\"> Votos </span>\n            <span *ngIf=\"idioma=='english'\"> Votes </span>\n            <span *ngIf=\"idioma=='italiano'\"> Voti </span>\n           | {{porcentajesi | number:'0.0-2'}} %\n        </strong>\n      </div>\n    </div>\n    <div class=\"row\" style=\"margin-bottom: 1em;\">\n      <div class=\"col-sm-2\">\n        <div class=\"progress\" style=\"height: 50px;\">\n          <div class=\"progress-bar bg-danger\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%\">\n            <span *ngIf=\"idioma=='español'\">No </span>\n            <span *ngIf=\"idioma=='english'\">No </span>\n            <span *ngIf=\"idioma=='italiano'\">Non </span>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-sm-10\" style=\"display: inline-flex; align-items: center;\">\n        <strong style=\"font-size: 18px\">\n          {{totalno}}\n          <span *ngIf=\"idioma=='español'\"> Votos </span>\n          <span *ngIf=\"idioma=='english'\"> Votes </span>\n          <span *ngIf=\"idioma=='italiano'\"> Voti </span>\n\n           | {{porcentajeno | number:'0.0-2'}} %\n\n        </strong>\n      </div>\n    </div>\n    <div class=\"row\" style=\"margin-bottom: 1em;\">\n      <div class=\"col-sm-2\">\n        <div class=\"progress\" style=\"height: 50px;\">\n          <div class=\"progress-bar bg-warning\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\"\n            style=\"width: 100%\">\n            <span *ngIf=\"idioma=='español'\">Blanco </span>\n            <span *ngIf=\"idioma=='english'\">Blank </span>\n            <span *ngIf=\"idioma=='italiano'\">Bianco </span>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-sm-10\" style=\"display: inline-flex; align-items: center;\">\n        <strong style=\"font-size: 18px\">\n          {{totalblanco}}\n          <span *ngIf=\"idioma=='español'\"> Votos </span>\n          <span *ngIf=\"idioma=='english'\"> Votes </span>\n          <span *ngIf=\"idioma=='italiano'\"> Voti </span>\n\n          | {{porcentajeno | number:'0.0-2'}} %\n\n        </strong>\n      </div>\n    </div>\n    <div class=\"progress\" style=\"height: 50px;\">\n      <div class=\"progress-bar progress-bar-striped progress-bar-animated bg-success\" role=\"progressbar\" [style.width.%]=\"porcentajesi\">{{porcentajesi | number:'0.0-2'}}%</div>\n      <div class=\"progress-bar progress-bar-striped progress-bar-animated bg-danger\" role=\"progressbar\"  [style.width.%]=\"porcentajeno\">{{porcentajeno | number:'0.0-2'}}%</div>\n      <div class=\"progress-bar progress-bar-striped progress-bar-animated bg-warning\" role=\"progressbar\" [style.width.%]=\"porcentajeblanco\">{{porcentajeblanco| number:'0.0-2'}}%</div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/componentes/encuestra/encuestra.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/componentes/encuestra/encuestra.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card {\n  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3); }\n"

/***/ }),

/***/ "./src/app/componentes/encuestra/encuestra.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/componentes/encuestra/encuestra.component.ts ***!
  \**************************************************************/
/*! exports provided: EncuestraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EncuestraComponent", function() { return EncuestraComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/auth.service */ "./src/app/core/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EncuestraComponent = /** @class */ (function () {
    function EncuestraComponent(db, auth) {
        this.db = db;
        this.auth = auth;
        this.contestada = false;
        this.votacion = {};
        this.user = {};
        this.totalsi = 0;
        this.totalno = 0;
        this.totalblanco = 0;
        this.totalvotaciones = 0;
        this.porcentajesi = 0;
        this.porcentajeno = 0;
        this.porcentajeblanco = 0;
    }
    EncuestraComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.info.estado == "Finalizada") {
            this.contestada = true;
        }
        this.auth.getCurrentUser().subscribe(function (user) {
            if (user) {
                if (user.length > 0) {
                    _this.user = user[0];
                    _this.db
                        .colWithIds$("respuestas-votaciones", function (ref) {
                        return ref.where("encuestaid", "==", _this.info.id);
                    })
                        .subscribe(function (resp) {
                        var contesto = resp.find(function (x) { return x.uid == _this.user.uid; });
                        if (contesto) {
                            _this.contestada = true;
                        }
                        _this.totalvotaciones = resp.length;
                        _this.totalsi = 0;
                        _this.totalno = 0;
                        resp.forEach(function (element) {
                            if (element.respuesta == "si") {
                                _this.totalsi += 1;
                            }
                            else if (element.respuesta == "no") {
                                _this.totalno += 1;
                            }
                            else if (element.respuesta == "blanco") {
                                _this.totalblanco += 1;
                            }
                        });
                        if (_this.totalvotaciones > 0) {
                            _this.porcentajesi = (_this.totalsi / _this.totalvotaciones) * 100;
                            _this.porcentajeno = (_this.totalno / _this.totalvotaciones) * 100;
                            _this.porcentajeblanco = (_this.totalblanco / _this.totalvotaciones) * 100;
                        }
                    });
                }
            }
        });
    };
    EncuestraComponent.prototype.responder = function (respuesta) {
        this.db
            .add("respuestas-votaciones", {
            encuestaid: this.info.id,
            uid: this.user.uid,
            respuesta: respuesta
        })
            .then(function (respa) {
            console.log("Listo pana!");
        });
        this.contestada = true;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EncuestraComponent.prototype, "info", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EncuestraComponent.prototype, "idioma", void 0);
    EncuestraComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "votacion",
            template: __webpack_require__(/*! ./encuestra.component.html */ "./src/app/componentes/encuestra/encuestra.component.html"),
            styles: [__webpack_require__(/*! ./encuestra.component.scss */ "./src/app/componentes/encuestra/encuestra.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"], _core_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], EncuestraComponent);
    return EncuestraComponent;
}());



/***/ }),

/***/ "./src/app/componentes/hero-banner/hero-banner.component.html":
/*!********************************************************************!*\
  !*** ./src/app/componentes/hero-banner/hero-banner.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"contenedor-image\">\n  <div class=\"overlay\">\n      <h1 *ngIf=\"idioma=='español'\">Congregación de la Pasión de Jesucristo</h1>\n      <h3 *ngIf=\"idioma=='español'\">Predicamos a Cristo crucificado</h3>\n      <h1 *ngIf=\"idioma=='english'\">Congregation of the Passion of Jesus Christ</h1>\n      <h3 *ngIf=\"idioma=='english'\">We preach Christ crucified</h3>\n      <h1 *ngIf=\"idioma=='italiano'\">Congregazione della Passione di Gesù Cristo</h1>\n      <h3 *ngIf=\"idioma=='italiano'\">Predichiamo Cristo crocifisso</h3>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/componentes/hero-banner/hero-banner.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/componentes/hero-banner/hero-banner.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-image: url('background-banner.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top center;\n  background-attachment: fixed;\n  height: 570px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px; }\n  .contenedor-image .overlay {\n    padding-top: 350px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n"

/***/ }),

/***/ "./src/app/componentes/hero-banner/hero-banner.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/componentes/hero-banner/hero-banner.component.ts ***!
  \******************************************************************/
/*! exports provided: HeroBannerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeroBannerComponent", function() { return HeroBannerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeroBannerComponent = /** @class */ (function () {
    function HeroBannerComponent(localstorage) {
        var _this = this;
        this.localstorage = localstorage;
        this.idioma = "español";
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
    }
    HeroBannerComponent.prototype.ngOnInit = function () { };
    HeroBannerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "hero-banner",
            template: __webpack_require__(/*! ./hero-banner.component.html */ "./src/app/componentes/hero-banner/hero-banner.component.html"),
            styles: [__webpack_require__(/*! ./hero-banner.component.scss */ "./src/app/componentes/hero-banner/hero-banner.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__["LocalstorageService"]])
    ], HeroBannerComponent);
    return HeroBannerComponent;
}());



/***/ }),

/***/ "./src/app/componentes/liturgias/liturgias.component.html":
/*!****************************************************************!*\
  !*** ./src/app/componentes/liturgias/liturgias.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\">\n\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a class=\"tappable\" *ngIf=\"idioma=='español'\">Liturgias </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='english'\">Liturgies </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='italiano'\">Liturgie </a>\n        </li>\n        <li>\n      </ul>\n      <!--<p class=\"introduccion\" *ngIf=\"idioma=='español'\">\n        ESPAÑOL Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor. Phasellus\n        in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere sem ultrices.\n        Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis congue nisl,\n        nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='english'\">\n        ENGLISH Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor. Phasellus\n        in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere sem ultrices.\n        Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis congue nisl,\n        nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='italiano'\">\n        ITALIANO Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor. Phasellus\n        in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere sem ultrices.\n        Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis congue nisl,\n        nec vulputate nunc.\n      </p>-->\n\n      <div class=\"row\">\n        <div class=\"row\" [ngClass]=\"{'col-sm-12': !carpetaLiturgiaSeleccionada, 'col-sm-6': carpetaLiturgiaSeleccionada}\">\n          <div class=\"card-liturgia\" [ngClass]=\"{'col-sm-2': !carpetaLiturgiaSeleccionada, 'col-sm-4': carpetaLiturgiaSeleccionada}\"\n              *ngFor=\"let item of carpetas; let i = index;\" (click)=\"seleccionarCarpeta(item)\">\n            <div class=\"card liturgias\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\" *ngIf=\"item.foto\">\n              <div class=\"card-body\">\n                <h6 class=\"card-title\" *ngIf=\"idioma=='español'\">{{item.nombre}}</h6>\n                <h6 class=\"card-title\" *ngIf=\"idioma=='english'\">{{item.nombreingles}}</h6>\n                <h6 class=\"card-title\" *ngIf=\"idioma=='italiano'\">{{item.nombreitaliano}}</h6>\n              </div>\n            </div>\n            <div class=\"card liturgias\" style=\"background-image: url('../../../assets/mock-background-liturgia.png')\" *ngIf=\"!item.foto\">\n              <div class=\"card-body\">\n                <h6 class=\"card-title\" *ngIf=\"idioma=='español'\">{{item.nombre}}</h6>\n                <h6 class=\"card-title\" *ngIf=\"idioma=='english'\">{{item.nombreingles}}</h6>\n                <h6 class=\"card-title\" *ngIf=\"idioma=='italiano'\">{{item.nombreitaliano}}</h6>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-sm-6 contenedor-liturgias\" *ngIf=\"carpetaLiturgiaSeleccionada\">\n          <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"cerrarLiturgia()\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n\n          <h5 style=\"color: #b1504d;margin-bottom:1em;\" *ngIf=\"idioma=='español'\">\n            <fa [name]=\"'chevron-left'\" *ngIf=\"carpetaLiturgiaSeleccionada.padre != carpetaliturgiaid\" class=\"tappable\" (click)=\"seleccionarCarpetaVolver(carpetaLiturgiaSeleccionada)\"></fa>\n            {{carpetaLiturgiaSeleccionada.nombre}}\n          </h5>\n\n          <h5 style=\"color: #b1504d;margin-bottom:1em;\" *ngIf=\"idioma=='english'\">\n            <fa [name]=\"'chevron-left'\" *ngIf=\"carpetaLiturgiaSeleccionada.padre != carpetaliturgiaid\" class=\"tappable\" (click)=\"seleccionarCarpetaVolver(carpetaLiturgiaSeleccionada)\"></fa>\n            {{carpetaLiturgiaSeleccionada.nombreingles}}\n          </h5>\n\n          <h5 style=\"color: #b1504d;margin-bottom:1em;\" *ngIf=\"idioma=='italiano'\">\n            <fa [name]=\"'chevron-left'\" *ngIf=\"carpetaLiturgiaSeleccionada.padre != carpetaliturgiaid\" class=\"tappable\" (click)=\"seleccionarCarpetaVolver(carpetaLiturgiaSeleccionada)\"></fa>\n            {{carpetaLiturgiaSeleccionada.nombreitaliano}}\n          </h5>\n\n          <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"idioma=='español' && carpetasLiturgiaSeleccionanda.length <= 0 && documentos.length <= 0\">\n            <fa [name]=\"'exclamation-circle'\"></fa> Esta liturgia no contiene carpetas ni documentos\n          </div>\n          <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"idioma=='english' && carpetasLiturgiaSeleccionanda.length <= 0 && documentos.length <= 0\">\n            <fa [name]=\"'exclamation-circle'\"></fa> This liturgy does not contain folders or documents\n          </div>\n          <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"idioma=='italiano' && carpetasLiturgiaSeleccionanda.length <= 0 && documentos.length <= 0\">\n            <fa [name]=\"'exclamation-circle'\"></fa> Questa liturgia non contiene cartelle o documenti\n          </div>\n\n          <h6 class=\"titulo\" *ngIf=\"carpetasLiturgiaSeleccionanda.length > 0\">\n            <span *ngIf=\"idioma=='español'\">\n              Carpetas\n            </span>\n            <span *ngIf=\"idioma=='english'\">\n              Folders\n            </span>\n            <span *ngIf=\"idioma=='italiano'\">\n              Cartelle\n            </span>\n          </h6>\n          <table class=\"table table-hover \">\n            <tbody>\n              <tr *ngFor=\"let item of carpetasLiturgiaSeleccionanda\" class=\"tappable\">\n                <td (click)=\"seleccionarCarpetaHijo(item)\">\n                  <fa style=\"color: #b1504d;margin-right:.3em;\" [name]=\"'folder'\"></fa>\n                  <span *ngIf=\"idioma=='español'\">\n                    {{item.nombre}}\n                  </span>\n                  <span *ngIf=\"idioma=='english'\">\n                    {{item.nombreingles}}\n                  </span>\n                  <span *ngIf=\"idioma=='italiano'\">\n                    {{item.nombreitaliano}}\n                  </span>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n\n          <h6 class=\"titulo\" *ngIf=\"documentos.length > 0\">\n            <span *ngIf=\"idioma=='español'\">\n              Documentos\n            </span>\n            <span *ngIf=\"idioma=='english'\">\n              Documents\n            </span>\n            <span *ngIf=\"idioma=='italiano'\">\n              Documentazione\n            </span>\n          </h6>\n          <table class=\"table table-hover \">\n            <tbody>\n              <tr *ngFor=\"let item of documentos\">\n                <td class=\"tappable\" (click)=\"selecturl(item,content)\" *ngIf=\"idioma == 'español' && item.idioma == 'español' || idioma == 'english' && item.idioma == 'ingles' || idioma == 'italiano' && item.idioma == 'italiano'\">\n                  <fa style=\"color: #b1504d;margin-right:.3em;\" [name]=\"'file'\"></fa>\n                  <span>\n                    {{item.nombre}}\n                  </span>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n</div>\n</section>\n\n<ng-template #content let-c=\"close\" let-d=\"dismiss\">\n  <div class=\"modal-header\" style=\"border:none;\">\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <iframe [src]='urlselect' style=\"height:80vh; width: 100%;\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen>\n    </iframe>\n  </div>\n</ng-template>\n\n<ngx-spinner *ngIf=\"idioma=='español'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\"  loadingText=\"Un momento por favor\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='english'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"One moment, please\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='italiano'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"Un momento, per favore\"></ngx-spinner>\n"

/***/ }),

/***/ "./src/app/componentes/liturgias/liturgias.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/componentes/liturgias/liturgias.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 1em 2em 3em;\n  background-color: #ECEBEB; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer;\n  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.4); }\n  .contenedor-liturgias {\n  background-color: #fff;\n  margin-left: 1em;\n  padding: 1em;\n  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.4); }\n  .contenedor-liturgias .titulo {\n    color: rgba(0, 0, 0, 0.4); }\n  .tappable {\n  cursor: pointer; }\n  .col-sm-2.card-liturgia,\n.col-sm-3.card-liturgia,\n.col-sm-4.card-liturgia,\n.col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-2.card-liturgia .card-title,\n  .col-sm-3.card-liturgia .card-title,\n  .col-sm-4.card-liturgia .card-title,\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .modal-dialog {\n  width: 100%;\n  height: 95%;\n  margin: 0;\n  padding: 0; }\n  .modal-content {\n  height: auto;\n  min-height: 95%;\n  border-radius: 0; }\n"

/***/ }),

/***/ "./src/app/componentes/liturgias/liturgias.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/componentes/liturgias/liturgias.component.ts ***!
  \**************************************************************/
/*! exports provided: LiturgiasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LiturgiasComponent", function() { return LiturgiasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _node_modules_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/@ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/ngx-spinner.umd.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ngx_spinner__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LiturgiasComponent = /** @class */ (function () {
    function LiturgiasComponent(localstorage, db, sanitizer, modalService, spinner) {
        var _this = this;
        this.localstorage = localstorage;
        this.db = db;
        this.sanitizer = sanitizer;
        this.modalService = modalService;
        this.spinner = spinner;
        this.idioma = "español";
        this.urlselect = "";
        this.type = "application/pdf";
        this.tituloDocumento = "";
        this.documentos = [];
        this.carpetaliturgiaid = "0";
        this.carpetas = [];
        this.carpetasLiturgiaSeleccionanda = [];
        this.carpetaAnterior = {};
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
        this.db
            .docWithRefs$("secciones-documentos/J1rG9aiLuN8chaikGHxG")
            .subscribe(function (resp) {
            if (resp.idcarpeta) {
                _this.carpetaliturgiaid = resp.idcarpeta;
                _this.db
                    .colWithIds$("rutas", function (ref) {
                    return ref.where("padre", "==", resp.idcarpeta);
                })
                    .subscribe(function (resp) {
                    _this.carpetas = resp;
                });
            }
        });
    }
    LiturgiasComponent.prototype.ngOnInit = function () { };
    LiturgiasComponent.prototype.seleccionarCarpeta = function (item) {
        var _this = this;
        this.carpetaLiturgiaSeleccionada = item;
        this.db
            .colWithIds$("documentos", function (ref) { return ref.where("ruta", "==", item.id); })
            .subscribe(function (documentos) {
            _this.documentos = documentos;
        });
        this.db
            .docWithRefs$("secciones-documentos/J1rG9aiLuN8chaikGHxG")
            .subscribe(function (resp) {
            if (resp.idcarpeta) {
                _this.db
                    .colWithIds$("rutas", function (ref) { return ref.where("padre", "==", item.id); })
                    .subscribe(function (resp) {
                    _this.carpetasLiturgiaSeleccionanda = resp;
                });
            }
        });
    };
    LiturgiasComponent.prototype.seleccionarCarpetaHijo = function (item) {
        var _this = this;
        this.carpetaLiturgiaSeleccionada = item;
        this.db
            .colWithIds$("documentos", function (ref) { return ref.where("ruta", "==", item.id); })
            .subscribe(function (documentos) {
            _this.documentos = documentos;
        });
        this.db
            .docWithRefs$("secciones-documentos/J1rG9aiLuN8chaikGHxG")
            .subscribe(function (resp) {
            if (resp.idcarpeta) {
                _this.db
                    .colWithIds$("rutas", function (ref) { return ref.where("padre", "==", item.id); })
                    .subscribe(function (resp) {
                    _this.carpetasLiturgiaSeleccionanda = resp;
                });
            }
        });
    };
    LiturgiasComponent.prototype.seleccionarCarpetaVolver = function (item) {
        var _this = this;
        console.log(item);
        this.db.docWithRefs$("rutas/" + item.padre).subscribe(function (carpetaant) {
            carpetaant.id = item.padre;
            _this.carpetaAnterior = carpetaant;
            _this.seleccionarCarpetaHijo(carpetaant);
        });
    };
    LiturgiasComponent.prototype.cerrarLiturgia = function () {
        this.carpetaLiturgiaSeleccionada = null;
    };
    LiturgiasComponent.prototype.selecturl = function (item, content) {
        var _this = this;
        this.urlselect = this.sanitizer.bypassSecurityTrustResourceUrl(item.url);
        this.type = item.type;
        this.tituloDocumento = item.nombre;
        this.spinner.show();
        setTimeout(function () {
            _this.spinner.hide();
        }, 3000);
        this.modalService
            .open(content, { size: "lg", centered: false })
            .result.then(function (result) { }, function (reason) { });
    };
    LiturgiasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "liturgias",
            template: __webpack_require__(/*! ./liturgias.component.html */ "./src/app/componentes/liturgias/liturgias.component.html"),
            styles: [__webpack_require__(/*! ./liturgias.component.scss */ "./src/app/componentes/liturgias/liturgias.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__["LocalstorageService"],
            _core_resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"],
            _node_modules_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"]])
    ], LiturgiasComponent);
    return LiturgiasComponent;
}());



/***/ }),

/***/ "./src/app/componentes/mensaje-papa/mensaje-papa.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/componentes/mensaje-papa/mensaje-papa.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row contenedor-mensaje\">\n  <div class=\"col-sm-6 foto-papa\">\n  </div>\n  <div class=\"col-sm-6 frase-papa\">\n    <h2 *ngIf=\"idioma=='español'\">Mensaje del santo padre</h2>\n    <h2 *ngIf=\"idioma=='english'\">Message from the holy father</h2>\n    <h2 *ngIf=\"idioma=='italiano'\">Messaggio dal santo padre</h2>\n\n\n    <h4>Papa Francisco</h4>\n    <p *ngIf=\"idioma=='español'\" [innerHtml]=\"info.contenido\">\n\n    </p>\n    <p *ngIf=\"idioma=='english'\" [innerHtml]=\"info.contenidoenglish\">\n\n    </p>\n    <p *ngIf=\"idioma=='italiano'\" [innerHtml]=\"info.contenidoitaliano\">\n    </p>\n    <a [href]=\"info.url\" target=\"_blank\" *ngIf=\"idioma=='español'\" class=\"btn btn-danger btn-lg\">\n      <span>Ver discurso</span>\n    </a>\n    <a [href]=\"info.urlenglish\" target=\"_blank\" *ngIf=\"idioma=='english'\" class=\"btn btn-danger btn-lg\">\n      <span>See speech</span>\n    </a>\n    <a [href]=\"info.urlitaliano\" target=\"_blank\" *ngIf=\"idioma=='italiano'\" class=\"btn btn-danger btn-lg\">\n      <span>Vedi discorso</span>\n    </a>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/componentes/mensaje-papa/mensaje-papa.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/componentes/mensaje-papa/mensaje-papa.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-mensaje {\n  padding: 3em 12em 0em; }\n\n.foto-papa {\n  background-image: url('foto-papa.png');\n  min-height: 350px;\n  background-size: contain;\n  background-position: center;\n  background-repeat: no-repeat; }\n\n.frase-papa {\n  text-align: right; }\n\n.frase-papa h2 {\n    color: #EE7835; }\n\n.frase-papa h4 {\n    color: #1F4027; }\n\n.frase-papa p {\n    color: #565857;\n    font-weight: 300;\n    line-height: 20px;\n    word-spacing: auto; }\n"

/***/ }),

/***/ "./src/app/componentes/mensaje-papa/mensaje-papa.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/componentes/mensaje-papa/mensaje-papa.component.ts ***!
  \********************************************************************/
/*! exports provided: MensajePapaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MensajePapaComponent", function() { return MensajePapaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MensajePapaComponent = /** @class */ (function () {
    function MensajePapaComponent(localstorage, db) {
        var _this = this;
        this.localstorage = localstorage;
        this.db = db;
        this.idioma = 'español';
        this.info = {};
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
        this.db.docWithRefs$("secciones-documentos/mensaje-papa").subscribe(function (resp) {
            _this.info = resp;
        });
    }
    MensajePapaComponent.prototype.ngOnInit = function () {
    };
    MensajePapaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'mensaje-papa',
            template: __webpack_require__(/*! ./mensaje-papa.component.html */ "./src/app/componentes/mensaje-papa/mensaje-papa.component.html"),
            styles: [__webpack_require__(/*! ./mensaje-papa.component.scss */ "./src/app/componentes/mensaje-papa/mensaje-papa.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__["LocalstorageService"],
            _core_resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"]])
    ], MensajePapaComponent);
    return MensajePapaComponent;
}());



/***/ }),

/***/ "./src/app/componentes/proyectos/proyectos.component.html":
/*!****************************************************************!*\
  !*** ./src/app/componentes/proyectos/proyectos.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\">\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a class=\"tappable\" *ngIf=\"idioma=='español'\">Proyecto </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='english'\">Project </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='italiano'\">Progetti </a>\n        </li>\n        <li>\n      </ul>\n      <!--<p class=\"introduccion\" *ngIf=\"idioma=='español'\">\n        ESPAÑOL Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor. Phasellus\n        in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere sem ultrices.\n        Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis congue nisl,\n        nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='english'\">\n        ENGLISH Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor. Phasellus\n        in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere sem ultrices.\n        Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis congue nisl,\n        nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='italiano'\">\n        ITALIANO Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor. Phasellus\n        in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere sem ultrices.\n        Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis congue nisl,\n        nec vulputate nunc.\n      </p>-->\n\n\n    </div>\n    <div class=\"col-md-12\">\n      <div class=\"row\">\n        <div class=\"col-md-4\" *ngFor=\"let item of proyectos | slice:0:3\">\n          <div class=\"card articulo\" style=\"width: 100%;\">\n            <a href=\"#/proyecto-detalle/{{item.id}}\" class=\"image-card\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\"></a>\n            <div class=\"card-body\">\n              <h5 class=\"card-title titulo\">{{item.titulo}}</h5>\n              <h6 class=\"card-subtitle mb-2 text-muted fecha\">{{item.createdAt.toDate() | date: 'mediumDate'}}</h6>\n              <p class=\"card-text\">{{item.resumen | slice:0:100}} ...</p>\n              <a class=\"link-ver\" *ngIf=\"idioma=='español'\" href=\"#/proyecto-detalle/{{item.id}}\">Leer más</a>\n              <a class=\"link-ver\" *ngIf=\"idioma=='english'\" href=\"#/proyecto-detalle/{{item.id}}\">Read more</a>\n              <a class=\"link-ver\" *ngIf=\"idioma=='italiano'\" href=\"#/proyecto-detalle/{{item.id}}\">Leggi di più</a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <br>\n\n    <div class=\"col-md-12\" style=\"margin-top:2em;\">\n      <a class=\"btn btn-danger btn-lg btn-ver\" href=\"#/proyectos\">\n        <span *ngIf=\"idioma=='español'\">Ver más</span>\n        <span *ngIf=\"idioma=='english'\">See more</span>\n        <span *ngIf=\"idioma=='italiano'\">Vedi di più</span>\n      </a>\n    </div>\n\n\n  </div>\n\n</section>\n"

/***/ }),

/***/ "./src/app/componentes/proyectos/proyectos.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/componentes/proyectos/proyectos.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  .image-card {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position-y: top;\n  background-position-x: center;\n  height: 250px;\n  width: 100%;\n  cursor: pointer; }\n  .image-card:hover {\n  opacity: .8; }\n  .card-body {\n  min-height: 210px; }\n  .card {\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.3); }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em;\n  background-color: #9F2820 important; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  @media screen and (max-width: 992px) {\n  .col-md-4 {\n    width: 50% !important;\n    max-width: 50% !important;\n    flex: 0 0 50%; } }\n"

/***/ }),

/***/ "./src/app/componentes/proyectos/proyectos.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/componentes/proyectos/proyectos.component.ts ***!
  \**************************************************************/
/*! exports provided: ProyectosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProyectosComponent", function() { return ProyectosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProyectosComponent = /** @class */ (function () {
    function ProyectosComponent(localstorage, db) {
        var _this = this;
        this.localstorage = localstorage;
        this.db = db;
        this.idioma = "español";
        this.idioma2 = "español";
        this.proyectos = [];
        this.localstorage.getIdioma().subscribe(function (event) {
            if (event.newValue == 'english') {
                _this.idioma2 = 'ingles';
            }
            else {
                _this.idioma2 = event.newValue;
            }
            _this.idioma = event.newValue;
            _this.db.colWithIds$('proyectos', function (ref) { return ref.where('idioma', '==', _this.idioma2); }).subscribe(function (proyectos) {
                _this.proyectos = proyectos;
            });
        });
        this.idioma = this.localstorage.getActualIdioma();
        if (this.idioma == 'english') {
            this.idioma2 = 'ingles';
        }
        else {
            this.idioma2 = this.idioma;
        }
        this.db.colWithIds$('proyectos', function (ref) { return ref.where('idioma', '==', _this.idioma2); }).subscribe(function (proyectos) {
            _this.proyectos = proyectos;
        });
    }
    ProyectosComponent.prototype.ngOnInit = function () { };
    ProyectosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "proyectos",
            template: __webpack_require__(/*! ./proyectos.component.html */ "./src/app/componentes/proyectos/proyectos.component.html"),
            styles: [__webpack_require__(/*! ./proyectos.component.scss */ "./src/app/componentes/proyectos/proyectos.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__["LocalstorageService"],
            _core_resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"]])
    ], ProyectosComponent);
    return ProyectosComponent;
}());



/***/ }),

/***/ "./src/app/core/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/core/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _resources_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AuthService = /** @class */ (function () {
    function AuthService(afAuth, afs, router, _db) {
        var _this = this;
        this.afAuth = afAuth;
        this.afs = afs;
        this.router = router;
        this._db = _db;
        this.user = this.afAuth.authState
            .switchMap(function (user) {
            if (user) {
                return _this._db.col$("asistentes", function (ref) { return ref.where('uid', '==', user.uid); });
            }
            else {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["of"])(null);
            }
        });
    }
    Object.defineProperty(AuthService.prototype, "authenticated", {
        get: function () {
            return this.user !== null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "currentUserObservable", {
        get: function () {
            return this.afAuth.authState;
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.getCurrentUser = function () {
        var _this = this;
        return this.afAuth.authState
            .switchMap(function (user) {
            if (user) {
                return _this._db.col$("asistentes", function (ref) { return ref.where('uid', '==', user.uid); });
            }
            else {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["of"])(null);
            }
        });
    };
    AuthService.prototype.emailLogin = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signInWithEmailAndPassword(email, password)
                .then(function (user) {
                _this._db.col$("asistentes", function (ref) { return ref.where('uid', '==', user.user.uid); }).subscribe(function (res) {
                    if (res.length > 0) {
                        resolve(res[0]);
                    }
                    else {
                        reject();
                    }
                });
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    // Sends email allowing user to reset password
    AuthService.prototype.resetPassword = function (email) {
        var fbAuth = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]();
        return fbAuth.sendPasswordResetEmail(email)
            .then(function () {
        })
            .catch(function (error) {
        });
    };
    AuthService.prototype.signOut = function () {
        var _this = this;
        this.afAuth.auth.signOut().then(function () {
            _this.router.navigate(['/']);
        });
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _resources_service__WEBPACK_IMPORTED_MODULE_3__["ResourcesService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/core/localstorage.service.ts":
/*!**********************************************!*\
  !*** ./src/app/core/localstorage.service.ts ***!
  \**********************************************/
/*! exports provided: LocalstorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageService", function() { return LocalstorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _node_modules_ngx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/ngx-store */ "./node_modules/ngx-store/ngx-store.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LocalstorageService = /** @class */ (function () {
    function LocalstorageService(sessionStorageService, localStorage) {
        this.sessionStorageService = sessionStorageService;
        this.localStorage = localStorage;
    }
    LocalstorageService.prototype.setIdioma = function (idioma) {
        this.localStorage.set("idioma", idioma);
    };
    LocalstorageService.prototype.getActualIdioma = function () {
        return this.localStorage.get('idioma');
    };
    LocalstorageService.prototype.getIdioma = function () {
        return this.localStorage.observe('idioma');
    };
    LocalstorageService.prototype.setUser = function (user) {
        this.localStorage.set("user", user);
    };
    LocalstorageService.prototype.getUser = function () {
        return this.localStorage.observe('user');
    };
    LocalstorageService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: "root"
        }),
        __metadata("design:paramtypes", [_node_modules_ngx_store__WEBPACK_IMPORTED_MODULE_1__["SessionStorageService"],
            _node_modules_ngx_store__WEBPACK_IMPORTED_MODULE_1__["LocalStorageService"]])
    ], LocalstorageService);
    return LocalstorageService;
}());



/***/ }),

/***/ "./src/app/core/resources.service.ts":
/*!*******************************************!*\
  !*** ./src/app/core/resources.service.ts ***!
  \*******************************************/
/*! exports provided: ResourcesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResourcesService", function() { return ResourcesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_do__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/do */ "./node_modules/rxjs-compat/_esm5/add/operator/do.js");
/* harmony import */ var rxjs_add_operator_take__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/take */ "./node_modules/rxjs-compat/_esm5/add/operator/take.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var rxjs_add_operator_switchMap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/switchMap */ "./node_modules/rxjs-compat/_esm5/add/operator/switchMap.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var _node_modules_angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../node_modules/@angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ResourcesService = /** @class */ (function () {
    function ResourcesService(afs, storage, db, http) {
        this.afs = afs;
        this.storage = storage;
        this.db = db;
        this.http = http;
    }
    ResourcesService.prototype.getPaises = function () {
        return this.http.get('assets/paises.json');
    };
    // Get Reference
    ResourcesService.prototype.col = function (ref, queryFn) {
        return typeof ref === 'string' ? this.afs.collection(ref, queryFn) : ref;
    };
    ResourcesService.prototype.doc = function (ref) {
        return typeof ref === 'string' ? this.afs.doc(ref) : ref;
    };
    // Get Data
    ResourcesService.prototype.doc$ = function (ref) {
        return this.doc(ref).snapshotChanges().map(function (doc) {
            return doc.payload.data();
        });
    };
    ResourcesService.prototype.col$ = function (ref, queryFn) {
        return this.col(ref, queryFn).snapshotChanges().map(function (docs) {
            return docs.map(function (a) { return a.payload.doc.data(); });
        });
    };
    // With Id
    ResourcesService.prototype.colWithIds$ = function (ref, queryFn) {
        return this.col(ref, queryFn).snapshotChanges().map(function (action) {
            return action.map(function (a) {
                var data = a.payload.doc.data();
                var id = a.payload.doc.id;
                return __assign({ id: id }, data);
            });
        });
    };
    ResourcesService.prototype.inspectDoc = function (ref) {
        var tick = new Date().getTime();
        this.doc(ref).snapshotChanges()
            .take(1)
            .do(function (d) {
            var tock = new Date().getTime() - tick;
            console.log("Loaded Document in " + tock + "ms", d);
        })
            .subscribe();
    };
    ResourcesService.prototype.inspectCol = function (ref) {
        var tick = new Date().getTime();
        this.col(ref).snapshotChanges()
            .take(1)
            .do(function (c) {
            var tock = new Date().getTime() - tick;
            console.log("Loaded Collection in " + tock + "ms", c);
        })
            .subscribe();
    };
    // Write Data
    ResourcesService.prototype.update = function (ref, data) {
        return this.doc(ref).update(__assign({}, data, { updatedAt: this.timestamp }));
    };
    ResourcesService.prototype.set = function (ref, data) {
        var timestamp = this.timestamp;
        return this.doc(ref).set(__assign({}, data, { updatedAt: timestamp, createdAt: timestamp }));
    };
    ResourcesService.prototype.add = function (ref, data) {
        var timestamp = this.timestamp;
        return this.col(ref).add(__assign({}, data, { updatedAt: timestamp, createdAt: timestamp }));
    };
    ResourcesService.prototype.delete = function (ref) {
        return this.doc(ref).delete();
    };
    ResourcesService.prototype.upsert = function (ref, data) {
        var _this = this;
        var doc = this.doc(ref).snapshotChanges().take(1).toPromise();
        return doc.then(function (snap) {
            return snap.payload.exists ? _this.update(ref, data) : _this.set(ref, data);
        });
    };
    Object.defineProperty(ResourcesService.prototype, "timestamp", {
        // Get Firebase Serve Timestamp
        get: function () {
            return firebase_app__WEBPACK_IMPORTED_MODULE_1__["firestore"].FieldValue.serverTimestamp();
        },
        enumerable: true,
        configurable: true
    });
    // GeoPoint
    ResourcesService.prototype.geopoint = function (lat, lng) {
        return new firebase_app__WEBPACK_IMPORTED_MODULE_1__["firestore"].GeoPoint(lat, lng);
    };
    // Documents
    ResourcesService.prototype.connect = function (host, key, doc) {
        return this.doc(host).update((_a = {}, _a[key] = this.doc(doc).ref, _a));
        var _a;
    };
    /// returns a documents references mapped to AngularFirestoreDocument
    ResourcesService.prototype.docWithRefs$ = function (ref) {
        var _this = this;
        return this.doc$(ref).map(function (doc) {
            for (var _i = 0, _a = Object.keys(doc); _i < _a.length; _i++) {
                var k = _a[_i];
                if (doc[k] instanceof firebase_app__WEBPACK_IMPORTED_MODULE_1__["firestore"].DocumentReference) {
                    doc[k] = _this.doc(doc[k].path);
                }
            }
            return doc;
        });
    };
    ResourcesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_7__["AngularFirestore"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_8__["AngularFireStorage"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_7__["AngularFirestore"],
            _node_modules_angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"]])
    ], ResourcesService);
    return ResourcesService;
}());



/***/ }),

/***/ "./src/app/core/upload.service.ts":
/*!****************************************!*\
  !*** ./src/app/core/upload.service.ts ***!
  \****************************************/
/*! exports provided: Upload, UploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Upload", function() { return Upload; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadService", function() { return UploadService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @covalent/core/loading */ "./node_modules/@covalent/core/esm5/covalent-core-loading.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Upload = /** @class */ (function () {
    function Upload(file) {
        this.createdAt = new Date();
        this.file = file;
    }
    return Upload;
}());

var UploadService = /** @class */ (function () {
    function UploadService(storage, db, _loadingService) {
        this.storage = storage;
        this.db = db;
        this._loadingService = _loadingService;
    }
    UploadService.prototype.uploadDocumento = function (upload) {
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_1__["storage"]().ref();
            var now = new Date().getTime();
            var uploadTask = storageRef.child("/documentos/" + now).put(upload.file);
            uploadTask.on(firebase_app__WEBPACK_IMPORTED_MODULE_1__["storage"].TaskEvent.STATE_CHANGED, function (snapshot) {
                upload.progress =
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            }, function (error) {
                return reject(error);
            }, function () {
                upload.url = uploadTask.snapshot.downloadURL;
                upload.name = upload.file.name;
                resolve(upload.url);
            });
        });
    };
    UploadService.prototype.uploadPic = function (upload) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._loadingService.register();
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_1__["storage"]().ref();
            var now = new Date().getTime();
            var uploadTask = storageRef.child("/fotos/" + now).put(upload.file);
            uploadTask.on(firebase_app__WEBPACK_IMPORTED_MODULE_1__["storage"].TaskEvent.STATE_CHANGED, function (snapshot) {
                upload.progress =
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            }, function (error) {
                return reject(error);
            }, function () {
                upload.url = uploadTask.snapshot.downloadURL;
                upload.name = upload.file.name;
                _this._loadingService.resolve();
                resolve(upload.url);
            });
        });
    };
    UploadService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__["AngularFireStorage"],
            _resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"],
            _covalent_core_loading__WEBPACK_IMPORTED_MODULE_3__["TdLoadingService"]])
    ], UploadService);
    return UploadService;
}());



/***/ }),

/***/ "./src/app/paginas/asistentes-grupos/asistentes-grupos.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/paginas/asistentes-grupos/asistentes-grupos.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\">\n  <div class=\"overlay\">\n    <h1 *ngIf=\"idioma=='español'\">Congregación de la Pasión de Jesucristo</h1>\n    <h3 *ngIf=\"idioma=='español'\">Predicamos a Cristo crucificado</h3>\n    <h1 *ngIf=\"idioma=='english'\">Congregation of the Passion of Jesus Christ</h1>\n    <h3 *ngIf=\"idioma=='english'\">We preach Christ crucified</h3>\n    <h1 *ngIf=\"idioma=='italiano'\">Congregazione della Passione di Gesù Cristo</h1>\n    <h3 *ngIf=\"idioma=='italiano'\">Predichiamo Cristo crocifisso</h3>\n  </div>\n</div>\n\n<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\" style=\"margin-bottom: 2em;\">\n      <input class=\"form-control mb-4 mr-sm-2\" type=\"search\" [placeholder]=\"placeholderinput\" [(ngModel)]=\"filter\" name=\"filterespanol\"  #inputFilterEsp>\n\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a *ngIf=\"idioma=='español'\">Participantes {{grupo.nombre}}</a>\n          <a *ngIf=\"idioma=='english'\">Participants {{grupo.nombre}}</a>\n          <a *ngIf=\"idioma=='italiano'\">Partecipanti {{grupo.nombre}}</a>\n        </li>\n        <li>\n      </ul>\n\n\n    </div>\n    <div class=\"col-sm-12\">\n      <div class=\"row\">\n        <div class=\"col-sm-2\" *ngFor=\"let item of participantes | filter:filter\">\n          <div class=\"card articulo\" style=\"width: 100%;\">\n            <div class=\"foto-participante\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\" *ngIf=\"item.foto\"></div>\n            <div class=\"foto-participante\" style=\"background-image: url('./../../../assets/no-foto.jpg')\" *ngIf=\"!item.foto\"></div>\n            <div class=\"card-body\">\n              <h4 class=\"card-title titulo\">{{item.nombre}}</h4>\n              <h6 class=\"card-subtitle text-muted fecha\">{{item.configuracion}} <span *ngIf=\"item.pais\">/</span> {{item.pais}}</h6>\n              <a href=\"mailto:{{item.correo}}\">{{item.correo}}</a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n</section>\n\n\n"

/***/ }),

/***/ "./src/app/paginas/asistentes-grupos/asistentes-grupos.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/paginas/asistentes-grupos/asistentes-grupos.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-image: url('background-banner.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 300px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px; }\n  .contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n  .foto-participante:hover {\n  opacity: .8; }\n  .card-body {\n  height: 200px; }\n  .card {\n  margin-bottom: 1em; }\n"

/***/ }),

/***/ "./src/app/paginas/asistentes-grupos/asistentes-grupos.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/paginas/asistentes-grupos/asistentes-grupos.component.ts ***!
  \**************************************************************************/
/*! exports provided: AsistentesGruposComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AsistentesGruposComponent", function() { return AsistentesGruposComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/ngx-spinner.umd.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ngx_spinner__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AsistentesGruposComponent = /** @class */ (function () {
    function AsistentesGruposComponent(db, localstorage, _route, spinner) {
        var _this = this;
        this.db = db;
        this.localstorage = localstorage;
        this._route = _route;
        this.spinner = spinner;
        this.participantes = [];
        this.staff = [];
        this.idioma = "español";
        this.filter = "";
        this.filteredItems = [];
        this.placeholderinput = "Buscar participante";
        this.grupo = {};
        this._route.params.subscribe(function (params) {
            _this.id = params.id;
            if (_this.id) {
                _this.spinner.show();
                setTimeout(function () {
                    _this.spinner.hide();
                }, 3000);
                _this.db.docWithRefs$("grupos/" + _this.id).subscribe(function (res) {
                    _this.grupo = res;
                    _this.db.colWithIds$("asistentes", function (ref) { return ref.orderBy('posicion', 'asc'); }).subscribe(function (resp) {
                        resp.forEach(function (element) {
                            var participantes = res.asistentes;
                            var encontro = participantes.find(function (x) { return x.asistenteid == element.id; });
                            if (encontro) {
                                _this.participantes.push(element);
                            }
                        });
                    });
                });
            }
        });
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
            switch (_this.idioma) {
                case "español":
                    _this.placeholderinput = "Buscar";
                    break;
                case "english":
                    _this.placeholderinput = "Search";
                    break;
                case "español":
                    _this.placeholderinput = "Cerca";
                    break;
            }
        });
        this.idioma = this.localstorage.getActualIdioma();
        switch (this.idioma) {
            case "español":
                this.placeholderinput = "Buscar";
                break;
            case "english":
                this.placeholderinput = "Search";
                break;
            case "español":
                this.placeholderinput = "Cerca";
                break;
        }
    }
    AsistentesGruposComponent.prototype.ngOnInit = function () { };
    AsistentesGruposComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-asistentes-grupos",
            template: __webpack_require__(/*! ./asistentes-grupos.component.html */ "./src/app/paginas/asistentes-grupos/asistentes-grupos.component.html"),
            styles: [__webpack_require__(/*! ./asistentes-grupos.component.scss */ "./src/app/paginas/asistentes-grupos/asistentes-grupos.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__["LocalstorageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"]])
    ], AsistentesGruposComponent);
    return AsistentesGruposComponent;
}());



/***/ }),

/***/ "./src/app/paginas/asistentes/asistentes.component.html":
/*!**************************************************************!*\
  !*** ./src/app/paginas/asistentes/asistentes.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\">\n  <div class=\"overlay\">\n    <h1 *ngIf=\"idioma=='español'\">Congregación de la Pasión de Jesucristo</h1>\n    <h3 *ngIf=\"idioma=='español'\">Predicamos a Cristo crucificado</h3>\n    <h1 *ngIf=\"idioma=='english'\">Congregation of the Passion of Jesus Christ</h1>\n    <h3 *ngIf=\"idioma=='english'\">We preach Christ crucified</h3>\n    <h1 *ngIf=\"idioma=='italiano'\">Congregazione della Passione di Gesù Cristo</h1>\n    <h3 *ngIf=\"idioma=='italiano'\">Predichiamo Cristo crocifisso</h3>\n  </div>\n</div>\n\n<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\" style=\"margin-bottom: 2em;\">\n      <input class=\"form-control mb-4 mr-sm-2\" type=\"search\" [placeholder]=\"placeholderinput\" [(ngModel)]=\"filter\" name=\"filterespanol\"  #inputFilterEsp>\n\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a *ngIf=\"idioma=='español'\">Participantes</a>\n          <a *ngIf=\"idioma=='english'\">Participants</a>\n          <a *ngIf=\"idioma=='italiano'\">Partecipanti</a>\n        </li>\n        <li>\n      </ul>\n\n\n    </div>\n    <div class=\"col-sm-12\">\n      <div class=\"row\">\n        <div class=\"col-sm-2\" *ngFor=\"let item of participantes | filter:filter\">\n          <div class=\"card articulo\" style=\"width: 100%;\">\n            <div class=\"foto-participante\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\" *ngIf=\"item.foto\"></div>\n            <div class=\"foto-participante\" style=\"background-image: url('./../../../assets/no-foto.jpg')\" *ngIf=\"!item.foto\"></div>\n            <div class=\"card-body\">\n              <h4 class=\"card-title titulo\">{{item.nombre}}</h4>\n              <h6 class=\"card-subtitle text-muted fecha\">{{item.configuracion}} <span *ngIf=\"item.pais\">/</span> {{item.pais}}</h6>\n              <a href=\"mailto:{{item.correo}}\">{{item.correo}}</a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"col-sm-12\" style=\"margin-bottom: 2em;\">\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a>Staff</a>\n        </li>\n        <li>\n      </ul>\n    </div>\n\n    <div class=\"col-sm-12\">\n      <div class=\"row\">\n        <div class=\"col-sm-2\" *ngFor=\"let item of staff | filter:filter\">\n          <div class=\"card articulo\" style=\"width: 100%;\">\n            <div class=\"foto-participante\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\" *ngIf=\"item.foto\"></div>\n            <div class=\"foto-participante\" style=\"background-image: url('./../../../assets/no-foto.jpg')\" *ngIf=\"!item.foto\"></div>\n            <div class=\"card-body\">\n              <h4 class=\"card-title titulo\">{{item.nombre}}</h4>\n              <h6 class=\"card-subtitle text-muted fecha\">{{item.configuracion}} <span *ngIf=\"item.pais\">/</span>\n                {{item.pais}}</h6>\n              <a href=\"mailto:{{item.correo}}\">{{item.correo}}</a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</section>\n\n\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/asistentes.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/paginas/asistentes/asistentes.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-image: url('background-banner.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 300px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px; }\n  .contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n  .foto-participante:hover {\n  opacity: .8; }\n  .card-body {\n  height: 200px; }\n  .card {\n  margin-bottom: 1em; }\n"

/***/ }),

/***/ "./src/app/paginas/asistentes/asistentes.component.ts":
/*!************************************************************!*\
  !*** ./src/app/paginas/asistentes/asistentes.component.ts ***!
  \************************************************************/
/*! exports provided: AsistentesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AsistentesComponent", function() { return AsistentesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AsistentesComponent = /** @class */ (function () {
    function AsistentesComponent(db, localstorage) {
        var _this = this;
        this.db = db;
        this.localstorage = localstorage;
        this.participantes = [];
        this.staff = [];
        this.idioma = "español";
        this.filter = "";
        this.filteredItems = [];
        this.placeholderinput = "Buscar participante";
        this.db.colWithIds$("asistentes", function (ref) { return ref.orderBy('posicion', 'asc').where('tipo', '==', 'participante'); }).subscribe(function (resp) {
            _this.participantes = resp;
        });
        this.db.colWithIds$("asistentes", function (ref) { return ref.orderBy('posicion', 'asc').where('tipo', '==', 'staff'); }).subscribe(function (resp) {
            _this.staff = resp;
        });
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
            switch (_this.idioma) {
                case "español":
                    _this.placeholderinput = "Buscar";
                    break;
                case "english":
                    _this.placeholderinput = "Search";
                    break;
                case "español":
                    _this.placeholderinput = "Cerca";
                    break;
            }
        });
        this.idioma = this.localstorage.getActualIdioma();
        switch (this.idioma) {
            case "español":
                this.placeholderinput = "Buscar";
                break;
            case "english":
                this.placeholderinput = "Search";
                break;
            case "español":
                this.placeholderinput = "Cerca";
                break;
        }
    }
    AsistentesComponent.prototype.ngOnInit = function () { };
    AsistentesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-asistentes",
            template: __webpack_require__(/*! ./asistentes.component.html */ "./src/app/paginas/asistentes/asistentes.component.html"),
            styles: [__webpack_require__(/*! ./asistentes.component.scss */ "./src/app/paginas/asistentes/asistentes.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__["LocalstorageService"]])
    ], AsistentesComponent);
    return AsistentesComponent;
}());



/***/ }),

/***/ "./src/app/paginas/cronica-detalle/cronica-detalle.component.html":
/*!************************************************************************!*\
  !*** ./src/app/paginas/cronica-detalle/cronica-detalle.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\" [ngStyle]=\"{'background-image': 'url(' + cronica.foto + ')'}\">\n</div>\n<div class=\"col-lg-10 col-sm-10 col-12 main-section\">\n  <div class=\"modal-content\">\n    <ul class=\"opciones-actividades\" style=\"margin-bottom: 2em;\">\n      <li class=\"active\">\n        <a class=\"tappable\">{{cronica.titulo}} </a>\n      </li>\n      <li>\n    </ul>\n    <p [innerHTML]=\"cronica.contenido\">\n    </p>\n  </div>\n</div>\n\n\n<ngx-spinner *ngIf=\"idioma=='español'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"Un momento por favor\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='english'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"One moment, please\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='italiano'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"Un momento, per favore\"></ngx-spinner>\n"

/***/ }),

/***/ "./src/app/paginas/cronica-detalle/cronica-detalle.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/paginas/cronica-detalle/cronica-detalle.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 400px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px;\n  background-attachment: fixed; }\n  .contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      font-size: 3em;\n      color: #EE7835 !important;\n      cursor: initial; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n  .foto-participante:hover {\n  opacity: .8; }\n  .card-body {\n  height: 200px; }\n  .card {\n  margin-bottom: 1em; }\n  .image-card {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position-y: top;\n  background-position-x: center;\n  height: 250px;\n  width: 100%;\n  cursor: pointer; }\n  .image-card:hover {\n  opacity: .8; }\n  .card-body {\n  min-height: 210px; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .card {\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.3); }\n  .main-section {\n  margin: 0 auto;\n  top: -5em;\n  background-color: #fff;\n  border-radius: 5px;\n  padding: 0px;\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.5); }\n  .modal-content {\n  padding: 2em; }\n  .user-img {\n  margin-top: -50px;\n  display: inline-flex;\n  justify-content: center; }\n  .user-img img {\n  height: 100px;\n  width: 100px;\n  background-color: #fff;\n  padding: 1em;\n  border-radius: 100px;\n  box-shadow: 4px 4px 8px rgba(0, 0, 0, 0.3); }\n  .user-name {\n  margin: 10px 0px; }\n  .user-name h1 {\n  font-size: 20px;\n  color: #676363;\n  padding-top: 1em; }\n  .user-name button {\n  position: absolute;\n  top: -50px;\n  right: 20px;\n  font-size: 30px; }\n  .form-input button {\n  width: 100%;\n  margin-bottom: 20px; }\n  .link-part {\n  border-radius: 0px 0px 5px 5px;\n  background-color: #ECF0F1;\n  padding: 15px;\n  border-top: 1px solid #c2c2c2; }\n  .open-modal {\n  margin-top: 100px !important; }\n  .backdrop {\n  opacity: 1 !important;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-image: url('background-banner.jpg');\n  height: 100%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  opacity: .3;\n  background-color: #000; }\n"

/***/ }),

/***/ "./src/app/paginas/cronica-detalle/cronica-detalle.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/paginas/cronica-detalle/cronica-detalle.component.ts ***!
  \**********************************************************************/
/*! exports provided: CronicaDetalleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CronicaDetalleComponent", function() { return CronicaDetalleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/@angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/ngx-spinner */ "./node_modules/ngx-spinner/ngx-spinner.umd.js");
/* harmony import */ var _node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CronicaDetalleComponent = /** @class */ (function () {
    function CronicaDetalleComponent(_route, db, localstorage, spinner) {
        var _this = this;
        this._route = _route;
        this.db = db;
        this.localstorage = localstorage;
        this.spinner = spinner;
        this.cronica = {};
        this.idioma = "español";
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
        this._route.params.subscribe(function (params) {
            _this.id = params.id;
            if (_this.id) {
                _this.spinner.show();
                setTimeout(function () {
                    _this.spinner.hide();
                }, 3000);
                _this.db
                    .docWithRefs$("cronicas/" + _this.id)
                    .subscribe(function (res) {
                    _this.cronica = res;
                });
            }
        });
    }
    CronicaDetalleComponent.prototype.ngOnInit = function () {
    };
    CronicaDetalleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cronica-detalle',
            template: __webpack_require__(/*! ./cronica-detalle.component.html */ "./src/app/paginas/cronica-detalle/cronica-detalle.component.html"),
            styles: [__webpack_require__(/*! ./cronica-detalle.component.scss */ "./src/app/paginas/cronica-detalle/cronica-detalle.component.scss")]
        }),
        __metadata("design:paramtypes", [_node_modules_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _core_resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_3__["LocalstorageService"],
            _node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"]])
    ], CronicaDetalleComponent);
    return CronicaDetalleComponent;
}());



/***/ }),

/***/ "./src/app/paginas/cronicas/cronicas.component.html":
/*!**********************************************************!*\
  !*** ./src/app/paginas/cronicas/cronicas.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\">\n  <div class=\"overlay\">\n    <h1 *ngIf=\"idioma=='español'\">Congregación de la Pasión de Jesucristo</h1>\n    <h3 *ngIf=\"idioma=='español'\">Predicamos a Cristo crucificado</h3>\n    <h1 *ngIf=\"idioma=='english'\">Congregation of the Passion of Jesus Christ</h1>\n    <h3 *ngIf=\"idioma=='english'\">We preach Christ crucified</h3>\n    <h1 *ngIf=\"idioma=='italiano'\">Congregazione della Passione di Gesù Cristo</h1>\n    <h3 *ngIf=\"idioma=='italiano'\">Predichiamo Cristo crocifisso</h3>\n  </div>\n</div>\n\n<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\" style=\"margin-bottom: 2em;\">\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a class=\"tappable\" *ngIf=\"idioma=='español'\">Cronicas </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='english'\">Chronicles </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='italiano'\">Cronache </a>\n        </li>\n        <li>\n      </ul>\n\n      <!--<p class=\"introduccion\" *ngIf=\"idioma=='español'\">\n        ESPAÑOL Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='english'\">\n        ENGLISH Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='italiano'\">\n        ITALIANO Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>-->\n\n      <input class=\"form-control mr-sm-2\" type=\"search\" [placeholder]=\"placeholderinput\" [(ngModel)]=\"filter\" name=\"filterespanol\"\n        #inputFilterEsp>\n\n    </div>\n    <div class=\"col-sm-12\">\n      <div class=\"row\">\n        <div class=\"col-sm-4\" *ngFor=\"let item of cronicas | filter:filter\">\n          <div class=\"card articulo\" style=\"width: 100%;\">\n            <a class=\"image-card\" href=\"#/cronica-detalle/{{item.id}}\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\"></a>\n            <div class=\"card-body\">\n              <h5 class=\"card-title titulo\">{{item.titulo}}</h5>\n              <h6 class=\"card-subtitle mb-2 text-muted fecha\">{{item.createdAt.toDate() | date: 'mediumDate'}}</h6>\n              <p class=\"card-text\">{{item.resumen | slice:0:100}} ...</p>\n              <a class=\"link-ver\" *ngIf=\"idioma=='español'\" href=\"#/cronica-detalle/{{item.id}}\">Leer más</a>\n              <a class=\"link-ver\" *ngIf=\"idioma=='english'\" href=\"#/cronica-detalle/{{item.id}}\">Read more</a>\n              <a class=\"link-ver\" *ngIf=\"idioma=='italiano'\" href=\"#/cronica-detalle/{{item.id}}\">Leggi di più</a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</section>\n"

/***/ }),

/***/ "./src/app/paginas/cronicas/cronicas.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/paginas/cronicas/cronicas.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-image: url('background-banner.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 300px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px; }\n  .contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n  .foto-participante:hover {\n  opacity: .8; }\n  .card-body {\n  height: 200px; }\n  .card {\n  margin-bottom: 1em; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  .image-card {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position-y: top;\n  background-position-x: center;\n  height: 250px;\n  width: 100%;\n  cursor: pointer; }\n  .image-card:hover {\n  opacity: .8; }\n  .card-body {\n  min-height: 210px; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .card {\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.3); }\n"

/***/ }),

/***/ "./src/app/paginas/cronicas/cronicas.component.ts":
/*!********************************************************!*\
  !*** ./src/app/paginas/cronicas/cronicas.component.ts ***!
  \********************************************************/
/*! exports provided: CronicasPaginaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CronicasPaginaComponent", function() { return CronicasPaginaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CronicasPaginaComponent = /** @class */ (function () {
    function CronicasPaginaComponent(db, localstorage) {
        var _this = this;
        this.db = db;
        this.localstorage = localstorage;
        this.cronicas = [];
        this.idioma = "español";
        this.filter = "";
        this.filteredItems = [];
        this.placeholderinput = "Buscar cronica";
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
            switch (_this.idioma) {
                case "español":
                    _this.placeholderinput = "Buscar cronica";
                    break;
                case "english":
                    _this.placeholderinput = "Search chronicle";
                    break;
                case "español":
                    _this.placeholderinput = "Cerca cronache";
                    break;
            }
            _this.db
                .colWithIds$("cronicas", function (ref) {
                return ref.where("idioma", "==", _this.idioma);
            })
                .subscribe(function (resp) {
                _this.cronicas = resp;
            });
        });
        this.idioma = this.localstorage.getActualIdioma();
        switch (this.idioma) {
            case "español":
                this.placeholderinput = "Buscar participante";
                break;
            case "english":
                this.placeholderinput = "Search participant";
                break;
            case "español":
                this.placeholderinput = "Cerca partecipante";
                break;
        }
        this.db
            .colWithIds$("cronicas", function (ref) { return ref.where("idioma", "==", _this.idioma); })
            .subscribe(function (resp) {
            _this.cronicas = resp;
        });
    }
    CronicasPaginaComponent.prototype.ngOnInit = function () { };
    CronicasPaginaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-cronicas",
            template: __webpack_require__(/*! ./cronicas.component.html */ "./src/app/paginas/cronicas/cronicas.component.html"),
            styles: [__webpack_require__(/*! ./cronicas.component.scss */ "./src/app/paginas/cronicas/cronicas.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__["LocalstorageService"]])
    ], CronicasPaginaComponent);
    return CronicasPaginaComponent;
}());



/***/ }),

/***/ "./src/app/paginas/encuestas/encuestas.component.html":
/*!************************************************************!*\
  !*** ./src/app/paginas/encuestas/encuestas.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\">\n  <div class=\"overlay\">\n    <h1 *ngIf=\"idioma=='español'\">Congregación de la Pasión de Jesucristo</h1>\n    <h3 *ngIf=\"idioma=='español'\">Predicamos a Cristo crucificado</h3>\n    <h1 *ngIf=\"idioma=='english'\">Congregation of the Passion of Jesus Christ</h1>\n    <h3 *ngIf=\"idioma=='english'\">We preach Christ crucified</h3>\n    <h1 *ngIf=\"idioma=='italiano'\">Congregazione della Passione di Gesù Cristo</h1>\n    <h3 *ngIf=\"idioma=='italiano'\">Predichiamo Cristo crocifisso</h3>\n  </div>\n</div>\n\n<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\" style=\"margin-bottom: 2em;\">\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a class=\"tappable\" *ngIf=\"idioma=='español'\">Votaciones </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='english'\">Votes </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='italiano'\">Feedback </a>\n        </li>\n        <li>\n      </ul>\n\n      <!--<p class=\"introduccion\" *ngIf=\"idioma=='español'\">\n        ESPAÑOL Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='english'\">\n        ENGLISH Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='italiano'\">\n        ITALIANO Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>-->\n\n    </div>\n\n    <div class=\"col-sm-12\">\n      <div class=\"row\">\n        <div class=\"col-sm-6\" *ngFor=\"let item of encuestas | orderBy:'createdAt'\">\n          <votacion [info]=\"item\" [idioma]=\"idioma\"></votacion>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n</section>\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/encuestas.component.scss":
/*!************************************************************!*\
  !*** ./src/app/paginas/encuestas/encuestas.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-image: url('background-banner.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 300px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px; }\n  .contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n  .foto-participante:hover {\n  opacity: .8; }\n  .card-body {\n  height: 200px; }\n  .card {\n  margin-bottom: 1em; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  .image-card {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position-y: top;\n  background-position-x: center;\n  height: 250px;\n  width: 100%;\n  cursor: pointer; }\n  .image-card:hover {\n  opacity: .8; }\n  .card-body {\n  min-height: 210px; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .card {\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.3); }\n"

/***/ }),

/***/ "./src/app/paginas/encuestas/encuestas.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/paginas/encuestas/encuestas.component.ts ***!
  \**********************************************************/
/*! exports provided: EncuestasPaginaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EncuestasPaginaComponent", function() { return EncuestasPaginaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EncuestasPaginaComponent = /** @class */ (function () {
    function EncuestasPaginaComponent(db, localstorage) {
        var _this = this;
        this.db = db;
        this.localstorage = localstorage;
        this.idioma = "español";
        this.encuestas = [];
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
        this.db.colWithIds$('encuestas').subscribe(function (resp) {
            _this.encuestas = resp;
        });
    }
    EncuestasPaginaComponent.prototype.ngOnInit = function () { };
    EncuestasPaginaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-encuestas",
            template: __webpack_require__(/*! ./encuestas.component.html */ "./src/app/paginas/encuestas/encuestas.component.html"),
            styles: [__webpack_require__(/*! ./encuestas.component.scss */ "./src/app/paginas/encuestas/encuestas.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__["LocalstorageService"]])
    ], EncuestasPaginaComponent);
    return EncuestasPaginaComponent;
}());



/***/ }),

/***/ "./src/app/paginas/foro-detalle/foro-detalle.component.html":
/*!******************************************************************!*\
  !*** ./src/app/paginas/foro-detalle/foro-detalle.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\" [ngStyle]=\"{'background-image': 'url(' + foro.foto + ')'}\">\n</div>\n<div class=\"col-lg-10 col-sm-10 col-12 main-section\">\n  <div class=\"modal-content\">\n    <ul class=\"opciones-actividades\" style=\"margin-bottom: 2em;\">\n      <li class=\"active\">\n        <a class=\"tappable\">{{foro.titulo}} </a>\n      </li>\n      <li>\n    </ul>\n    <p>\n      <span class=\"badge badge-pill badge-danger\" *ngIf=\"idioma=='español'\">\n        {{foro.idioma|uppercase}}\n      </span>\n      <span class=\"badge badge-pill badge-primary\" *ngIf=\"idioma=='english'\">\n        {{foro.idioma|uppercase}}\n      </span>\n      <span class=\"badge badge-pill badge-success\" *ngIf=\"idioma=='italiano'\">\n        {{foro.idioma|uppercase}}\n      </span>\n      |\n      <span>{{foro.hilos}}\n        <fa [name]=\"'comment'\" style=\"color: rgba(0,0,0,.3)\"></fa>\n      </span>\n      |\n      <span *ngIf=\"idioma=='español'\">{{foro.createdAt | date:'mediumDate'}}</span>\n      <span *ngIf=\"idioma=='english'\">{{foro.createdAt | date:'mediumDate'}}</span>\n      <span *ngIf=\"idioma=='italiano'\">{{foro.createdAt | date:'mediumDate'}}</span>\n    </p>\n\n    <p [innerHTML]=\"foro.contenido\"></p>\n\n    <div class=\"input-group\" style=\"margin-bottom: 1em;\">\n      <div class=\"input-group-prepend\">\n        <span class=\"input-group-text\">\n          <div class=\"avatar\" [ngStyle]=\"{'background-image': 'url(' + user.foto + ')'}\"></div>\n        </span>\n      </div>\n      <textarea class=\"form-control\" [(ngModel)]=\"mensaje\" placeholder=\"Escribe tu comentario\"></textarea>\n      <div class=\"input-group-append\">\n        <button class=\"btn btn-outline-secondary\" type=\"button\" id=\"button-addon2\" (click)=\"enviar()\" [disabled]=\"!mensaje\">Enviar</button>\n      </div>\n    </div>\n\n    <ngb-alert type=\"success\" *ngIf=\"!staticAlertClosed\" (close)=\"staticAlertClosed = true\">Tú mensaje fue enviado exitosamente!</ngb-alert>\n\n\n    <div class=\"card\" *ngFor=\"let item of hilos\" style=\"margin-bottom: 10px;\">\n\n      <div class=\"card-body\" style=\"padding-bottom: 10px;\">\n        {{item.mensaje}}\n      </div>\n      <div class=\"card-title\">\n        <div class=\"avatar-user\" [ngStyle]=\"{'background-image': 'url(' + item.usuario.foto + ')'}\"></div>\n        <span>{{item.usuario.nombre}} | {{item.createdAt|date:'medium'}}</span>\n      </div>\n    </div>\n\n    <div class=\"col-lg-12 col-sm-12 col-12\">\n    </div>\n\n\n  </div>\n</div>\n\n\n<ngx-spinner *ngIf=\"idioma=='español'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"Un momento por favor\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='english'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"One moment, please\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='italiano'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"Un momento, per favore\"></ngx-spinner>\n"

/***/ }),

/***/ "./src/app/paginas/foro-detalle/foro-detalle.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/paginas/foro-detalle/foro-detalle.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".avatar {\n  width: 40px;\n  height: 40px;\n  border-radius: 40px;\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-position: center;\n  margin-right: 1em !important; }\n\n.card-title {\n  padding-left: 1.25rem;\n  display: inline-flex; }\n\n.card-title span {\n    margin-left: 1em;\n    font-size: 12px;\n    color: rgba(0, 0, 0, 0.5); }\n\n.avatar-user {\n  width: 20px;\n  height: 20px;\n  border-radius: 20px;\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-position: center; }\n\n.contenedor-image {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 400px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px;\n  background-attachment: fixed; }\n\n.contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n\n.contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n\n.contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n\n.opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n\n.opciones-actividades li {\n    margin: 0 0 0;\n    border-bottom: 4px solid #9F2820; }\n\n.opciones-actividades li a {\n      font-size: 3em;\n      color: #EE7835 !important;\n      cursor: initial; }\n\np.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n\n.actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n\n.card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n\n.col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n\n.col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n\n.card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n\n.contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n\n@-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n\n@keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n\n@-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n\n@keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n\n.btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n\n.titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n\n.fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n\n.link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n\n.link-ver:hover {\n  color: #9F2820 !important; }\n\n.link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n\n.articulo img {\n  cursor: pointer; }\n\n.articulo img:hover {\n  opacity: .8; }\n\n.foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n\n.foto-participante:hover {\n  opacity: .8; }\n\n.image-card {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position-y: top;\n  background-position-x: center;\n  height: 250px;\n  width: 100%;\n  cursor: pointer; }\n\n.image-card:hover {\n  opacity: .8; }\n\np.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n\n.actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n\n.card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n\n.col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n\n.col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n\n.card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n\n.contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n\n@keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n\n@keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n\n.btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n\n.titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n\n.fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n\n.link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n\n.link-ver:hover {\n  color: #9F2820 !important; }\n\n.link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n\n.articulo img {\n  cursor: pointer; }\n\n.articulo img:hover {\n  opacity: .8; }\n\n.card {\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.3); }\n\n.main-section {\n  margin: 0 auto;\n  top: -5em;\n  background-color: #fff;\n  border-radius: 5px;\n  padding: 0px;\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.5); }\n\n.modal-content {\n  padding: 2em; }\n\n.user-img {\n  margin-top: -50px;\n  display: inline-flex;\n  justify-content: center; }\n\n.user-img img {\n  height: 100px;\n  width: 100px;\n  background-color: #fff;\n  padding: 1em;\n  border-radius: 100px;\n  box-shadow: 4px 4px 8px rgba(0, 0, 0, 0.3); }\n\n.user-name {\n  margin: 10px 0px; }\n\n.user-name h1 {\n  font-size: 20px;\n  color: #676363;\n  padding-top: 1em; }\n\n.user-name button {\n  position: absolute;\n  top: -50px;\n  right: 20px;\n  font-size: 30px; }\n\n.form-input button {\n  width: 100%;\n  margin-bottom: 20px; }\n\n.link-part {\n  border-radius: 0px 0px 5px 5px;\n  background-color: #ECF0F1;\n  padding: 15px;\n  border-top: 1px solid #c2c2c2; }\n\n.open-modal {\n  margin-top: 100px !important; }\n\n.backdrop {\n  opacity: 1 !important;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-image: url('background-banner.jpg');\n  height: 100%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  opacity: .3;\n  background-color: #000; }\n"

/***/ }),

/***/ "./src/app/paginas/foro-detalle/foro-detalle.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/paginas/foro-detalle/foro-detalle.component.ts ***!
  \****************************************************************/
/*! exports provided: ForoDetalleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForoDetalleComponent", function() { return ForoDetalleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/@angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/ngx-spinner */ "./node_modules/ngx-spinner/ngx-spinner.umd.js");
/* harmony import */ var _node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _core_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../core/auth.service */ "./src/app/core/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ForoDetalleComponent = /** @class */ (function () {
    function ForoDetalleComponent(_route, db, localstorage, spinner, auth) {
        var _this = this;
        this._route = _route;
        this.db = db;
        this.localstorage = localstorage;
        this.spinner = spinner;
        this.auth = auth;
        this.foro = {};
        this.idioma = "español";
        this.user = {};
        this.staticAlertClosed = true;
        this.hilos = [];
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.auth.getCurrentUser().subscribe(function (user) {
            if (user) {
                if (user.length > 0) {
                    _this.user = user[0];
                }
            }
        });
        this.idioma = this.localstorage.getActualIdioma();
        this._route.params.subscribe(function (params) {
            _this.id = params.id;
            if (_this.id) {
                _this.spinner.show();
                setTimeout(function () {
                    _this.spinner.hide();
                }, 3000);
                _this.db.docWithRefs$("foros/" + _this.id).subscribe(function (res) {
                    _this.foro = res;
                    var consulta = _this.db
                        .colWithIds$("hilos-foros", function (ref) {
                        return ref.where("foroid", "==", _this.id);
                    })
                        .subscribe(function (hilos) {
                        _this.hilos = hilos;
                        _this.foro.hilos = hilos.length;
                        _this.hilos.forEach(function (element) {
                            var qryuser = _this.db.colWithIds$('asistentes', function (ref) { return ref.where('uid', '==', element.uid); }).subscribe(function (asistentes) {
                                if (asistentes.length > 0) {
                                    element.usuario = asistentes[0];
                                }
                            });
                        });
                    });
                });
            }
        });
    }
    ForoDetalleComponent.prototype.ngOnInit = function () { };
    ForoDetalleComponent.prototype.enviar = function () {
        var _this = this;
        this.db
            .add("hilos-foros", {
            uid: this.user.uid,
            mensaje: this.mensaje,
            foroid: this.id
        })
            .then(function () {
            _this.mensaje = "";
            _this.staticAlertClosed = false;
        });
    };
    ForoDetalleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-foro-detalle",
            template: __webpack_require__(/*! ./foro-detalle.component.html */ "./src/app/paginas/foro-detalle/foro-detalle.component.html"),
            styles: [__webpack_require__(/*! ./foro-detalle.component.scss */ "./src/app/paginas/foro-detalle/foro-detalle.component.scss")]
        }),
        __metadata("design:paramtypes", [_node_modules_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _core_resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_3__["LocalstorageService"],
            _node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"],
            _core_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]])
    ], ForoDetalleComponent);
    return ForoDetalleComponent;
}());



/***/ }),

/***/ "./src/app/paginas/foros/foros.component.html":
/*!****************************************************!*\
  !*** ./src/app/paginas/foros/foros.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\">\n  <div class=\"overlay\">\n    <h1 *ngIf=\"idioma=='español'\">Congregación de la Pasión de Jesucristo</h1>\n    <h3 *ngIf=\"idioma=='español'\">Predicamos a Cristo crucificado</h3>\n    <h1 *ngIf=\"idioma=='english'\">Congregation of the Passion of Jesus Christ</h1>\n    <h3 *ngIf=\"idioma=='english'\">We preach Christ crucified</h3>\n    <h1 *ngIf=\"idioma=='italiano'\">Congregazione della Passione di Gesù Cristo</h1>\n    <h3 *ngIf=\"idioma=='italiano'\">Predichiamo Cristo crocifisso</h3>\n  </div>\n</div>\n\n<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\" style=\"margin-bottom: 2em;\">\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a class=\"tappable\" *ngIf=\"idioma=='español'\">Foros </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='english'\">Forums </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='italiano'\">Forum </a>\n        </li>\n        <li>\n      </ul>\n\n      <!--<p class=\"introduccion\" *ngIf=\"idioma=='español'\">\n        ESPAÑOL Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='english'\">\n        ENGLISH Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='italiano'\">\n        ITALIANO Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere\n        sem ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis\n        congue nisl, nec vulputate nunc.\n      </p>-->\n\n      <input class=\"form-control mr-sm-2\" type=\"search\" [placeholder]=\"placeholderinput\" [(ngModel)]=\"filter\" name=\"filterespanol\"\n        #inputFilterEsp>\n\n    </div>\n    <div class=\"col-sm-12\">\n      <div class=\"row\">\n        <div class=\"col-sm-4\" *ngFor=\"let item of foros | filter:filter\">\n          <div class=\"card articulo\" style=\"width: 100%;\">\n            <a class=\"image-card\" href=\"#/foro-detalle/{{item.id}}\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\"></a>\n            <div class=\"card-body\">\n              <h5 class=\"card-title titulo\">{{item.titulo}}</h5>\n              <h6 class=\"card-subtitle mb-2 text-muted fecha\">{{item.createdAt | date: 'mediumDate'}}</h6>\n              <p class=\"card-text\">{{item.objetivo}}</p>\n              <span class=\"badge badge-pill badge-danger\" *ngIf=\"item.idioma=='español'\">\n                {{item.idioma|uppercase}}\n              </span>\n              <span class=\"badge badge-pill badge-primary\" *ngIf=\"item.idioma=='ingles'\">\n                English\n              </span>\n              <span class=\"badge badge-pill badge-success\" *ngIf=\"item.idioma=='italiano'\">\n                {{item.idioma|uppercase}}\n              </span>\n               |\n              <span>{{item.hilos}} <fa [name]=\"'comment'\"></fa></span>\n               |\n              <span *ngIf=\"idioma=='español'\">{{item.createdAt | date:'mediumDate'}}</span>\n              <span *ngIf=\"idioma=='english'\">{{item.createdAt | date:'mediumDate'}}</span>\n              <span *ngIf=\"idioma=='italiano'\">{{item.createdAt | date:'mediumDate'}}</span>\n\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</section>\n"

/***/ }),

/***/ "./src/app/paginas/foros/foros.component.scss":
/*!****************************************************!*\
  !*** ./src/app/paginas/foros/foros.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-image: url('background-banner.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 300px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px; }\n  .contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n  .foto-participante:hover {\n  opacity: .8; }\n  .card-body {\n  height: 200px; }\n  .card {\n  margin-bottom: 1em; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  .image-card {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position-y: top;\n  background-position-x: center;\n  height: 250px;\n  width: 100%;\n  cursor: pointer; }\n  .image-card:hover {\n  opacity: .8; }\n  .card-body {\n  min-height: 210px; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .card {\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.3); }\n"

/***/ }),

/***/ "./src/app/paginas/foros/foros.component.ts":
/*!**************************************************!*\
  !*** ./src/app/paginas/foros/foros.component.ts ***!
  \**************************************************/
/*! exports provided: ForosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForosComponent", function() { return ForosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ForosComponent = /** @class */ (function () {
    function ForosComponent(db, localstorage) {
        var _this = this;
        this.db = db;
        this.localstorage = localstorage;
        this.foros = [];
        this.idioma = "español";
        this.filter = "";
        this.filteredItems = [];
        this.placeholderinput = "Buscar proyecto";
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
            switch (_this.idioma) {
                case "español":
                    _this.placeholderinput = "Buscar foro";
                    break;
                case "english":
                    _this.placeholderinput = "Search forum";
                    break;
                case "español":
                    _this.placeholderinput = "Cerca forum";
                    break;
            }
            _this.db
                .colWithIds$("foros")
                .subscribe(function (resp) {
                _this.foros = resp;
            });
        });
        this.idioma = this.localstorage.getActualIdioma();
        switch (this.idioma) {
            case "español":
                this.placeholderinput = "Buscar foro";
                break;
            case "english":
                this.placeholderinput = "Search forum";
                break;
            case "español":
                this.placeholderinput = "Cerca forum";
                break;
        }
        this.db
            .colWithIds$("foros")
            .subscribe(function (resp) {
            resp.forEach(function (element) {
                var consulta = _this.db.colWithIds$('hilos-foros', function (ref) { return ref.where('foroid', '==', element.id); }).subscribe(function (hilos) {
                    consulta.unsubscribe();
                    element.hilos = hilos.length;
                });
            });
            _this.foros = resp;
        });
    }
    ForosComponent.prototype.ngOnInit = function () { };
    ForosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-foros",
            template: __webpack_require__(/*! ./foros.component.html */ "./src/app/paginas/foros/foros.component.html"),
            styles: [__webpack_require__(/*! ./foros.component.scss */ "./src/app/paginas/foros/foros.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__["LocalstorageService"]])
    ], ForosComponent);
    return ForosComponent;
}());



/***/ }),

/***/ "./src/app/paginas/grupos/grupos.component.html":
/*!******************************************************!*\
  !*** ./src/app/paginas/grupos/grupos.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\">\n  <div class=\"overlay\">\n    <h1 *ngIf=\"idioma=='español'\">Congregación de la Pasión de Jesucristo</h1>\n    <h3 *ngIf=\"idioma=='español'\">Predicamos a Cristo crucificado</h3>\n    <h1 *ngIf=\"idioma=='english'\">Congregation of the Passion of Jesus Christ</h1>\n    <h3 *ngIf=\"idioma=='english'\">We preach Christ crucified</h3>\n    <h1 *ngIf=\"idioma=='italiano'\">Congregazione della Passione di Gesù Cristo</h1>\n    <h3 *ngIf=\"idioma=='italiano'\">Predichiamo Cristo crocifisso</h3>\n  </div>\n</div>\n\n<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\" style=\"margin-bottom: 2em;\">\n      <input class=\"form-control mb-4 mr-sm-2\" type=\"search\" [placeholder]=\"placeholderinput\" [(ngModel)]=\"filter\" name=\"filterespanol\"\n        #inputFilterEsp>\n\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a *ngIf=\"idioma=='español'\">Grupos</a>\n          <a *ngIf=\"idioma=='english'\">Groups</a>\n          <a *ngIf=\"idioma=='italiano'\">Gruppi</a>\n        </li>\n        <li>\n      </ul>\n\n\n    </div>\n    <div class=\"col-sm-12\">\n      <div class=\"row\">\n        <div class=\"col-sm-3\" *ngFor=\"let item of grupos | filter:filter\">\n          <div class=\"card articulo\" style=\"width: 100%;\" [routerLink]=\"['/groups/participants',item.id]\">\n            <div class=\"card-body\">\n              <h4 class=\"card-title titulo\">{{item.nombre}}</h4>\n              <h6 class=\"card-subtitle text-muted fecha\">{{item.descripcion | slice:0:200}}</h6>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n</section>\n"

/***/ }),

/***/ "./src/app/paginas/grupos/grupos.component.scss":
/*!******************************************************!*\
  !*** ./src/app/paginas/grupos/grupos.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-image: url('background-banner.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 300px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px; }\n  .contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.articulo {\n  width: 100%;\n  height: 120px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer;\n  color: #fff !important;\n  text-shadow: 0 4px 8px rgba(0, 0, 0, 0.5); }\n  .card.articulo:hover {\n    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.5); }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: #fff;\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: #fff !important;\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n  .foto-participante:hover {\n  opacity: .8; }\n  .card-body {\n  height: 200px; }\n  .card {\n  margin-bottom: 1em; }\n"

/***/ }),

/***/ "./src/app/paginas/grupos/grupos.component.ts":
/*!****************************************************!*\
  !*** ./src/app/paginas/grupos/grupos.component.ts ***!
  \****************************************************/
/*! exports provided: GruposComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GruposComponent", function() { return GruposComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GruposComponent = /** @class */ (function () {
    function GruposComponent(db, localstorage) {
        var _this = this;
        this.db = db;
        this.localstorage = localstorage;
        this.grupos = [];
        this.staff = [];
        this.idioma = "español";
        this.filter = "";
        this.filteredItems = [];
        this.placeholderinput = "Buscar participante";
        this.db.colWithIds$("grupos").subscribe(function (resp) {
            _this.grupos = resp;
        });
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
            switch (_this.idioma) {
                case "español":
                    _this.placeholderinput = "Buscar";
                    break;
                case "english":
                    _this.placeholderinput = "Search";
                    break;
                case "español":
                    _this.placeholderinput = "Cerca";
                    break;
            }
        });
        this.idioma = this.localstorage.getActualIdioma();
        switch (this.idioma) {
            case "español":
                this.placeholderinput = "Buscar";
                break;
            case "english":
                this.placeholderinput = "Search";
                break;
            case "español":
                this.placeholderinput = "Cerca";
                break;
        }
    }
    GruposComponent.prototype.ngOnInit = function () { };
    GruposComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-grupos",
            template: __webpack_require__(/*! ./grupos.component.html */ "./src/app/paginas/grupos/grupos.component.html"),
            styles: [__webpack_require__(/*! ./grupos.component.scss */ "./src/app/paginas/grupos/grupos.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__["LocalstorageService"]])
    ], GruposComponent);
    return GruposComponent;
}());



/***/ }),

/***/ "./src/app/paginas/home/home.component.html":
/*!**************************************************!*\
  !*** ./src/app/paginas/home/home.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<hero-banner></hero-banner>\n<mensaje-papa></mensaje-papa>\n<section class=\"barra-opciones-grupo\">\n  <div class=\"row\">\n    <div class=\"col-sm-6 btn-right\">\n      <button type=\"button\" class=\"btn btn-default btn-lg\" routerLinkActive=\"active\" [routerLink]=\"['/participants']\">\n        <span *ngIf=\"idioma=='español'\">Participantes</span>\n        <span *ngIf=\"idioma=='english'\">Participants</span>\n        <span *ngIf=\"idioma=='italiano'\">Partecipanti</span>\n      </button>\n    </div>\n    <div class=\"col-sm-6 btn-left\">\n      <button type=\"button\" class=\"btn btn-default btn-lg\" routerLinkActive=\"active\" [routerLink]=\"['/groups']\">\n        <span *ngIf=\"idioma=='español'\">Grupos de trabajo</span>\n        <span *ngIf=\"idioma=='english'\">Work groups</span>\n        <span *ngIf=\"idioma=='italiano'\">Gruppi di lavoro</span>\n      </button>\n    </div>\n  </div>\n</section>\n\n<agenda id=\"agenda\"></agenda>\n<liturgias id=\"liturgias\"></liturgias>\n<cronicas id=\"cronicas\"></cronicas>\n\n<section class=\"barra-opciones-grupo\">\n  <div class=\"row\">\n    <div class=\"col-sm-6 btn-right\">\n      <button type=\"button\" class=\"btn btn-default btn-lg\" [routerLink]=\"['/foros']\">\n        <span *ngIf=\"idioma=='español'\">Foros</span>\n        <span *ngIf=\"idioma=='english'\">Forums</span>\n        <span *ngIf=\"idioma=='italiano'\">Forum</span>\n      </button>\n    </div>\n    <div class=\"col-sm-6 btn-left\">\n      <button type=\"button\" class=\"btn btn-default btn-lg\" routerLinkActive=\"active\" [routerLink]=\"['/votaciones']\">\n        <span *ngIf=\"idioma=='español'\">Votaciones</span>\n        <span *ngIf=\"idioma=='english'\">Votes</span>\n        <span *ngIf=\"idioma=='italiano'\">Feedback</span>\n      </button>\n    </div>\n  </div>\n</section>\n\n<biblioteca id=\"articulos\"></biblioteca>\n<proyectos id=\"proyectos\"></proyectos>\n\n<section class=\"barra-opciones-grupo\" style=\"height: 50px;\">\n  <div class=\"row\">\n  </div>\n</section>\n"

/***/ }),

/***/ "./src/app/paginas/home/home.component.scss":
/*!**************************************************!*\
  !*** ./src/app/paginas/home/home.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".barra-opciones-grupo {\n  height: 150px;\n  background-color: #9F2820;\n  padding-top: 51px; }\n  .barra-opciones-grupo .btn {\n    width: 200px;\n    color: #9F2820; }\n  .barra-opciones-grupo .btn:hover {\n    opacity: .8; }\n  .barra-opciones-grupo .btn-right {\n    text-align: right; }\n  .barra-opciones-grupo .btn-left {\n    text-align: left; }\n  a:target ~ #articulos section {\n  -webkit-transform: translateY(0px);\n  transform: translateY(0px); }\n  a[id=\"galeria\"]:target ~ #main section {\n  -webkit-transform: translateY(-500px);\n  transform: translateY(-500px); }\n  a[id=\"contacto\"]:target ~ #main section {\n  -webkit-transform: translateY(-1000px);\n  transform: translateY(-1000px); }\n"

/***/ }),

/***/ "./src/app/paginas/home/home.component.ts":
/*!************************************************!*\
  !*** ./src/app/paginas/home/home.component.ts ***!
  \************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = /** @class */ (function () {
    function HomeComponent(localstorage) {
        var _this = this;
        this.localstorage = localstorage;
        this.idioma = 'español';
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/paginas/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/paginas/home/home.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_localstorage_service__WEBPACK_IMPORTED_MODULE_1__["LocalstorageService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/paginas/inicio/inicio.component.html":
/*!******************************************************!*\
  !*** ./src/app/paginas/inicio/inicio.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <div class=\"backdrop\"></div>\n\n  <div class=\"col-lg-4 col-sm-8 col-12 main-section\">\n    <div class=\"modal-content\">\n      <div class=\"col-lg-12 col-sm-12 col-12 user-img\">\n        <img src=\"assets/LogoEsp.svg\">\n      </div>\n      <div class=\"col-lg-12 col-sm-12 col-12 user-name\">\n        <h1>Ingreso / Entry / Ingresso</h1>\n      </div>\n      <div class=\"col-lg-12 col-sm-12 col-12 form-input\">\n        <form>\n          <div class=\"form-group\">\n            <input type=\"email\" class=\"form-control\" placeholder=\"Correo / Email\" name=\"email\" [(ngModel)]=\"info.email\">\n          </div>\n          <div class=\"form-group\">\n            <input type=\"password\" class=\"form-control\" placeholder=\"Contraseña / Password\" name=\"password\" [(ngModel)]=\"info.password\">\n          </div>\n          <div class=\"input-group mb-3\">\n            <select class=\"custom-select\" id=\"inputGroupSelect01\" name=\"idioma\" [(ngModel)]=\"info.idioma\">\n              <option selected value=\"\">Idioma / Language / Lingua</option>\n              <option value=\"español\">Español</option>\n              <option value=\"english\">English</option>\n              <option value=\"italiano\">Italiano</option>\n            </select>\n          </div>\n          <button type=\"submit\" class=\"btn btn-success\" (click)=\"login()\" [disabled]=\"!info.email || !info.password || !info.idioma\">Ingresar / Enter / Accedere</button>\n        </form>\n      </div>\n    </div>\n  </div>\n"

/***/ }),

/***/ "./src/app/paginas/inicio/inicio.component.scss":
/*!******************************************************!*\
  !*** ./src/app/paginas/inicio/inicio.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  font-family: 'Barlow Semi Condensed', sans-serif; }\n\n.main-section {\n  margin: 0 auto;\n  margin-top: 15%;\n  background-color: #fff;\n  border-radius: 5px;\n  padding: 0px;\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.5); }\n\n.user-img {\n  margin-top: -50px;\n  display: inline-flex;\n  justify-content: center; }\n\n.user-img img {\n  height: 100px;\n  width: 100px;\n  background-color: #fff;\n  padding: 1em;\n  border-radius: 100px;\n  box-shadow: 4px 4px 8px rgba(0, 0, 0, 0.3); }\n\n.user-name {\n  margin: 10px 0px; }\n\n.user-name h1 {\n  font-size: 20px;\n  color: #676363;\n  padding-top: 1em; }\n\n.user-name button {\n  position: absolute;\n  top: -50px;\n  right: 20px;\n  font-size: 30px; }\n\n.form-input button {\n  width: 100%;\n  margin-bottom: 20px; }\n\n.link-part {\n  border-radius: 0px 0px 5px 5px;\n  background-color: #ECF0F1;\n  padding: 15px;\n  border-top: 1px solid #c2c2c2; }\n\n.open-modal {\n  margin-top: 100px !important; }\n\n.backdrop {\n  opacity: 1 !important;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-image: url('background-banner.jpg');\n  height: 100%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  opacity: .3;\n  background-color: #000; }\n"

/***/ }),

/***/ "./src/app/paginas/inicio/inicio.component.ts":
/*!****************************************************!*\
  !*** ./src/app/paginas/inicio/inicio.component.ts ***!
  \****************************************************/
/*! exports provided: InicioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioComponent", function() { return InicioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/auth.service */ "./src/app/core/auth.service.ts");
/* harmony import */ var _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/@angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/ngx-spinner.umd.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ngx_spinner__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var InicioComponent = /** @class */ (function () {
    function InicioComponent(auth, _router, spinner, localstorage) {
        var _this = this;
        this.auth = auth;
        this._router = _router;
        this.spinner = spinner;
        this.localstorage = localstorage;
        this.info = {
            idioma: 'español'
        };
        this.localstorage.getUser().subscribe(function (event) {
            _this._router.navigate(['/inicio']);
        });
        this.auth.getCurrentUser().subscribe(function (user) {
            if (user) {
                if (user.length > 0) {
                    _this._router.navigate(['/inicio']);
                }
            }
        });
    }
    InicioComponent.prototype.ngOnInit = function () {
    };
    InicioComponent.prototype.login = function () {
        var _this = this;
        this.auth.emailLogin(this.info.email, this.info.password).then(function (user) {
            _this.localstorage.setUser(user);
            _this.localstorage.setIdioma(_this.info.idioma);
            _this.spinner.show();
            setTimeout(function () {
                _this.spinner.hide();
                _this._router.navigate(['/inicio']);
            }, 3000);
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()({
                type: "error",
                title: "Error en ingreso / Entry error / Errore di immissione",
                text: "Usuario o contraseña incorrectos / Incorrect username or password / Nome utente o password errati ",
                confirmButtonText: "Ok",
            }).then(function () { }, function (dismiss) { });
        });
    };
    InicioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-inicio',
            template: __webpack_require__(/*! ./inicio.component.html */ "./src/app/paginas/inicio/inicio.component.html"),
            styles: [__webpack_require__(/*! ./inicio.component.scss */ "./src/app/paginas/inicio/inicio.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_3__["NgxSpinnerService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_4__["LocalstorageService"]])
    ], InicioComponent);
    return InicioComponent;
}());



/***/ }),

/***/ "./src/app/paginas/proyecto-detalle/proyecto-detalle.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/paginas/proyecto-detalle/proyecto-detalle.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\" [ngStyle]=\"{'background-image': 'url(' + proyecto.foto + ')'}\">\n</div>\n<div class=\"col-lg-10 col-sm-10 col-12 main-section\">\n  <div class=\"modal-content\">\n    <ul class=\"opciones-actividades\" style=\"margin-bottom: 2em;\">\n      <li class=\"active\">\n        <a class=\"tappable\">{{proyecto.titulo}} </a>\n      </li>\n      <li>\n    </ul>\n    <p [innerHTML]=\"proyecto.contenido\">\n    </p>\n\n    <iframe [src]='urlselect' style=\"height:80vh; width: 100%;\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen\n      allowfullscreen>\n    </iframe>\n\n  </div>\n</div>\n\n\n<ngx-spinner *ngIf=\"idioma=='español'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"Un momento por favor\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='english'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"One moment, please\"></ngx-spinner>\n<ngx-spinner *ngIf=\"idioma=='italiano'\" bdColor=\"rgba(159,40,32,1)\" size=\"default\" color=\"#ffffff\" type=\"ball-fussion\" loadingText=\"Un momento, per favore\"></ngx-spinner>\n"

/***/ }),

/***/ "./src/app/paginas/proyecto-detalle/proyecto-detalle.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/paginas/proyecto-detalle/proyecto-detalle.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 400px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px;\n  background-attachment: fixed; }\n  .contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      font-size: 3em;\n      color: #EE7835 !important;\n      cursor: initial; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n  .foto-participante:hover {\n  opacity: .8; }\n  .card-body {\n  height: 200px; }\n  .card {\n  margin-bottom: 1em; }\n  .image-card {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position-y: top;\n  background-position-x: center;\n  height: 250px;\n  width: 100%;\n  cursor: pointer; }\n  .image-card:hover {\n  opacity: .8; }\n  .card-body {\n  min-height: 210px; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .card {\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.3); }\n  .main-section {\n  margin: 0 auto;\n  top: -5em;\n  background-color: #fff;\n  border-radius: 5px;\n  padding: 0px;\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.5); }\n  .modal-content {\n  padding: 2em; }\n  .user-img {\n  margin-top: -50px;\n  display: inline-flex;\n  justify-content: center; }\n  .user-img img {\n  height: 100px;\n  width: 100px;\n  background-color: #fff;\n  padding: 1em;\n  border-radius: 100px;\n  box-shadow: 4px 4px 8px rgba(0, 0, 0, 0.3); }\n  .user-name {\n  margin: 10px 0px; }\n  .user-name h1 {\n  font-size: 20px;\n  color: #676363;\n  padding-top: 1em; }\n  .user-name button {\n  position: absolute;\n  top: -50px;\n  right: 20px;\n  font-size: 30px; }\n  .form-input button {\n  width: 100%;\n  margin-bottom: 20px; }\n  .link-part {\n  border-radius: 0px 0px 5px 5px;\n  background-color: #ECF0F1;\n  padding: 15px;\n  border-top: 1px solid #c2c2c2; }\n  .open-modal {\n  margin-top: 100px !important; }\n  .backdrop {\n  opacity: 1 !important;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-image: url('background-banner.jpg');\n  height: 100%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  opacity: .3;\n  background-color: #000; }\n"

/***/ }),

/***/ "./src/app/paginas/proyecto-detalle/proyecto-detalle.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/paginas/proyecto-detalle/proyecto-detalle.component.ts ***!
  \************************************************************************/
/*! exports provided: ProyectoDetalleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProyectoDetalleComponent", function() { return ProyectoDetalleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/@angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
/* harmony import */ var _node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/ngx-spinner */ "./node_modules/ngx-spinner/ngx-spinner.umd.js");
/* harmony import */ var _node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProyectoDetalleComponent = /** @class */ (function () {
    function ProyectoDetalleComponent(_route, db, localstorage, spinner, sanitizer) {
        var _this = this;
        this._route = _route;
        this.db = db;
        this.localstorage = localstorage;
        this.spinner = spinner;
        this.sanitizer = sanitizer;
        this.proyecto = {};
        this.idioma = "español";
        this.urlselect = "";
        this.type = "application/pdf";
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
        });
        this.idioma = this.localstorage.getActualIdioma();
        this._route.params.subscribe(function (params) {
            _this.id = params.id;
            if (_this.id) {
                _this.spinner.show();
                setTimeout(function () {
                    _this.spinner.hide();
                }, 3000);
                _this.db.docWithRefs$("proyectos/" + _this.id).subscribe(function (res) {
                    _this.proyecto = res;
                    _this.selecturl();
                });
            }
        });
    }
    ProyectoDetalleComponent.prototype.ngOnInit = function () { };
    ProyectoDetalleComponent.prototype.selecturl = function () {
        var _this = this;
        this.urlselect = this.sanitizer.bypassSecurityTrustResourceUrl(this.proyecto.documentourl);
        this.spinner.show();
        setTimeout(function () {
            _this.spinner.hide();
        }, 3000);
    };
    ProyectoDetalleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-proyecto-detalle",
            template: __webpack_require__(/*! ./proyecto-detalle.component.html */ "./src/app/paginas/proyecto-detalle/proyecto-detalle.component.html"),
            styles: [__webpack_require__(/*! ./proyecto-detalle.component.scss */ "./src/app/paginas/proyecto-detalle/proyecto-detalle.component.scss")]
        }),
        __metadata("design:paramtypes", [_node_modules_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _core_resources_service__WEBPACK_IMPORTED_MODULE_2__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_3__["LocalstorageService"],
            _node_modules_ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"]])
    ], ProyectoDetalleComponent);
    return ProyectoDetalleComponent;
}());



/***/ }),

/***/ "./src/app/paginas/proyectos/proyectos.component.html":
/*!************************************************************!*\
  !*** ./src/app/paginas/proyectos/proyectos.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<barra-navegacion></barra-navegacion>\n<div class=\"contenedor-image\">\n  <div class=\"overlay\">\n    <h1 *ngIf=\"idioma=='español'\">Congregación de la Pasión de Jesucristo</h1>\n    <h3 *ngIf=\"idioma=='español'\">Predicamos a Cristo crucificado</h3>\n    <h1 *ngIf=\"idioma=='english'\">Congregation of the Passion of Jesus Christ</h1>\n    <h3 *ngIf=\"idioma=='english'\">We preach Christ crucified</h3>\n    <h1 *ngIf=\"idioma=='italiano'\">Congregazione della Passione di Gesù Cristo</h1>\n    <h3 *ngIf=\"idioma=='italiano'\">Predichiamo Cristo crocifisso</h3>\n  </div>\n</div>\n\n<section class=\"actividades\">\n\n  <div class=\"row contenedor-mensaje\">\n    <div class=\"col-sm-12\" style=\"margin-bottom: 2em;\">\n      <ul class=\"opciones-actividades\">\n        <li class=\"active\">\n          <a class=\"tappable\" *ngIf=\"idioma=='español'\">Proyectos </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='english'\">Projects </a>\n          <a class=\"tappable\" *ngIf=\"idioma=='italiano'\">Progetti </a>\n        </li>\n        <li>\n      </ul>\n\n      <!--<p class=\"introduccion\" *ngIf=\"idioma=='español'\">\n        ESPAÑOL Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere sem\n        ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis congue\n        nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='english'\">\n        ENGLISH Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere sem\n        ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis congue\n        nisl, nec vulputate nunc.\n      </p>\n\n      <p class=\"introduccion\" *ngIf=\"idioma=='italiano'\">\n        ITALIANO Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum mauris vitae felis imperdiet porttitor.\n        Phasellus in erat iaculis, mollis dui a, auctor nisi. Suspendisse consequat elit in augue pretium, sit amet posuere sem\n        ultrices. Suspendisse maximus aliquet feugiat. Nam fringilla nunc purus, et dapibus tellus elementum eu. In quis congue\n        nisl, nec vulputate nunc.\n      </p>-->\n\n      <input class=\"form-control mr-sm-2\" type=\"search\" [placeholder]=\"placeholderinput\" [(ngModel)]=\"filter\" name=\"filterespanol\"\n        #inputFilterEsp>\n\n    </div>\n    <div class=\"col-sm-12\">\n      <div class=\"row\">\n        <div class=\"col-sm-4\" *ngFor=\"let item of proyectos | filter:filter\">\n          <div class=\"card articulo\" style=\"width: 100%;\">\n            <a class=\"image-card\" href=\"#/proyecto-detalle/{{item.id}}\" [ngStyle]=\"{'background-image': 'url(' + item.foto + ')'}\"></a>\n            <div class=\"card-body\">\n              <h5 class=\"card-title titulo\">{{item.titulo}}</h5>\n              <h6 class=\"card-subtitle mb-2 text-muted fecha\">{{item.createdAt | date: 'mediumDate'}}</h6>\n              <p class=\"card-text\">{{item.resumen | slice:0:100}} ...</p>\n              <a class=\"link-ver\" *ngIf=\"idioma=='español'\" href=\"#/proyecto-detalle/{{item.id}}\">Leer más</a>\n              <a class=\"link-ver\" *ngIf=\"idioma=='english'\" href=\"#/proyecto-detalle/{{item.id}}\">Read more</a>\n              <a class=\"link-ver\" *ngIf=\"idioma=='italiano'\" href=\"#/proyecto-detalle/{{item.id}}\">Leggi di più</a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</section>\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/proyectos.component.scss":
/*!************************************************************!*\
  !*** ./src/app/paginas/proyectos/proyectos.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenedor-image {\n  background-image: url('background-banner.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: top;\n  background-attachment: fixed;\n  height: 300px;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  text-align: center;\n  margin-top: 114px; }\n  .contenedor-image .overlay {\n    padding-top: 150px;\n    position: absolute;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n  .contenedor-image .overlay h1 {\n      color: #fff;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .contenedor-image .overlay h3 {\n      color: #EE7835;\n      text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n      z-index: 999999; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @-webkit-keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @-webkit-keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .foto-participante {\n  height: 200px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  cursor: pointer; }\n  .foto-participante:hover {\n  opacity: .8; }\n  .card-body {\n  height: 200px; }\n  .card {\n  margin-bottom: 1em; }\n  .opciones-actividades {\n  padding: 0;\n  list-style: none;\n  display: inline-flex; }\n  .opciones-actividades li {\n    margin: 0 0 0;\n    font-size: 3em;\n    border-bottom: 4px solid #9F2820; }\n  .opciones-actividades li a {\n      color: #EE7835 !important; }\n  .opciones-actividades li a:hover {\n      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);\n      text-decoration: none !important; }\n  .image-card {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position-y: top;\n  background-position-x: center;\n  height: 250px;\n  width: 100%;\n  cursor: pointer; }\n  .image-card:hover {\n  opacity: .8; }\n  .card-body {\n  min-height: 210px; }\n  p.introduccion {\n  color: #565857;\n  font-family: \"Roboto\";\n  text-align: justify; }\n  .actividades {\n  padding: 2em 4em !important;\n  background-color: #fff; }\n  .card.liturgias {\n  width: 100%;\n  height: 200px;\n  background-image: url('mock-background-liturgia.png');\n  background-size: cover;\n  border: 0;\n  margin-bottom: 1em;\n  cursor: pointer; }\n  .col-sm-6.card-liturgia {\n  padding-right: 0;\n  margin-right: 0;\n  position: relative; }\n  .col-sm-6.card-liturgia .card-title {\n    position: absolute;\n    bottom: 0;\n    top: initial;\n    left: 0;\n    padding: 10px;\n    background: rgba(159, 40, 32, 0.8);\n    right: 0;\n    margin: 0;\n    color: #fff;\n    font-weight: 100;\n    text-align: center;\n    transition: 3s;\n    -webkit-animation: decrecer 1s;\n            animation: decrecer 1s; }\n  .card-liturgia:hover .card-title {\n  top: 0;\n  -webkit-animation: crecer .5s;\n          animation: crecer .5s; }\n  .contenedor-documento {\n  height: 100%;\n  background-color: #9F2820; }\n  @keyframes crecer {\n  from {\n    top: 100px; }\n  to {\n    top: 0; } }\n  @keyframes decrecer {\n  from {\n    top: 0; }\n  to {\n    top: 78%; } }\n  .btn-ver {\n  padding-right: 2em;\n  padding-left: 2em; }\n  .titulo {\n  font-size: 22px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 500; }\n  .fecha {\n  font-size: 14px !important;\n  color: rgba(0, 0, 0, 0.5);\n  font-weight: 100; }\n  .link-ver {\n  cursor: pointer;\n  width: 100%;\n  float: right;\n  text-align: right;\n  color: black; }\n  .link-ver:hover {\n  color: #9F2820 !important; }\n  .link-ver::before {\n  content: \"\";\n  display: inline-block;\n  height: 0.8em;\n  vertical-align: bottom;\n  width: 75%;\n  margin-left: -100%;\n  margin-right: 10px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5); }\n  .articulo img {\n  cursor: pointer; }\n  .articulo img:hover {\n  opacity: .8; }\n  .card {\n  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.3); }\n"

/***/ }),

/***/ "./src/app/paginas/proyectos/proyectos.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/paginas/proyectos/proyectos.component.ts ***!
  \**********************************************************/
/*! exports provided: ProyectosPaginaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProyectosPaginaComponent", function() { return ProyectosPaginaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_resources_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/resources.service */ "./src/app/core/resources.service.ts");
/* harmony import */ var _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/localstorage.service */ "./src/app/core/localstorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProyectosPaginaComponent = /** @class */ (function () {
    function ProyectosPaginaComponent(db, localstorage) {
        var _this = this;
        this.db = db;
        this.localstorage = localstorage;
        this.proyectos = [];
        this.idioma = "español";
        this.filter = "";
        this.filteredItems = [];
        this.placeholderinput = "Buscar proyecto";
        this.localstorage.getIdioma().subscribe(function (event) {
            _this.idioma = event.newValue;
            switch (_this.idioma) {
                case "español":
                    _this.placeholderinput = "Buscar proyecto";
                    break;
                case "english":
                    _this.placeholderinput = "Search project";
                    break;
                case "español":
                    _this.placeholderinput = "Cerca progetti";
                    break;
            }
            _this.db.colWithIds$("proyectos", function (ref) { return ref.where('idioma', '==', _this.idioma); }).subscribe(function (resp) {
                _this.proyectos = resp;
            });
        });
        this.idioma = this.localstorage.getActualIdioma();
        switch (this.idioma) {
            case "español":
                this.placeholderinput = "Buscar proyecto";
                break;
            case "english":
                this.placeholderinput = "Search project";
                break;
            case "español":
                this.placeholderinput = "Cerca progetti";
                break;
        }
        this.db.colWithIds$("proyectos", function (ref) { return ref.where('idioma', '==', _this.idioma); }).subscribe(function (resp) {
            _this.proyectos = resp;
        });
    }
    ProyectosPaginaComponent.prototype.ngOnInit = function () { };
    ProyectosPaginaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-proyectos',
            template: __webpack_require__(/*! ./proyectos.component.html */ "./src/app/paginas/proyectos/proyectos.component.html"),
            styles: [__webpack_require__(/*! ./proyectos.component.scss */ "./src/app/paginas/proyectos/proyectos.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_resources_service__WEBPACK_IMPORTED_MODULE_1__["ResourcesService"],
            _core_localstorage_service__WEBPACK_IMPORTED_MODULE_2__["LocalstorageService"]])
    ], ProyectosPaginaComponent);
    return ProyectosPaginaComponent;
}());



/***/ }),

/***/ "./src/app/pipes/order-by.pipe.ts":
/*!****************************************!*\
  !*** ./src/app/pipes/order-by.pipe.ts ***!
  \****************************************/
/*! exports provided: OrderByPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderByPipe", function() { return OrderByPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var OrderByPipe = /** @class */ (function () {
    function OrderByPipe() {
    }
    OrderByPipe.prototype.transform = function (array, args) {
        if (typeof args[0] === "undefined") {
            return array;
        }
        var direction = args[0][0];
        var column = args.replace('-', '');
        array.sort(function (a, b) {
            var left = Number(new Date(a[column]));
            var right = Number(new Date(b[column]));
            return (direction === "-") ? right - left : left - right;
        });
        return array;
    };
    OrderByPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'orderBy'
        })
    ], OrderByPipe);
    return OrderByPipe;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/checho/Trabajo/Trusty/Proyectos/Pasionistas/sitio-web/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map