import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../core/resources.service';
import { LocalstorageService } from '../../core/localstorage.service';

@Component({
  selector: "app-asistentes",
  templateUrl: "./asistentes.component.html",
  styleUrls: ["./asistentes.component.scss"]
})
export class AsistentesComponent implements OnInit {
  participantes: any[] = [];
  staff: any[] = [];

  idioma = "español";
  filter: any = "";
  filteredItems: any[] = [];
  placeholderinput = "Buscar participante";

  constructor(
    private db: ResourcesService,
    public localstorage: LocalstorageService
  ) {

    this.db.colWithIds$("asistentes", ref => ref.orderBy('posicion', 'asc').where('tipo', '==','participante')).subscribe((resp: any[]) => {
      this.participantes = resp;
    });

    this.db.colWithIds$("asistentes", ref => ref.orderBy('posicion', 'asc').where('tipo', '==', 'staff')).subscribe((resp: any[]) => {
      this.staff = resp;
    });

    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;

      switch (this.idioma) {
        case "español":
          this.placeholderinput = "Buscar";
          break;

        case "english":
          this.placeholderinput = "Search";
          break;

        case "español":
          this.placeholderinput = "Cerca";
          break;
      }
    });

    this.idioma = this.localstorage.getActualIdioma();

    switch (this.idioma) {
      case "español":
        this.placeholderinput = "Buscar";
        break;

      case "english":
        this.placeholderinput = "Search";
        break;

      case "español":
        this.placeholderinput = "Cerca";
        break;
    }

  }

  ngOnInit() {}


}
