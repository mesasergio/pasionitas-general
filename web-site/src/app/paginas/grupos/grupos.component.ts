import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../core/resources.service';
import { LocalstorageService } from '../../core/localstorage.service';

@Component({
  selector: "app-grupos",
  templateUrl: "./grupos.component.html",
  styleUrls: ["./grupos.component.scss"]
})
export class GruposComponent implements OnInit {
  grupos: any[] = [];
  staff: any[] = [];

  idioma = "español";
  filter: any = "";
  filteredItems: any[] = [];
  placeholderinput = "Buscar participante";

  constructor(
    private db: ResourcesService,
    public localstorage: LocalstorageService
  ) {

    this.db.colWithIds$("grupos").subscribe((resp: any[]) => {
      this.grupos = resp;
    });

    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;

      switch (this.idioma) {
        case "español":
          this.placeholderinput = "Buscar";
          break;

        case "english":
          this.placeholderinput = "Search";
          break;

        case "español":
          this.placeholderinput = "Cerca";
          break;
      }
    });

    this.idioma = this.localstorage.getActualIdioma();

    switch (this.idioma) {
      case "español":
        this.placeholderinput = "Buscar";
        break;

      case "english":
        this.placeholderinput = "Search";
        break;

      case "español":
        this.placeholderinput = "Cerca";
        break;
    }

  }

  ngOnInit() {}


}
