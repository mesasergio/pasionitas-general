import { Component, OnInit } from "@angular/core";
import { ResourcesService } from "../../core/resources.service";
import { LocalstorageService } from "../../core/localstorage.service";

@Component({
  selector: "app-cronicas",
  templateUrl: "./cronicas.component.html",
  styleUrls: ["./cronicas.component.scss"]
})
export class CronicasPaginaComponent implements OnInit {
  cronicas: any[] = [];
  idioma = "español";
  filter: any = "";
  filteredItems: any[] = [];
  placeholderinput = "Buscar cronica";

  constructor(
    private db: ResourcesService,
    public localstorage: LocalstorageService
  ) {
    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;

      switch (this.idioma) {
        case "español":
          this.placeholderinput = "Buscar cronica";
          break;

        case "english":
          this.placeholderinput = "Search chronicle";
          break;

        case "español":
          this.placeholderinput = "Cerca cronache";
          break;
      }

      this.db
        .colWithIds$("cronicas", ref =>
          ref.where("idioma", "==", this.idioma)
        )
        .subscribe((resp: any[]) => {
          this.cronicas = resp;
        });
    });

    this.idioma = this.localstorage.getActualIdioma();

    switch (this.idioma) {
      case "español":
        this.placeholderinput = "Buscar participante";
        break;

      case "english":
        this.placeholderinput = "Search participant";
        break;

      case "español":
        this.placeholderinput = "Cerca partecipante";
        break;
    }

    this.db
      .colWithIds$("cronicas", ref => ref.where("idioma", "==", this.idioma))
      .subscribe((resp: any[]) => {
        this.cronicas = resp;
      });
  }

  ngOnInit() {}
}
