import { Component, OnInit } from "@angular/core";
import { ResourcesService } from '../../core/resources.service';
import { LocalstorageService } from '../../core/localstorage.service';

@Component({
  selector: "app-encuestas",
  templateUrl: "./encuestas.component.html",
  styleUrls: ["./encuestas.component.scss"]
})
export class EncuestasPaginaComponent implements OnInit {

  idioma = "español";
  encuestas: any[] = [];

  constructor(
    private db: ResourcesService,
    public localstorage: LocalstorageService
  ) {

    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    })

    this.idioma = this.localstorage.getActualIdioma();

    this.db.colWithIds$('encuestas').subscribe((resp:any[])=>{
      this.encuestas = resp;
    })

  }

  ngOnInit() {}
}
