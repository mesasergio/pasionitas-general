import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { Router } from '../../../../node_modules/@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { LocalstorageService } from '../../core/localstorage.service';
import swal from "sweetalert2";


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  info:any = {
    idioma: 'español'
  };
  constructor(
    private auth: AuthService,
    private _router: Router,
    public spinner: NgxSpinnerService,
    public localstorage: LocalstorageService
  ) {

    this.localstorage.getUser().subscribe(event => {
        this._router.navigate(['/inicio']);
    });

    this.auth.getCurrentUser().subscribe((user:any[])=>{
      if(user){
        if(user.length > 0){
          this._router.navigate(['/inicio']);
        }
      }
    })

  }

  ngOnInit() {
  }

  login(){

    this.auth.emailLogin(this.info.email, this.info.password).then((user:any)=>{
      this.localstorage.setUser(user);
      this.localstorage.setIdioma(this.info.idioma);
      this.spinner.show();

      setTimeout(() => {
        this.spinner.hide();
        this._router.navigate(['/inicio']);
      }, 3000);

    }).catch(error=>{
      swal({
        type: "error",
        title: "Error en ingreso / Entry error / Errore di immissione",
        text: "Usuario o contraseña incorrectos / Incorrect username or password / Nome utente o password errati ",
        confirmButtonText: "Ok",
      }).then(() => {}, dismiss => {});
    })



  }



}
