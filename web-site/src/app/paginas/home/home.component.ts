import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from '../../core/localstorage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  idioma = 'español';

  constructor(
    public localstorage: LocalstorageService
  ) {


    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    })

    this.idioma = this.localstorage.getActualIdioma();

  }

  ngOnInit() {
  }

}
