import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../core/resources.service';
import { LocalstorageService } from '../../core/localstorage.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: "app-asistentes-grupos",
  templateUrl: "./asistentes-grupos.component.html",
  styleUrls: ["./asistentes-grupos.component.scss"]
})
export class AsistentesGruposComponent implements OnInit {
  participantes: any[] = [];
  staff: any[] = [];

  idioma = "español";
  filter: any = "";
  filteredItems: any[] = [];
  placeholderinput = "Buscar participante";
  id:any;
  grupo:any = {};

  constructor(
    private db: ResourcesService,
    public localstorage: LocalstorageService,
    private _route: ActivatedRoute,
    private spinner: NgxSpinnerService,
  ) {

    this._route.params.subscribe(params => {
      this.id = params.id;
      if (this.id) {

        this.spinner.show();

        setTimeout(() => {
          this.spinner.hide();
        }, 3000);

        this.db.docWithRefs$(`grupos/${this.id}`).subscribe((res: any) => {

          this.grupo = res;

          this.db.colWithIds$("asistentes", ref => ref.orderBy('posicion', 'asc')).subscribe((resp: any[]) => {

            resp.forEach(element => {
              let participantes:any[] = res.asistentes;
              let encontro = participantes.find(x => x.asistenteid == element.id);
              if(encontro){
                this.participantes.push(element);
              }

            });

          });

        });
      }
    });


    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;

      switch (this.idioma) {
        case "español":
          this.placeholderinput = "Buscar";
          break;

        case "english":
          this.placeholderinput = "Search";
          break;

        case "español":
          this.placeholderinput = "Cerca";
          break;
      }
    });

    this.idioma = this.localstorage.getActualIdioma();

    switch (this.idioma) {
      case "español":
        this.placeholderinput = "Buscar";
        break;

      case "english":
        this.placeholderinput = "Search";
        break;

      case "español":
        this.placeholderinput = "Cerca";
        break;
    }

  }

  ngOnInit() { }


}
