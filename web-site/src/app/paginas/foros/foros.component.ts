import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../core/resources.service';
import { LocalstorageService } from '../../core/localstorage.service';

@Component({
  selector: "app-foros",
  templateUrl: "./foros.component.html",
  styleUrls: ["./foros.component.scss"]
})
export class ForosComponent implements OnInit {
  foros: any[] = [];
  idioma = "español";
  filter: any = "";
  filteredItems: any[] = [];
  placeholderinput = "Buscar proyecto";

  constructor(
    private db: ResourcesService,
    public localstorage: LocalstorageService
  ) {
    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;

      switch (this.idioma) {
        case "español":
          this.placeholderinput = "Buscar foro";
          break;

        case "english":
          this.placeholderinput = "Search forum";
          break;

        case "español":
          this.placeholderinput = "Cerca forum";
          break;
      }

      this.db
        .colWithIds$("foros")
        .subscribe((resp: any[]) => {
          this.foros = resp;
        });
    });

    this.idioma = this.localstorage.getActualIdioma();

    switch (this.idioma) {
      case "español":
        this.placeholderinput = "Buscar foro";
        break;

      case "english":
        this.placeholderinput = "Search forum";
        break;

      case "español":
        this.placeholderinput = "Cerca forum";
        break;
    }

    this.db
      .colWithIds$("foros")
      .subscribe((resp: any[]) => {
        resp.forEach(element => {
          let consulta = this.db.colWithIds$('hilos-foros',ref=>ref.where('foroid','==',element.id)).subscribe((hilos:any[])=>{
            consulta.unsubscribe();
            element.hilos = hilos.length;
          })
        });
        this.foros = resp;
      });
  }

  ngOnInit() {}
}
