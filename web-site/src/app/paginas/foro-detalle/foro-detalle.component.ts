import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "../../../../node_modules/@angular/router";
import { ResourcesService } from "../../core/resources.service";
import { LocalstorageService } from "../../core/localstorage.service";
import { NgxSpinnerService } from "../../../../node_modules/ngx-spinner";
import { AuthService } from '../../core/auth.service';

@Component({
  selector: "app-foro-detalle",
  templateUrl: "./foro-detalle.component.html",
  styleUrls: ["./foro-detalle.component.scss"]
})
export class ForoDetalleComponent implements OnInit {
  id: any;
  foro: any = {};
  idioma = "español";
  mensaje: any;
  user: any = {};
  staticAlertClosed:boolean = true;
  hilos: any[] = [];

  constructor(
    private _route: ActivatedRoute,
    private db: ResourcesService,
    public localstorage: LocalstorageService,
    private spinner: NgxSpinnerService,
    private auth: AuthService
  ) {
    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    });

    this.auth.getCurrentUser().subscribe((user: any[]) => {
      if (user) {
        if (user.length > 0) {
          this.user = user[0];
        }
      }
    });

    this.idioma = this.localstorage.getActualIdioma();

    this._route.params.subscribe(params => {
      this.id = params.id;
      if (this.id) {
        this.spinner.show();

        setTimeout(() => {
          this.spinner.hide();
        }, 3000);

        this.db.docWithRefs$(`foros/${this.id}`).subscribe((res: any) => {
          this.foro = res;

          let consulta = this.db
            .colWithIds$("hilos-foros", ref =>
              ref.where("foroid", "==", this.id)
            )
            .subscribe((hilos: any[]) => {
              this.hilos = hilos;
              this.foro.hilos = hilos.length;

              this.hilos.forEach(element => {
                let qryuser = this.db.colWithIds$('asistentes',ref=>ref.where('uid','==',element.uid)).subscribe((asistentes:any[])=>{
                  if(asistentes.length > 0){
                    element.usuario = asistentes[0];
                  }
                })
              });
            });
        });
      }
    });
  }

  ngOnInit() {}

  enviar() {
    this.db
      .add("hilos-foros", {
        uid: this.user.uid,
        mensaje: this.mensaje,
        foroid: this.id
      })
      .then(() => {
        this.mensaje = "";
        this.staticAlertClosed = false;
      });
  }
}
