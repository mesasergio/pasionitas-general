import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { ResourcesService } from '../../core/resources.service';
import { LocalstorageService } from '../../core/localstorage.service';
import { NgxSpinnerService } from '../../../../node_modules/ngx-spinner';
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-proyecto-detalle",
  templateUrl: "./proyecto-detalle.component.html",
  styleUrls: ["./proyecto-detalle.component.scss"]
})
export class ProyectoDetalleComponent implements OnInit {
  id: any;
  proyecto: any = {};
  idioma = "español";
  urlselect: any = "";
  type = "application/pdf";

  constructor(
    private _route: ActivatedRoute,
    private db: ResourcesService,
    public localstorage: LocalstorageService,
    private spinner: NgxSpinnerService,
    public sanitizer: DomSanitizer
  ) {
    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    });

    this.idioma = this.localstorage.getActualIdioma();

    this._route.params.subscribe(params => {
      this.id = params.id;
      if (this.id) {
        this.spinner.show();

        setTimeout(() => {
          this.spinner.hide();
        }, 3000);

        this.db.docWithRefs$(`proyectos/${this.id}`).subscribe((res: any) => {
          this.proyecto = res;
          this.selecturl();
        });
      }
    });
  }

  ngOnInit() {}

  selecturl() {
    this.urlselect = this.sanitizer.bypassSecurityTrustResourceUrl(this.proyecto.documentourl);
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 3000);

  }
}
