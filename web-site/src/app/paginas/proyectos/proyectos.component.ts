import { Component, OnInit } from "@angular/core";
import { ResourcesService } from "../../core/resources.service";
import { LocalstorageService } from "../../core/localstorage.service";

@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
  styleUrls: ['./proyectos.component.scss']
})
export class ProyectosPaginaComponent implements OnInit {

  proyectos: any[] = [];
  idioma = "español";
  filter: any = "";
  filteredItems: any[] = [];
  placeholderinput = "Buscar proyecto";

  constructor(
    private db: ResourcesService,
    public localstorage: LocalstorageService
  ) {

    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;

      switch (this.idioma) {
        case "español":
          this.placeholderinput = "Buscar proyecto";
          break;

        case "english":
          this.placeholderinput = "Search project";
          break;

        case "español":
          this.placeholderinput = "Cerca progetti";
          break;
      }

      this.db.colWithIds$("proyectos", ref => ref.where('idioma', '==', this.idioma)).subscribe((resp: any[]) => {
        this.proyectos = resp;
      });

    });

    this.idioma = this.localstorage.getActualIdioma();

    switch (this.idioma) {
      case "español":
        this.placeholderinput = "Buscar proyecto";
        break;

      case "english":
        this.placeholderinput = "Search project";
        break;

      case "español":
        this.placeholderinput = "Cerca progetti";
        break;
    }

    this.db.colWithIds$("proyectos", ref => ref.where('idioma', '==', this.idioma)).subscribe((resp: any[]) => {
      this.proyectos = resp;
    });

  }

  ngOnInit() { }


}
