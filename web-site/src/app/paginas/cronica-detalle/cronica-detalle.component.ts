import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "../../../../node_modules/@angular/router";
import { ResourcesService } from "../../core/resources.service";
import { LocalstorageService } from "../../core/localstorage.service";
import { NgxSpinnerService } from "../../../../node_modules/ngx-spinner";

@Component({
  selector: 'app-cronica-detalle',
  templateUrl: './cronica-detalle.component.html',
  styleUrls: ['./cronica-detalle.component.scss']
})
export class CronicaDetalleComponent implements OnInit {

  id: any;
  cronica: any = {};
  idioma = "español";


  constructor(
    private _route: ActivatedRoute,
    private db: ResourcesService,
    public localstorage: LocalstorageService,
    private spinner: NgxSpinnerService

  ) {

    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    });

    this.idioma = this.localstorage.getActualIdioma();

    this._route.params.subscribe(params => {
      this.id = params.id;
      if (this.id) {
        this.spinner.show();

        setTimeout(() => {
          this.spinner.hide();
        }, 3000);

        this.db
          .docWithRefs$(`cronicas/${this.id}`)
          .subscribe((res: any) => {
            this.cronica = res;
          });
      }
    });

  }

  ngOnInit() {
  }

}
