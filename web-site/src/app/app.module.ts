import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BannerCarouselComponent } from './componentes/banner-carousel/banner-carousel.component';
import { HttpClientModule } from "@angular/common/http";
import { BarraNavegacionComponent } from './componentes/barra-navegacion/barra-navegacion.component';
import { HeroBannerComponent } from './componentes/hero-banner/hero-banner.component';
import { MensajePapaComponent } from './componentes/mensaje-papa/mensaje-papa.component';
import { AgendaComponent } from './componentes/agenda/agenda.component';
import { LiturgiasComponent } from './componentes/liturgias/liturgias.component';
import { CronicasComponent } from './componentes/cronicas/cronicas.component';
import { BibliotecaComponent } from './componentes/biblioteca/biblioteca.component';
import { ProyectosComponent } from './componentes/proyectos/proyectos.component';
import { AsistentesComponent } from './paginas/asistentes/asistentes.component';
import { HomeComponent } from './paginas/home/home.component';
import { AppRoutingModule } from './app.routing.module';
import { ResourcesService } from './core/resources.service';
import { AuthService } from './core/auth.service';
import { UploadService } from './core/upload.service';

import { WebStorageModule } from "ngx-store";
import { Angular2FontawesomeModule } from "angular2-fontawesome/angular2-fontawesome";
import { NgxSpinnerModule } from "ngx-spinner";
import { FormsModule } from '../../node_modules/@angular/forms';
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { InicioComponent } from './paginas/inicio/inicio.component';
import { ProyectosPaginaComponent } from './paginas/proyectos/proyectos.component';
import { ProyectoDetalleComponent } from './paginas/proyecto-detalle/proyecto-detalle.component';
import { CronicasPaginaComponent } from './paginas/cronicas/cronicas.component';
import { CronicaDetalleComponent } from './paginas/cronica-detalle/cronica-detalle.component';
import { EncuestraComponent } from './componentes/encuestra/encuestra.component';
import { EncuestasPaginaComponent } from './paginas/encuestas/encuestas.component';
import { OrderByPipe } from './pipes/order-by.pipe';
import { ForosComponent } from './paginas/foros/foros.component';
import { ForoDetalleComponent } from './paginas/foro-detalle/foro-detalle.component';
import { GruposComponent } from './paginas/grupos/grupos.component';
import { AsistentesGruposComponent } from './paginas/asistentes-grupos/asistentes-grupos.component';

import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireAuthModule } from "@angular/fire/auth";


export const firebaseConfig = {
  apiKey: "AIzaSyAaLH5P2QXTeSNN2S8wLkYEftf_3qj9UTw",
  authDomain: "pasionistas-conferencia.firebaseapp.com",
  databaseURL: "https://pasionistas-conferencia.firebaseio.com",
  projectId: "pasionistas-conferencia",
  storageBucket: "pasionistas-conferencia.appspot.com",
  messagingSenderId: "799598907957"
};


@NgModule({
  declarations: [
    AppComponent,
    BannerCarouselComponent,
    BarraNavegacionComponent,
    HeroBannerComponent,
    MensajePapaComponent,
    AgendaComponent,
    LiturgiasComponent,
    CronicasComponent,
    BibliotecaComponent,
    ProyectosComponent,
    AsistentesComponent,
    HomeComponent,
    InicioComponent,
    ProyectosPaginaComponent,
    ProyectoDetalleComponent,
    CronicasPaginaComponent,
    CronicaDetalleComponent,
    EncuestraComponent,
    EncuestasPaginaComponent,
    OrderByPipe,
    ForosComponent,
    ForoDetalleComponent,
    GruposComponent,
    AsistentesGruposComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    HttpClientModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    WebStorageModule,
    Angular2FontawesomeModule,
    NgxSpinnerModule,
    FormsModule,
    Ng2SearchPipeModule,
  ],
  providers: [ResourcesService, AuthService, UploadService],
  bootstrap: [AppComponent]
})
export class AppModule {}
