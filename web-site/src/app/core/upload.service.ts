import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { ResourcesService } from './resources.service';
import { TdLoadingService } from '@covalent/core/loading';
import { AngularFireStorage } from '@angular/fire/storage';

export class Upload {
  $key: string;
  file: File;
  name: string;
  url: string;
  progress: number;
  createdAt: Date = new Date();
  constructor(file: File) {
    this.file = file;
  }
}

@Injectable()
export class UploadService {
  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;
  public uploads: Observable<Upload[]>;

  constructor(
    private storage: AngularFireStorage,
    private db: ResourcesService,
    private _loadingService: TdLoadingService
  ) {}

  uploadDocumento(upload: Upload) {
    return new Promise((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let now = new Date().getTime();
      let uploadTask = storageRef.child(`/documentos/${now}`).put(upload.file);

      uploadTask.on(
        firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: any) => {
          upload.progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        },
        error => {
          return reject(error);
        },
        () => {
          upload.url = uploadTask.snapshot.downloadURL;
          upload.name = upload.file.name;
          resolve(upload.url);
        }
      );
    });
  }

  uploadPic(upload: Upload) {
    return new Promise((resolve, reject) => {
      this._loadingService.register();

      let storageRef = firebase.storage().ref();
      let now = new Date().getTime();
      let uploadTask = storageRef.child(`/fotos/${now}`).put(upload.file);

      uploadTask.on(
        firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: any) => {
          upload.progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        },
        error => {
          return reject(error);
        },
        () => {
          upload.url = uploadTask.snapshot.downloadURL;
          upload.name = upload.file.name;
          this._loadingService.resolve();
          resolve(upload.url);
        }
      );
    });
  }
}
