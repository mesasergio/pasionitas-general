import { NgModule } from '@angular/core';

import { AuthService } from './auth.service';
import { ResourcesService } from './resources.service';
import { UploadService } from './upload.service';
import { AuthGuard } from './auth.guard';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

@NgModule({
  imports: [
    AngularFireAuthModule,
    AngularFireStorageModule,
  ],
  providers: [AuthService, ResourcesService, UploadService, AuthGuard],
})
export class CoreModule { }

