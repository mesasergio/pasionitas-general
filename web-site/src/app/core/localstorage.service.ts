import { Injectable } from '@angular/core';
import { SessionStorageService, LocalStorageService } from "../../../node_modules/ngx-store";

@Injectable({
  providedIn: "root"
})
export class LocalstorageService {
  constructor(
    public sessionStorageService: SessionStorageService,
    public localStorage: LocalStorageService
  ) {
  }

  setIdioma(idioma) {
    this.localStorage.set("idioma", idioma);
  }

  getActualIdioma(){
    return this.localStorage.get('idioma');
  }

  getIdioma() {
    return this.localStorage.observe('idioma');
  }

  setUser(user){
    this.localStorage.set("user", user);
  }

  getUser(){
    return this.localStorage.observe('user');
  }
}
