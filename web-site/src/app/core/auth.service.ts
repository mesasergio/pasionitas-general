import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase/app';
import { switchMap } from 'rxjs/operators';
import { ResourcesService } from './resources.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';

interface User {
  uid: string;
  correo?: string | null;
  estado?: string;
}

interface Admin {
  uid?: string;
  correo?: string;
  nombre?: string;
  apellido?: string;
  rol?: string;
  estado?: boolean;
}

@Injectable()
export class AuthService {

  user: Observable<User | null>;

  constructor(private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private _db: ResourcesService
  ) {

    this.user = this.afAuth.authState
    .switchMap((user) => {
      if (user) {
        return this._db.col$(`asistentes`, ref => ref.where('uid', '==', user.uid));
      } else {
        return of(null)
      }
    });
  }

  get authenticated(): boolean {
    return this.user !== null;
  }

  get currentUserObservable(): any {
    return this.afAuth.authState
  }

  getCurrentUser() {
    return this.afAuth.authState
      .switchMap((user:any) => {
        if (user) {
          return this._db.col$(`asistentes`, ref => ref.where('uid', '==', user.uid))
        } else {
          return of(null);
        }
      });
  }

  emailLogin(email: string, password: string) {
    return new Promise((resolve, reject) => {

      this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then((user: firebase.auth.UserCredential) => {
          this._db.col$(`asistentes`, ref => ref.where('uid', '==', user.user.uid)).subscribe((res:any[]) => {
            if (res.length > 0) {
              resolve(res[0])
            } else {
              reject()
            }
          })
        })
        .catch((error) => {
          reject(error)
        });
    })

  }

  // Sends email allowing user to reset password
  resetPassword(email: string) {
    const fbAuth = firebase.auth();

    return fbAuth.sendPasswordResetEmail(email)
      .then(() => {

      })
      .catch((error) => {

      });
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }


}
