import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private auth: AuthService, 
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean 
    {
      let islogued = false;
      this.auth.getCurrentUser().subscribe((res) => {
         if(res){
           console.log(res)
           islogued = true
          }else{
           console.log(res)
            islogued = false
            this.router.navigate(['/login']);
          }
      })

      return islogued;

  }
}