import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { HomeComponent } from './paginas/home/home.component';
import { AsistentesComponent } from './paginas/asistentes/asistentes.component';
import { InicioComponent } from './paginas/inicio/inicio.component';
import { ProyectosPaginaComponent } from './paginas/proyectos/proyectos.component';
import { ProyectoDetalleComponent } from './paginas/proyecto-detalle/proyecto-detalle.component';
import { CronicasPaginaComponent } from './paginas/cronicas/cronicas.component';
import { CronicaDetalleComponent } from './paginas/cronica-detalle/cronica-detalle.component';
import { EncuestasPaginaComponent } from './paginas/encuestas/encuestas.component';
import { ForosComponent } from './paginas/foros/foros.component';
import { ForoDetalleComponent } from './paginas/foro-detalle/foro-detalle.component';
import { GruposComponent } from './paginas/grupos/grupos.component';
import { AsistentesGruposComponent } from './paginas/asistentes-grupos/asistentes-grupos.component';

const options: ExtraOptions = {
  useHash: true
};

const routes: Routes = [
  { path: "", component: InicioComponent },
  { path: "inicio", component: HomeComponent },
  { path: "participants", component: AsistentesComponent },
  { path: "proyectos", component: ProyectosPaginaComponent },
  { path: "proyecto-detalle/:id", component: ProyectoDetalleComponent },
  { path: "cronicas", component: CronicasPaginaComponent },
  { path: "cronica-detalle/:id", component: CronicaDetalleComponent },
  { path: "votaciones", component: EncuestasPaginaComponent },
  { path: "foros", component: ForosComponent },
  { path: "foro-detalle/:id", component: ForoDetalleComponent },
  { path: "groups", component: GruposComponent },
  { path: "groups/participants/:id", component: AsistentesGruposComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, options)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
