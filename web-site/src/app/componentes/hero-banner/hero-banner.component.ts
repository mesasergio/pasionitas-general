import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from '../../core/localstorage.service';

@Component({
  selector: "hero-banner",
  templateUrl: "./hero-banner.component.html",
  styleUrls: ["./hero-banner.component.scss"]
})
export class HeroBannerComponent implements OnInit {
  idioma = "español";



  constructor(public localstorage: LocalstorageService) {
    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    });

    this.idioma = this.localstorage.getActualIdioma();

  }

  ngOnInit() {}
}
