import { Component, OnInit } from "@angular/core";
import { LocalstorageService } from "../../core/localstorage.service";
import { ResourcesService } from "../../core/resources.service";

@Component({
  selector: "cronicas",
  templateUrl: "./cronicas.component.html",
  styleUrls: ["./cronicas.component.scss"]
})
export class CronicasComponent implements OnInit {
  idioma = "español";
  idioma2 = "español";

  cronicas: any[] = [];

  constructor(
    public localstorage: LocalstorageService,
    public db: ResourcesService
  ) {

    this.idioma = this.localstorage.getActualIdioma();


    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;

      if (event.newValue == 'english') {
        this.idioma2 = 'ingles'
      }
      else {
        this.idioma2 = event.newValue;
      }


      this.db
        .colWithIds$("cronicas", ref =>
          ref.where("idioma", "==", this.idioma2)
        )
        .subscribe((cronicas: any[]) => {
          this.cronicas = cronicas;
        });
    });

    if (this.idioma == "english") {
      this.idioma2 = "ingles";
    } else {
      this.idioma2 = this.idioma;
    }

    this.db
      .colWithIds$("cronicas", ref =>
        ref.where("idioma", "==", this.idioma2)
      )
      .subscribe((cronicas: any[]) => {
        this.cronicas = cronicas;
      });
  }
  ngOnInit() {}
}
