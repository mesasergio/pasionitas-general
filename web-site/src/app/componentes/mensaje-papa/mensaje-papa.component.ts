import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from '../../core/localstorage.service';
import { ResourcesService } from '../../core/resources.service';

@Component({
  selector: 'mensaje-papa',
  templateUrl: './mensaje-papa.component.html',
  styleUrls: ['./mensaje-papa.component.scss']
})
export class MensajePapaComponent implements OnInit {

  idioma = 'español';

  info:any = {};

  constructor(
    public localstorage: LocalstorageService,
    public db: ResourcesService
  ) {

    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    })

    this.idioma = this.localstorage.getActualIdioma();

    this.db.docWithRefs$("secciones-documentos/mensaje-papa").subscribe((resp: any) => {
      this.info = resp;
    });

  }

  ngOnInit() {
  }

}
