import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from '../../core/localstorage.service';
import { ResourcesService } from '../../core/resources.service';

@Component({
  selector: "proyectos",
  templateUrl: "./proyectos.component.html",
  styleUrls: ["./proyectos.component.scss"]
})
export class ProyectosComponent implements OnInit {
  idioma = "español";
  idioma2 = "español";

  proyectos:any[] = [];

  constructor(
    public localstorage: LocalstorageService,
    public db: ResourcesService
  ) {
    this.localstorage.getIdioma().subscribe(event => {
      if (event.newValue == 'english'){
        this.idioma2 = 'ingles'
      }
      else{
        this.idioma2 = event.newValue;
      }

      this.idioma = event.newValue;
      this.db.colWithIds$('proyectos',ref=>ref.where('idioma','==',this.idioma2)).subscribe((proyectos:any[])=>{
        this.proyectos = proyectos;
      })
    });

    this.idioma = this.localstorage.getActualIdioma();

    if (this.idioma == 'english') {
      this.idioma2 = 'ingles'
    }
    else {
      this.idioma2 = this.idioma;
    }

    this.db.colWithIds$('proyectos', ref => ref.where('idioma', '==', this.idioma2)).subscribe((proyectos: any[]) => {
      this.proyectos = proyectos;
    })

  }
  ngOnInit() {}
}
