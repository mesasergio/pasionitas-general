import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { LocalstorageService } from '../../core/localstorage.service';

@Component({
  selector: 'banner-carousel',
  templateUrl: './banner-carousel.component.html',
  styleUrls: ['./banner-carousel.component.scss']
})
export class BannerCarouselComponent implements OnInit {

  images: any[] = [
    'assets/background-banner.JPG'
  ]

  idioma = 'español';


  constructor(
    private _http: HttpClient,
    public localstorage: LocalstorageService
) {
    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    })
}

  ngOnInit() {}


}
