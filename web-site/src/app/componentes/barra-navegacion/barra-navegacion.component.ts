import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from '../../core/localstorage.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../core/auth.service';
import { Router, NavigationEnd } from '../../../../node_modules/@angular/router';

@Component({
  selector: "barra-navegacion",
  templateUrl: "./barra-navegacion.component.html",
  styleUrls: ["./barra-navegacion.component.scss"]
})
export class BarraNavegacionComponent implements OnInit {
  idioma = "español";
  user: any = {};
  isCollapsed = true;


  constructor(
    public localstorage: LocalstorageService,
    private modalService: NgbModal,
    private auth: AuthService,
    private _router: Router

  ) {

    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    });

    this.idioma = this.localstorage.getActualIdioma();
    console.log(this.idioma);

    if(!this.idioma){
      this.idioma = "español";
    }

    this.auth.getCurrentUser().subscribe((user:any[]) => {
      if (user) {
        if(user.length > 0){
          this.user = user[0];
        }else{
          this._router.navigate(['/']);
        }
      }else{
        this._router.navigate(['/']);
      }
    });
  }

  open(content) {
    this.modalService.open(content, {
      windowClass: "dark-modal",
      backdropClass: "dark-back"
    });
  }

  ngOnInit() {
    this.idioma = this.localstorage.getActualIdioma();

    this._router.events.subscribe( event => {
      if (event instanceof NavigationEnd) {
        const tree = this._router.parseUrl(this._router.url);
        if (tree.fragment) {
          const element = document.querySelector("#" + tree.fragment);
          if (element) { element.scrollIntoView(); }
        }
      }
    });

  }

  changeIdioma(idioma) {
    this.idioma = idioma;
    this.localstorage.setIdioma(idioma);
  }

  close(){
    this.auth.signOut();
    this._router.navigate(['/']);
  }
}
