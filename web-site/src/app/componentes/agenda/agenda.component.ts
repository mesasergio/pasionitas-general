import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../core/resources.service';
import { LocalstorageService } from '../../core/localstorage.service';

@Component({
  selector: "agenda",
  templateUrl: "./agenda.component.html",
  styleUrls: ["./agenda.component.scss"]
})
export class AgendaComponent implements OnInit {

  opcion = "calendario";
  idioma = 'español';

  tareasEnRevision: any[] = [];
  tareasPendientes: any[] = [];
  tareasEnProceso: any[] = [];
  tareasTerminadas: any[] = [];

  actividades:any[] = [];

  constructor(
    private db: ResourcesService,
    public localstorage: LocalstorageService
  ) {

    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    })

    this.idioma = this.localstorage.getActualIdioma();

    this.db
      .colWithIds$(`tareas`, ref =>
        ref.where("estado", "==", "pendiente").orderBy("index", "asc")
      )
      .subscribe((resp: any[]) => {
        this.tareasPendientes = resp;
      });

    this.db
      .colWithIds$(`tareas`, ref =>
        ref.where("estado", "==", "proceso").orderBy("index", "asc")
      )
      .subscribe((resp: any[]) => {
        this.tareasEnProceso = resp;
      });

    this.db
      .colWithIds$(`tareas`, ref =>
        ref.where("estado", "==", "revision").orderBy("index", "asc")
      )
      .subscribe((resp: any[]) => {
        this.tareasEnRevision = resp;
      });

    this.db
      .colWithIds$(`tareas`, ref =>
        ref.where("estado", "==", "terminada").orderBy("index", "asc")
      )
      .subscribe((resp: any[]) => {
        this.tareasTerminadas = resp;
      });


    this.db
      .colWithIds$(`actividades`, ref =>
        ref.orderBy("dia", "asc").orderBy("horainicio", "asc")
      )
      .subscribe((resp: any[]) => {
        this.actividades = [];
        resp.forEach(element => {

          let dia;
          let indexfind;

          this.actividades.forEach((item,index) => {
            if(item.dia == element.dia){
              dia = item;
              indexfind = index;
            }
          });

          //console.log(dia)
          if(!dia){
            this.actividades.push({
              dia: element.dia,
              fecha: element.fecha,
              actividades: [
                element
              ]
            })
          }else{
            this.actividades[indexfind].actividades.push(element);
          }
        });
        //this.actividades = resp;
      });
  }

  ngOnInit() {}
}
