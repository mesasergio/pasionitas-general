import { Component, OnInit, Input } from '@angular/core';
import { ResourcesService } from '../../core/resources.service';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: "votacion",
  templateUrl: "./encuestra.component.html",
  styleUrls: ["./encuestra.component.scss"]
})
export class EncuestraComponent implements OnInit {
  @Input()
  info: any;
  @Input()
  idioma: any;

  contestada: boolean = false;
  votacion: any = {};
  user: any = {};
  totalsi: number = 0;
  totalno: number = 0;
  totalblanco: number = 0;
  totalvotaciones: number = 0;
  porcentajesi: number = 0;
  porcentajeno: number = 0;
  porcentajeblanco: number = 0;

  constructor(private db: ResourcesService, private auth: AuthService) {}

  ngOnInit() {
    if (this.info.estado == "Finalizada") {
      this.contestada = true;
    }

    this.auth.getCurrentUser().subscribe((user: any[]) => {
      if (user) {
        if (user.length > 0) {
          this.user = user[0];
          this.db
            .colWithIds$("respuestas-votaciones", ref =>
              ref.where("encuestaid", "==", this.info.id)
            )
            .subscribe((resp: any[]) => {
              let contesto = resp.find(x => x.uid == this.user.uid);

              if (contesto) {
                this.contestada = true;
              }

              this.totalvotaciones = resp.length;
              this.totalsi = 0;
              this.totalno = 0;
              resp.forEach(element => {
                if (element.respuesta == "si") {
                  this.totalsi += 1;
                } else if (element.respuesta == "no") {
                  this.totalno += 1;
                } else if (element.respuesta == "blanco") {
                  this.totalblanco += 1;
                }
              });

              if (this.totalvotaciones > 0) {
                this.porcentajesi = (this.totalsi / this.totalvotaciones) * 100;
                this.porcentajeno = (this.totalno / this.totalvotaciones) * 100;
                this.porcentajeblanco = (this.totalblanco / this.totalvotaciones) * 100;
              }
            });
        }
      }
    });
  }

  responder(respuesta) {
    this.db
      .add("respuestas-votaciones", {
        encuestaid: this.info.id,
        uid: this.user.uid,
        respuesta: respuesta
      })
      .then((respa: any) => {
        console.log("Listo pana!");
      });

    this.contestada = true;
  }
}
