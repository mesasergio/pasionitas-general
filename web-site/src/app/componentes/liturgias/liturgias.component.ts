import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from '../../core/localstorage.service';
import { ResourcesService } from '../../core/resources.service';
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "liturgias",
  templateUrl: "./liturgias.component.html",
  styleUrls: ["./liturgias.component.scss"]
})
export class LiturgiasComponent implements OnInit {
  idioma = "español";
  urlselect: any = "";
  type = "application/pdf";
  tituloDocumento = "";
  documentos: any[] = [];
  carpetaliturgiaid: any = "0";
  carpetas: any[] = [];
  carpetaLiturgiaSeleccionada: any;
  carpetasLiturgiaSeleccionanda: any[] = [];
  carpetaAnterior: any = {};

  constructor(
    public localstorage: LocalstorageService,
    private db: ResourcesService,
    public sanitizer: DomSanitizer,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService
  ) {
    this.localstorage.getIdioma().subscribe(event => {
      this.idioma = event.newValue;
    });

    this.idioma = this.localstorage.getActualIdioma();


    this.db
      .docWithRefs$("secciones-documentos/J1rG9aiLuN8chaikGHxG")
      .subscribe((resp: any) => {
        if (resp.idcarpeta) {
          this.carpetaliturgiaid = resp.idcarpeta;
          this.db
            .colWithIds$("rutas", ref =>
              ref.where("padre", "==", resp.idcarpeta)
            )
            .subscribe((resp: any[]) => {
              this.carpetas = resp;
            });
        }
      });
  }

  ngOnInit() {}

  seleccionarCarpeta(item) {
    this.carpetaLiturgiaSeleccionada = item;
    this.db
      .colWithIds$(`documentos`, ref => ref.where("ruta", "==", item.id))
      .subscribe((documentos: any[]) => {
        this.documentos = documentos;
      });

    this.db
      .docWithRefs$("secciones-documentos/J1rG9aiLuN8chaikGHxG")
      .subscribe((resp: any) => {
        if (resp.idcarpeta) {
          this.db
            .colWithIds$("rutas", ref => ref.where("padre", "==", item.id))
            .subscribe((resp: any[]) => {
              this.carpetasLiturgiaSeleccionanda = resp;
            });
        }
      });
  }

  seleccionarCarpetaHijo(item) {
    this.carpetaLiturgiaSeleccionada = item;
    this.db
      .colWithIds$(`documentos`, ref => ref.where("ruta", "==", item.id))
      .subscribe((documentos: any[]) => {
        this.documentos = documentos;
      });

    this.db
      .docWithRefs$("secciones-documentos/J1rG9aiLuN8chaikGHxG")
      .subscribe((resp: any) => {
        if (resp.idcarpeta) {
          this.db
            .colWithIds$("rutas", ref => ref.where("padre", "==", item.id))
            .subscribe((resp: any[]) => {
              this.carpetasLiturgiaSeleccionanda = resp;
            });
        }
      });
  }

  seleccionarCarpetaVolver(item) {
    console.log(item);
    this.db.docWithRefs$(`rutas/${item.padre}`).subscribe((carpetaant: any) => {
      carpetaant.id = item.padre;
      this.carpetaAnterior = carpetaant;
      this.seleccionarCarpetaHijo(carpetaant);
    });
  }

  cerrarLiturgia() {
    this.carpetaLiturgiaSeleccionada = null;
  }

  selecturl(item, content) {
    this.urlselect = this.sanitizer.bypassSecurityTrustResourceUrl(item.url);
    this.type = item.type;
    this.tituloDocumento = item.nombre;
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 3000);

    this.modalService
      .open(content, { size: "lg", centered: false })
      .result.then(result => {}, reason => {});

  }
}
